<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use common\models\helper\ThumbnailImageHelper;
use common\models\Settings;
use frontend\models\TextBlock;

function url($url = '', $scheme = false){
    return Url::to($url, $scheme);
}

function thumb($imagePath, $width, $height = null, $outbound = false){
    return \Yii::getAlias('@frontendWebroot') . ThumbnailImageHelper::thumbnailFileUrl(
            $imagePath,
            $width,
            $height,
            $outbound ? ThumbnailImageHelper::THUMBNAIL_OUTBOUND : ThumbnailImageHelper::THUMBNAIL_INSET
        );
}

function t($message, $category='app', $params = [], $language = null ){
    return Yii::t($category, $message, $params, $language);
}

function h($text){
    return Html::encode($text);
}

function ph($text){
    return HtmlPurifier::process($text);
}

function param($name, $default = null){
    return ArrayHelper::getValue(Yii::$app->params, $name, $default);
}

function setting($key){
    return Settings::getValue($key);
}

function siteName(){
    return t('site_name');
}

function txt($key){
    return TextBlock::getValue($key);
}

function companyName(){
    return Yii::$app->params['companyName'];
}