<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@statics', dirname(dirname(__DIR__)) . '/statics');
Yii::setAlias('@assets', dirname(dirname(__DIR__)) . '/statics/web/assets/');
Yii::setAlias('@images', dirname(dirname(__DIR__)) . '/statics/web/uploads/images/');

Yii::setAlias('frontendWebroot', 'http://cms.local/');
Yii::setAlias('backendWebroot', 'http://panel.cms.local/');
Yii::setAlias('staticsWebroot', 'http://st.cms.local/');
Yii::setAlias('imagesWebroot', 'http://st.cms.local/uploads/images/');