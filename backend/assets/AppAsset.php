<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/pace-theme-center-simple.css',
        'css/AdminLTE.min.css',
        'css/skin-purple.min.css',
        'css/font-awesome.min.css',
        'css/mdtoast.min.css',
        'css/site.css',
    ];
    public $js = [
        'js/adminlte.min.js',
        'js/bootstrap.js',
        'js/mdtoast.min.js',
        'js/pace.min.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
