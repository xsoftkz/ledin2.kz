<?php
namespace backend\controllers;

use common\modules\content\models\Content;
use common\modules\content\models\ContentCategory;
use common\modules\content\services\PortfolioWaterMarkService;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'cat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionWm()
    {
        $models = Content::find()->andWhere(['section' => Content::SECTION_PORTFOLIOS])->all();
        foreach($models as $model){
            (new PortfolioWaterMarkService($model))->run();
        }
        return $this->request->referrer;
    }

    public function actionCat(){
        $models = Content::find()->andWhere(['section' => Content::SECTION_ARTICLE])->all();
        /** @var Content $model */
        foreach($models as $model){
            (new ContentCategory([
                'content_id' => $model->id,
                'category_id' => $model->category_id,
            ]))->save();
        }
        return $this->request->referrer;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
