<?php

namespace backend\controllers;

use Yii;
use common\models\Settings;
use common\models\search\SettingsSearch;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class SettingsController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $models = Settings::find()->all();
//        $models = Yii::$app->params['systemParams'];
        return $this->render('index', [
            'models' => $models,
        ]);
    }


    public function actionCreate()
    {
        $model = new Settings();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'param_name' => $model->param_name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($param_name)
    {
        $model = $this->findModel($param_name);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
            return $this->redirect(['update', 'param_name' => $model->param_name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    protected function findModel($param_name)
    {
        if (($model = Settings::findOne(['param_name' => $param_name])) !== null) {
            return $model;
        } else {
            return null;
        }
    }
}
