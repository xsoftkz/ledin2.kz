<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">AB</span><span class="logo-lg">' . Yii::$app->params['appDomain'] . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="active" target="_blank" href="<?= Url::to(['@frontendWebroot/'], 'http') ?>">
                        <i class="fa fa-fw fa-desktop"></i> Перейти на веб-сайт</a>
                </li>

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs">
                            <?php $user = \common\models\User::getCurrent(); ?>
                            <?= ($user) ? ($user->firstname . ' ' . $user->lastname) : '' ?>
                        </span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <?= Html::a(
                                'Профиль',
                                ['/user/admin/update', 'id' => $user->id],
                                ['class' => '']
                            ) ?>
                        </li>
                        <li>
                            <?= Html::a(
                                'Выйти',
                                ['/user/auth/logout'],
                                ['data-method' => 'post', 'class' => '']
                            ) ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
