<?php
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <?php
        $menuItems = [
            ['label' => 'Главная', 'icon' => 'tachometer', 'url' => ['/']],
            ['label' => 'Обратная связь', 'icon' => 'envelope-o', 'url' => ['/feedback/feedback/index']],
            [
                'label' => 'Каталог', 'icon' => 'tags', 'url' => '#',
                'items' => [
                    ['label' => 'Товары', 'icon' => 'angle-double-right', 'url' => ['/shop/product/index']],
                    ['label' => 'Категория', 'icon' => 'angle-double-right', 'url' => ['/shop/category/index']],
                    ['label' => 'Атрибуты', 'icon' => 'angle-double-right', 'url' => ['/eav/param/index'],],
                    ['label' => 'Типы атрибутов', 'icon' => 'angle-double-right', 'url' => ['/eav/type/index'],],
                ],
            ],
            [
                'label' => 'Контент', 'icon' => 'th-large', 'url' => '#',
                'items' => [
                    ['label' => 'Страницы', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'page']],
                    ['label' => 'Баннеры', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'banner']],
                    ['label' => 'Портфолио', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'portfolio']],
                    ['label' => 'Услуги', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'service']],
                    ['label' => 'Вакансии', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'vacancy']],
                    ['label' => 'Новости', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'news']],
                    ['label' => 'Блог', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'article']],
                    ['label' => 'Вопрос-ответ', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'faq']],
                    ['label' => 'Отзывы', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'review']],
                    ['label' => 'Партнеры', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'partners']],
                    ['label' => 'Контакты', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'contact']],
                    ['label' => 'Текстовые блоки', 'icon' => 'angle-double-right', 'url' => ['/text/text/index']],
                    ['label' => 'О компании', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'company']],
                    ['label' => 'Офферы', 'icon' => 'angle-double-right', 'url' => ['/content/content/index', 'section' => 'offer']],
                ],
            ],
            [
                'label' => 'Система', 'icon' => 'cogs', 'url' => '#',
                'items' => [
                    ['label' => 'Настройки', 'icon' => 'cog', 'url' => ['/settings/index']],
                    ['label' => 'Администраторы', 'icon' => 'lock', 'url' => ['/user/admin']],
                ],
            ],
        ];

        echo \backend\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree main-menu', 'data-widget'=> 'tree'],
                'items' => $menuItems,
            ]
        )

        ?>

    </section>

</aside>
