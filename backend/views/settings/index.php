<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Settings;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SettingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Настройки');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <table class="table table-striped table-bordered ">
                <thead>
                <tr>
                    <th>Системная переменная</th>
                    <th>Значение</th>
                    <th>Комментарий</th>
                    <th class="text-center">Действие</th>
                </tr>
                </thead>
                <tbody>
                    <?php if ($models) { ?>
                        <?php foreach ($models as $model) { ?>
                            <tr>
                                <td><?= $model->param_name ?></td>
                                <td><?= $model->param_value ?></td>
                                <td><?= $model->comment ?></td>
                                <td class="text-center"><?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'param_name' => $model->param_name]) ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>

</div>
