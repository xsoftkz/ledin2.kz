<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <div class="panel panel-default">
        <div class="panel-body">

            <?php $form = ActiveForm::begin(); ?>

            <?php if($model->isNewRecord){ ?>
                <?= $form->field($model, 'param_name')->textInput() ?>
            <?php } ?>

            <?= $form->field($model, 'param_value')->textInput() ?>

            <?= $form->field($model, 'comment')->textarea(['rows' => 2]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
