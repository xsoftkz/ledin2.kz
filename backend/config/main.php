<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log', 'eav'],
    'modules' => [
        'user' => [
            'basePath' => '@common/modules/user',
            'class' => 'common\modules\user\Module',
            'controllerNamespace' => 'common\modules\user\controllers',
            'viewPath' => '@common/modules/user/views',
        ],
        'content' => [
            'basePath' => '@common/modules/content',
            'class' => 'common\modules\content\Module',
            'controllerNamespace' => 'common\modules\content\controllers',
            'viewPath' => '@common/modules/content/views',
        ],
        'text' => [
            'basePath' => '@common/modules/text',
            'class' => 'common\modules\text\Module',
            'controllerNamespace' => 'common\modules\text\controllers',
            'viewPath' => '@common/modules/text/views',
        ],
        'review' => [
            'basePath' => '@common/modules/review',
            'class' => 'common\modules\review\Module',
            'controllerNamespace' => 'common\modules\review\controllers',
            'viewPath' => '@common/modules/review/views',
        ],
        'shop' => [
            'basePath' => '@common/modules/shop',
            'class' => 'common\modules\shop\Module',
            'controllerNamespace' => 'common\modules\shop\controllers',
            'viewPath' => '@common/modules/shop/views',
        ],
        'feedback' => [
            'basePath' => '@common/modules/feedback',
            'class' => 'common\modules\feedback\Module',
            'controllerNamespace' => 'common\modules\feedback\controllers',
            'viewPath' => '@common/modules/feedback/views',
        ],
        'eav' => [
            'basePath' => '@common/modules/eav',
            'class' => 'common\modules\eav\Module',
            'controllerNamespace' => 'common\modules\eav\controllers',
            'viewPath' => '@common/modules/eav/views',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl'=>['/user/auth/login'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'assetManager' => [
            'linkAssets' => true,
            'appendTimestamp' => true,
        ],
    ],
    'params' => $params,
];
