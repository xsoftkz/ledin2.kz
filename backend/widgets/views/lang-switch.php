<?php
?>

<?php if ($langs){
    foreach ($langs as $lang) { ?>
        <a class="btn btn-sm <?= ($lang->id == $current) ? 'btn-info' : 'btn-default' ?> "
           href="<?= \yii\helpers\Url::toRoute(['update', 'id' => $model_id, 'lang_id' => $lang->id]) ?>">
            <?= $lang->name ?>
        </a>
    <?php }
} ?>

