<?php


namespace backend\widgets;


use common\models\Lang;
use yii\base\Widget;

class LangSwitchWidget extends Widget
{
    public $current;
    public $model_id;

    public function init()
    {
        parent::init();
        $langs = Lang::find()->orderBy('id DESC')->all();
        echo $this->render('lang-switch', [
            'langs' => $langs,
            'current' => $this->current,
            'model_id' => $this->model_id
        ]);
    }
}