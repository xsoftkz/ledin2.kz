<?php

namespace console\migrations;

use yii\db\Migration;

/**
 * Class M200119061820CreateLanguageTable
 */
class M200119061820CreateLanguageTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lang}}', [
            'id' => $this->string(2)->notNull(),
            'locale' => $this->string(8)->notNull(),
            'name' => $this->string(32)->notNull(),
            'status' => $this->smallInteger(),
            'PRIMARY KEY (id)',
        ], $tableOptions);

        $this->batchInsert('{{%lang}}', ['id', 'locale', 'name', 'status'],
            [
                ['ru', 'ru-RU', 'Русский', 1],
                ['kz', 'kz-KZ', 'Қазақша', 1],
                ['en', 'en-US', 'English', 1],
            ]
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%lang}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "M200119061820CreateLanguageTable cannot be reverted.\n";

        return false;
    }
    */
}
