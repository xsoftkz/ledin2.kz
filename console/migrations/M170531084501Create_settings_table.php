<?php

namespace console\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class M170531084501Create_settings_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'param_name' => $this->string()->notNull(),
            'param_value' => $this->text()->notNull(),
            'module' => $this->string()->notNull(),
            'comment' => $this->text(),
            'status_id' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->batchInsert('{{%settings}}',
            ['module', 'param_name', 'param_value'],
            [
                ['site', 'site_name', 'LEDIN | LED INNOVATION - уличное, архитектурное и интерьерное освещение в Астане'],
                ['site', 'site_meta_desc', 'Мета описание сайта'],
                ['site', 'site_descriptor', 'Дескриптор инетрнет магазина'],
                ['site', 'site_email', 'info@xsoft.kz'],
                ['site', 'site_phone', '+7 (707) 397 56 35'],
            ]
        );
    }

    public function down()
    {
        $this->dropTable('{{%settings}}');
    }
}
