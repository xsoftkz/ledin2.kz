<?php
namespace console\migrations;

use yii\db\Migration;


class m161016_050238_create_image_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'filename' => $this->string()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'model_name' => $this->string()->notNull(),
            'title' => $this->string(),
            'folder' => $this->string(),
            'sort_index' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%image}}');
    }
}
