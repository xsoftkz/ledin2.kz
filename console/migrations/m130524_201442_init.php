<?php
namespace console\migrations;

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),

            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'secondname' => $this->string(),
            'username' => $this->string(),
            'email' => $this->string()->notNull()->unique(),
            'phone' => $this->string(),

            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email_activation_key' => $this->string()->unique(),

            'role' => $this->smallInteger()->notNull()->defaultValue(0),
            'activated' => $this->smallInteger()->notNull()->defaultValue(0),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->batchInsert('{{%user}}',
            ['firstname', 'lastname', 'secondname', 'username', 'email',  'phone', 'auth_key', 'password_hash', 'role', 'activated', 'created_at', 'updated_at'],
            [
                ['Эъзоз', 'Хидирбаев', 'Каримбаевич', 'administrator', 'e.hidirbaev@gmail.com', '77073975635', 'KSIUVGQArA59pE-EZFY4CCD8MpvIOvE0',
                    '$2y$13$bTxUjWT.LwyH0lj7y9M78ObKWA2cgg8FBBy3KeBELlL75q2hfdzt2', '10', '1', time(), time()],
            ]
        );
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
