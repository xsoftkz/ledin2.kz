<?php

namespace console\migrations;

use yii\db\Migration;

/**
 * Class M200129045617CreateFileTable
 */
class M200129045617CreateFileTable extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%file}}', [
            'id' => $this->primaryKey(),
            'filename' => $this->string()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'model_name' => $this->string()->notNull(),
            'title_ru' => $this->string(),
            'title_kz' => $this->string(),
            'title_en' => $this->string(),
            'folder' => $this->string(),
            'sort_index' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%file}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "M200129045617CreateFileTable cannot be reverted.\n";

        return false;
    }
    */
}
