$(document).ready(function () {

    $(".fancybox").fancybox({
        overlayColor: '#ffffff',
    });

    $('input').removeAttr('autofocus');

    $('.js-feedback').click(function () {
        $('#feedbackModal').modal('show');
    });

    $('.js-models-tab').click(function () {
        $('html, body').animate({
            scrollTop: ($("#productTabContent").offset().top - 150)
        }, 1000);
        setTimeout("$('#params-tab').tab('show')", 1000);
    });

    let productGallery = $('.product-gallery').lightSlider({
        gallery:true,
        item:1,
        loop:true,
        thumbItem:8,
        enableDrag: false,
    });

    $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        let facadeGallery = $('#facade-images').lightSlider({
            gallery:true,
            item:1,
            loop:true,
            thumbItem:8,
            enableDrag: false,
        });
    });

    $('.product-slide').lightSlider({
        autoWidth:true,
        loop:false,
        pager:false,
        controls: true,
    });

    $('.product-portfolio').lightSlider({
        item:4,
        loop:false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                }
            }
        ]
    });

    $('.compare-products-list').lightSlider({
        item:4,
        loop:false,
        pager:false,
        responsive : [
            {
                breakpoint:991,
                settings: {
                    item:1,
                    slideMove:1
                }
            }
        ]
    });

    $('#main-project-items').lightSlider({
        item:3,
        loop:false,
        pager:false,
        slideMargin:0,
        auto: true,
        pause: 5000,
        speed: 500,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                }
            }
        ]
    });

    $('#main-news-items').lightSlider({
        item:4,
        loop:false,
        pager:false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                }
            }
        ]
    });

    $('#reviews-list').lightSlider({
        item:3,
        loop:false,
        controls:true,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                }
            }
        ]
    });

    $('#partners-list').lightSlider({
        item:4,
        loop:false,
        controls:true,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:2,
                    slideMove:1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                }
            }
        ]
    });


    $('#main-slides').lightSlider({
        item: 1,
        loop: true,
        controls: false,
        pager:false,
        auto: true,
        pause: 4000,
        mode: 'fade'
    });

    setHeaderStyle();

    function setHeaderStyle() {
        var scroll = $(window).scrollTop();
        if (scroll > 150) {
            $(".header").addClass("header-compact");
            $(".header.header-home").removeClass("header-main");
            $(".header.header-home").addClass("header-default");
        } else {
            $(".header").removeClass("header-compact");
            $(".header.header-home").addClass("header-main");
            $(".header.header-home").removeClass("header-default");
        }
    }

    $(window).scroll(function() {
        setHeaderStyle();
    });


    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });
        return false;
    });

    // избранные товары
    $('body').on('click', '.js-favorite-add', function () {
        $(this).toggleClass('active');
        var position_id = $(this).data('id');
        addFavorite(position_id);
        return false;
    });
    // избранные товары
    function addFavorite(position_id) {
        $.ajax({
            type: "POST",
            url: '/favorite/add',
            data: {
                'prod_id': position_id,
                _csrf: yii.getCsrfToken()
            }
        }).done(function (data) {
            $('.js-favorite-count').text(JSON.stringify(data.count));
        }).fail(function (data) {
        });
    }



    // сравнение товаров
    $('body').on('click', '.js-compare-add', function () {
        $(this).toggleClass('active');
        var position_id = $(this).data('id');
        addCompare(position_id);
        return false;
    });
    // сравнение товаров
    function addCompare(position_id) {
        $.ajax({
            type: "POST",
            url: '/compare/add',
            data: {
                'prod_id': position_id,
                _csrf: yii.getCsrfToken()
            }
        }).done(function (data) {
            $('.js-compare-count').text(JSON.stringify(data.count));
        }).fail(function (data) {
        });
    }


    // корзина
    $('body').on('click', '.js-cart-add', function () {
        $(this).toggleClass('active');
        var position_id = $(this).data('id');
        addCart(position_id);
        return false;
    });

    function addCart(position_id) {
        $.ajax({
            type: "POST",
            url: '/cart/add',
            data: {
                'prod_id': position_id,
                _csrf: yii.getCsrfToken()
            }
        }).done(function (data) {
            $('.js-cart-count').text(JSON.stringify(data.count));
            if (data.result != false){
                $.fancybox.open( $('#cartModal') );
            }
        }).fail(function (data) {
        });
    }

    function updatePosition(position_id, quantity) {
        $.ajax({
            type: "POST",
            url: '/cart/update',
            data: {
                'prod_id': position_id,
                'quantity': quantity,
                _csrf: yii.getCsrfToken()
            }
        }).done(function (data) {
        });
    }

    function removePosition(prod_id) {
        $.ajax({
            type: "POST",
            url: '/cart/remove',
            data: {
                'prod_id': prod_id,
                _csrf: yii.getCsrfToken()
            }
        }).done(function (data) {
        });
    }


    $('body').on('click', '.js-plus', function () {

        let quantity = $(this).parent().prev(".cart__quantity");
        let count = parseInt(quantity.attr("data-count-value")) + 1;
        let product_id = quantity.attr("data-position-id");

        $(quantity).attr("data-count-value", count);
        $(quantity).text(count);

        updatePosition(product_id, count)
        return false;
    });


    $('body').on('click', '.js-minus', function () {

        let quantity = $(this).parent().next(".cart__quantity");
        let count = parseInt(quantity.attr("data-count-value")) - 1;
        let product_id = quantity.attr("data-position-id");

        if (count > 0){
            $(quantity).attr("data-count-value", count);
            $(quantity).text(count);

            updatePosition(product_id, count)
        }
        return false;
    });


    $('body').on('click', '.js-remove', function () {
        let product_id = $(this).attr("data-position-id");
        $(this).closest('.js-cart-item').remove();
        removePosition(product_id)
        return false;
    });

    $('body').on('click', '.js-category-item', function (e) {
        $('#fd-link').val($(this).attr('data-link'))
    });

    $('body').on('click', '.filter-item input:checkbox', function (e) {
        $('#params-filter-form').submit()
    });

    $(function(){
        const hash = window.location.hash;
        hash && $('ul.nav-pills a[href="' + hash + '"]').tab('show');
    });

    // search
    new Vue({
        el: '#search-box',
        data: {
            query: "",
            isLoading: false,
            section: "catalog",
            pages: []
        },
        methods: {
            searchApi: function () {
                this.isLoading = true
                $.ajax({
                    type: "get",
                    url: this.$refs.url.action,
                    context: this,
                    data: {
                        'query': this.query,
                        'section': this.section,
                    }
                }).done(function (response) {
                    this.pages = response
                    this.isLoading = false
                })
            }
        },
        watch: {
            query() {
                this.searchApi()
            },
            section(){
                this.searchApi()
            }
        }
    });

    //Disable mouse right click
    $("body").on("contextmenu",function(e){
        return false;
    });

    //Disable cut copy paste
    $('body').bind('cut copy', function (e) {
        e.preventDefault();
    });
});




