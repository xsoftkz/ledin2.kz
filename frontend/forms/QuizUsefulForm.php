<?php

namespace frontend\forms;

use common\models\Settings;
use frontend\models\Feedback;
use Yii;
use yii\base\Model;

class QuizUsefulForm extends Model
{
    public $question1;
    public $question2;

    public function rules()
    {
        return [
          [['question1', 'question2'], 'required'],
          [['question1', 'question2'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
          'question1' => 'Что вам понравилось больше всего?',
          'question2' => 'Что сделает рассылку ещё интереснее?',
        ];
    }

    public function save()
    {
        $message = '';
        $message .= $this->getAttributeLabel('question1').'<br/>';
        $message .= $this->question1.'<br/>';
        $message .= $this->getAttributeLabel('question2').'<br/>';
        $message .= $this->question2;

        $model = new Feedback();
        $model->type_id = Feedback::QUIZ;
        $model->username = 'Без имени';
        $model->phone = '-';
        $model->email = '-';
        $model->comment = $message;

        if (!$model->save(false)) {
            return false;
        } else {
            $this->sendEmail($model);
            return true;
        }
    }

    public function sendEmail($model)
    {
        return \Yii::$app->mailer->compose('@common/modules/feedback/mail/feedback', [
            'username' => $model->username,
            'phone' => $model->phone,
            'comment' => $model->comment,
        ])
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['appDomain']])
            ->setTo(Settings::getValue('marketing_email'))
            ->setSubject('Обратная связь с сайта')
            ->send();
    }
}