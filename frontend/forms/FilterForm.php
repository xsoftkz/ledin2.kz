<?php

namespace frontend\forms;

use frontend\models\Category;
use frontend\models\EntityParam;
use frontend\models\Param;
use frontend\models\ParamOption;
use frontend\models\ProductModel;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class FilterForm
{
    private $category;
    private $param_values;
    public $search_params;

    public function __construct(Category $category, $paramValues)
    {
        $this->category = $category;
        $this->loadParamValues($paramValues);
    }

    private function loadParamValues($paramValues)
    {
        if (!$paramValues) {
            return [];
        }
        $options = [];
        foreach ($paramValues as $key => $value) {
            if ($paramValues[$key]['value'] !== '') {
                $options[] = $paramValues[$key]['value'];
            }
        }

        $this->search_params = $options;

        $values = [];
        foreach ($options as $key => $value) {
            foreach ($value as $k => $v) {
                $values[] = $v;
            }
        }
        $this->param_values = $values;
    }

    private function getOptions()
    {
        if ($this->param_values) {
            $model_ids = $this->getModelIdsByParams();

            if (!$model_ids) {
                $model_ids = $this->getCategoryModelIds();
            }

        } else {
            $model_ids = $this->getCategoryModelIds();
        }

        $entity_params = EntityParam::find()->alias('entity_param')
            ->select('entity_param.value_ru')->distinct()
            ->leftJoin('{{%eav_param}} as param', 'param.id = entity_param.param_id')
            ->andWhere(['IS NOT', 'entity_param.value_ru', null])
            ->andWhere(['<>', 'entity_param.value_ru', ''])
            ->andWhere(['entity_param.entity_id' => $model_ids])
            ->andWhere(['param.type_id' => [Param::TYPE_CHECKBOXLIST, Param::TYPE_SELECT]])
            ->asArray()
            ->all();

        $option_ids = ArrayHelper::getColumn($entity_params, 'value_ru');

        $options = ParamOption::find()->alias('option')
            ->select(['option.param_id', 'option.id', 'option.param_id', 'option.value_ru'])
            ->leftJoin('{{%eav_param}} as param', 'param.id = option.param_id')
            ->andWhere(['option.id' => $option_ids])
            ->orderBy('option.value_ru')
            ->asArray()->all();

        ArrayHelper::multisort($options, 'param_id');
        return $options;
    }

    private function getFilterItems()
    {

        $options = $this->getOptions();
        $params = array_unique(ArrayHelper::getColumn($options, 'param_id'));

        $filterItems = [];

        foreach ($params as $param) {
            $paramOptions = [];

            foreach ($options as $option) {
                if ($option['param_id'] == $param) {
                    $paramOptions[$option['id']] = $option['value_ru'];
                }
            }

            $filterItem = new FilterItem();
            $filterItem->param_id = $param;
            $filterItem->options = $paramOptions;
            $filterItem->value = $this->param_values;

            $filterItems[] = $filterItem;
        }

        return $filterItems;
    }

    private function getModelIdsByParams()
    {
        $models = ProductModel::find()->alias('product_model')
            ->select('product_model.id')
            ->leftJoin('{{%shop_product}} as product', 'product.id = product_model.product_id')
            ->leftJoin('{{%shop_category}} as category', 'category.id = product.category_id')
            ->leftJoin('{{%eav_entity_param_value}} as entity_param', 'product_model.id = entity_param.entity_id')
            ->andWhere(['product.category_id' => $this->category->id])
            ->andWhere(['product.status_id' => 1])
            ->andWhere(['entity_param.value_ru' => $this->param_values])
            ->groupBy('product_model.id')
            ->having('COUNT(entity_param.entity_id) = '.count($this->search_params))
            ->asArray()
            ->all();
        return ArrayHelper::getColumn($models, 'id');
    }

    private function getCategoryModelIds()
    {
        $models = ProductModel::find()->alias('product_model')
            ->select('product_model.id')
            ->leftJoin('{{%shop_product}} as product', 'product.id = product_model.product_id')
            ->leftJoin('{{%shop_category}} as category', 'category.id = product.category_id')
            ->andWhere(['product.category_id' => $this->category->id])
            ->andWhere(['product.status_id' => 1])
            ->asArray()
            ->all();

        return ArrayHelper::getColumn($models, 'id');
    }

    public function getFormData()
    {
        return [
            'filter_items' => $this->getFilterItems(),
            'category_slug' => $this->category->slug
        ];
    }

    public function search()
    {
        if ($this->param_values) {
            $model_ids = $this->getModelIdsByParams();
        } else {
            $model_ids = $this->getCategoryModelIds();
        }

        return ProductModel::find()->where(['id' => $model_ids]);
    }
}

