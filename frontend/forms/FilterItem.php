<?php

namespace frontend\forms;

use frontend\models\Param;
use yii\base\Model;

class FilterItem extends Model
{
    public $param_id;
    public $options;
    public $value;

    public function getParamLabel()
    {
        if ($model = Param::findOne($this->param_id)) {
            return $model->getTitle();
        }
        return 'не задано';
    }

}