<?php
return [
    'Catalog' => 'Catalog',
    'Description' => 'Description',
    'Specifications' => 'Specifications',
    'Code' => 'Code',
    'About company' => 'About company',
    'Portfolio' => 'Portfolio',
    'Partners' => 'Partners',
    'Services' => 'Services',
    'Contacts' => 'Contacts',
    'Vacancies' => 'Vacancies',
    'News' => 'News',
    'Download' => 'Download',
    'Installed on objects' => 'Installed on objects',
    'See all projects' => 'See all projects',
    'Our projects' => 'Our projects',
    'Latest news' => 'Latest news',
    'Installed products' => 'Installed products',
    'About' => 'About',

    'Favorites' => 'Favorites',
    'Compare' => 'Compare',
    'Callback' => 'Callback',
    'The list of products is empty' => 'The list of products is empty',
    'Clear favorites list' => 'Clear favorites list',
    'Clear compare list' => 'Clear compare list',
    'Product images' => 'Product images',
    'Facade images' => 'Facade images',
    'Video' => 'Video',
    'Product code' => 'Product code',
    'Add to favorites' => 'Add to favorites',
    'Product' => 'Product',
    'Model' => 'Model',
    'Compare products' => 'Compare products',
    'All' => 'All',
    'Search' => 'Search',
    'Reset' => 'Reset',
    'Readmore' => 'Readmore',
    'Career' => 'Career',
    'Contact us' => 'Contacts us',
    'Contact us desc' => 'If you have any questions, send us a message and we will reply to you shortly',
    'Contact us phone' => 'or call the number',
    'You name' => 'Your name',
    'You phone' => 'Your phone',
    'Message' => 'Message',
    'Success page text' => 'We will reply you shortly',
    'Thanks' => 'Thanks',
    'Learn more' => 'Learn more',
    'Filter' => 'Filter',
    'Site search' => 'Site search',
    'Since 2015' => 'Since 2015',
    'Faq' => 'FAQ',
    'Copyright text' => 'The rights to the materials published on the site belong to LED INNOVATIONS',
    'We work all over Kazakhstan' => 'We work all over Kazakhstan',
    'site_name' => 'LEDIN.kz — lighting in Astana and Almaty',
    'site_meta_desc' => 'Outdoor and indoor lighting, lamps, spotlights, solar panels, LED strips in Astana from LED INNOVATION',
    'Single number in Kazakhstan' => 'Single number in Kazakhstan',
    'Cart' => 'Cart',
    'Add to cart' => 'Add to cart',
    'Cart commercial proposal' => 'Send a request for a commercial proposal',
    'Organization' => 'Organization',
    'Requisites' => 'Requisites',
    'Send order' => 'Send order',
    'Download catalog' => 'Download catalog',
    'Download catalog text' => 'Leave your contact details and catalog download will start automatically',
    'Article' => 'Blog',
    'Articles' => 'Blog',
    'Vacancy contact desc' => 'ВYou can send your resume by filling out this form and we will reply to you shortly',
    'Resume' => 'Resume',   
    'Success page vacancy text' => 'We have received your resume.. <br> If your professional experience meets the requirements for this vacancy, we will contact you',
    'Success Thanks' => 'Thank you for choosing us',
    'Success client choice' => 'We know that you <strong> have a choice </strong>',
    'Success page desc' => 'You can familiarize yourself with our blog and with videos of realized objects. <br> Spend time with benefit.',
    'For project managers' => 'For designers',
    'For project builders' => 'For developers',
    'Go to blog' => 'Go to blog',
    'Go to youtube' => 'Go to YouTube',
    'Telegram widget title' => 'Lighting news on Your Phone',
    'Telegram widget text' => 'Once a week we send news about fixtures with useful articles.',
    'Telegram widget link' => 'Subscribe to Telegram',	
    'On order' => 'On order',
    'Telegram subscription text' => 'For a subscription you will receive a memo Design of architectural lighting in Nur-Sultan',
    'Side of the building' => 'Parties to be illuminated',
    'Colour temperature' => 'Luminaire color temperature',
    'Budget' => 'Budget',
    'Download sketch' => 'Upload sketch',
    'Warm color' => 'Warm',
    'White color' => 'White',
    'RGB color' => 'RGB',
    'Left side' => 'Left side',
    'Right side' => 'Right side',
    'Center facade' => 'Central facade',
    'Send consultation order' => 'Leave a request for consultation',
    'Consultation order' => 'Application for consultation',
    'Project implementation date' => 'Project implementation date',
    'Sale out' => 'Sale out',
    'Enter word' => 'Enter a word...',
    'Search result' => 'Searching results',
    'Pages' => 'Pages',
    'Portfolio {year}' => 'Portfolio for {year} year: ',
    'More' => 'More',

    'Group type' => 'По типу',
    'Group color' => 'По цвету',
    'Group power' => 'По мощности',
    'Group popular brands' => 'Популярные бренды',
    'Group country' => 'По стране',

    'Send' => 'Send',
    '' => '',

];





