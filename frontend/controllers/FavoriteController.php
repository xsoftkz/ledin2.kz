<?php

namespace frontend\controllers;

use frontend\models\Favorite;
use yii\web\Controller;
use Yii;

class FavoriteController extends Controller
{

    public function actionIndex()
    {
        $models = Yii::$app->favorite->getPositions();
        return $this->render('index', [
            'models' => $models
        ]);
    }

    public function actionAdd()
    {
        Yii::$app->response->format = 'json';
        Favorite::add(Yii::$app->request->post('prod_id'));
        return Favorite::getInfo();
    }

    public function actionClear()
    {
        \Yii::$app->favorite->removeAll();
        return $this->redirect(Yii::$app->request->referrer);
    }

}