<?php
namespace frontend\controllers;

use frontend\models\Content;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class NewsController extends Controller
{

    public function actionIndex()
    {
        $category = null;

        $query = Content::find()->published();
        $query->withSection(Content::SECTION_NEWS);
        $query->orderBy('sort_index DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }


    public function actionView($slug){

        $model = $this->findModel($slug);
        $model->updateCounters(['views' => 1]);

        return $this->render('view', [
            'model' => $model
        ]);

    }


    protected function findModel($slug)
    {
        if (($model = Content::find()->where(['slug' => $slug])->withSection(Content::SECTION_NEWS)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(t('Page not found'));
        }
    }
}
