<?php
namespace frontend\controllers;

use frontend\models\Feedback;
use common\modules\feedback\models\FeedbackItem;
use frontend\models\Content;
use frontend\models\FeedbackForm;
use frontend\models\Portfolio;
use frontend\components\Roistat;
use frontend\models\SearchService;
use frontend\services\SitemapService;
use yii\web\Controller;
use Yii;
use yii\web\Response;


/**
 * Site controller
 */
class SiteController extends Controller
{
    public function actionIndex()
    {
        $banners = Portfolio::find()->withSection(Portfolio::SECTION_BANNER)->published()->limit(3)->orderBy('sort_index DESC')->all();
        $projects = Portfolio::find()->withSection(Portfolio::SECTION_PORTFOLIOS)->published()->andWhere(['add_int' => 1])->limit(10)->all();
        $news = Portfolio::find()->withSection(Portfolio::SECTION_NEWS)->published()->orderBy('created_at DESC')->limit(6)->all();
        $offers = Portfolio::find()->withSection(Portfolio::SECTION_OFFER)->published()->orderBy('created_at DESC')->limit(6)->all();

        return $this->render('index', [
            'banners' => $banners,
            'projects' => $projects,
            'news' => $news,
            'offers' => $offers,
        ]);
    }

    public function actionAbout(){
        $services = Content::find()->withSection(Content::SECTION_COMPANY)->published()->orderBy('sort_index desc')->all();
        $faqList = Content::find()->withSection(Content::SECTION_FAQ)->published()->orderBy('sort_index desc')->all();
        $reviews = Content::find()->withSection(Content::SECTION_REVIEW)->published()->orderBy('sort_index desc')->all();
        $partners = Content::find()->withSection(Content::SECTION_PARTNERS)->published()->orderBy('sort_index desc')->all();

        return $this->render('about', [
            'services' => $services,
            'reviews' => $reviews,
            'partners' => $partners,
            'faqList' => $faqList
        ]);
    }

    public function actionContact()
    {
        $contacts = Content::find()->withSection(Content::SECTION_CONTACT)->published()->all();
        return $this->render('contact', [
            'contacts' => $contacts
        ]);
    }

    public function actionFeedback(){

        $model = new Feedback();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        	
        	//ROISTAT BEGIN
        	$name = isset($_REQUEST['Feedback']['username']) ? $_REQUEST['Feedback']['username'] : null;
        	$phone = isset($_REQUEST['Feedback']['phone']) ? $_REQUEST['Feedback']['phone'] : null;
        	$comment = isset($_REQUEST['Feedback']['comment']) ? $_REQUEST['Feedback']['comment'] : null;

        	$roistat = new Roistat();
        	$roistat -> createProxyLead($name, $phone, '', "Новый лид с ledin.kz", $comment, 0);

        	//ROISTAT END
        	
            if ($model->save()) {
                $model->sendEmailToAdmin();
				$model->sendDataToSendPulse();
                if ($model->link){
                    return $this->redirect($model->link);
                } else {
                    return $this->render('success');
                }
            }else {
                Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка при отправке');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        else {
            Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка');
            return $this->redirect(['/']);
        }
    }

    public function actionOrder(){

        $model = new Feedback();
        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
        	
            $model->uploadFile();
            if ($positions = Yii::$app->cart->getPositions()){
                foreach ($positions as $position) {
                    $item = new FeedbackItem();
                    $item->feedback_id = $model->id;
                    $item->product_id = $position->id;
                    $item->quantity = $position->quantity;
                    $item->save();
					//ROISTAT BEGIN
                    $product = $position->getProduct();
                    if($product) $list .= "{$product -> name}  в кол-ве {$position->quantity} шт".PHP_EOL;
                    //ROISTAT END
                }
                \Yii::$app->cart->removeAll();
            }
            
            //ROISTAT BEGIN
            $name = isset($_REQUEST['Feedback']['username']) ? $_REQUEST['Feedback']['username'] : null;
            $phone = isset($_REQUEST['Feedback']['phone']) ? $_REQUEST['Feedback']['phone'] : null;
            $comment = isset($_REQUEST['Feedback']['comment']) ? $_REQUEST['Feedback']['comment'].PHP_EOL : null;
            $comment .= isset($_REQUEST['Feedback']['company']) ? "Компания: {$_REQUEST['Feedback']['company']}".PHP_EOL : null;
            $comment .= $list ? "Содержимое заказа: {$list}" : null;
            
            $roistat = new Roistat();
            $roistat -> createProxyLead($name, $phone, $email, "Новый заказ с ledin.kz", $comment, 0);
             
            //ROISTAT END
            
            if ($model->sendEmailToAdmin()) {
				$model->sendDataToSendPulse();
                return $this->render('success');
            }else {
                Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка при отправке');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        else {
            return $this->redirect(['/']);
        }
    }

    public function actionResume(){
        $model = new Feedback();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $model->uploadFile();
                $model->sendEmailToHr();
                return $this->render('success-vacancy', [
                    'model' => $model
                ]);
            }else {
                Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка при отправке');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        else {
            Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка');
            return $this->redirect(['/']);
        }
    }

    public function actionExtendFeedback(){
        $model = new FeedbackForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			//ROISTAT BEGIN
			$name = isset($_REQUEST['FeedbackForm']['name']) ? $_REQUEST['FeedbackForm']['name'] : null;
        	$phone = isset($_REQUEST['FeedbackForm']['phone']) ? $_REQUEST['FeedbackForm']['phone'] : null;
        	$email = isset($_REQUEST['FeedbackForm']['email']) ? $_REQUEST['FeedbackForm']['email'] : null;
        	$comment = isset($_REQUEST['FeedbackForm']['budget']) ? "Бюджет: {$_REQUEST['FeedbackForm']['budget']}".PHP_EOL : null;
        	if(is_array($_REQUEST['FeedbackForm']['sides'])){
        		$comment .= "Стороны, которые необходимо осветить:".PHP_EOL;
        		foreach($_REQUEST['FeedbackForm']['sides'] as $side){
        			switch($side){
        				case '1':
        					$comment .= "Левая сторона".PHP_EOL;
        				break;	
        				case '2':
        					$comment .= "Правая сторона".PHP_EOL;
        				break;	
        				case '3':
        					$comment .= "Центральный фасад".PHP_EOL;
        				break;	
        			}
        		}
        	}
        	
        	if(is_array($_REQUEST['FeedbackForm']['colors'])){
        		$comment .= "Цветовая температура светильников:".PHP_EOL;
        		foreach($_REQUEST['FeedbackForm']['colors'] as $color){
        			switch($color){
        				case '1':
        					$comment .= "Теплый".PHP_EOL;
        					break;
        				case '2':
        					$comment .= "Белый".PHP_EOL;
        					break;
        				case '3':
        					$comment .= "RGB".PHP_EOL;
        					break;
        			}
        		}
        	}
        	
        	$roistat = new Roistat();
        	$roistat -> createProxyLead($name, $phone, $email, "Новый лид с ledin.kz", $comment, 0);
        	//ROISTAT END
            if ($model->save()) {
                return $this->render('success');
            }else {
                Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка при отправке');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        else {
            return $this->render('success');
        }
    }

	public function actionSuccess()
    {
        return $this->render('success');
    }

    public function actionSearch($query, $section)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!$query){ return []; }

        $service = new SearchService();
        return $service->search($query, $section);
    }

    public function actionSitemap()
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $urls = SitemapService::generate();

        return $this->renderPartial('sitemap', [
            'urls' => $urls,
        ]);
    }

    public function actionSmartLighting(){
        return $this->render('smart-lighting');
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

}
