<?php
namespace frontend\controllers;

use common\modules\content\models\ModuleConstant;
use frontend\models\Category;
use frontend\models\Content;
use frontend\models\ContentCategory;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ArticleController extends Controller
{

    public function actionIndex($c = false)
    {
        $category = null;

        $query = Content::find()->alias('t1')
            ->leftJoin('{{%content_category}} as cat', 'cat.content_id=t1.id')
            ->andWhere(['t1.status_id' => ModuleConstant::ACTIVE])
            ->andWhere(['t1.section' => Content::SECTION_ARTICLE])
            ->orderBy('t1.sort_index DESC');

        if ($c) {
            $category = $this->findCategory($c);
            $query->andWhere(['cat.category_id' => $category->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);

        $categories = ContentCategory::find()->published()->withSection(Content::SECTION_ARTICLE)->all();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => $category,
            'categories' => $categories,
        ]);
    }


    public function actionView($slug){

        $model = $this->findModel($slug);
        $model->updateCounters(['views' => 1]);

        return $this->render('view', [
            'model' => $model
        ]);

    }


    protected function findModel($slug)
    {
        if (($model = Content::find()->where(['slug' => $slug])->withSection(Content::SECTION_ARTICLE)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(t('Page not found'));
        }
    }

    /**
     * @param $slug
     * @return array|Category|null
     * @throws NotFoundHttpException
     */
    protected function findCategory($slug)
    {
        if (($model = ContentCategory::find()->where(['slug' => $slug])
                ->withSection(Content::SECTION_ARTICLE)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(t('Page not found'));
        }
    }
}
