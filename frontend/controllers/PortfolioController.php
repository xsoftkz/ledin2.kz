<?php

namespace frontend\controllers;

use common\modules\content\models\ModuleConstant;
use frontend\models\ContentCategory;
use frontend\models\Portfolio;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PortfolioController extends Controller
{
    public function actionIndex($slug = false, $year = false){

        if (!$slug){

            $models = ContentCategory::find()->withSection(Portfolio::SECTION)->published()->orderBy('sort_index')->all();
            return $this->render('index', [
                'models' => $models
            ]);

        } else {

            $category = $this->findCategory($slug);

            $query = Portfolio::find()->alias('t1')
                ->leftJoin('{{%content_category}} as cat', 'cat.content_id=t1.id')
                ->andWhere(['t1.status_id' => ModuleConstant::ACTIVE])
                ->andWhere(['t1.section' => Portfolio::SECTION])
                ->andWhere(['cat.category_id' => $category->id]);

            if (!$year) {
                $year = date('Y');
            }

            if ($year != 'all') {
                $query->andWhere(['t1.year' => $year]);
            }

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 20,
                ],
            ]);

            return $this->render('category', [
                'year' => $year,
                'category' => $category,
                'dataProvider' => $dataProvider
            ]);
        }

    }


    public function actionView($slug){

        $model = $this->findModel($slug);
        $model->updateCounters(['views' => 1]);

        $relatedProducts = $model->products;


        return $this->render('view', [
            'model' => $model,
            'relatedProducts' => $relatedProducts,
        ]);

    }


    protected function findCategory($slug)
    {
        if (($model = ContentCategory::find()->withSlug($slug)->withSection(Portfolio::SECTION)->published()->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Page not found'));
        }
    }


    protected function findModel($slug)
    {
        if (($model = Portfolio::find()->withSlug($slug)->withSection(Portfolio::SECTION)->published()->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('app', 'Page not found'));
        }
    }
}