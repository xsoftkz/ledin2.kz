<?php

namespace frontend\controllers;

use frontend\models\Cart;
use yii\helpers\VarDumper;
use yii\web\Controller;
use Yii;

class CartController extends Controller
{

    public function actionIndex()
    {
        $models = Yii::$app->cart->getPositions();
        return $this->render('index', [
            'models' => $models
        ]);
    }

    public function actionAdd()
    {
        Yii::$app->response->format = 'json';
        $result = Cart::add(Yii::$app->request->post('prod_id'));
        $data = Cart::getInfo();
        $data['result'] = $result;
        return $data;
    }


    public function actionUpdate()
    {
        Yii::$app->response->format = 'json';
        $product_id = Yii::$app->request->post('prod_id');
        $quantity = Yii::$app->request->post('quantity');
        Cart::update($product_id, $quantity);
        return true;
    }


    public function actionRemove()
    {
        Yii::$app->response->format = 'json';
        $product_id = Yii::$app->request->post('prod_id');
        Cart::remove($product_id);
        return true;
    }


    public function actionClear()
    {
        \Yii::$app->cart->removeAll();
        return $this->redirect(Yii::$app->request->referrer);
    }

}