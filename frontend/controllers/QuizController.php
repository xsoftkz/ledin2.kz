<?php

namespace frontend\controllers;

use frontend\forms\QuizUsefulForm;
use frontend\forms\QuizUselessForm;
use Yii;
use yii\helpers\VarDumper;
use yii\web\Controller;

class QuizController extends Controller
{
    public function actionUseful()
    {
        $model = new QuizUsefulForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect('useful-result');
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('useful', [
            'model' => $model
        ]);
    }

    public function actionUsefulResult()
    {
        return $this->render('useful-result');
    }

    public function actionUseless()
    {
        $model = new QuizUselessForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                return $this->redirect('useless-result');
            } else {
                Yii::$app->getSession()->setFlash('danger', 'Возникла ошибка');
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

        return $this->render('useless', [
            'model' => $model
        ]);
    }

    public function actionUselessResult()
    {
        return $this->render('useless-result');
    }
}