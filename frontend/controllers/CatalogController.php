<?php

namespace frontend\controllers;

use common\modules\shop\models\Brand;
use common\modules\shop\models\CategoryProduct;
use frontend\forms\FilterForm;
use Yii;
use frontend\models\Category;
use frontend\models\Product;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CatalogController extends Controller
{

    // страница категории товаров
    public function actionIndex($slug = false)
    {
        if (!$slug){
            throw new NotFoundHttpException(t('Page not found.', 'yii'));
        }

        $query = Product::find()->published();

        $category = $this->findCategory($slug);
        $query->withCategory($category->id);

        if ($category->categories && $category->getLevel() < 2){

            $query = Category::find()->withCategory($category->id)->published()->orderBy('sort');

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => ['pageSize' => 100],
            ]);

            return $this->render('category', [
                'dataProvider' => $dataProvider,
                'category' => $category,
            ]);

        } else {

            $cat_prods = CategoryProduct::findAll(['category_id' => $category->id]);
            $prod_ids = ArrayHelper::getColumn($cat_prods, 'product_id');

            $query->orWhere(['id' => $prod_ids]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => 100],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }

    // страница фильтра товаров
    public function actionFilter($slug)
    {
        $category = $this->findCategory($slug);
        $filterParams = Yii::$app->request->get('FilterItem');

        $form = new FilterForm($category, $filterParams);

        $dataProvider = new ActiveDataProvider([
            'query' => $form->search(),
            'pagination' => [
                'pageSize' => 16,
            ],
        ]);

        return $this->render('filter', [
            'dataProvider' => $dataProvider,
            'formData' => $form->getFormData(),
            'category' => $category,
        ]);
    }


    public function actionView($slug)
    {
        $model = $this->findProduct($slug);

        if ($model->id == 343) {
            return $this->redirect('/site/smart-lighting');
        }

        $productModels = $model->productModels;
        $productInfoItems = $model->productInfoItems;
        $productVideoItems = $model->productVideoItems;

        $relatedProducts = Product::find()->withCategory($model->category_id)->published()->limit(100)->all();

        return $this->render('view', [
            'model' => $model,
            'relatedProducts' => $relatedProducts,
            'productModels' => $productModels,
            'productInfoItems' => $productInfoItems,
            'productVideoItems' => $productVideoItems,
        ]);
    }


    // быстрый просмотр товара
    public function actionInfo($id)
    {
        $model = Product::findOne($id);

        return $this->renderPartial('_info', [
            'model' => $model,
        ]);
    }


    public function actionCategory()
    {
        $models = Category::find()->isParent()->with('categories')->published()->all();
        return $this->render('category', [
            'models' => $models
        ]);
    }

    /**
     * @return Category
     */
    protected function findCategory($slug)
    {
        if (($model = Category::find()->withSlug($slug)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }

    protected function findProduct($slug)
    {
        if (($model = Product::find()->withSlug($slug)->published()->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }


    protected function findBrand($slug)
    {
        if (($model = Brand::find()->withSlug($slug)->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }


}