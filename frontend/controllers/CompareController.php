<?php
namespace frontend\controllers;

use frontend\models\Compare;
use frontend\models\Type;
use yii\web\Controller;
use Yii;

class CompareController extends Controller
{

    public function actionIndex()
    {
        $positions = Yii::$app->compare->getPositions();
        $type = Type::findOne(Compare::getTypeId());

        $params = ($type) ? $type->params : null;

        return $this->render('index', [
            'positions' => $positions,
            'params' => $params
        ]);
    }


    public function actionAdd()
    {
        Yii::$app->response->format = 'json';
        Compare::add(Yii::$app->request->post('prod_id'));
        return Compare::getInfo();
    }


    public function actionClear()
    {
        \Yii::$app->compare->removeAll();
        return $this->redirect(Yii::$app->request->referrer);
    }

}