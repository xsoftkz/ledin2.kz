<?php

namespace frontend\widgets\portfolio;

use frontend\models\Portfolio;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class PortfolioSorterWidget extends Widget
{
    public $defaultYear;
    public $category;

    public function init()
    {
        parent::init();

        $years = Portfolio::find()->select(['year'])
            ->withSection(Portfolio::SECTION)->withCategory($this->category->id)
            ->andWhere(['not', ['year' => null]])
            ->published()->distinct()->orderBy('year desc')
            ->asArray()->all();

        $years = ArrayHelper::getColumn($years, 'year');

        $yearParam = Yii::$app->request->getQueryParam('year');

        if ($yearParam) {
            if($yearParam == 'all'){
                $label = t('All');
            } else {
                $label = $yearParam;
            }
        } else {
            $label = $this->defaultYear;
        }

        echo $this->render('portfolio-sorter', [
            'label' => $label,
            'years' => $years,
            'category' => $this->category,
        ]);
    }
}