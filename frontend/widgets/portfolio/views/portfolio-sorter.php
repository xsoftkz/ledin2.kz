<?php
use yii\helpers\Url;
?>


<div class="text-right">
    <strong><?= t('Project implementation date') ?></strong>:

    <a href="#" type="button" class="btn btn-sm btn-outline-main dropdown-toggle ml-2 " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= $label ?>
    </a>

    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
        <?php foreach ($years as $year): ?>
            <a class="dropdown-item" href="<?= url(['/portfolio/index', 'slug' => $category->slug, 'year' => $year ]) ?>"><?= $year ?></a>
        <?php endforeach; ?>

        <a class="dropdown-item" href="<?= url(['/portfolio/index', 'slug' => $category->slug, 'year' => 'all']) ?>"><?= t('All') ?></a>
    </div>
</div>

