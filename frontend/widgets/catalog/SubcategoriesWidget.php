<?php
namespace frontend\widgets\catalog;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use frontend\models\Category;

class SubcategoriesWidget extends Widget
{
    public $category_id;

    public function run()
    {
        $category = Category::findOne($this->category_id);

        $indexes = [];
        if($categories = $category->categories) {
            $indexes = array_unique(ArrayHelper::getColumn($categories, 'group_id'));
        }

        echo $this->render('subcategories', [
            'indexes' => $indexes,
            'categories' => $category->categories
        ]);
    }

}
