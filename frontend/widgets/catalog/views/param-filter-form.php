<?php
?>

<?php if (!\Yii::$app->devicedetect->isMobile() && !\Yii::$app->devicedetect->isTablet()) { ?>
    <div class="filter-box">
        <?= $this->render('_param-filter-form-inner',
            [
                'formData' => $formData,
            ]);
        ?>
    </div>
<?php } else { ?>
    <div class="mb-4 collapse" id="filterCollapse">
        <div class="card card-body">
            <?= $this->render('_param-filter-form-inner',
                [
                    'formData' => $formData,
                ]);
            ?>
        </div>
    </div>
<?php }?>

