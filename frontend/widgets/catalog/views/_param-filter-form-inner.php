<?php

use frontend\forms\FilterItem;
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;

/** @var array $models */
/** @var array $formData */
/** @var FilterItem $model */

$models = $formData['filter_items'];
$category_slug = $formData['category_slug'];

$form = ActiveForm::begin(
    [
        'id' => 'params-filter-form',
        'method' => 'GET',
    ]
);

foreach ($models as $model) {
    echo $form->field($model, '[' . $model->param_id . ']value')
        ->checkboxList($model->options, ['class' => 'filter-item'])
        ->label($model->getParamLabel());
}

echo Html::submitButton(t('Search'), ['class' => 'btn btn-light mr-2']);
echo Html::a(t('Reset'), ['/catalog/filter', 'slug' => $category_slug], ['class' => 'btn btn-light']);

ActiveForm::end();
