<?php
?>

<div style="display: none;" id="cartModal" class="shadow">
    <div class="cart-modal-wrap">
        <p>
            <img src="/svg/success-green.svg" alt="success">
             Вы добавили новый товар в корзину. У вас <span class="js-cart-count">1</span> товар(а) в корзине.
        </p>
        <div>
            <a href="<?= url(['/cart']) ?>" class="btn btn-lg btn-outline-main btn-rounded">
                Перейти в корзину
            </a>
            <a href="#" class="btn btn-lg btn-outline-main btn-rounded" data-fancybox-close>
                Продолжить покупки
            </a>
        </div>
    </div>
</div>

