<?php
use frontend\models\Category;
/**
 * @var Category $category
 * @var array $indexes
 * @var Category[] $categories
 */
?>

<div class="subcategories p-2">
    <div class="row row-no-gutters">
        <?php foreach ($indexes as $index) { ?>
            <div class="col-lg-12 mb-3">
                <h5 class="font-weight-bold"><?= Category::getGroupName($index) ?></h5>
                <?php foreach ($categories as $category) { ?>
                    <?php if ($category->group_id == $index) { ?>
                        <a href="<?= url(['/catalog/index', 'slug' => $category->slug]) ?>" class="d-inline-block pr-2 pb-2 p-lg-2 text-primary">
                            <?= $category->title ?>
                        </a>
                        <span class="d-none d-lg-inline"> | </span>
                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>


