<?php

namespace frontend\widgets\catalog;

use yii\base\Widget;

class ParamFilterFormWidget extends Widget
{
    public $formData;

    public function init()
    {
        parent::init();

        echo $this->render('param-filter-form', [
            'formData' => $this->formData,
        ]);
    }

}
