<?php
namespace frontend\widgets\catalog;

use yii\base\Widget;

class CartModalWidget extends Widget
{
    public function run()
    {
        echo $this->render('cart-modal');
    }

}
