<?php
namespace frontend\widgets\feedback;

use yii\base\Widget;

class FeedbackTelegramWidget extends Widget
{
    public function run()
    {
        echo $this->render('telegram');
    }

}
