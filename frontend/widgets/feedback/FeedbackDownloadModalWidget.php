<?php
namespace frontend\widgets\feedback;

use frontend\models\Feedback;
use yii\base\Widget;

class FeedbackDownloadModalWidget extends Widget
{
    public function run()
    {
        $model = new Feedback();
        $model->type_id = Feedback::CATALOG;
        echo $this->render('feedback-download-modal', [
            'model' => $model
        ]);
    }

}
