<?php
namespace frontend\widgets\feedback;

use frontend\models\Feedback;
use yii\base\Widget;

class ConsultationModalWidget extends Widget
{
    public function run()
    {
        $model = new Feedback();
        $model->type_id = Feedback::CONSULTATION;
		$model->username = 'Без имени';
		$model->email = 'не задано';

        echo $this->render('consultation-modal', [
            'model' => $model
        ]);
    }

}
