<?php
namespace frontend\widgets\feedback;

use frontend\models\Feedback;
use yii\base\Widget;

class FeedbackFormWidget extends Widget
{
    public function run()
    {
        $model = new Feedback();
        $model->type_id = Feedback::LETTER;
        echo $this->render('feedback-form', [
            'model' => $model
        ]);
    }

}
