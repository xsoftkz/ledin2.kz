<?php
namespace frontend\widgets\feedback;

use frontend\models\Feedback;
use yii\base\Widget;

class FreeCallbackModalWidget extends Widget
{
    public function run()
    {
        $model = new Feedback();
        $model->type_id = Feedback::CALLBACK;
		$model->is_mailing = 1;
        echo $this->render('free-callback-modal', [
            'model' => $model
        ]);
    }

}
