<?php
namespace frontend\widgets\feedback;

use frontend\models\Feedback;
use yii\base\Widget;

class FeedbackCartFormWidget extends Widget
{
    public function run()
    {
        $model = new Feedback();
        $model->type_id = Feedback::PRODUCT;
        echo $this->render('feedback-cart-form', [
            'model' => $model
        ]);
    }

}
