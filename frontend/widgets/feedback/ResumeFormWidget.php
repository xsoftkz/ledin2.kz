<?php
namespace frontend\widgets\feedback;

use frontend\models\Feedback;
use yii\base\Widget;

class ResumeFormWidget extends Widget
{
    public $vacancy_name;
    public function run()
    {
        $model = new Feedback();
        $model->type_id = Feedback::RESUME;
        echo $this->render('resume-form', [
            'model' => $model
        ]);
    }

}
