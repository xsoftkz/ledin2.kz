<?php
?>

<div class="section-item telegram-subscribe text-center mx-auto">
    <h4><?= t('Telegram widget title') ?></h4>
    <p><?= t('Telegram widget text') ?></p>
    <br>
    <p>
        <a href="<?= setting('telegram') ?>" class="btn btn-telegram">
            <img src="/svg/telegram.svg" alt="Telegram">
            <?= t('Telegram widget link') ?>
        </a>
    </p>
    <br>
    <p>
        <?= t('Telegram subscription text') ?>
    </p>
</div>





