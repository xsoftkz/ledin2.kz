<?php

use frontend\models\FeedbackForm;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/**
 * @var $model \frontend\models\FeedbackForm
 */

?>

<div style="display: none;" id="feedback-modal" class=" shadow">
    <div class="feedback-modal-wrap">
        <div class="text-center pb-3">
            <h3 class="font-primary">
                <?= t('Send order') ?>
            </h3>
        </div>

        <?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'id' => 'form-extended-modal',
            'action' => ['/site/extend-feedback'],
        ]); ?>

            <div>
                <?=  $form->field($model, 'name'); ?>

                <?=  $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(),
                    ['mask'=> '+7 (999) 999-99-99']); ?>

                <?=  $form->field($model, 'email'); ?>

                <?=  $form->field($model, 'sides')->checkboxList(FeedbackForm::getSideList(), ['class' => 'feedback-checkboxes']); ?>

                <?=  $form->field($model, 'colors')->checkboxList(FeedbackForm::getColorList(), ['class' => 'feedback-checkboxes']); ?>

                <?=  $form->field($model, 'budget'); ?>

                <?= $form->field($model, 'files[]')->fileInput(['multiple' => true]); ?>
                <?=  $form->field($model, 'utm')->hiddenInput(['value' => Yii::$app->request->get("utm_source")])->label(false); ?>

                <?= $form->field($model, 'reCaptcha')->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha2::className(),
                    [
                        'siteKey' => \frontend\models\Feedback::RECAPTCHA_SITE_KEY,
                    ]
                )->label(false) ?>
            </div>

        <div class="text-center pt-4">
            <?=  Html::submitButton(t('Send order'), ['class' => 'btn btn-lg btn-outline-main']); ?>
        </div>

        <?php  ActiveForm::end(); ?>
    </div>
</div>

