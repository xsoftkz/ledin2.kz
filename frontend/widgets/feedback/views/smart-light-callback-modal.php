<?php

use frontend\models\Feedback;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\MaskedInput;

?>

<div style="display: none;" id="smart-light-callback-modal" class=" shadow">
    <div class="feedback-modal-wrap">
        <div class="text-center pb-3">
            <h3 class="font-primary">
                <?= t('Callback') ?>
            </h3>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'smart-light-form-modal', 'action' => ['/site/feedback']]); ?>

            <div>
                <?=  $form->field($model, 'username')->hiddenInput(['id' => 'call-username'])->label(false); ?>
  				
				<?=  $form->field($model, 'email')->hiddenInput(['value' => 'не задано', 'id' => 'call-email'])->label(false); ?>
                <?= $form->field($model, 'link')->hiddenInput(['value' => Yii::getAlias('@frontendWebroot').'/uploads/files/smart_ligting_ledin.pdf', 'id' => 'lids-link'])->label(false); ?>

                <?=  $form->field($model, 'phone')
                    ->widget(MaskedInput::className(), [
                        'mask'=> '+7 (999) 999-99-99',
                        'options' => [
                            'id' => 'smart-light-call-phone',
                        ],
                    ])
                    ->label(t('You phone')); ?>

                <?= $form->field($model, 'type_id')->hiddenInput(['id' => 'call-type_id'])->label(false); ?>

                <?=  $form->field($model, 'is_mailing')->checkbox(['id' => 'call-is_mailing'])->label('Нажимая кнопку "Жду звонка", я даю свое согласие на обработку моих персональных данных, в соответствии с Законом Республики Казахстан от 21 мая 2013 года № 94-V "О персональных данных и их защите", на условиях и для целей, определенных в Согласии на обработку персональных данных'); ?>

                <?= $form->field($model, 'reCaptcha')->widget(
                    ReCaptcha2::className(),
                    [
                        'siteKey' => Feedback::RECAPTCHA_SITE_KEY,
                        'id' => 'fd-smart-light-reCaptcha',
                    ]
                )->label(false) ?>

            </div>



        <div class="text-center pt-4">
            <?=  Html::submitButton(t('Жду звонка'), ['class' => 'btn btn-lg btn-outline-main']); ?>
        </div>

        <?php  ActiveForm::end(); ?>
    </div>
</div>

