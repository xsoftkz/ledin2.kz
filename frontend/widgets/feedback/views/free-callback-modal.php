<?php
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
?>

<div style="display: none;" id="free-callback-modal" class=" shadow">
    <div class="feedback-modal-wrap">
        <div class="text-center pb-3">
            <h3 class="font-primary">
                Получить бесплатно
            </h3>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'free-form-modal', 'action' => ['/site/feedback']]); ?>

            <div>
                <?=  $form->field($model, 'username')->hiddenInput(['value' => 'Без имени'])->label(false); ?>

                <?=  $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(),
                    ['mask'=> '+7 (999) 999-99-99'])->label(t('You phone')); ?>

                <?=  $form->field($model, 'email')->textInput()->label(t('Email')); ?>

                <?= $form->field($model, 'type_id')->hiddenInput()->label(false); ?>
                <?=  $form->field($model, 'utm')->hiddenInput(['value' => Yii::$app->request->get("utm_source")])->label(false); ?>

                <?= $form->field($model, 'reCaptcha')->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha2::className(),
                    [
                        'siteKey' => \frontend\models\Feedback::RECAPTCHA_SITE_KEY,
                    ]
                )->label(false) ?>

            </div>

        <div class="text-center pt-4">
            <?=  Html::submitButton(t('Send order'), ['class' => 'btn btn-lg btn-outline-main']); ?>
        </div>
        <br>
        <?=  $form->field($model, 'is_mailing')->checkbox()->label('Я согласен получить рассылку от LEDIN'); ?>

        <?php  ActiveForm::end(); ?>
    </div>
</div>

