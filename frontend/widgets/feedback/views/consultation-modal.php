<?php
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
?>

<div style="display: none;" id="consultation-modal" class=" shadow">
    <div class="feedback-modal-wrap">
        <div class="text-center pb-3">
            <h3 class="font-primary">
                <?= t('Consultation order') ?>
            </h3>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'consultation-modal', 'action' => ['/site/feedback']]); ?>

            <div>
               	<?=  $form->field($model, 'username')->hiddenInput()->label(false); ?>

				<?=  $form->field($model, 'email')->hiddenInput(['value' => 'не задано'])->label(false); ?>
               
				<?=  $form->field($model, 'phone')->textInput()->label(t('You phone')); ?>
				
                <?= $form->field($model, 'type_id')->hiddenInput()->label(false); ?>
                <?=  $form->field($model, 'utm')->hiddenInput(['value' => Yii::$app->request->get("utm_source")])->label(false); ?>

                <?= $form->field($model, 'reCaptcha')->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha2::className(),
                    [
                        'siteKey' => \frontend\models\Feedback::RECAPTCHA_SITE_KEY,
                    ]
                )->label(false) ?>
            </div>

        <div class="text-center pt-4">
            <?=  Html::submitButton(t('Send order'), ['class' => 'btn btn-lg btn-outline-main']); ?>
        </div>

        <?php  ActiveForm::end(); ?>
    </div>
</div>

