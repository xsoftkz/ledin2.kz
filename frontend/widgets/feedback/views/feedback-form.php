<?php
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

?>

<div class="feedback">
    <div class="feedback-inner">
        <div class="container">
            <div class="feedback-wrap">
                <div class="text-center pb-4">
                    <h3 class="font-primary pb-4">
                        <?= t('Contact us') ?>
                    </h3>
                    <p>
                        <?= t('Contact us desc') ?>
                    </p>
                    <p class="font-weight-bold">
                        <?= t('Contact us phone') ?>: <a href="tel:<?= setting('site_phone') ?>"><?= setting('site_phone') ?></a>
                    </p>
                </div>

                <?php $form = ActiveForm::begin(['id' => 'form-feedback', 'action' => ['/site/feedback']]); ?>

                <div class="row">
                    <div class="col-lg-6">
                        <?=  $form->field($model, 'username')->textInput()->label(t('You name')); ?>

						<?=  $form->field($model, 'email')->hiddenInput(['value' => 'не задано'])->label(false); ?>
						
                        <?=  $form->field($model, 'utm')->hiddenInput(['value' => Yii::$app->request->get("utm_source")])->label(false); ?>

                        <?=  $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(),
                            ['mask'=> '+7 (999) 999-99-99'])->label(t('You phone')); ?>

                        <?= $form->field($model, 'type_id')->hiddenInput()->label(false); ?>
                    </div>
                    <div class="col-lg-6">
                        <?=  $form->field($model, 'comment')->textarea(['rows' => '5'])->label(t('Message')); ?>
                    </div>
                </div>

                <?= $form->field($model, 'reCaptcha')->widget(
                    \himiklab\yii2\recaptcha\ReCaptcha2::className(),
                    [
                        'siteKey' => \frontend\models\Feedback::RECAPTCHA_SITE_KEY,
                    ]
                )->label(false) ?>

                <div class="text-center pt-4">
                    <?=  Html::submitButton(t('Send order'), ['class' => 'btn btn-lg btn-outline-main']); ?>
                </div>

                <?php  ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>