<?php

use frontend\models\Feedback;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
?>

<div style="display: none;" id="feedback-download-modal" class=" shadow">
    <div class="feedback-modal-wrap">
        <div class="text-center pb-3">
            <h3 class="font-primary">
                <?= t('Download catalog') ?>
            </h3>
            <p><small><?= t('Download catalog text') ?></small></p>
        </div>

        <?php $form = ActiveForm::begin(['id' => 'form-download-modal', 'action' => ['/site/feedback']]); ?>

        <div>
            <?=  $form->field($model, 'username')->textInput(['id' => 'fd-username'])->label(t('You name')); ?>

			<?=  $form->field($model, 'email')->hiddenInput(['value' => 'не задано', 'id' => 'fd-email'])->label(false); ?>
			
            <?=  $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(),
                ['mask'=> '+7 (999) 999-99-99',
                    'options' => [
                        'id' => 'fd-phone',
                    ],
                ])->label(t('You phone')); ?>

            <?= $form->field($model, 'type_id')->hiddenInput(['id' => 'fd-type_id'])->label(false); ?>

            <?= $form->field($model, 'link')->hiddenInput(['id' => 'fd-link'])->label(false); ?>

            <?=  $form->field($model, 'comment')->textarea(['rows' => '2', 'id' => 'fd-comment'])->label(t('Message')); ?>

            <?= $form->field($model, 'reCaptcha')->widget(
                ReCaptcha2::className(),
                [
                    'siteKey' => Feedback::RECAPTCHA_SITE_KEY,
                    'id' => 'fd-reCaptcha',
                ]
            )->label(false) ?>
        </div>

        <div class="text-center pt-4">
            <?=  Html::submitButton(t('Send order'), ['class' => 'btn btn-lg btn-outline-main']); ?>
        </div>

        <?php  ActiveForm::end(); ?>
    </div>
</div>

