<?php
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
?>

<div class="feedback-cart">
    <?php $form = ActiveForm::begin(['id' => 'form-cart-feedback', 'action' => ['/site/order']]); ?>

    <?=  $form->field($model, 'username')->textInput()->label(t('You name')); ?>
	
	<?=  $form->field($model, 'email')->hiddenInput(['value' => 'не задано'])->label(false); ?>

    <?=  $form->field($model, 'company')->textInput()->label(t('Organization')); ?>

    <?=  $form->field($model, 'phone')->widget(\yii\widgets\MaskedInput::className(),
        ['mask'=> '+7 (999) 999-99-99'])->label(t('You phone')); ?>

    <?= $form->field($model, 'file')->fileInput()->label(t('Requisites')); ?>

    <?=  $form->field($model, 'comment')->textarea(['rows' => '3'])->label(t('Message')); ?>

    <?= $form->field($model, 'type_id')->hiddenInput()->label(false); ?>

    <?=  $form->field($model, 'utm')->hiddenInput(['value' => Yii::$app->request->get("utm_source")])->label(false); ?>

    <?= $form->field($model, 'reCaptcha')->widget(
        \himiklab\yii2\recaptcha\ReCaptcha2::className(),
        [
            'siteKey' => \frontend\models\Feedback::RECAPTCHA_SITE_KEY,
        ]
    )->label(false) ?>

    <div class="text-center pt-4">
        <?=  Html::submitButton(t('Send order'), ['class' => 'btn btn-lg btn-outline-main']); ?>
    </div>

    <?php  ActiveForm::end(); ?>
</div>