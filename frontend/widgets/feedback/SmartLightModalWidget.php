<?php
namespace frontend\widgets\feedback;

use frontend\models\Feedback;
use yii\base\Widget;

class SmartLightModalWidget extends Widget
{
    public function run()
    {
        $model = new Feedback();
        $model->type_id = Feedback::CALLBACK;
        $model->username = 'Без имени';
        $model->is_mailing = true;
        echo $this->render('smart-light-callback-modal', [
            'model' => $model
        ]);
    }

}
