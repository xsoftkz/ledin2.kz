<?php
namespace frontend\widgets\feedback;

use frontend\models\FeedbackForm;
use yii\base\Widget;

class FeedbackModalWidget extends Widget
{
    public function run()
    {
        $model = new FeedbackForm();
        echo $this->render('feedback-modal', [
            'model' => $model
        ]);
    }

}
