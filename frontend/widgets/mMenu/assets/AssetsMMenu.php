<?php
namespace frontend\widgets\mMenu\assets;

use Yii;
use yii\web\AssetBundle;

class AssetsMMenu extends AssetBundle
{
	public $sourcePath = '@mMenu/lib';
	public $basePath = '@webroot/assets';
	public $js = [
        'jquery.mmenu.js',
        'mmenu.js',
    ];
	public $css = [
		'hamburgers.min.css',
		'jquery.mmenu.css',
		'mmenu.css'
	];
	public $depends = [
		'yii\web\JqueryAsset',
	];

	public function init() {
		Yii::setAlias('@mMenu', __DIR__);
		return parent::init();
	}
}