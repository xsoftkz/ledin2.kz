$(document).ready(function () {

// mobile menu
    $menu = $("#mobile-menu").mmenu({
        offCanvas: {
            position  : "right"
        },
        navbar: {
            title: false
        },
        searchfield: {
            resultsPanel: true,
            showTextItems: true
        }
    });

    var $icon = $("#mmenu-icon");
    var API = $menu.data("mmenu");

    $icon.on("click", function () {
        API.open();
    });

    API.bind( "open:finish", function() {
        setTimeout(function() {
            $icon.addClass( "is-active" );
        }, 100);
    });
    API.bind( "close:finish", function() {
        setTimeout(function() {
            $icon.removeClass( "is-active" );
        }, 100);
    });
    /**
     * Created by hp on 03.10.2017.
     */
});
