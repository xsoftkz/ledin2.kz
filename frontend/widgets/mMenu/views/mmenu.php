<?php

use frontend\models\Lang;
use yii\helpers\Url;

\frontend\widgets\mMenu\assets\AssetsMMenu::register($this);

?>

<nav id="mobile-menu">
    <ul>
        <li>
            <a href="#">
                <?= Lang::getCurrent()->name ?>
            </a>
            <ul>
                <?php foreach ($langs as $lang):?>
                    <li>
                        <a href="<?= '/'.$lang->id.Yii::$app->getRequest()->getLangUrl() ?>"><?= $lang->name?></a>
                    </li>
                <?php endforeach;?>
            </ul>
        </li>
        <?php if ($models) { ?>
            <?php foreach ($models as $model) { ?>
                <li>
                    <a href="<?= Url::to(['/catalog/index', 'slug' => $model->slug]) ?>"><?= $model->title ?></a>
                    <?php if ($model->categories) { ?>
                        <ul>
                            <?php foreach ($model->categories as $child) { ?>
                                <li>
                                    <a href="<?= Url::to(['/catalog/index', 'slug' => $child->slug]) ?>"><?= $child->title ?></a>
                                    <?php if ($child->categories) { ?>
                                        <ul>
                                            <?php foreach ($child->categories as $secondChild) { ?>
                                                <li>
                                                    <a href="<?= Url::to(['/catalog/index', 'slug' => $secondChild->slug]) ?>"><?= $secondChild->title ?></a>

                                                    <?php if ($secondChild->categories) { ?>
                                                        <ul>
                                                            <?php foreach ($secondChild->categories as $thirdChild) { ?>
                                                                <li>
                                                                    <a href="<?= Url::to(['/catalog/index', 'slug' => $thirdChild->slug]) ?>">
                                                                        <?= $thirdChild->title ?></a>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>

                                                </li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </li>
            <?php } ?>
        <?php } ?>
        <?php if ($portfolios){ ?>
          <li>
              <a href="<?= url(['/portfolio']) ?>"><?= t('Portfolio') ?></a>
              <ul>
                  <?php foreach ($portfolios as $portfolio){ ?>
                      <li>
                          <a href="<?= Url::toRoute(['/portfolio/index', 'slug' => $portfolio->slug]) ?>">
                              <?= $portfolio->title ?>
                          </a>
                      </li>
                  <?php } ?>
              </ul>
          </li>
        <?php } ?>
        <li>
            <a href="<?= url('@filesWebroot/sale_out.pdf') ?>" download><?= t('Sale out') ?></a>
        </li>
        <?php if ($models){ ?>
            <li>
                <a href="#"><?= t('Download catalog') ?></a>
                <ul>
                    <?php foreach ($models as $model){ ?>
                        <li>
                            <a href="javascript:;" class="js-category-item" data-fancybox data-src="#feedback-download-modal" data-link="<?= $model->link ?>">
                                <?= $model->title ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
        <li><a href="<?= url(['/about']) ?>"><?= t('About company') ?></a></li>
        <li><a href="<?= url(['/vacancy']) ?>"><?= t('Vacancies') ?></a></li>
        <li><a href="<?= url(['/services']) ?>"><?= t('Services') ?></a></li>
        <li><a href="<?= url(['/news']) ?>"><?= t('News') ?></a></li>
        <li><a href="<?= url(['/contacts']) ?>"><?= t('Contacts') ?></a></li>
        <li>
            <a href="<?= url(['/compare']) ?>">
                <?= t('Compare') ?> (<span class="js-compare-count"><?= Yii::$app->compare->getCount() ?></span>)
            </a>
        </li>
        <li>
            <a href="<?= url(['/favorite']) ?>">
                <?= t('Favorites') ?> (<span class="js-favorite-count"><?= Yii::$app->favorite->getCount() ?></span>)
            </a>
        </li>
        <li>
            <a href="<?= url(['/cart']) ?>">
                <?= t('Cart') ?> (<span class="js-cart-count"><?= Yii::$app->cart->getCount() ?></span>)
            </a>
        </li>
    </ul>
</nav>
