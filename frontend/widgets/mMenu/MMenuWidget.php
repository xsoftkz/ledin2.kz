<?php
namespace frontend\widgets\mMenu;

use frontend\models\Category;
use frontend\models\ContentCategory;
use frontend\models\Lang;
use frontend\models\Portfolio;
use yii\base\Widget;

class MMenuWidget extends Widget {
    public function init()
    {
        parent::init();
        $models = Category::find()->isParent()->published()->orderBy('sort')->all();
        $portfolios = ContentCategory::find()->withSection(Portfolio::SECTION)->published()->orderBy('sort_index')->all();
        $langs = Lang::find()->all();

        echo $this->render('mmenu', [
            'models' => $models,
            'portfolios' => $portfolios,
            'langs' => $langs,
        ]);
    }
}

