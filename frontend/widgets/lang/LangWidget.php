<?php
namespace frontend\widgets\lang;

use frontend\models\Lang;
use yii\bootstrap\Widget;

class LangWidget extends Widget
{
    public function init(){}

    public function run() {
        return $this->render('view', [
            'current' => Lang::getCurrent(),
            'langs' => Lang::find()->all(),
        ]);
    }
}