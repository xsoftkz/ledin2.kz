<?php

use yii\helpers\Html;
use frontend\models\Lang;

?>
<a class="nav-link dropdown-toggle"
   id="navbarDropdownMenuLink-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <?= Lang::getCurrent()->name ?>
</a>
<div class="dropdown-menu lang-menu" aria-labelledby="navbarDropdownMenuLink-4">
    <?php foreach ($langs as $lang): ?>
        <?= Html::a($lang->name, '/' . $lang->id . Yii::$app->getRequest()->getLangUrl(),
            [
                'class' => 'dropdown-item',
                'onclick' => "yaCounter47713843.reachGoal('ya_header_changelang_$lang->id'); return true;",
            ]) ?>
    <?php endforeach; ?>
</div>