<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'language' => 'ru-RU',
    'charset' => "utf-8",
    'sourceLanguage' => 'en-US',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'devicedetect', 'shop',
        'frontend\components\Bootstrap',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => [
        'shop' => [
            'basePath' => '@common/modules/shop',
            'class' => 'common\modules\shop\Module',
        ],
    ],
    'components' => [
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'forceTranslation' => true
                ],
            ],
        ],
        'seo' => [
            'class' => 'frontend\components\SeoComponent',
        ],
        'devicedetect' => [
            'class' => 'alexandernst\devicedetect\DeviceDetect'
        ],
        'compare' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'compare',
        ],
        'favorite' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'favorite',
        ],
        'cart' => [
            'class' => 'yz\shoppingcart\ShoppingCart',
            'cartId' => 'cart',
        ],
        'request' => [
            'class' => 'frontend\components\LangRequest',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'loginUrl' => ['/auth/login'],
            'identityClass' => 'common\modules\user\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class'=>'frontend\components\LangUrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'sitemap.xml' => 'site/sitemap',
                '/' => 'site/index',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                    'js' => [],
                ],
            ],
            'appendTimestamp' => true,
            'linkAssets' => true,
        ],
        'reCaptcha' => [
            'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
            'siteKeyV2' => \frontend\models\Feedback::RECAPTCHA_SITE_KEY,
            'secretV2' => \frontend\models\Feedback::RECAPTCHA_SECRET_KEY,
        ],		
		
		
    ],
    'params' => $params,
];
