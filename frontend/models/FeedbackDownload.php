<?php

namespace frontend\models;

class FeedbackDownload extends Feedback
{
    public function formName()
    {
        return 'fd';
    }
}