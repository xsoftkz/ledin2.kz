<?php

namespace frontend\models;

use yii\base\Model;
use Yii;

class Cart extends Model
{
    public static function getInfo(){
        $cart = Yii::$app->cart;
        $data['count'] = $cart->getCount();
        $data['positions']= $cart->getPositions();
        return $data;
    }


    //проверка товара в списке корзины
    public static function inCart($product_id){
        $product = ProductModel::findOne($product_id);
        if ($product && $product->cartPosition) {
            $position = \Yii::$app->cart->getPositionById($product->cartPosition->getId());
            return ($position) ? true : false;
        }
        return false;
    }


    public static function add($product_id){
        $cart = Yii::$app->cart;

        $product = ProductModel::findOne($product_id);

        if ($product && $product->cartPosition) {
            $position = $cart->getPositionById($product->cartPosition->getId());

            if (!$position){
                $cart->put($product->cartPosition, 1);
                return true;
            } else {
                $cart->remove($position);
                return false;
            }
        }
    }

    public static function update($product_id, $quantity){
        $cart = Yii::$app->cart;

        $product = ProductModel::findOne($product_id);

        if ($product && $product->cartPosition) {
            $position = $cart->getPositionById($product->cartPosition->getId());
            if ($position){
                $cart->update($product->cartPosition, $quantity);
            }
        }
    }

    public static function remove($product_id){
        $cart = Yii::$app->cart;
        $product = ProductModel::findOne($product_id);

        if ($product && $product->cartPosition) {
            $position = $cart->getPositionById($product->cartPosition->getId());
            if ($position){
                Yii::$app->cart->remove($position);
            }
        }
    }


}