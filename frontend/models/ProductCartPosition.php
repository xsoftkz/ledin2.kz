<?php
namespace frontend\models;

use yii\base\BaseObject;
use yii\base\Component;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CostCalculationEvent;

class ProductCartPosition extends BaseObject implements CartPositionInterface
{
    /**
     * @var Product
     */
    public $id;
    protected $_product;
    protected $_quantity;

    public function getId()
    {
        return md5(serialize(
            [
                $this->id,
            ]
        ));
    }

    public function getPrice()
    {
        return $this->getProduct()->getPrice();
    }

    public function getProduct()
    {
        if ($this->_product === null) {
            $this->_product = ProductModel::findOne($this->id);
        }
        return $this->_product;
    }

    public function getCost($withDiscount = true)
    {
        /** @var Component|CartPositionInterface|self $this */

        $cost = $this->getQuantity() * $this->getPrice();

        $costEvent = new CostCalculationEvent([
            'baseCost' => $cost,
        ]);
        if ($this instanceof Component)
            $this->trigger(CartPositionInterface::EVENT_COST_CALCULATION, $costEvent);
        if ($withDiscount)
            $cost = max(0, $cost - $costEvent->discountValue);
        return (int) $cost;
    }


    public function getQuantity()
    {
        return $this->_quantity;
    }

    public function setQuantity($quantity)
    {
        $this->_quantity = $quantity;
    }

    public function getTypeId(){
        return ($this->getProduct()) ? $this->getProduct()->type_id : 0;
    }


}