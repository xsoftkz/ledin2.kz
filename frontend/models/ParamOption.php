<?php

namespace frontend\models;

class ParamOption extends \common\modules\eav\models\ParamOption
{
    public function getValue(){
        switch (Lang::getCurrent()->id){
            case Lang::LANG_EN :
                $value = ($this->value_en) ?$this->value_en : $this->value_ru;
                break;
            case Lang::LANG_KZ :
                $value = ($this->value_kz) ?$this->value_kz : $this->value_ru;
                break;
            default:
                $value = $this->value_ru;
        }
        return $value;
    }

}