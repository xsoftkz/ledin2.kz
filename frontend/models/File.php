<?php

namespace frontend\models;

class File extends \common\models\File
{
    public function getTitle(){
        switch (Lang::getCurrent()->id){
            case Lang::LANG_EN :
                $title = ($this->title_en) ?$this->title_en : $this->title_ru;
                break;
            case Lang::LANG_KZ :
                $title = ($this->title_kz) ?$this->title_kz : $this->title_ru;
                break;
            default:
                $title = $this->title_ru;
        }
        return $title;
    }
}