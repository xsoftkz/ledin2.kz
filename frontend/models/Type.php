<?php
namespace frontend\models;


class Type extends \common\modules\eav\models\Type
{
    public function getParams()
    {
        return $this->hasMany(Param::className(), ['id' => 'param_id'])
            ->viaTable('{{%eav_type_param}}', ['type_id' => 'id'])->orderBy('type_id');
    }
}