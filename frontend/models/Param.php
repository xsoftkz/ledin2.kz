<?php

namespace frontend\models;

use common\modules\eav\models\lang\ParamLang;

/**
 * This is the model class for table "xs_shop_product_model".
 *
 * @property array $content
 */

class Param extends \common\modules\eav\models\Param
{
    public function getContent(){
        $content = ParamLang::find()->where(['param_id' => $this->id, 'lang_id' => Lang::getCurrent()->id])->one();
        if (!$content) {
            $content = ParamLang::find()->where(['param_id' => $this->id, 'lang_id' => Lang::getDefaultLang()->id])->one();
        }
        return $content;
    }

    public function getTitle(){
        return ($this->content) ? $this->content->title: '';
    }

    public function getOptions(){
        return $this->hasMany(ParamOption::className(), ['param_id' => 'id'])->orderBy('sort');
    }
}