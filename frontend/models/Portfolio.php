<?php

namespace frontend\models;


class Portfolio extends Content
{
    const SECTION = 'portfolio';

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('{{%shop_product_portfolio}}', ['portfolio_id' => 'id'])
            ->andWhere(['status_id' => 1]);
    }
}