<?php

namespace frontend\models;

use common\modules\feedback\models\Feedback;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class FeedbackForm extends Model
{
    const SIDE_TYPE_LEFT = 1;
    const SIDE_TYPE_RIGHT = 2;
    const SIDE_TYPE_CENTER = 3;

    const COLOR_WARM = 1;
    const COLOR_WHITE = 2;
    const COLOR_RGB = 3;

    public $name;
    public $phone;
    public $email;
    public $sides;
    public $colors;
    public $budget;
    public $utm;
    public $files;

    public $reCaptcha;

    public function rules()
    {
        return [
            [['name', 'phone', 'sides', 'colors', 'budget'], 'required'],
            [['name', 'phone', 'email', 'budget', 'utm'], 'string', 'max' => 255],
            [['files'], 'file', 'maxFiles' => 10],
            [['sides', 'colors'], 'safe'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className(),
                'secret' => \frontend\models\Feedback::RECAPTCHA_SECRET_KEY,
                'uncheckedMessage' => Yii::t('app', 'Please confirm that you are not a bot.')],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => t('You name'),
            'files' => t('Download sketch'),
            'phone' => t('You phone'),
            'email' => t('Email'),
            'sides' => t('Side of the building'),
            'colors' => t('Colour temperature'),
            'budget' => t('Budget'),
        ];
    }

    public static function getSideList(){
        return [
            self::SIDE_TYPE_LEFT => t('Left side'),
            self::SIDE_TYPE_RIGHT => t('Right side'),
            self::SIDE_TYPE_CENTER => t('Center facade'),
        ];
    }

    public static function getColorList(){
        return [
            self::COLOR_WARM => t('Warm color'),
            self::COLOR_WHITE => t('White color'),
            self::COLOR_RGB => t('RGB color'),
        ];
    }

    private function getMessage(){

        $msg = '';
        $sides = [];
        $colors = [];

        foreach ($this->sides as $item) {
            $sides[] = FeedbackForm::getSideList()[$item];
        }

        foreach ($this->colors as $color) {
            $colors[] = FeedbackForm::getColorList()[$color];
        }

        $msg .= t('Side of the building').': '. implode($sides, ',') .'<br>';
        $msg .= t('Colour temperature').': '. implode($colors, ',') .'<br>';
        $msg .= t('Budget').': '.$this->budget.'<br>';

        return $msg;
    }

    public function save(){
        $model = new Feedback();
        $model->type_id = Feedback::EXTEND_FEEDBACK;
        $model->username = $this->name;
        $model->phone = $this->phone;
        $model->email = $this->email;
        $model->utm = $this->utm;
        $model->comment = $this->getMessage();

        if ($model->save()){
            $this->uploadFiles($model->id);
            $model->sendEmailToAdmin();
            return $model;
        } else {
            return  false;
        }
    }

    public function uploadFiles($item_id)
    {
        $this->files = UploadedFile::getInstances($this, 'files');
        if ($this->files && $this->validate()) {
            foreach ($this->files as $fileItem) {
                $file = new File();
                $file->item_id = $item_id;
                $file->model_name = 'feedback';
                $file->folder = Feedback::FOLDER;
                $file->filename = uniqid() . '.' . $fileItem->extension;

                if ($file->save(false)){
                    $fileItem->saveAs(Yii::getAlias('@files/'.$file->folder . '/' . $file->filename));
                }
            }
        }
    }


}