<?php
namespace frontend\models;

use yii\base\Model;
use Yii;

class Compare extends Model
{
    public static function getInfo(){
        $compare = Yii::$app->compare;
        $data['count'] = $compare->getCount();
        $data['positions']= $compare->getPositions();
        return $data;
    }


    //проверка товара в списке избранных
    public static function isCompare($product_id){
        $product = ProductModel::findOne($product_id);
        if ($product && $product->cartPosition) {
            $position = \Yii::$app->compare->getPositionById($product->cartPosition->getId());
            return ($position) ? true : false;
        }
        return false;
    }


    //добавление в список сравнений
    public static function add($product_id){
        $compare = Yii::$app->compare;

        $product = ProductModel::findOne($product_id);

        if ($product && $product->cartPosition) {
            $position = $compare->getPositionById($product->cartPosition->getId());

            if (!$position){
                $compare->put($product->cartPosition, 1);
            } else {
                $compare->remove($position);
            }
        }
    }

    public static function getTypeId(){
        $type_id = null;
        $compare = Yii::$app->compare;
        if ($compare->getPositions()) {
            foreach ($compare->getPositions() as $item) {
                $type_id = $item->typeId;
                break;
            }
        }
        return $type_id;
    }


}