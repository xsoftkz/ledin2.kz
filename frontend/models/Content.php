<?php

namespace frontend\models;

use common\modules\content\models\lang\ContentLang;

class Content extends \common\modules\content\models\Content
{

    public function getContent()
    {
        $content = ContentLang::find()->where(['content_id' => $this->id, 'lang_id' => Lang::getCurrent()->id])->one();
        if (!$content) {
            $content = ContentLang::find()->where(['content_id' => $this->id, 'lang_id' => Lang::getDefaultLang()->id])->one();
        }
        return $content;
    }

    public function getCategory()
    {
        return $this->hasOne(ContentCategory::className(), ['id' => 'category_id']);
    }

    public function getTitle()
    {
        return ($this->content) ? $this->content->title : '';
    }

    public function getDesc()
    {
        return ($this->content) ? $this->content->desc : '';
    }

    public function getIntro()
    {
        return ($this->content) ? $this->content->intro : '';
    }

    public function getMetaTitle()
    {
        return ($this->content) ? $this->content->meta_title : '';
    }

    public function getSeoTitle(){
        if ($this->content && $this->content->seo_title) {
            return $this->content->seo_title;
        }
        return ( $this->getTitle().' || '.siteName() );
    }

    public function getMetaKeywords()
    {
        return ($this->content) ? $this->content->meta_keywords : '';
    }

    public function getMetaDescription()
    {
        return ($this->content) ? $this->content->meta_description : '';
    }

    public function getSearchUrl(){
        switch ($this->section){
            case self::SECTION_PAGE :
                $url = ['/page/view', 'slug' => $this->slug];
                break;
            case self::SECTION_PORTFOLIOS :
                $url = ['/portfolio/view', 'slug' => $this->slug];
                break;
            case self::SECTION_NEWS :
                $url = ['/news/view', 'slug' => $this->slug];
                break;
            case self::SECTION_VACANCY :
                $url = ['/vacancy/view', 'slug' => $this->slug];
                break;
            default:
                $url = ['#'];
        }
        return $url;
    }
}