<?php

namespace frontend\models;

use common\modules\text\models\Text;

class TextBlock extends Text
{
    public static function getValue($key) {
        $value = '';
        $model = self::findOne(['key' => $key]);

        if (!$model) {
            return $value;
        }

        switch (Lang::getCurrent()->id){
            case Lang::LANG_EN :
                $value = ($model->value_en) ? $model->value_en : $model->value_ru;
                break;
            case Lang::LANG_KZ :
                $value = ($model->value_kz) ? $model->value_kz : $model->value_ru;
                break;
            default:
                $value = $model->value_ru;
        }
        return $value;
    }
}