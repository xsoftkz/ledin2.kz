<?php

namespace frontend\models;

use common\modules\shop\models\lang\ProductLang;
use Yii;

class Product extends \common\modules\shop\models\Product
{

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getPortfolios()
    {
        return $this->hasMany(Content::className(), ['id' => 'portfolio_id'])
            ->viaTable('{{%shop_product_portfolio}}', ['product_id' => 'id']);
    }


    public function getProductFiles()
    {
        return $this->hasMany(File::className(), ['item_id' => 'id'])->where(['model_name' => self::MODEL_NAME]);
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getContent()
    {
        $content = ProductLang::find()->where(['product_id' => $this->id, 'lang_id' => Lang::getCurrent()->id])->one();
        if (!$content) {
            $content = ProductLang::find()->where(['product_id' => $this->id, 'lang_id' => Lang::getDefaultLang()->id])->one();
        }
        return $content;
    }

    public function getTitle()
    {
        return ($this->content) ? $this->content->title : '';
    }

    public function getSeoTitle(){
        if ($this->content && $this->content->seo_title) {
            return $this->content->seo_title;
        }
        $meta_title = '';
        $meta_title .= ($this->category) ? $this->category->getSingularTitle() : '';
        $meta_title .= ' '.$this->title;
        $meta_title .= ' в Нур-Султане (Астане) и Алматы.';
        return $meta_title;
    }

    public function getIntro()
    {
        return ($this->content) ? $this->content->intro : '';
    }

    public function getPriceLabel()
    {
        return ($this->content) ? $this->content->price_label : '';
    }

    public function getDesc()
    {
        return ($this->content) ? $this->content->desc : '';
    }

    public function getMetaKeywords()
    {
        return ($this->content) ? $this->content->meta_keywords : '';
    }

    public function getMetaDescription()
    {
        $meta_desc = 'Покупайте ';
        $meta_desc .= ($this->category) ? $this->category->getSingularTitle() : '';
        $meta_desc .= ' '.$this->title;
        $meta_desc .= ' с доставкой по Астане, Алматы и Казахстану. Выгодные цены! ☎ '.setting('site_phone');
        $meta_desc .= ' || '.companyName();
        return $meta_desc;
    }

    public function getCategoryName(){
        return ($this->category) ? $this->category->title : '';
    }

    public function getProductModels()
    {
        return $this->hasMany(ProductModel::className(), ['product_id' => 'id']);
    }


    public function getProductModelsParams()
    {
        $query = Param::find();
        $query->innerJoin('{{%eav_entity_param_value}} as entity_param', '{{%eav_param}}.id = entity_param.param_id');
        $query->innerJoin('{{%shop_product_model}} as product_model', 'entity_param.entity_id = product_model.id');
        $query->innerJoin('{{%shop_product}} as product', 'product_model.product_id = product.id');
        $query->andWhere(['product.id' => $this->id]);
        return $query->all();
    }

    public function getProductParamsValues(){
        $query = EntityParam::find();
        $query->innerJoin('{{%shop_product_model}} as product_model', '{{%eav_entity_param_value}}.entity_id = product_model.id');
        $query->innerJoin('{{%shop_product}} as product', 'product_model.product_id = product.id');
        $query->andWhere(['product.id' => $this->id]);
        return $query->all();
    }


    public function getProductInfoItems()
    {
        return $this->hasMany(ProductInfo::className(), ['product_id' => 'id'])->where(['type_id' => ProductInfo::TYPE_INFO]);
    }

    public function getProductVideoItems()
    {
        return $this->hasMany(ProductInfo::className(), ['product_id' => 'id'])->where(['type_id' => ProductInfo::TYPE_VIDEO]);
    }

    public function getAllProductImages(){
        return array_merge($this->allImages, $this->facadeImages);
    }
}