<?php
namespace frontend\models;

use frontend\models\ProductModel;
use yii\base\Model;
use Yii;

class Favorite extends Model
{
    public static function getInfo(){
        $favorite = Yii::$app->favorite;
        $data['count'] = $favorite->getCount();
        $data['positions']= $favorite->getPositions();
        return $data;
    }


    //проверка товара в списке избранных
    public static function isFavorite($product_id){
        $product = ProductModel::findOne($product_id);
        if ($product && $product->cartPosition) {
            $position = \Yii::$app->favorite->getPositionById($product->cartPosition->getId());

            return ($position) ? true : false;
        }
        return false;
    }


    public static function add($product_id){
        $favorite = Yii::$app->favorite;

        $product = ProductModel::findOne($product_id);

        if ($product && $product->cartPosition) {
            $position = $favorite->getPositionById($product->cartPosition->getId());

            if (!$position){
                $favorite->put($product->cartPosition, 1);
            } else {
                $favorite->remove($position);
            }
        }
    }


}