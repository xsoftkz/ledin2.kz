<?php

namespace frontend\models;

use common\modules\shop\models\lang\ProductInfoLang;

class ProductInfo extends \common\modules\shop\models\ProductInfo
{
    public function getContent()
    {
        $content = ProductInfoLang::find()->where(['item_id' => $this->id, 'lang_id' => Lang::getCurrent()->id])->one();
        if (!$content) {
            $content = ProductInfoLang::find()->where(['item_id' => $this->id, 'lang_id' => Lang::getDefaultLang()->id])->one();
        }
        return $content;
    }

    public function getTitle()
    {
        return ($this->content) ? $this->content->title : '';
    }

    public function getDesc()
    {
        return ($this->content) ? $this->content->desc : '';
    }

    public function getVideoId(){
        return self::getYouTubeIdFromURL($this->title);
    }

    private static function getYouTubeIdFromURL($url)
    {
        $url_string = parse_url($url, PHP_URL_QUERY);
        parse_str($url_string, $args);
        return isset($args['v']) ? $args['v'] : false;
    }
}