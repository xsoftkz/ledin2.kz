<?php

namespace frontend\models;

class SearchService
{
    const SECTION_CATALOG = 'catalog';
    const SECTION_PAGE = 'page';


    public function search($query, $section)
    {
        $key = strip_tags($query);

        $data = [];

        switch ($section){
            case self::SECTION_CATALOG:
                $data = array_merge(
                    $this->findProductCategories($key),
                    $this->findProductModels($key) );
                break;
            case self::SECTION_PAGE:
                $data = $this->findContents($key);
                break;
        }

        return $data;
    }

    private function findProductModels($key)
    {
        $query = ProductModel::find();
        $query->innerJoin('{{%shop_product}} as product', 'product.id = {{%shop_product_model}}.product_id');
        $query->innerJoin('{{%shop_product_lang}} as product_lang', 'product_lang.product_id = product.id');
        $query->where(['product_lang.lang_id' => Lang::getCurrent()->id]);
        $query->filterWhere(
            ['or',
                ['like', 'product_lang.title', $key],
                ['like', 'product.model_name', $key],
                ['like', '{{%shop_product_model}}.name', $key]
            ]
        );
        $query->limit(30);

        $data = [];

        $products = $query->all();
        foreach ($products as $model)
        {
            if ( $model->product) {
                $data[] = [
                    'title' => $model->getSearchTitle(),
                    'img' => thumb($model->product->imagePath, 100, 100),
                    'sub_title' => $model->getSearchSubTitle(),
                    'url' => url(['catalog/view', 'slug' => $model->product->slug]),
                ];
            }

        }

        return $data;
    }

    private function findProductCategories($key)
    {
        $query = Category::find();
        $query->innerJoin('{{%shop_category_lang}} as category_lang', 'category_lang.category_id = {{%shop_category}}.id');
        $query->where(['category_lang.lang_id' => Lang::getCurrent()->id]);
        $query->filterWhere(['like', 'category_lang.title', $key]);
        $query->limit(30);

        $data = [];

        $models = $query->all();
        foreach ($models as $model)
        {
            $data[] = [
                'title' => $model->getTitle(),
                'img' => thumb($model->imagePath, 100, 100),
                'sub_title' => t('Catalog'),
                'url' => url(['catalog/index', 'slug' => $model->slug]),
            ];
        }

        return $data;
    }

    private function findContents($key)
    {
        $query = Content::find();
        $query->innerJoin('{{%content_lang}} as content_lang', 'content_lang.content_id = {{%content}}.id');
        $query->where(['content_lang.lang_id' => Lang::getCurrent()->id]);
        $query->filterWhere(['like', 'content_lang.title', $key]);
        $query->andWhere(['{{%content}}.section' => [Content::SECTION_PAGE, Content::SECTION_VACANCY, Content::SECTION_NEWS, Content::SECTION_PORTFOLIOS]]);
        $query->limit(30);

        $data = [];

        $models = $query->all();
        foreach ($models as $model)
        {
            $data[] = [
                'title' => $model->getTitle(),
                'img' => thumb($model->imagePath, 100, 100),
                'sub_title' => t($model->section),
                'url' => url($model->searchUrl),
            ];
        }

        return $data;
    }
}