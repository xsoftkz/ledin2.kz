<?php

namespace frontend\models;

use yii\helpers\ArrayHelper;

class EntityParam extends \common\modules\eav\models\EntityParam
{
    public function getParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'param_id']);
    }

    public function getParamOptions()
    {
        return $this->hasMany(ParamOption::className(), ['id' => 'value_ru']);
    }

    public function getParamOption()
    {
        return $this->hasOne(ParamOption::className(), ['id' => 'value_ru']);
    }

    public function getValueText()
    {
        if ($this->param) {
            switch ($this->param->type_id) {
                case Param::TYPE_CHECKBOXLIST:
                    if ($this->paramOptions) {
                        if ($this->param->optionTypeIsFile()){
                            return $this->paramOptions ? ArrayHelper::getColumn($this->paramOptions, 'image') : [];
                        } else {
                            return implode(', ', $this->paramOptions ? ArrayHelper::getColumn($this->paramOptions, 'value') : '');
                        }
                    }
                    break;
                case Param::TYPE_SELECT:
                    return $this->paramOption ? $this->paramOption->value : '';
                    break;
                case Param::TYPE_CHECKBOX:
                    return ($this->value) ? t('Yes') : t('No');
                    break;
                case Param::TYPE_STRING:
                    return $this->value;
                    break;
                case Param::TYPE_TEXT:
                    return $this->value;
                    break;
            }
        }
    }

    public static function getValueField(){
        switch (Lang::getCurrent()->id){
            case Lang::LANG_EN :
                $value = 'value_en';
                break;
            case Lang::LANG_KZ :
                $value = 'value_kz';
                break;
            default:
                $value = 'value_ru';
        }
        return $value;
    }

}