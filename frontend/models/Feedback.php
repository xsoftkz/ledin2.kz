<?php

namespace frontend\models;


class Feedback extends \common\modules\feedback\models\Feedback
{
    const RECAPTCHA_SITE_KEY = '6LfbFN4ZAAAAAAkt-BRPLCWGfHmbU623evTgrBRM';
    const RECAPTCHA_SECRET_KEY = '6LfbFN4ZAAAAAK8S2kwTB6rRoYDMoKYdL9WX39n7';

    public $reCaptcha;

    public function rules()
    {
        return [
            [['type_id', 'username', 'phone', 'email'], 'required'],
            [['type_id', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'is_mailing'], 'integer'],
            [['comment'], 'string', 'max' => 1000 ],
            [['username', 'phone', 'email', 'subject', 'company', 'filename', 'utm'], 'string', 'max' => 255],
            [['link'], 'string'],
            [['file'], 'file'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className(),
                'secret' => self::RECAPTCHA_SECRET_KEY,
                'uncheckedMessage' => \Yii::t('app', 'Please confirm that you are not a bot.')],
        ];
    }


}