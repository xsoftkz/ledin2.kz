<?php

namespace frontend\models;


use common\modules\content\models\lang\CategoryLang;

class ContentCategory extends \common\modules\content\models\Category
{
    public function getContent(){
        $content = CategoryLang::find()->where(['category_id' => $this->id, 'lang_id' => Lang::getCurrent()->id])->one();
        if (!$content) {
            $content = CategoryLang::find()->where(['category_id' => $this->id, 'lang_id' => Lang::getDefaultLang()->id])->one();
        }
        return $content;
    }

    public function getTitle(){
        return ($this->content) ? $this->content->title: '';
    }

    public function getDesc(){
        return ($this->content) ? $this->content->desc: '';
    }

    public function getMetaTitle(){
        return ($this->content) ? $this->content->meta_title: '';
    }

    public function getSeoTitle(){
        if ($this->content && $this->content->seo_title) {
            return $this->content->seo_title;
        }
        return ( $this->getMetaTitle().' || '.siteName() );
    }

    public function getMetaKeywords(){
        return ($this->content) ? $this->content->meta_keywords: '';
    }

    public function getMetaDescription(){
        return ($this->content) ? $this->content->meta_description: '';
    }
}