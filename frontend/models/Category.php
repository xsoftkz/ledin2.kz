<?php

namespace frontend\models;

use common\models\traits\ImageTrait;
use common\modules\content\models\lang\ContentLang;
use common\modules\shop\models\lang\CategoryLang;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "xs_shop_product_model".
 *
 * @property ContentLang $content
 */

class Category extends \common\modules\shop\models\Category
{
    use ImageTrait;

    public function getContent(){
        $content = CategoryLang::find()->where(['category_id' => $this->id, 'lang_id' => Lang::getCurrent()->id])->one();
        if (!$content) {
            $content = CategoryLang::find()->where(['category_id' => $this->id, 'lang_id' => Lang::getDefaultLang()->id])->one();
        }
        return $content;
    }

    public function getTitle(){
        return ($this->content) ? $this->content->title: '';
    }

    public function getSeoTitle(){
        if ($this->content && $this->content->seo_title) {
            return $this->content->seo_title;
        }
        return ( $this->getMetaTitle().' || '.siteName() );
    }

    public function getSingularTitle(){
        return ($this->content) ? $this->content->singular_title: '';
    }

    public function getDesc(){
        return ($this->content) ? $this->content->desc: '';
    }

    public function getMetaTitle(){
        return ($this->content) ? $this->content->meta_title: '';
    }

    public function getMetaKeywords(){
        return ($this->content) ? $this->content->meta_keywords: '';
    }

    public function getMetaDescription(){
        return ($this->content) ? $this->content->meta_description: '';
    }

    public function getLevel()
    {
        $level = 1;
        $parent = $this->parent;

        while ($parent) {
            $level++;
            $parent = $parent->parent;
        }
        return $level;
    }

    public static function getGroupName($index)
    {
        $groups = [
            1 => t('Group type'),
            2 => t('Group color'),
            3 => t('Group power'),
            4 => t('Group popular brands'),
            5 => t('Group country'),
        ];

        return ArrayHelper::getValue($groups, $index);
    }
}