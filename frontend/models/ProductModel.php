<?php

namespace frontend\models;

use yz\shoppingcart\CartPositionProviderInterface;
use Yii;

/**
 * This is the model class for table "xs_shop_product_model".
 *
 * @property array $paramsValues
 */

class ProductModel extends \common\modules\shop\models\ProductModel implements CartPositionProviderInterface
{
    public function getCartPosition($params = [])
    {
        return Yii::createObject([
            'class' => 'frontend\models\ProductCartPosition',
            'id' => $this->id,
        ]);
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getParamsValues()
    {
        return $this->hasMany(EntityParam::className(), ['entity_id' => 'id']);
    }

    public function isFavorite(){
        return Favorite::isFavorite($this->id);
    }

    public function isCompare(){
        return Compare::isCompare($this->id);
    }

    public function inCart(){
        return Cart::inCart($this->id);
    }

    public function getSearchTitle()
    {
        $title = '';
        if ($this->product) {
            $title .= $this->product->title.' / '.$this->name;
        }
        return $title;
    }

    public function getSearchSubTitle()
    {
        $title = '';
        if ($this->product) {
            if ($this->product->category) {
                $title = $this->product->category->getTitle();
            }
        }
        return $title;
    }
}