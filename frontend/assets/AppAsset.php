<?php
namespace frontend\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/lightslider.min.css',
        'css/jquery.fancybox.min.css',
        'css/menu.css',
        'css/styles.css',
        'css/media.css',
    ];
    public $js = [
        'js/lightslider.min.js',
        'js/jquery.fancybox.min.js',
        'js/vue.min.js',
        'js/scripts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'frontend\assets\BootstrapAsset',
    ];

}
