<?php

namespace frontend\services;

use frontend\models\Category;
use frontend\models\Content;
use frontend\models\ContentCategory;
use frontend\models\Lang;
use frontend\models\Portfolio;
use frontend\models\Product;
use function React\Promise\all;

class SitemapService
{
    public static function generate()
    {
        return (new self())->getUrls();
    }

    private function getUrls()
    {
        $urls = [];

        $currentLang = Lang::getCurrent();

        $langs = Lang::find()->all();

        foreach ($langs as $lang) {
            Lang::setCurrent($lang->id);

            // категория товаров
            $prodCats = Category::find()->published()->all();
            foreach ($prodCats as $pCat) {
                $urls[] = [
                    'loc' =>  url(['/catalog/index', 'slug' => $pCat->slug]),
                    'lastmod' => date( DATE_W3C, $pCat->updated_at ),
                    'priority' => 1.0
                ];
            }

            // товары
            $products = Product::find()->published()->all();
            foreach ($products as $prod) {
                $urls[] = [
                    'loc' =>  url(['/catalog/view', 'slug' => $prod->slug]),
                    'lastmod' => date( DATE_W3C, $prod->updated_at ),
                    'priority' => 1.0
                ];
            }

            // портфолио
            $portfolioCats = ContentCategory::find()->withSection(Portfolio::SECTION)->published()->all();
            foreach ($portfolioCats as $prCat) {
                $urls[] = [
                    'loc' =>  url(['/portfolio/index', 'slug' => $prCat->slug]),
                    'lastmod' => date( DATE_W3C, $prCat->updated_at ),
                    'priority' => 1.0
                ];
            }
            $portfolios = Portfolio::find()->withSection(Portfolio::SECTION)->published()->all();
            foreach ($portfolios as $portfolio) {
                $urls[] = [
                    'loc' =>  url(['/portfolio/view', 'slug' => $portfolio->slug]),
                    'lastmod' => date( DATE_W3C, $portfolio->updated_at ),
                    'priority' => 1.0
                ];
            }

            // услуги
            $services = Content::find()->withSection(Content::SECTION_SERVICE)->published()->all();
            foreach ($services as $service) {
                $urls[] = [
                    'loc' =>  url(['/services/view', 'slug' => $service->slug]),
                    'lastmod' => date( DATE_W3C, $service->updated_at ),
                    'priority' => 1.0
                ];
            }

            // вакансия
            $vacancies = Content::find()->withSection(Content::SECTION_VACANCY)->published()->all();
            foreach ($vacancies as $vacancy) {
                $urls[] = [
                    'loc' =>  url(['/vacancy/view', 'slug' => $vacancy->slug]),
                    'lastmod' => date( DATE_W3C, $vacancy->updated_at ),
                    'priority' => 1.0
                ];
            }

            // новости
            $news = Content::find()->withSection(Content::SECTION_NEWS)->published()->all();
            foreach ($news as $newsItem) {
                $urls[] = [
                    'loc' =>  url(['/news/view', 'slug' => $newsItem->slug]),
                    'lastmod' => date( DATE_W3C, $newsItem->updated_at ),
                    'priority' => 1.0
                ];
            }

            // блог
            $articles = Content::find()->withSection(Content::SECTION_ARTICLE)->published()->all();
            foreach ($articles as $article) {
                $urls[] = [
                    'loc' =>  url(['/article/view', 'slug' => $article->slug]),
                    'lastmod' => date( DATE_W3C, $article->updated_at ),
                    'priority' => 1.0
                ];
            }

            foreach ($this->getStaticPages() as $page) {
                $urls[] = [
                    'loc' =>  url(['/'.$page]),
                    'lastmod' => date( DATE_W3C, time() ),
                    'priority' => 1.0
                ];
            }
        }

        Lang::setCurrent($currentLang->id);

        return $urls;
    }

    private function getStaticPages()
    {
        return [
          'about', 'portfolio', 'services', 'vacancy', 'contacts', 'article', 'news'
        ];
    }
}