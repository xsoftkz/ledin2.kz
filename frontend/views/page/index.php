<?php
use yii\widgets\ListView;

$this->title = t('Pages'). siteName();

$this->params['breadcrumbs'][] = t('Pages');
$this->registerMetaTag(['name' => 'description', 'content' => '']);

?>
<div class="container">
    <h1 class="page-title"><?= t('Pages') ?></h1>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'layout' => "{items}\n{pager}",
    ]) ?>
</div>