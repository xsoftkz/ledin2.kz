<?php
?>
<?php if ($productModels) { ?>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="nav-prod-models__title">
                        <strong class="p-2 ">#<?= t('Code') ?></strong>
                    </div>
                    <div class="nav-prod-models__wrap">
                        <ul class="nav nav-prod-models nav-pills p-2" id="product-models-tab" role="tablist">
                            <?php $i = 0; foreach ($productModels as $productModel) { $i++; ?>
                                <li class="nav-item">
                                    <a class="nav-link <?= $i == 1 ? 'active' : '' ?>"
                                       id="pills-<?= $productModel->id ?>-tab"
                                       data-toggle="pill"
                                       href="#md-<?= $productModel->name ?>"
                                       role="tab"
                                       aria-controls="pills-<?= $productModel->id ?>"
                                       aria-selected="<?= $i == 1 ? 'true' : '' ?>"
                                    >
                                        <?= $productModel->name ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-9">
                    <div class="tab-content" id="product-models-content">
                        <?php $i = 0; foreach ($productModels as $productModel) { $i++; ?>
                            <div
                                    class="tab-pane fade <?= $i == 1 ? 'show active' : '' ?>"
                                    id="md-<?= $productModel->name ?>"
                                    role="tabpanel"
                                    aria-labelledby="pills-<?= $productModel->id ?>-tab">

                                <div class="row align-items-center mb-2">
                                    <div class="col-lg-12">
                                        <h6 class="product-model__title text-center font-weight-bold">
                                            <?= t('Product code') ?>: <?= h($model->model_name) ?>
                                        </h6>
                                    </div>
                                    <div class="col-lg-12">
                                        <nav class="pr-model__actions">
                                                <a href="javascript:void(0);" data-id="<?= $productModel->id ?>" class="js-cart-add <?= $productModel->inCart() ? 'active' : '' ?>">
                                                    <span class="action__icon cart__icon"></span>
                                                    <span><?= t('Add to cart')?></span>
                                                </a>
                                                <a href="javascript:void(0);" data-id="<?= $productModel->id ?>" class="js-favorite-add <?= $productModel->isFavorite() ? 'active' : '' ?>">
                                                    <span class="action__icon favorite__icon"></span>
                                                    <span><?= t('Add to favorites')?></span>
                                                </a>
                                                <a href="javascript:void(0);" data-id="<?= $productModel->id ?>" class="js-compare-add <?= $productModel->isCompare() ? 'active' : '' ?>">
                                                    <span class="action__icon compare__icon"></span>
                                                    <span><?= t('Compare')?></span>
                                                </a>
                                        </nav>
                                    </div>
                                </div>

                                <table class="table table-bordered table-striped">
                                    <?php if ($productModel->paramsValues) {
                                        foreach ($productModel->paramsValues as $paramValue) {
                                            if ($paramValue->valueText){ ?>
                                                <tr>
                                                    <td><?= ($paramValue->param) ? $paramValue->param->title : '-' ?></td>
                                                    <td>
                                                        <?php
                                                            if($paramValue->param->optionTypeIsFile()) {
                                                                foreach ($paramValue->valueText as $value){ ?>
                                                                    <img src="<?= $value ?>" alt="" width="30px">
                                                                <?php }
                                                            } else {
                                                                echo $paramValue->valueText;
                                                            }
                                                        ?>
                                                    </td>
                                                </tr>
                                        <?php }
                                        }
                                    } ?>
                                </table>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="text-center">
                        <a href="#"  data-fancybox data-src="#callback-modal" href="javascript:;" class="btn btn-outline-main">
                            <?= t('Send consultation order') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
