<?php
use yii\helpers\Url;

$this->title = $model->getSeoTitle();

$this->registerMetaTag(['name' => 'description', 'content' => $model->getMetaDescription()]);
$this->registerMetaTag(['name' => 'keywords', 'content' => $model->getMetaKeywords()]);

$this->params['breadcrumbs'][] = ['label' => t('Catalog'), 'url' => ['/catalog/index']];
if ($model->category){
    $this->params['breadcrumbs'][] = ['label' => $model->category->title, 'url' => ['/catalog/index', 'slug' => $model->category->slug]];
}
$this->params['breadcrumbs'][] = $model->title;

\Yii::$app->seo->putFacebookMetaTags([
    'og:url'        => Url::canonical(),
    'og:type'       => 'website',
    'og:title'      => $model->title,
    'og:description' => strip_tags(substr($model->desc, 0, 200)),
    'og:image'      => $model->image,
]);

\Yii::$app->seo->putTwitterMetaTags([
    'twitter:site'        => Url::canonical(),
    'twitter:title'       => $model->title,
    'twitter:description' => strip_tags(substr($model->desc, 0, 200)),
    'twitter:image:src'   => $model->image,
    'twitter:card'=> 'summary',
]);
?>

