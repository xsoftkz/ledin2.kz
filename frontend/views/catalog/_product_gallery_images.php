<?php
?>

<?php if ($images) { ?>
    <div id="<?= $slider_id ?>" class="product-gallery">
        <?php foreach ($images as $item) { ?>
            <a href="<?= $item->wmImage ?>" data-fancybox="images" data-caption="<?= $model->title ?>" class="product-gallery__item"
               data-thumb="<?= thumb($item->imagePath, 100, 100, true) ?>" data-src="<?= $item->wmImage ?>">
                <img src="<?= thumb($item->wmImagePath, 600) ?>" alt="<?= $model->categoryName.' '.$model->title ?>" />
            </a>
        <?php } ?>
    </div>
<?php } ?>

