<?php
/**
 * @var $model \frontend\models\Product
 */
?>

<a href="<?= url(['catalog/view', 'slug' => $model->slug]) ?>" class="pr-item shadow-cs h-100">
    <span class="pr-item__img <?= ($model->icon) ? 'pr-item__img-front' : '' ?> ">
        <img src="<?= thumb($model->imagePath, 400)  ?>" alt="<?= $model->categoryName.' '.$model->title ?>">
    </span>
    <?php if ($model->icon) { ?>
        <span class="pr-item__img pr-item__img-back">
            <img src="<?= thumb($model->iconImagePath, 400)  ?>" alt="<?= $model->categoryName.' '.$model->title ?>">
        </span>
    <?php } ?>
    <span class="pr-item__title text-accent text-center">
        <?= $model->title ?>
    </span>
    <?php if ($model->getIntro() ): ?>
        <span class="d-block pr-item__intro mb-2"><?= $model->getIntro() ?> </span>
    <?php endif; ?>
    <?php if ($model->getPriceLabel() ): ?>
        <span class="d-block pr-item__price"> <?= $model->getPriceLabel() ?> </span>
    <?php endif; ?>
</a>
