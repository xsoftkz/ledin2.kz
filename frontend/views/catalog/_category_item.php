<?php

?>

<a href="<?= url(['/catalog/index', 'slug' => $model->slug]) ?>" class="pr-item shadow-cs h-100">
    <span class="pr-item__img">
        <img src="<?= thumb($model->imagePath, 400)  ?>" alt="">
    </span>
    <span class="pr-item__title">
        <?= $model->title ?>
    </span>
</a>
