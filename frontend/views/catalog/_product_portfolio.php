<?php
?>

<?php if ($model->portfolios){ ?>
    <h4 class="text-accent text-center"><?= t('Installed on objects') ?></h4>

    <div class="product-portfolio">
        <?php foreach ($model->portfolios as $item){ ?>
            <a href="<?= url(['/portfolio/view', 'slug' => $item->slug]) ?>" class="pr-portfolio-item d-block">
            <span class="pr-portfolio-item__img">
                <img src="<?= thumb($item->imagePath, 600, 400, true) ?>" alt="" class="hoverable">
            </span>
            <span class="pr-portfolio-item__title text-accent">
                <?= h($item->title) ?>
            </span>
            </a>
        <?php } ?>
    </div>
<?php } ?>

