<?php
use yii\widgets\ListView;

$this->render('_head_index', ['category' => $category]);

?>

<div class="container">
    <div class="row align-items-center">
        <div class="col-lg-10 col-10">
            <h1 class="page-title">
                <?= ($category) ? $category->title : t('Catalog') ?>
            </h1>
        </div>
        <div class="col-lg-2 col-2 text-right">
            <a href="<?= url(['/catalog/filter', 'slug' => $category->slug]) ?>" class="filter__link">
                <img src="/svg/controls.svg" alt="">
                <span><?= t('Filter') ?></span>
            </a>
        </div>
    </div>

    <hr>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_category_item',
        'options' => [
            'tag' => 'div',
            'class' => 'product-list'
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'pr-item-col',
        ],
        'pager' => [
            'maxButtonCount' => 5,
        ],
        'layout' => "{items}\n<div class='clearfix'></div>{pager}",
    ]) ?>

	<?php if($category && $category->desc){ ?>
        <br>
        <div class="category-desc">
            <?= $category->desc ?>
        </div>
        <br>
    <?php } ?>

</div>





