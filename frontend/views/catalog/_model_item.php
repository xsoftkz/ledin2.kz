<?php
$product = $model->product;

if ($product) { ?>
    <a href="<?= url(['/catalog/view', 'slug' => $product->slug, '#' => 'md-'.$model->name]) ?>" class="pr-item shadow-cs">
        <span class="pr-item__img">
            <img src="<?= thumb($product->imagePath, 300) ?>" alt="">
        </span>
        <span class="pr-item__title text-accent">
            <small class="text-secondary"><?= t('Product') ?>: </small><?= $product->title ?>
        </span>
        <span class="pr-item__title text-accent">
            <small class="text-secondary"><?= t('Model') ?>: </small><?= $model->name ?>
        </span>
    </a>
<?php } ?>


