<?php
?>

<?php if ($models) { ?>
    <?php foreach ($models as $model) { ?>
        <div class="pr-view__video">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?= $model->videoId ?>" frameborder="0"
                    allowfullscreen>
            </iframe>
        </div>
    <?php } ?>
<?php } ?>
