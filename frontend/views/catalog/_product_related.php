<?php if ($relatedProducts) {
    foreach ($relatedProducts as $prod) { ?>
        <a href="<?= url(['/catalog/view', 'slug' => $prod->slug]) ?>" class="pr-slide-item">
            <span class="pr-slide-item__img">
                <img src="<?= thumb($prod->imagePath, 200) ?>" alt="">
            </span>
            <span class="pr-slide-item__title">
                <?= $prod->title ?>
            </span>
        </a>
    <?php }
} ?>