<?php

use frontend\widgets\catalog\ParamFilterFormWidget;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);
$this->title = t('Filter').' '.$category->title.' || '.siteName();
$this->params['breadcrumbs'][] = $category->title;

?>

<div class="container">
    <div class="row align-items-center mb-4">
        <div class="col-lg-10 col-10">
            <h1 class="page-title">
                <?= ($category) ? $category->title : t('Catalog') ?>
            </h1>
        </div>
        <div class="col-lg-2 col-2 text-right d-block d-lg-none ">
            <a class="filter__link" data-toggle="collapse" href="#filterCollapse" role="button" aria-expanded="false" aria-controls="collapseExample">
                <img src="/svg/controls.svg" alt="">
                <span><?= t('Filter') ?></span>
            </a>
        </div>
    </div>

    <div class="filter-wrap">
        <div class="filter-left">
            <?= ParamFilterFormWidget::widget(['formData' => $formData]) ?>
        </div>
        <div class="filter-right">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_model_item',
                'options' => [
                    'tag' => 'div',
                    'class' => 'product-list'
                ],
                'itemOptions' => [
                    'tag' => 'div',
                    'class' => 'pr-item-col col-count-4',
                ],
                'pager' => [
                    'maxButtonCount' => 5,
                ],
                'layout' => "{items}\n<div class='clearfix'></div>{pager}",
            ]) ?>
        </div>
    </div>

</div>





