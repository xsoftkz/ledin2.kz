<?php
use frontend\models\Category;
use yii\helpers\Url;

/** @var Category $category */

$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);

$this->title = $category->getSeoTitle();

$title_h1 = $category->metaTitle;
$description = $category->desc;


if ($category && $category->parent) {
    $this->params['breadcrumbs'][] = ['label' => $category->parent->metaTitle,
        'url' => ['/catalog/index', 'slug' => $category->parent->slug]];
}


if ($category) {
    \Yii::$app->seo->putFacebookMetaTags([
        'og:url'        => Url::canonical(),
        'og:type'       => 'website',
        'og:title'      => $category->title,
        'og:description'=> substr($category->desc, 0, 200),
        'og:image'      => $category->image,
    ]);

    \Yii::$app->seo->putTwitterMetaTags([
        'twitter:site'        => Url::canonical(),
        'twitter:title'       => $category->title,
        'twitter:description' => substr($category->desc, 0, 200),
        'twitter:image:src'   => $category->image,
        'twitter:card'=> 'summary',
    ]);


    $this->params['breadcrumbs'][] = $category->getMetaTitle();

    $this->registerMetaTag(['name' => 'description', 'content' => $category->getMetaDescription()]);
    $this->registerMetaTag(['name' => 'keywords', 'content' => $category->getMetaKeywords()]);
}

?>