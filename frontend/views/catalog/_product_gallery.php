<?php
$facadeImages = $model->facadeImages;
$productImages = $model->allImages;
?>


<?php if (\Yii::$app->devicedetect->isMobile() || \Yii::$app->devicedetect->isTablet()) { ?>

    <ul class="pr-gallery-tab-nav nav nav-pills mb-3 justify-content-center" id="photos-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="photos-prod-tab" data-toggle="pill" href="#photos-prod" role="tab" aria-controls="photos-prod" aria-selected="true">
                <?= t('Product images') ?>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="photos-facade-tab" data-toggle="pill" href="#photos-facade" role="tab" aria-controls="photos-facade" aria-selected="false">
                <?= t('Facade images') ?>
            </a>
        </li>

    </ul>
    <div class="tab-content" id="photos-tabContent">
        <div class="tab-pane fade show active" id="photos-prod" role="tabpanel" aria-labelledby="photos-prod-tab">
            <div class="pr-view__img shadow-cs">
                <?= $this->render('_product_gallery_images', [
                    'images' => $productImages,
                    'model' => $model,
                    'slider_id' => '#product-images',
                ]) ?>
            </div>
        </div>
        <div class="tab-pane fade" id="photos-facade" role="tabpanel" aria-labelledby="photos-facade-tab">
            <div class="pr-view__img shadow-cs">
                <?= $this->render('_product_gallery_images', [
                    'images' => $facadeImages,
                    'model' => $model,
                    'slider_id' => '#facade-images',
                ]) ?>
            </div>

        </div>
    </div>

<?php } else { ?>

    <div class="row">
        <div class="col-lg-6">
            <div class="text-accent text-center pb-3"><?= t('Product images') ?></div>
            <div class="pr-view__img gallery-products shadow-cs">
                <?= $this->render('_product_gallery_images', [
                    'images' => $model->allImages,
                    'model' => $model,
                    'slider_id' => '#product-images',
                ]) ?>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="text-accent text-center pb-3"><?= t('Facade images') ?></div>
            <div class="pr-view__img gallery-facades shadow-cs">
                <?= $this->render('_product_gallery_images', [
                    'images' => $model->facadeImages,
                    'model' => $model,
                    'slider_id' => '#facade-images',
                ]) ?>
            </div>
        </div>
    </div>

<?php } ?>

