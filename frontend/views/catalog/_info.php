<?php
use frontend\widgets\eav\EntityParamsWidget;
?>

<div class="pr-fv-container">
    <div class="pr-fv__header"><?= $model->meta_title ?></div>

    <div class="pr-fv-content">
        <div class="row">
            <div class="col-lg-4">
                <div class="pr-fv__img text-center">
                    <img src="<?= $model->preview ?>" alt="<?= $model->title ?>" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-4">
                <div class="pr-fv-params">
                    <div class="pb-2">Характеристики</div>
                    <?= EntityParamsWidget::widget(['models' => $model->mainParamsMap]) ?>
                </div>
                <?php if ($model->brand) { ?>
                    <div class="pr-fv__brand">
                        <img src="<?= $model->brand->preview ?>" alt="<?= $model->brand->title ?>" title="<?= $model->brand->title ?>">
                    </div>
                <?php } ?>

            </div>
            <div class="col-lg-4">
                <div class="pr-fv__price">
                    <span class="price-value"><?= $model->price ?> </span>
                    <span class="price-currency">тнг.</span>
                </div>
                <div class="pr-fv__cart">
                    <?= $this->render('_cart_button', ['model' => $model]) ?>
                </div>
                <div class="pr-fv__likes">
                    <a href="javascript:void(0);" data-id="<?= $model->id ?>" class="like-item js-favorite-add <?= $model->isFavorite() ? 'active' : '' ?>">
                        <i class="<?= $model->isFavorite() ? 'fa fa-heart' : 'fa fa-heart-o' ?>" aria-hidden="true"></i> В закладки
                    </a>
                    <a href="javascript:void(0);" data-id="<?= $model->id ?>" class="like-item js-compare-add <?= $model->isCompare() ? 'active' : '' ?>">
                        <i class="fa fa-bar-chart" aria-hidden="true"></i> Сравнить
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>



