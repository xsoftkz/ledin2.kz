<?php

$this->render('_head_view', ['model' => $model]);

?>

<div class="pr-view">
    <div class="container">

        <div class="row align-items-center">
            <div class="col-lg-6 col-8">
                <h1><?= h($model->title) ?></h1>
            </div>
            <div class="col-lg-6 col-4">
                <div class="product-actions d-flex align-items-center justify-content-end">
                    <div class="product-action__item">
                        <?= $this->render('_product_files', [
                            'model' => $model,
                        ]) ?>
                    </div>
                </div>

            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <div class="pr-view__img gallery-products shadow-cs">
                    <?= $this->render('_product_gallery_images', [
                        'images' => $model->allProductImages,
                        'model' => $model,
                        'slider_id' => '#product-images',
                    ]) ?>
                </div>
            </div>
            <div class="col-lg-6">
                <?= $this->render('_product_models', [
                    'model' => $model,
                    'productModels' => $productModels,
                ]) ?>
            </div>
        </div>

        <div class="product-view__content">
            <div class="bgc-accent mb-3">
                <ul class="nav-product-params nav nav-tabs font-primary" id="productTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="desc-tab" data-toggle="tab" href="#desc" role="tab"
                           aria-controls="desc" aria-selected="true"><?= t('Description') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="video-tab" data-toggle="tab" href="#video" role="tab"
                           aria-controls="video" aria-selected="false"><?= t('Video') ?></a>
                    </li>
                </ul>
            </div>

            <div class="tab-content" id="productTabContent">
                <div class="tab-pane fade show active" id="desc" role="tabpanel" aria-labelledby="desc-tab">
                    <div class="pr-section">
                        <div class="content">
                            <?= ph($model->desc) ?>
                        </div>
                    </div>
                    <?= $this->render('_product_info_items', ['models' => $productInfoItems]) ?>
                </div>

                <div class="tab-pane fade" id="video" role="tabpanel" aria-labelledby="video-tab">
                    <?= $this->render('_product_video_items', ['models' => $productVideoItems]) ?>
                </div>
            </div>
        </div>

        <div class="pr-view__portfolio">
            <?= $this->render('_product_portfolio', [
                'model' => $model,
            ]) ?>
        </div>

        <?php if ($relatedProducts) { ?>
            <div class="pr-slide-list product-slide">
                <?= $this->render('_product_related', [
                    'relatedProducts' => $relatedProducts,
                ]) ?>
            </div>
        <?php } ?>

    </div>
</div>


