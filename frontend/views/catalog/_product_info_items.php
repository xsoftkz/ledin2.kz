<?php
?>

<?php if ($models) { ?>
    <div class="row">
        <?php foreach ($models as $model) { ?>
            <?php if ($model->photo) { ?>
                <div class="col-lg-3 col-6 mb-2">
                    <div class="card pr-info-item">
                        <div class="card-body">
                            <div class="pr-info-item__title">
                                <strong><?= h($model->title) ?></strong>
                            </div>
                            <div class="pr-info-item__img">
                                <a href="<?= $model->image ?>" data-fancybox="images" data-caption="<?= $model->title ?>">
                                    <img src="<?= thumb($model->imagePath, 200) ?>" alt="<?= $model->title ?>" />
                                </a>
                            </div>
                            <div class="pr-info-item__desc">
                                <?= h($model->desc) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>

    <div class="row">
        <?php foreach ($models as $model) { ?>
            <?php if (!$model->photo) { ?>
                <div class="col-lg-6 mb-2">
                    <div class="card pr-info-item">
                        <div class="card-body">
                            <div class="pr-info-item__title">
                                <strong><?= h($model->title) ?></strong>
                            </div>
                            <div class="pr-info-item__desc">
                                <?= h($model->desc) ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }  ?>
        <?php } ?>
    </div>
<?php } ?>
