<?php
?>

<?php if ($model->productFiles){ ?>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="action__icon download__icon"></span>
        <span class="action__icon__title"><?= t('Download')?></span>
    </a>
    <div class="dropdown-menu dropdown-menu-right">
        <div class="text-center d-block d-md-none">
            <?= t('Download files')?>
            <hr>
        </div>
        <?php foreach ($model->productFiles as $file){ ?>
            <a href="<?= $file->fileLink ?>" class="dropdown-item" target="_blank" download>
                <?= $file->title ?>
            </a>
        <?php } ?>
    </div>
<?php } ?>
