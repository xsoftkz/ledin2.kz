<?php
$product = $model->product;

if ($product) { ?>
    <div class="cart-item pr-item shadow-cs">
        <span class="row align-items-center">
            <span class="col-4 col-lg-3">
                 <a href="<?= url(['/catalog/view', 'slug' => $product->slug]) ?>">
                     <span class="cart-item__img">
                         <img src="<?= thumb($product->imagePath, 200) ?>" alt="">
                     </span>
                 </a>
            </span>
            <span class="col-8 col-lg-9">
                <span class="row align-items-center mb-2">
                    <span class="col-12 col-lg-6 text-left mb-3">
                        <a href="<?= url(['/catalog/view', 'slug' => $product->slug]) ?>">
                            <span class="cart-item__title text-accent d-block">
                                 <small class="text-secondary"><?= t('Product') ?>: </small><?= $product->title ?>
                             </span>
                             <span class="cart-item__title text-accent d-block">
                                 <small class="text-secondary"><?= t('Model') ?>: </small><?= $model->name ?>
                             </span>
                        </a>
                    </span>
                    <span class="col-6 col-lg-3">
                        <span class="cart__quantity__wrap">
                            <span class="cart__minus">
                                <img src="/svg/minus.svg" alt="" class="js-minus">
                            </span>
                            <span class="cart__quantity" data-position-id="<?= $model->id ?>" data-count-value="<?= $position->quantity ?>" >
                                <?= $position->getQuantity() ?>
                            </span>
                            <span class="cart__plus">
                                <img src="/svg/plus.svg" alt="" class="js-plus">
                            </span>
                        </span>
                    </span>
                    <span class="col-2 col-6 col-lg-3 text-right">
                         <img src="/svg/close.svg" alt="" style="width: 20px;" class="js-remove" data-position-id="<?= $model->id ?>">
                    </span>
                </span>
            </span>
        </span>
    </div>

<?php } ?>


