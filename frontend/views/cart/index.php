<?php
use yii\helpers\Url;

$this->title = t('Cart');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">

    <h1 class="page-title font-primary"><?= t('Cart') ?></h1>
    <p><?= t('Cart commercial proposal') ?></p>

    <div class="row">
        <div class="col-lg-6">
            <?php if ($models){ ?>
                <div class="cart-list product-list">
                    <?php foreach ($models as $model){ ?>
                        <div class="cart-item-col js-cart-item">
                            <?= $this->render('_item', [
                                'position' => $model,
                                'model' => $model->product,
                            ]); ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } else { ?>
                <br>
                <h6><?= t('The list of products is empty') ?></h6>
            <?php } ?>
        </div>
        <div class="col-lg-4">
            <?= \frontend\widgets\feedback\FeedbackCartFormWidget::widget() ?>
        </div>
    </div>

</div>

<style>
    .feedback {
        display: none;
    }
</style>
