<?php
use yii\widgets\ListView;
use common\models\Settings;

$this->title = t('Partners'). Settings::getValue('site_name');

$this->params['breadcrumbs'][] = t('Partners');
$this->registerMetaTag(['name' => 'description', 'content' => '']);

?>
<div class="container">
    <h1 class="page-title"><?= t('Partners') ?></h1>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'tag' => 'div',
            'class' => 'partner-list row'
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'col-lg-3',
        ],
        'itemView' => '_item',
        'layout' => "{items}\n{pager}",
    ]) ?>
</div>