<?php
?>
<div class="partner-item hoverable card">
    <div class="card-body">
        <a href="<?= url(['/partners/view', 'slug' => $model->slug]) ?>" class="partner-item__img">
            <img src="<?= thumb($model->imagePath, 300) ?>" alt="" class="mb-3">
        </a>
        <div class="partner-item__title pt-2">
            <a href="<?= url(['/partners/view', 'slug' => $model->slug]) ?>">
                <?= h($model->title) ?>
            </a>
        </div>
    </div>
</div>







