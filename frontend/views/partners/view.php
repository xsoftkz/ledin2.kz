<?php
use common\models\Settings;

$this->title = $model->title.' | '.siteName();

$this->params['breadcrumbs'][] = ['label' => t('Partners'), 'url' => ['/partners']];
$this->params['breadcrumbs'][] = $model->title;

$this->registerMetaTag(['name'=> 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name'=> 'description', 'content' => $model->meta_description]);

?>

<div class="container">
    <div class="content-view">
        <h1 class="page-title"><?= $model->title?></h1>
        <br>
        <?= $model->desc?>
    </div>
</div>