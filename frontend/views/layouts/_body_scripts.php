<?php
?>


<script>
    (function(w,d,u){
        var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
        var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
    })(window,document,'https://cdn-ru.bitrix24.kz/b840351/crm/tag/call.tracker.js');
</script>

<!-- Marquiz script start -->
<!-- <script src="//script.marquiz.ru/v1.js" type="application/javascript"></script>
<script type="text/javascript">
var jsRoistatVisitId='';
window.onRoistatModuleLoaded = function() {
 window.roistatVisitCallback = function(visitId) {
	 Marquiz.init({
		    host: '//quiz.marquiz.ru',
		    id: '5f75b4613112e3004425419d',
		    autoOpen: 10,
		    autoOpenFreq: 'once',
		    openOnExit: false
		  });
  jsRoistatVisitId = visitId;
};
};
</script> -->
<!-- Marquiz script end -->


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(69204433, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
    });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/69204433" style="position:absolute; left:-9999px;" alt="yandex" /></div></noscript>
<!-- /Yandex.Metrika counter -->



<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-182558554-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-182558554-1');
</script>



<?php if(Yii::$app->language == 'ru-RU') : ?>
    <!-- ROISTAT BEGIN -->
    <script>
        (function(w, d, s, h, id) {
            w.roistatProjectId = id; w.roistatHost = h;
            var p = d.location.protocol == "https:" ? "https://" : "http://";
            var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
            var m = d.createElement("meta"); m.name = "referrer"; m.content = "no-referrer-when-downgrade"; var e = d.getElementsByTagName("head")[0]; if (e) e.appendChild(m);
            var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
        })(window, document, 'script', 'cloud.roistat.com', '3e8264d5462b9e542099cd1befe1d2b5');
    </script>
    <!-- ROISTAT END -->

    <!-- BEGIN BITRIX24 WIDGET INTEGRATION WITH ROISTAT -->
    <script>
        (function(w, d, s, h) {
            w.roistatLanguage = '';
            var p = d.location.protocol == "https:" ? "https://" : "http://";
            var u = "/static/marketplace/Bitrix24Widget/script.js";
            var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
        })(window, document, 'script', 'cloud.roistat.com');
    </script>
    <!-- END BITRIX24 WIDGET INTEGRATION WITH ROISTAT -->
    <!-- BEGIN WHATSAPP INTEGRATION WITH ROISTAT -->
    <script type="bogus" class="js-whatsapp-message-container">Обязательно отправьте это сообщение, и дождитесь ответа. Ваш номер:  {roistat_visit}</script>
    <script>
        (function() {
            if (window.roistat !== undefined) {
                handler();
            } else {
                var pastCallback = typeof window.onRoistatAllModulesLoaded === "function" ? window.onRoistatAllModulesLoaded : null;
                window.onRoistatAllModulesLoaded = function () {
                    if (pastCallback !== null) {
                        pastCallback();
                    }
                    handler();
                };
            }

            function handler() {
                function init() {
                    appendMessageToLinks();

                    var delays = [1000, 5000, 15000];
                    setTimeout(function func(i) {
                        if (i === undefined) {
                            i = 0;
                        }
                        appendMessageToLinks();
                        i++;
                        if (typeof delays[i] !== 'undefined') {
                            setTimeout(func, delays[i], i);
                        }
                    }, delays[0]);


                    var visitId = window.roistatGetCookie('roistat_visit');
                    if(visitId != undefined){
                        $(document).on('click', 'a[href*="wa.me"]', function() {

                            roistatGoal.reach({
                                leadName: 'Новый лид с ledin.kz',
                                email : visitId + "@ledin.kz",
                                fields: {
                                    'UF_CRM_1603183552':'{landingPage}',
                                    'UF_CRM_1603183577':'{source}',
                                    'UF_CRM_1603183599':'{city}',
                                    'UF_CRM_1603183631':'{utmSource}',
                                    'UF_CRM_1603183647':'{utmMedium}',
                                    'UF_CRM_1603183665':'{utmCampaign}',
                                    'UF_CRM_1603183701':'{utmTerm}',
                                    'UF_CRM_1603183721':'{utmContent}'
                                }
                            });
                        })
                    }


                }

                function replaceQueryParam(url, param, value) {
                    var explodedUrl = url.split('?');
                    var baseUrl = explodedUrl[0] || '';
                    var query = '?' + (explodedUrl[1] || '');
                    var regex = new RegExp("([?;&])" + param + "[^&;]*[;&]?");
                    var queryWithoutParameter = query.replace(regex, "$1").replace(/&$/, '');
                    return baseUrl + (queryWithoutParameter.length > 2 ? queryWithoutParameter  + '&' : '?') + (value ? param + "=" + value : '');
                }

                function appendMessageToLinks() {
                    var message = document.querySelector('.js-whatsapp-message-container').text;
                    var text = message.replace(/{roistat_visit}/g, window.roistatGetCookie('roistat_visit'));
                    var linkElements = document.querySelectorAll('[href*="//wa.me"], [href*="//api.whatsapp.com/send"], [href*="//web.whatsapp.com/send"], [href^="whatsapp://send"]');
                    for (var elementKey in linkElements) {
                        if (linkElements.hasOwnProperty(elementKey)) {
                            var element = linkElements[elementKey];
                            element.href = replaceQueryParam(element.href, 'text', text);
                        }
                    }
                }
                if (document.readyState === 'loading') {
                    document.addEventListener('DOMContentLoaded', init);
                } else {
                    init();
                }
            };
        })();
    </script>
    <!-- END WHATSAPP INTEGRATION WITH ROISTAT -->
    <script>
        window.roistatVisitCallback = function (visitId) {
            console.log(visitId);
            (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
            })(window,document,'https://cdn-ru.bitrix24.kz/b840351/crm/site_button/loader_2_59o68x.js');

            window.Bitrix24WidgetObject = window.Bitrix24WidgetObject || {};
            window.Bitrix24WidgetObject.handlers = {
                'form-init': function (form) {
                    form.presets = {
                        'roistatID': visitId
                    };
                }
            };
        }
    </script>
<?php else: ?>
    <script>
        (function(w,d,u){
            var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
            var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn-ru.bitrix24.kz/b840351/crm/site_button/loader_2_59o68x.js');
    </script>
<?php endif; ?>


<script type="text/javascript">
    _linkedin_partner_id = "3162801";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
    (function(){var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
    <img height="1" width="1" style="display:none;" alt="linkedin" src="https://px.ads.linkedin.com/collect/?pid=3162801&fmt=gif" />
</noscript>


