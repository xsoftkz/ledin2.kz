<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

\frontend\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="yandex-verification" content="5e3330083e7890fe" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	
    <?= $this->render('_head_scripts') ?>

</head>
<body>
<?php $this->beginBody() ?>
	
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KXN64RL"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


<div class="wrapper">

    <?php if (\Yii::$app->devicedetect->isMobile() || \Yii::$app->devicedetect->isTablet()) {
        echo $this->render('@app/views/common/header-mobile');
    } else {
        echo $this->render('@app/views/common/header');
    }
    ?>

    <div class="layout-content">
        <div class="container">
            <?= Breadcrumbs::widget([
                'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n", // template for all links
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]);
            ?>
            <?= \common\widgets\Alert::widget() ?>
        </div>
        <?= $content ?>
    </div>

</div>

<?= $this->render('@app/views/common/footer') ?>

	
<?= \frontend\widgets\feedback\CallbackModalWidget::widget() ?>
<?= \frontend\widgets\feedback\FreeCallbackModalWidget::widget() ?>
<?= \frontend\widgets\feedback\FeedbackModalWidget::widget() ?>
<?= \frontend\widgets\feedback\FeedbackDownloadModalWidget::widget() ?>
<?= \frontend\widgets\catalog\CartModalWidget::widget() ?>

<?php $this->endBody() ?>

<?= $this->render('_body_scripts') ?>

</body>
</html>
<?php $this->endPage() ?>
