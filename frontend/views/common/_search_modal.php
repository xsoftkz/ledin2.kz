<?php
?>

<div style="display: none" id="searchModal">
    <div id="search-box">
        <div class="search-header shadow-sm">
            <div class="container">
                <div class="row">
                    <div class="search-box-size">
                        <h3 class="font-weight-bold"><?= t('Site search') ?></h3>
                        <div>
                            <form action="<?= url(['/search']) ?>" ref="url" v-on:submit.prevent="searchApi">
                                <div class="row no-gutters">
                                    <div class="col-lg-9 col-8">
                                        <input class="form-control form-control-lg" type="text"
                                               placeholder="<?= t('Enter word') ?>" aria-label="" v-model="query">
                                    </div>
                                    <div class="col-lg-3 col-4">
                                        <select class="form-control form-control-lg" v-model="section">
                                            <option value="catalog"><?= t('Catalog') ?></option>
                                            <option value="page"><?= t('Pages') ?></option>
                                        </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="search-result">
            <div class="container">
                <div class="search-box-size">
                    <h6 class="pt-lg-3 pt-2 pb-1 font-weight-bold"><?= t('Search result') ?></h6>
                    <div class="text-center search-indicator" v-show="isLoading">
                        <img src="/svg/loading.svg" alt="loading">
                    </div>
                    <div id="search-results">
                        <div v-show="pages.length === 0">
                            <h6><?= t('No results found.', 'yii') ?></h6>
                        </div>
                        <a v-for="page in pages" :href="page.url" class="pt-2 pb-2 search-result-item">
                            <span class="search-result-item__img">
                                <img :src="page.img" alt="">
                            </span>
                            <span class="search-result-item__text">
                                <h5 class="mb-1">{{page.title}}</h5>
                                <p class="mb-0 text-gray">{{page.sub_title}}</p>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
