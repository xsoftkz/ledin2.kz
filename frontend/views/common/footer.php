<?php
use common\modules\shop\models\Category;
use frontend\models\Content;
use frontend\models\ContentCategory;

$services = Content::find()->withSection(Content::SECTION_SERVICE)->published()->orderBy('sort_index')->all();
$portfolios = ContentCategory::find()->withSection(Content::SECTION_PORTFOLIOS)->published()->orderBy('sort_index')->all();

?>
<?php if (!in_array(Yii::$app->controller->id, ['article', 'vacancy'])) {
    echo \frontend\widgets\feedback\FeedbackTelegramWidget::widget();
} ?>
<br/>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <img src="/img/logo.png" alt="ledin.kz">
                <ul class="f-social-links list-unstyled">
                    <li>
                        <a href="<?= setting('facebook') ?> ">
                            <img src="/svg/facebook.svg" alt="facebook">
                        </a>
                    </li>
                    <li>
                        <a href="<?= setting('instagram') ?> ">
                            <img src="/svg/instagram.svg" alt="instagram">
                        </a>
                    </li>
                    <li>
                        <a href="<?= setting('telegram') ?> ">
                            <img src="/svg/telegram.svg" alt="telegram">
                        </a>
                    </li>					
                    <li>
                        <a href="<?= setting('youtube') ?> ">
                            <img src="/svg/youtube.svg" alt="youtube">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-3">
                <h5><?= t('About company') ?></h5>
                <ul class="f-col-list">
                    <li>
                        <a href="<?= url(['/about']) ?>"><?= t('About') ?></a>
                    </li>
                    <li>
                        <a href="<?= url(['/vacancy']) ?>"><?= t('Career') ?></a>
                    </li>
                    <li>
                        <a href="<?= url(['/portfolio']) ?>"><?= t('Portfolio') ?></a>
                    </li>
                    <li>
                        <a href="<?= url(['/news']) ?>"><?= t('News') ?></a>
                    </li>
                    <li>
                        <a href="<?= url(['/article']) ?>"><?= t('Articles') ?></a>
                    </li>
                    <li>
                        <a href="<?= url(['/contacts']) ?>"><?= t('Contacts') ?></a>
                    </li>
                    <li>
                        <a data-fancybox data-src="#callback-modal" href="javascript:;">
                            <?= t('Callback') ?>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-4">
                <h5><?= t('Services') ?></h5>
                <ul class="f-col-list">
                    <?php if ($services) {
                        foreach ($services as $service) { ?>
                            <li>
                                <a href="<?= url(['/services/view', 'slug' => $service->slug]) ?>"><?= $service->title ?></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
            </div>
            <div class="col-lg-3">
                <h5><?= t('Portfolio') ?></h5>
                <ul class="f-col-list">
                    <?php if ($portfolios) {
                        foreach ($portfolios as $portfolio) { ?>
                            <li>
                                <a href="<?= url(['/portfolio/index', 'slug' => $portfolio->slug]) ?>"><?= $portfolio->title ?></a>
                            </li>
                        <?php }
                    } ?>
                </ul>
                <p class="font-weight-bold">
                    <a href="tel:<?= setting('site_phone') ?>"><?= setting('site_phone') ?></a>
                    <br>
                    <?= t('Single number in Kazakhstan') ?>
                </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-6">
                <small>
                    <span><?= t('Copyright text') ?></span> &copy; <?= date('Y')?>
                </small>
            </div>
            <div class="col-lg-6 text-right"></div>
        </div>
    </div>
</footer>

<?php if (\Yii::$app->devicedetect->isMobile()) {
    echo frontend\widgets\mMenu\MMenuWidget::widget();
} ?>

<?= $this->render('_search_modal') ?>





