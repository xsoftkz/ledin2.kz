<?php
use yii\helpers\Url;
use frontend\models\Category;
use frontend\models\Portfolio;
use frontend\models\ContentCategory;

$categories = Category::find()->isParent()->published()->orderBy('sort')->all();
$portfolios = ContentCategory::find()->withSection(Portfolio::SECTION)->published()->orderBy('sort_index')->all();

if ( Url::toRoute( Url::home() ) == Url::toRoute( Yii::$app->controller->getRoute() ) ) {
    $isHome = true;
} else {
    $isHome = false;
}
?>

<div class="header <?= ($isHome) ? 'header-home header-main fixed-top' : 'header-default' ?> ">
    <div class="top-header">
        <div class="container">
            <div class="row">
                <div class="col-2">
                    <?= \frontend\widgets\lang\LangWidget::widget(); ?>
                </div>
                <div class="col-10">
                    <ul class="top-nav nav justify-content-end">
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" data-fancybox data-src="#searchModal" href="javascript:;">
                                <span class="nav__icon search-dark__icon"></span>
                                <span>
                                    <?= t('Site search') ?>
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" href="<?= url(['/cart']) ?>">
                                <span class="nav__icon cart-dark__icon"></span>
                                <span>
                                    <?= t('Cart') ?>
                                </span>
                                <span class="nav-link__badge">
                                    (<span class="js-cart-count"><?= Yii::$app->cart->getCount() ?></span>)
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" href="<?= url(['/favorite']) ?>">
                                <span class="nav__icon favorite-dark__icon"></span>
                                <span>
                                    <?= t('Favorites') ?>
                                </span>
                                <span class="nav-link__badge">
                                    (<span class="js-favorite-count"><?= Yii::$app->favorite->getCount() ?></span>)
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" href="<?= url(['/compare']) ?>">
                                <span class="nav__icon compare-dark__icon"></span>
                                <span>
                                    <?= t('Compare') ?>
                                </span>
                                <span class="nav-link__badge">
                                    (<span class="js-compare-count"><?= Yii::$app->compare->getCount() ?></span>)
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex align-items-center" data-fancybox data-src="#callback-modal" href="javascript:;">
                                <span class="nav__icon support__icon"></span>
                                <span><?= t('Callback') ?></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link d-flex" href="tel:<?= setting('site_phone') ?>">
                                <span class="nav__icon phone__icon"></span>
                                <span><?= setting('site_phone') ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="header-middle <?= ($isHome) ? '' : 'shadow-sm'?> ">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <nav class="navbar navbar-expand-lg navbar-classic">
                        <div class="navbar-descriptor">
                            <div class="navbar-descriptor__logo">
                                <a href="<?= url(['/']) ?>">
                                    <img src="/img/logo.png" alt="ledin.kz" class="h__logo">
                                </a>
                            </div>
                            <div class="navbar-descriptor__text">
                                <span>
									Поставка светотехнического <br> оборудования по всему <br> Казахстану
                                </span>
                            </div>
                        </div>

                        <div class="collapse navbar-collapse" id="navbar-classic">
                            <ul class="navbar-nav ml-auto mt-2 mt-lg-0 mr-3">
                                <li class="nav-item dropdown mega-dropdown">
                                    <a class="nav-link dropdown-toggle" href="<?= url(['/about']) ?>" id="about" aria-haspopup="true" aria-expanded="false">
                                        <?= t('About company') ?>
                                    </a>
                                    <ul class="dropdown-menu mega-dropdown-menu justify-content-center" aria-labelledby="about">
                                        <li>
                                            <a class="dropdown-item" href="<?= url(['/about']) ?>">
                                                <span class="dropdown-item__img">
                                                    <img src="/img/company.png" alt="<?= t('About') ?>">
                                                </span>
                                                <span><?= t('About') ?></span>
                                            </a>
                                        </li>

                                        <li>
                                            <a class="dropdown-item" href="<?= url(['/news']) ?>">
                                                <span class="dropdown-item__img">
                                                    <img src="/img/news.png" alt="<?= t('News') ?>">
                                                 </span>
                                                <span><?= t('News') ?></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="<?= url(['/article']) ?>">
                                                <span class="dropdown-item__img">
                                                    <img src="/img/article.png" alt="<?= t('Articles') ?>">
                                                 </span>
                                                <span><?= t('Articles') ?></span>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="<?= url(['/vacancy']) ?>">
                                                <span class="dropdown-item__img">
                                                    <img src="/img/company.png" alt="<?= t('Vacancies') ?>">
                                                 </span>
                                                <span><?= t('Vacancies') ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php if ($categories){ ?>
                                    <?php foreach ($categories as $category) { ?>
                                        <?php if ($category->categories) { ?>
                                            <li class="nav-item dropdown mega-dropdown">
                                                <a class="nav-link dropdown-toggle" href="<?= url(['/catalog/index', 'slug' => $category->slug]) ?>"
                                                   id="menu-<?= $category->id ?>" aria-haspopup="true" aria-expanded="false">
                                                    <?= $category->title ?>
                                                </a>

                                                <ul class="dropdown-menu mega-dropdown-menu" aria-labelledby="menu-<?= $category->id ?>">
                                                    <?php foreach ($category->categories as $subcategory){ ?>
                                                        <li>
                                                            <a href="<?= Url::toRoute(['/catalog/index', 'slug' => $subcategory->slug]) ?>" class="dropdown-item">
                                                                <span class="dropdown-item__img">
                                                                    <img src="<?= thumb($subcategory->iconImagePath, 100) ?>" alt="<?= $subcategory->title ?>">
                                                                </span>
                                                                <span><?= $subcategory->title ?></span>
                                                            </a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php } else { ?>
                                            <li class="nav-item">
                                                <a class="nav-link" href="<?= url(['/catalog/index', 'slug' => $category->slug]) ?>"><?= $category->title ?></a>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>

                                <li class="nav-item dropdown mega-dropdown">
                                    <a class="nav-link dropdown-toggle" href="<?= url(['/portfolio/index']) ?>" id="portfolio" aria-haspopup="true" aria-expanded="false">
                                        <?= t('Portfolio') ?>
                                    </a>
                                    <ul class="dropdown-menu mega-dropdown-menu justify-content-end" aria-labelledby="portfolio">
                                        <?php if ($portfolios){ ?>
                                            <?php foreach ($portfolios as $portfolio){ ?>
                                                <li>
                                                    <a href="<?= Url::toRoute(['/portfolio/index', 'slug' => $portfolio->slug]) ?>" class="dropdown-item">
                                                        <span class="dropdown-item__img">
                                                            <img src="<?= thumb($portfolio->imagePath, 100) ?>" alt="<?= $portfolio->title ?>">
                                                        </span>
                                                        <span><?= $portfolio->title ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </li>

                                <li class="nav-item dropdown mega-dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="download" aria-haspopup="true" aria-expanded="false">
                                        <?= t('Download') ?>
                                    </a>
                                    <ul class="dropdown-menu mega-dropdown-menu justify-content-end" aria-labelledby="download">
                                        <?php if ($categories){ ?>
                                            <?php foreach ($categories as $category) { ?>
                                                <li>
                                                    <a href="javascript:;" class="dropdown-item js-category-item" data-fancybox data-src="#feedback-download-modal" data-link="<?= $category->link ?>">
                                                        <span class="dropdown-item__img">
                                                            <img src="<?= thumb($category->imagePath, 100) ?>" alt="<?= $category->title ?>">
                                                        </span>
                                                        <span><?= $category->title ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </li>

                                <li class="nav-item dropdown mega-dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="ddm" aria-haspopup="true" aria-expanded="false">
                                        <?= t('More') ?>
                                    </a>
                                    <ul class="dropdown-menu mega-dropdown-menu justify-content-end" aria-labelledby="ddm">
                                        <li>
                                            <a class="dropdown-item" href="<?= url('@filesWebroot/sale_out.pdf') ?>"><?= t('Sale out') ?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="<?= url(['/services']) ?>"><?= t('Services') ?></a>
                                        </li>
                                        <li>
                                            <a class="dropdown-item" href="<?= url(['/contacts']) ?>"><?= t('Contacts') ?></a>
                                        </li>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

