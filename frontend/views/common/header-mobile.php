<?php
use yii\helpers\Url;
use common\models\Settings;
?>

<div class="m-header shadow-sm">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-3">
                <ul class="nav header-top-menu">
                    <li class="nav-item">
                        <div id="mmenu-icon" class="Fixed hamburger hamburger--collapse">
                             <span class="hamburger-box">
                                 <span class="hamburger-inner"></span>
                             </span>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-5 text-center">
                <a href="<?= url(['/']) ?>">
                    <img src="/img/logo.png" alt="" style="height: 30px">
                </a>
            </div>
            <div class="col-2 text-right">
                <a  data-fancybox data-src="#searchModal" href="javascript:;">
                    <img src="/svg/search-accent.svg" alt="" style="width: 25px;">
                </a>
            </div>
            <div class="col-2 text-right">
                  <a href="tel:<?= setting('site_phone') ?>">
                    <img src="/svg/phone-accent.svg" alt="" style="width: 25px;">
                </a>
            </div>
        </div>
    </div>
</div>
