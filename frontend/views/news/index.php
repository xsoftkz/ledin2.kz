<?php
use yii\widgets\ListView;

$this->title = t('News').' | '. siteName();

$this->params['breadcrumbs'][] = t('News');
$this->registerMetaTag(['name' => 'description', 'content' => 'Последние новости о светотехнике. Будьте в курсе новинок и трендов.']);

?>
<div class="container">
    <h1 class="page-title"><?= t('News') ?></h1>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'options' => [
            'class' => 'news-list row'
        ],
        'itemOptions' => [
            'class' => 'col-lg-3 mb-3 ',
        ],
        'layout' => "{items}\n{pager}",
    ]) ?>
</div>