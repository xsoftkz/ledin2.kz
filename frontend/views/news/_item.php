<?php
?>

<div class="news-item card hoverable h-100">
    <a href="<?= url(['/news/view', 'slug' => $model->slug]) ?>" class="d-block">
        <img src="<?= thumb($model->imagePath, 600) ?>" alt="">
        <span class="d-block card-body">
            <small class="d-block text-accent mb-1">
                <?= Yii::$app->formatter->asDate($model->created_at) ?>
            </small>
            <span class="d-block">
                <?= $model->title ?>
            </span>
        </span>
    </a>
</div>






