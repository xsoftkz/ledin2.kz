<?php
use common\models\Settings;

$this->title = $model->title.' | '.siteName();

$this->params['breadcrumbs'][] = ['label' => t('News'), 'url' => ['/news']];
$this->params['breadcrumbs'][] = $model->title;

$this->registerMetaTag(['name'=> 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name'=> 'description', 'content' => $model->meta_description]);

?>

<div class="container">
    <div class="content-view">
        <h1 class="page-title"><?= $model->title?></h1>

        <br>
        <div class="row">
            <div class="col-lg-3">
                <a href="<?= $model->image ?>" data-fancybox="images" data-caption="<?= $model->title ?>" class="" data-src="<?= $model->image ?>">
                    <img src="<?= thumb($model->imagePath, 400) ?>" alt="<?= $model->title ?>" />
                </a>
            </div>
            <div class="col-lg-9">
                <?= $model->desc?>
            </div>
        </div>
    </div>
</div>