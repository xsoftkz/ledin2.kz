<?php
?>

<div class="services-item card hoverable h-100">
    <a href="<?= url(['/services/view', 'slug' => $model->slug]) ?>" class="d-block card-body">
        <img src="<?= thumb($model->imagePath, 400, 300, true) ?>" alt="" class="mb-3">
        <span class="d-block"> <?= $model->title ?> </span>
    </a>
</div>






