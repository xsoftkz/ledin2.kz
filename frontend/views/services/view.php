<?php
use common\models\Settings;
use frontend\models\Content;

/** @var Content $model */
$this->title = $model->getSeoTitle();

$this->params['breadcrumbs'][] = ['label' => t('Services'), 'url' => ['/news']];
$this->params['breadcrumbs'][] = $model->title;

$this->registerMetaTag(['name'=> 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name'=> 'description', 'content' => $model->meta_description]);

?>


<div class="container">
    <div class="content-view">
        <div class="row">
            <div class="col-lg-3 order-lg-1 order-2">
                <?php if ($models) { ?>
                    <div class="list-group">
                        <?php foreach ($models as $item){ ?>
                            <a href="<?= url(['/services/view', 'slug' => $item->slug]) ?>" class="list-group-item list-group-item-action">
                                <?= h($item->title) ?>
                            </a>
                        <?php  } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-9 order-lg-2 order-1 mb-3">
                <h1 class="page-title"><?= $model->title?></h1>
                <?php if ($model->photo) { ?>
                    <div class="text-center mt-2 mb-2">
                        <img src="<?= $model->image ?>" alt="<?= $model->title ?>" />
                    </div>
                <?php } ?>

                <?= $model->desc?>
            </div>

        </div>
    </div>
</div>