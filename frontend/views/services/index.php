<?php
use yii\widgets\ListView;

$this->title = t('Services').' | '. siteName();

$this->params['breadcrumbs'][] = t('Services');

$this->registerMetaTag(['name'=> 'keywords', 'content' => 'Проект электроосвещения, Аудит и оптимизация существующего проекта, Подбор светотехнического оборудование, Светотехническое проектирование, Светотехнический расчет, Светодизайн']);
$this->registerMetaTag(['name' => 'description', 'content' => 'Наша компания предоставляет клиентам широкий спектр услуг: от разработки плана освещения и его реализации до аудита уже существующего проекта.']);

?>
<div class="container">
    <h1 class="page-title"><?= t('Services') ?></h1>
	
	<div>
		<?= txt('services_desc') ?>
	</div>
	<br/>
	
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'options' => [
            'class' => 'services-list row'
        ],
        'itemOptions' => [
            'class' => 'col-lg-3 mb-3 ',
        ],
        'layout' => "{items}\n{pager}",
    ]) ?>
</div>