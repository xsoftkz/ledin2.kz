<?php
use yii\helpers\Url;

$this->title = t('Compare products');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h1 class="page-title font-primary"><?= t('Compare products') ?></h1>

    <?php if ($positions){ ?>
        <div class="compare-wrap">
            <div class="compare-left">
                <div class="compare-params__list">
                    <div class="compare-params__header d-flex">
                        <a href="<?= Url::toRoute(['/compare/clear']) ?>" class="clear-link">
                            <img src="/img/trash.svg" alt="">
                            <span><?= t('Clear compare list') ?></span>
                        </a>
                    </div>
                    <?php if ($params){ ?>
                        <?php foreach ($params as $param){ ?>
                            <div class="compare-param-item">
                                <?= h($param->title) ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
            <div class="compare-right">
                <div class="compare-products-list">
                    <?php foreach ($positions as $position){
                        echo $this->render('_position_item', [
                            'position' => $position,
                            'params' => $params,
                        ]);
                    } ?>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <br>
        <h6><?= t('The list of products is empty') ?></h6>
    <?php } ?>

</div>


