<?php
$productModel = $position->product; // модель товара
$product = $productModel->product; // товар

if ($product) { ?>
    <div class="compare-position-col">
        <a href="<?= url(['/catalog/view', 'slug' => $product->slug]) ?>" class="compare-product shadow-cs">
            <span class="compare-product__img">
                <img src="<?= thumb($product->imagePath, 200) ?>" alt="">
            </span>
            <span class="compare-product__title text-accent">
                <small class="text-secondary"><?= t('Product') ?>: </small>
                <?= $product->title ?>
            </span>
            <span class="compare-product__title text-accent">
                <small class="text-secondary"><?= t('Model') ?>: </small>
                <?= $productModel->name ?>
            </span>
        </a>

        <?php
            if ($params){
                foreach ($params as $param){
                    echo '<div class="compare-param-item justify-content-center">';
                        if ($productModel && $productModel->paramsValues) {
                            foreach ($productModel->paramsValues as $paramValue) {
                                if ($param->id == $paramValue->param_id ){
                                    if($paramValue->param->optionTypeIsFile()) {
                                        if ($paramValue->valueText){
                                            foreach ($paramValue->valueText as $value){ ?>
                                                <img src="<?= $value ?>" alt="" width="30px">
                                            <?php }
                                        }
                                    } else {
                                        echo $paramValue->valueText;
                                    }
                                }
                            }
                        }
                    echo '</div>';
                }
            }
        ?>
    </div>

<?php } ?>


