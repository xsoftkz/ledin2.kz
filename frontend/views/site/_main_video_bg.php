<?php
?>
<div class="main-video-wrap">
    <div class="overlay"></div>
    <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
        <source src="/uploads/files/dvorec_main.mp4" type="video/mp4">
    </video>

    <div class="main-offer__wrap">
        <h1 class="main-offer"><?= txt('main_offer') ?></h1>
        <a data-fancybox data-src="#feedback-modal" href="javascript:;"
           class="btn btn-lg btn-outline-main btn-rounded main-offer__link">
            <?= t('Send order') ?>
        </a>
    </div>
</div>

