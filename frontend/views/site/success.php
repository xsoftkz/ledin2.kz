<?php
$this->title = t('Thanks');
?>

<div class="container success-page">
    <img src="/svg/tick.svg" alt="" class="icon">
    <h4 class="text-uppercase">
        <span class="success-page__main"><?= t('Success client choice') ?></span>
    </h4>
    <h4><strong><?= t('Success Thanks') ?>!</strong></h4>
    <h4><strong><?= t('Success page text') ?></strong></h4>
    <br>
    <h5><?= t('Success page desc') ?></h5>

    <div class="success-page__recommended">
        <div class="row">
            <div class="col-lg-4">
                <div class="card mb-4">
                    <img src="/img/s-prize.jpg" alt="">
                    <div class="card-body">
                        <a href="<?= url('@filesWebroot/vlijanie_tekstury_na_jarkost_fasada.pdf') ?>" class="btn btn-lg btn-main w-100">
                            <?= t('For project managers') ?>
                        </a>
                        <a href="<?= url('@filesWebroot/dlya_zastroyshikov.pdf') ?>" class="btn btn-lg btn-main w-100">
                            <?= t('For project builders') ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card mb-4">
                    <img src="/img/s-blog.jpg" alt="">
                    <div class="card-body">
                        <a href="<?= url(['/article']) ?>" class="btn btn-lg btn-main w-100">
                            <?= t('Go to blog') ?>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card mb-4">
                    <img src="/img/s-youtube.jpg" alt="">
                    <div class="card-body">
                        <a href="https://www.youtube.com/channel/UCWXPncoy90P25jHuhp2Wb2Q" class="btn btn-lg btn-main w-100">
                            <?= t('Go to youtube') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
