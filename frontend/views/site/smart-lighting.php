<?php
$this->title = 'Оборудование для умного освещения';

$this->params['breadcrumbs'][] = $this->title;

$this->registerMetaTag(['name'=> 'keywords', 'content' => '']);
$this->registerMetaTag(['name'=> 'description', 'content' => '']);
?>

<div class="content-header blog-column">
    <h1 class="page-title text-center"><?= $this->title ?></h1>
</div>
<div class="content-page">
    <div class="content-column">
        <div class="content-body">
            <div>
                <h3>Модуль индифидуального управления светильником</h3>
                <br>
                <div class="row f-aic">
                    <div class="col-lg-5">
                        <img src="/img/promo/NEMA3.jpg" alt="">
                    </div>
                    <div class="col-lg-7">
                        <div class="">
                            Модуль индивидуального управления светильником - важная часть системы управления уличными светильниками.
                            Его основной чип использует модуль беспроводной связи LoRaWAN, ZigBee, LTE(4G), который дает
                            возможность светильникам подключаться к профессиональному оборудованию и программному обеспечению.
                            Продукт обладает преимуществами разных функций, небольшими размерами, простотой реализации,
                            бесплатным подключением, надежной работой, простотой в обслуживании и т. д. Это высокоэффективный и
                            энергосберегающий продукт, специально разработанный для умного уличного освещения.
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div>
                <table class="table-product table table-striped">
                    <thead class=" w-100">
                        <tr>
                            <td>
                                <span class="table-title">
                                    Виды <br> модулей
                                </span>
                            </td>
                            <td>
                                <img src="/img/promo/LI-NEMA-01.jpg" alt="">
                                <span>LI-NEMA-01</span>
                            </td>
                            <td>
                                <img src="/img/promo/LI-R1A-01.jpg" alt="">
                                <span>LI-R1A-01</span>
                            </td>
                            <td>
                                <img src="/img/promo/LI-NEMA-02.jpg" alt="">
                                <span>LI-NEMA-02</span>
                            </td>
                            <td>
                                <img src="/img/promo/LI-R1A-02.jpg" alt="">
                                <span>LI-R1A-02</span>
                            </td>
                            <td>
                                <img src="/img/promo/LI-NEMA-03.jpg" alt="">
                                <span>LI-NEMA-03</span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Канал связи</td>
                            <td>ZigBee</td>
                            <td>ZigBee</td>
                            <td>LoRA</td>
                            <td>LoRA</td>
                            <td>LTE NB-IoT B3/B5/B8/B26</td>
                        </tr>
                        <tr>
                            <td>Напряжение питания</td>
                            <td>AC110～277В 50/60Гц</td>
                            <td>AC90～305В 50/60Гц</td>
                            <td>AC110-277В</td>
                            <td>AC85-265V</td>
                            <td>AC85-265V</td>
                        </tr>
                        <tr>
                            <td>Номинальный выходной ток</td>
                            <td>Максимум 4А</td>
                            <td>Максимум 2А</td>
                            <td>Максимум 4А</td>
                            <td>Максимум 5А</td>
                            <td>Максимум 2А</td>
                        </tr>
                        <tr>
                            <td>Тип диммирования</td>
                            <td>0-10V/PWM</td>
                            <td>0-10V/PWM</td>
                            <td>0-10V/PWM</td>
                            <td>0-10V/PWM</td>
                            <td>0-10V/PWM</td>
                        </tr>
                        <tr>
                            <td>Размеры </td>
                            <td>Φ84 x 97.8 (мм)</td>
                            <td>118×43×30 (мм)</td>
                            <td>Φ84 x 97.8 (мм)</td>
                            <td>121×66×36 (мм)</td>
                            <td>84Dia.×97.8(мм)</td>
                        </tr>
                        <tr>
                            <td>Степень защиты </td>
                            <td>IP65</td>
                            <td>IP65</td>
                            <td>IP65</td>
                            <td>IP65</td>
                            <td>IP65</td>
                        </tr>
                        <tr>
                            <td>Температура эксплуатации</td>
                            <td>-40°C~+85°C</td>
                            <td>-40°C~+85°C</td>
                            <td>-40°C~+85°C</td>
                            <td>-40°C~+85°C</td>
                            <td>-40°C~+85°C</td>
                        </tr>
                        <tr>
                            <td>Диапазон частот</td>
                            <td>2.4G</td>
                            <td>2.4G</td>
                            <td>2.4G</td>
                            <td>470-510MHz</td>
                            <td>LTE NB-IoT B3/B5/B8/B26</td>
                        </tr>
                        <tr>
                            <td>Расстояние передачи</td>
                            <td>400м</td>
                            <td>400м</td>
                            <td>400м</td>
                            <td>400м</td>
                            <td>400м</td>
                        </tr>
                        <tr>
                            <td>Тип антенны</td>
                            <td>Встроенная </td>
                            <td>Наружная </td>
                            <td>Встроенная </td>
                            <td>Наружная </td>
                            <td>Встроенная </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div>
                <h3>Передатчик/контроллер управления освещением</h3>
                <br>
                <p>
                    Передатчик/контроллер IoT может реализовывать сети на основе протоколов и связи, а также преобразование протоколов между различными типами сетей с информацией. Кроме того, может быть реализована функция управления устройством, можно управлять нижележащими узлами считывания. Также можно получить  соответствующую информацию о каждом узле и реализовать дистанционное управление светильниками.
                </p>
                <p>
                    Передатчик/контроллер - это устройство для передачи данных с использованием коммуникационных технологий, подключающее датчики к облаку и обеспечивающее обмен данными и аналитикой в реальном времени, которые можно использовать для повышения эффективности и производительности.
                </p>
                <p>
                    Он обеспечивает прозрачную и двунаправленную передачу данных между несколькими устройствами и подходит для промышленных беспроводных измерений и управляющей связи, сбора данных с беспроводных датчиков, умных городов, умных производств, умного сельского хозяйства, умных домов, умного транспорта, умных сетей, испытаний на месторождениях, экологических испытаний и другие.
                </p>
            </div>
            <br>
            <div>
                <table class="table-product table table-striped">
                    <thead class=" w-100">
                        <tr>
                            <td>
                                <span class="table-title">
                                    Модель <br> продукта
                                </span>
                            </td>
                            <td>
                                <img src="/img/promo/LI-GWS-01.jpg" alt="">
                                <span>LI-GWS-01/LI-GWS-02</span>
                            </td>
                            <td>
                                <img src="/img/promo/LI-GW-01.jpg" alt="">
                                <span>LI-GW-01/LI-GW-02</span>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Вид передатчика </td>
                            <td>ZigBee/LoRa</td>
                            <td>ZigBee/LoRa</td>
                        </tr>
                        <tr>
                            <td>Сетевой стандарт</td>
                            <td>4G</td>
                            <td>4G</td>
                        </tr>
                        <tr>
                            <td>Интерфейс</td>
                            <td>Антенна, Ethernet</td>
                            <td>Антенна, Ethernet, порт AD/IO, порт COM, порт USB</td>
                        </tr>
                        <tr>
                            <td>Источник питания</td>
                            <td>AC110-277В</td>
                            <td>9-24В </td>
                        </tr>
                        <tr>
                            <td>Размер </td>
                            <td>225.3 × 225.3 × 79.3</td>
                            <td>147 x 147 x 58.5</td>
                        </tr>
                        <tr>
                            <td>Температура эксплуатации </td>
                            <td>-40～+80ºC</td>
                            <td>-20～+80ºC</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div>
                <h3>Программное обеспечение </h3>
                <h3>Облачная платформа умного управления</h3>
                <p>Платформа управления умным городом включает в себя систему мониторинга освещения, систему видеонаблюдения, систему статистической сигнализации плотности населения, систему сбора данных с датчиков, мониторинг окружающей среды, музыкальную систему синхронизации пейджинга вещания, WI-FI Интернет.</p>
                <br>
                <a href="/img/promo/sr1.jpg" data-fancybox="images">
                    <img src="/img/promo/sr1.jpg" alt="" class="w-100">
                </a>
                <br>
                <br>
                <a href="/img/promo/src2.jpg" data-fancybox="images">
                    <img src="/img/promo/src2.jpg" alt="" class="w-100">
                </a>
            </div>
            <br>
            <br>
            <div class="text-center">
                <nav>
                    <a href="/uploads/files/smart_ligting_ledin.pdf" class="btn btn-lg btn-main btn-rounded m-2">
                        Скачать презентацию
                    </a>
                    <a data-fancybox data-src="#smart-light-callback-modal" href="javascript:;" class="btn btn-lg btn-outline-main btn-rounded">
                        Получить консультацию
                    </a>
                </nav>
            </div>
            <br>
            <br>
        </div>
    </div>
</div>

<style>
    .table-product td, .table-product th {
        width: calc(100%/6);
        min-width: 120px;
    }
    .table-product td span{
        display: block;
        margin: 5px 0;
    }
    .table-product thead td{
        vertical-align: bottom;
    }
    .table-product thead {
        text-align: center;
    }
    .table-product thead img{
        max-height: 100px;
    }
    .table-product thead span.table-title {
        font-size: 1.5rem;
        line-height: 1;
        text-align: left;
    }
</style>

<?= \frontend\widgets\feedback\SmartLightModalWidget::widget() ?>
