<?php

use common\models\Settings;

$this->title = siteName();
$this->registerMetaTag(['name' => 'description', 'content' => t('site_meta_desc')]);
?>


<?= $this->render('_main_special_bg', [
    'offers' => $offers
]) ?>


<?php if ($banners) { ?>
    <div id="main-banner-items" class="container-fluid">
        <div class="row">
            <?php foreach ($banners as $banner){ ?>
                <a href="<?= url($banner->link) ?>" class="col banner-item transitionable">
                <span class="banner-item__img">
                    <img src="<?= thumb($banner->imagePath, 800) ?>" alt="<?= $banner->title ?>">
                </span>
                    <span class="banner-item__title"><?= $banner->title ?></span>
                </a>
            <?php } ?>
        </div>
    </div>
<?php } ?>


<?php if ($projects) { ?>
    <section class="section-item pb-0">
         <h2 class="section-title text-center text-accent"><?= t('Our projects') ?></h2>
                <div id="main-project-items">
                <?php foreach ($projects as $project){ ?>
                    <div class="project-item" style="background-image: url('<?= thumb($project->imagePath, 600) ?>')">
                        <a href="<?= url(['/portfolio/view', 'slug' => $project->slug]) ?>" class="project-item__link">
                        <span class="project-item__title"><?= $project->title ?></span>
                    </a>
                </div>
            <?php } ?>
            <div class="project-item" style="background-image: url('/img/downlights.jpg')">
                <a href="<?= url(['/portfolio']) ?>" class="project-item__link">
                    <span class="project-item__title"><?= t('See all projects') ?></span>
                </a>
            </div>
        </div>
    </section>
<?php } ?>

<?= $this->render('_index_lids') ?>

<?php if ($news) { ?>
    <section class="section-item">
        <h2 class="section-title text-center text-accent"><?= t('Latest news') ?></h2>
        <div class="container">
            <div id="main-news-items">
                <?php foreach ($news as $item){ ?>
                    <div class="main-news-item">
                        <div class="news-item card hoverable h-100">
                            <a href="<?= url(['/news/view', 'slug' => $item->slug]) ?>" class="d-block">
                                <img src="<?= thumb($item->imagePath, 400, 400) ?>" alt="<?= $item->title ?>">
                                <span class="d-block card-body">
                                    <small class="d-block text-accent mb-1">
                                        <?= Yii::$app->formatter->asDate($item->created_at) ?>
                                    </small>
                                    <span class="d-block">
                                        <?= $item->title ?>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>
