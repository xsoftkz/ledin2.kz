<?php
use common\models\Settings;

$this->title = t('About company').' | '.siteName();
$this->params['breadcrumbs'][] = t('About company');

$this->registerMetaTag(['name'=> 'keywords', 'content' => '']);
$this->registerMetaTag(['name'=> 'description', 'content' => 'На рынке с 2015 года. Основная деятельность нашей компании – это разработка проектов с нуля
и поставка оборудования для наружного и внутреннего освещения.']);

?>
<div class="company-page">
    <!-- Текст о компании -->
    <div class="company-page__main">
        <div class="container">
            <div class="company__text checked-list">
                <?= txt('company-text') ?>
            </div>
        </div>
    </div>

    <div class="team section-item">
        <div class="container">
            <h2 class="section-title text-accent text-uppercase">
                Команда
            </h2>
            <div class="row justify-content-center">
                <div class="col-lg-4 mb-1">
                    <a href="/img/team3.jpg" data-fancybox="images" data-caption="">
                        <img src="/img/team3-min.jpg" alt="" />
                    </a>
                </div>
                <div class="col-lg-4 mb-1">
                    <a href="/img/team1.jpg" data-fancybox="images" data-caption="">
                        <img src="/img/team1-min.jpg" alt="" />
                    </a>
                </div>
                <div class="col-lg-4 mb-1">
                    <a href="/img/team2.jpg" data-fancybox="images" data-caption="">
                        <img src="/img/team2-min.jpg" alt="" />
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="section-item">
        <div class="container adv">
            <div class="row">
                <div class="col-lg-2">
                    <img src="/img/adv-icon-1.png" alt="" class="adv__icon">
                </div>
                <div class="col-lg-10 checked-list adv-item">
                    <?= txt('about-adv-1') ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section-item">
        <div class="container adv">
            <div class="row">
                <div class="col-lg-2">
                    <img src="/img/adv-icon-2.png" alt="" class="adv__icon">
                </div>
                <div class="col-lg-10 checked-list adv-item">
                    <?= txt('about-adv-2') ?>
                </div>
            </div>
        </div>
    </div>

    <div class="section-item">
        <div class="container adv">
            <div class="adv-item">
                <?= txt('about-adv-3') ?>
            </div>
        </div>
    </div>


    <!-- Карта -->
    <div class="container mb-4">
        <div class="map-desc__wrap">
            <div class="map-desc">
                <div class="map-title text-accent font-primary mb-2">
                    <?= txt('company-regions') ?>
                </div>
                <div class="map-objects">
                    <div class="map-objects__icon">
                        <img src="/svg/marker.svg" alt="">
                    </div>
                    <div class="map-objects__text">
                        <?= txt('company-objects-text')?>
                    </div>
                </div>
            </div>
        </div>
        <img src="/img/kzmap.jpg" alt="">
    </div>


    <!-- Нам доверяют -->
    <div class="container mb-4">
        <div class="about-awards">
            <h2 class="section-title text-accent text-center text-uppercase">
                Заслужили доверие и благодарность:
            </h2>
            <h6 class="awards-title text-center font-primary mb-4">
                <?= txt('about_award_title') ?>
            </h6>
            <div id="awards_images" class="d-flex">
                <a href="/img/sert.jpg" data-fancybox="images">
                    <img src="/img/sert-min.jpg" alt="">
                </a>
                <a href="/img/award1.jpg" data-fancybox="images">
                    <img src="/img/award1-min.jpg" alt="">
                </a>
                <a href="/img/award2.jpg" data-fancybox="images">
                    <img src="/img/award2-min.jpg" alt="">
                </a>
            </div>
            <div class="awards_text">
                <?= txt('about_award_text') ?>
            </div>
        </div>
    </div>

    <!-- Услугм -->
    <div class="container main-services-list">
        <div class="row">
            <?php if ($services) {
                foreach ($services as $service ) { ?>
                    <div class="col-lg-3 col-6">
                        <div class="main-service-item">
                            <div class="main-service-item__title"><?= $service->title ?></div>
                            <div class="main-service-item__img">
                                <img src="<?= thumb($service->imagePath, 600) ?>" alt="">
                            </div>
                            <div class="main-service-item__desc">
                                <?= $service->desc ?>
                            </div>
                        </div>
                    </div>
                <?php }
            } ?>
        </div>
    </div>

    <!-- Отзывы -->
    <?php if ($reviews) { ?>
        <section class="section-item">
            <div class="container">
                <h2 class="section-title text-accent text-center text-uppercase">
                    Отзывы клиентов
                </h2>
                <div class="reviews-list-wrap">
                    <div id="reviews-list">
                        <?php  foreach ($reviews as $review ) { ?>
                            <div>
                                <div class="review-item" style="background-image: url('<?= thumb($review->imagePath, 600) ?>')">
                                    <span>
                                        <a href="<?= $review->getIntro() ?>" data-fancybox>
                                            <img src="/svg/youtube.svg" alt="">
                                        </a>
                                    </span>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </section>
    <?php } ?>

    <!-- Партнеры -->
    <?php if ($partners) { ?>
        <section class="section-item">
            <div class="container">
                <h2 class="section-title text-accent text-center text-uppercase">
                    Партнеры
                </h2>
                <div class="partners-list-wrap">
                    <div id="partners-list">
                        <?php  foreach ($partners as $partner ) { ?>
                            <div class="p-1">
                                <?= $this->render('/partners/_item', ['model' => $partner]) ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>


    <div class="section-item">
        <div class="container adv">
            <div class="adv-item font-weight-bold">
                <?= txt('about-adv-4') ?>
            </div>
            <div class="text-center">
                <a data-fancybox data-src="#callback-modal" href="javascript:;" class="btn btn-lg btn-outline-main">
                    Оставить заявку
                </a>
            </div>
        </div>
    </div>

    <!-- FAQ -->
    <div class="container">
        <div class="faq-section__title font-primary text-accent">
            <?= t('FAQ') ?>
        </div>
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="faq-section__list">
                    <div class="accordion" id="accordionExample">
                        <?php if ($faqList) { $i = 0; foreach ($faqList as $item) { $i++; ?>
                            <div class="faq-item">
                                <h5>
                                    <a href="#" id="heading<?= $item->id ?>" class="faq-item__title collapsed shadow-sm d-block p-3"
                                       data-target="#collapse<?= $item->id ?>" aria-expanded="<?= $i==1 ? 'true' : 'false' ?>"
                                       data-toggle="collapse" aria-controls="collapse<?= $item->id ?>">
                                        <?= $item->title ?>
                                        <span class="float-right">
                                            <img src="/svg/down.svg" alt="" style="width: 15px;">
                                        </span>
                                    </a>
                                </h5>
                                <div id="collapse<?= $item->id ?>" class="collapse <?= $i==1 ? 'show' : '' ?> " aria-labelledby="heading<?= $item->id ?>" data-parent="#accordionExample">
                                    <div class="card-body faq-item__desc">
                                        <?= $item->desc ?>
                                    </div>
                                </div>
                            </div>
                        <?php } } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<style>


</style>