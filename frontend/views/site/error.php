<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="container">

    <h1 class="page-title font-primary"><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <br>
    <a href="<?= url(['/']) ?>" class="btn btn-sm btn-link"><?= t('Go to home') ?></a>


</div>
