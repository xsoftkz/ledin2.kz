<?php
use common\models\Settings;

$this->title = t('About company').' | '.siteName();
$this->params['breadcrumbs'][] = t('About company');

$this->registerMetaTag(['name'=> 'keywords', 'content' => '']);
$this->registerMetaTag(['name'=> 'description', 'content' => '']);

?>

<div class="container about-page">

    <h1 class="text-center mb-4"><?= t('About company') ?></h1>

    <div class="row">
        <div class="col-lg-8 offset-lg-2 mb-4">
            <div class="about-main__text bg-accent text-white text-center">
                <?= txt('about_main_text') ?>
            </div>
        </div>

        <div class="col-lg-10 offset-lg-1">

            <div class="row f-aic about-item">
                <div class="col-lg-6 col-5 text-center">
                    <img src="/img/about_70.png" alt="">
                </div>
                <div class="col-lg-6 col-7">
                    <?= txt('about_projects') ?>
                </div>
            </div>

            <div class="row f-aic about-item">
                <div class="col-lg-6 col-5  text-center">
                    <img src="/img/about_14.png" alt="">
                </div>
                <div class="col-lg-6 col-7 ">
                    <?= txt('about_year') ?>
                </div>
            </div>

            <div class="row f-aic about-item">
                <div class="col-lg-6 col-5  text-center">
                    <img src="/img/about_400.png" alt="">
                </div>
                <div class="col-lg-6 col-7 ">
                    <?= txt('about_product') ?>
                </div>
            </div>

            <div class="row f-aic about-item">
                <div class="col-lg-6 col-5  text-center">
                    <img src="/img/about_6657.png" alt="">
                </div>
                <div class="col-lg-6 col-7 ">
                    <?= txt('about_delivery') ?>
                </div>
            </div>

            <h4 class="mt-4 mb-4 text-center">
                <?= t('Customers trust us') ?>
            </h4>
            <div class="row f-aic about-partners-list text-center">
                <div class="col-lg-3 col-6 mb-2">
                    <div class="card">
                        <div class="card-body">
                            <img src="/img/basic.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 mb-2">
                    <div class="card">
                        <div class="card-body">
                            <img src="/img/orda.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 mb-2">
                    <div class="card">
                        <div class="card-body">
                            <img src="/img/bigroup.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-6 mb-2">
                    <div class="card">
                        <div class="card-body">
                            <img src="/img/gorsvet.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>