<?php
$this->title = t('Thanks');
?>

<div class="container success-page">
    <img src="/svg/tick.svg" alt="" class="icon" >
    <h4 class="text-uppercase">
        <span class="success-page__main"><?= t('Thanks') ?>!</span>
    </h4>
    <br>
    <h5><?= t('Success page vacancy text') ?></h5>
</div>
