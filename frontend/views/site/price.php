<?php
use common\models\Settings;

$this->title = 'Прайс-лист на продукцию | '.Settings::getValue('site_name');

$this->registerMetaTag(['name'=> 'keywords', 'content' => '']);
$this->registerMetaTag(['name'=> 'description', 'content' => '']);

?>

<div class="container">
    <div class="content-view">
        <h1 class="page-title font-primary">Прайс-лист на продукцию</h1>
    </div>
</div>