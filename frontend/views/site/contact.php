<?php
use common\models\Settings;

$this->title = t('Contacts').' | '.siteName();
$this->params['breadcrumbs'][] = t('Contacts');

$this->registerMetaTag(['name'=> 'keywords', 'content' => '']);
$this->registerMetaTag(['name'=> 'description', 'content' => 'Мы работаем по всему Казахстану. Адреса и телефоны наших офисов в разных городах на сайте.']);

?>

<div class="container">
    <div class="content-view">
        <h1 class="page-title"><?= t('Contacts') ?></h1>
        <?= t('We work all over Kazakhstan') ?>
        <br>
        <br>

        <?php if ($contacts) { ?>

            <nav>
                <div class="nav nav-tabs contact-tabs" id="nav-tab" role="tablist">
                    <?php $i=0; foreach ($contacts as $key => $contact) { $i++;?>
                        <a class="nav-item nav-link <?= ($i == 1) ? 'active' : '' ?> "
                           id="nav-contact-tab-<?= $contact->id ?>"
                           data-toggle="tab"
                           href="#nav-contact-<?= $contact->id ?>"
                           role="tab"
                           aria-controls="nav-contact" aria-selected="<?= ($i == 1) ? 'true' : 'false' ?>">
                            <?= $contact->title ?>
                        </a>
                    <?php } ?>
                </div>
            </nav>
            <div class="tab-content pt-4" id="nav-tabContent">
                <?php $i=0; foreach ($contacts as $key => $contact) { $i++;?>
                    <div class="tab-pane fade <?= ($i == 1) ? 'show active' : '' ?>"
                         id="nav-contact-<?= $contact->id ?>"
                         role="tabpanel"
                         aria-labelledby="nav-contact-tab-<?= $contact->id ?>"
                    >
                        <div class="row">
                            <div class="col-lg-4 contact-desc mb-2">
                                <?= ph($contact->desc) ?>
                            </div>
                            <div class="col-lg-8">
                                <div class="contact-map-box">
                                    <?= $contact->intro ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

        <?php } ?>
    </div>
</div>