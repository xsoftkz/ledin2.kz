<?php
/** @var \frontend\models\Content[] $offers  */
?>

<div class="main-special-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div id="main-slides">
                    <?php foreach ($offers as $offer) {?>
                        <div>
                            <h1 class="main-special-offer text-uppercase">
                                <?= $offer->getTitle() ?>
                            </h1>
                            <div class="main-special-list">
                                <?= $offer->getDesc() ?>
                            </div>
                            <br>
                            <div>
                                <a data-fancybox data-src="#free-callback-modal" href="javascript:;"
                                   class="btn btn-lg btn-main btn-rounded main-offer__link text-uppercase">
                                    <?= $offer->getIntro() ?>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <img src="/img/main-special-img.png" class="main-special__img d-none d-lg-block" alt="ledin.kz">
</div>


