<?php

use yii\widgets\ListView;
use yii\helpers\Url;

$this->title = siteName();
$this->params['breadcrumbs'][] = 'Поиск товаров';

?>

<div class="container">

    <div class="row">
        <div class="col-lg-12">

            <h1><?= t('Site search') ?></h1>

            <form action="<?= Url::toRoute(['/site/search']) ?>" method="get">
                <div class="h-search-box">
                    <input name="key" class="form-control h-search-input" type="text" placeholder="Поиск"
                           value="<?= $key ?>">
                    <button class="h-search-btn">
                        <img src="/svg/search.svg" alt="">
                    </button>
                </div>
            </form>

            <br>
            <ul class="nav nav-tabs" id="searchTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="product-tab" data-toggle="tab" href="#product" role="tab" aria-controls="product" aria-selected="true">
                        <?= t('Products') ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="category-tab" data-toggle="tab" href="#category" role="tab" aria-controls="category" aria-selected="false">
                        <?= t('Categories') ?>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="page-tab" data-toggle="tab" href="#page" role="tab" aria-controls="page" aria-selected="false">
                        <?= t('Information') ?>
                    </a>
                </li>
            </ul>
            <div class="tab-content pt-3" id="searchTabContent">
                <div class="tab-pane fade show active" id="product" role="tabpanel" aria-labelledby="product-tab">
                    <?php if ($dataProvider) {
                        echo ListView::widget([
                            'dataProvider' => $dataProvider,
                            'itemView' => '/catalog/_item',
                            'options' => [
                                'tag' => 'div',
                                'class' => 'product-list'
                            ],
                            'itemOptions' => [
                                'tag' => 'div',
                                'class' => 'pr-item-col search-pr-item-col',
                            ],
                            'pager' => [
                                'maxButtonCount' => 3,
                            ],
                            'layout' => "{items}\n<div class='clearfix'></div>{pager}",
                        ]) ;
                    } ?>
                </div>
                <div class="tab-pane fade" id="category" role="tabpanel" aria-labelledby="category-tab">
                    <?php if ($categories){ ?>
                        <?php foreach ($categories as $category) { ?>
                            <a href="<?= url(['/catalog', 'slug' => $category->slug]) ?>" class="dashed d-inline-block mb-3"><?= $category->title ?></a><br>
                        <?php } ?>
                    <?php } else { ?>
                        <span><?= t('No results found.', 'yii') ?></span>
                    <?php } ?>
                </div>
                <div class="tab-pane fade" id="page" role="tabpanel" aria-labelledby="page-tab">
                    <?php if ($pages){ ?>
                        <?php foreach ($pages as $key => $model) { ?>
                            <h6 class="mt-4">
                                <strong><?= t($key) ?></strong>
                            </h6>
                            <a href="<?= url($model->searchUrl) ?>" class="dashed"><?= $model->title ?></a>
                        <?php } ?>
                    <?php } else { ?>
                        <span><?= t('No results found.', 'yii') ?></span>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>

</div>





