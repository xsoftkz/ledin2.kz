<?php
?>

<div class="main-image-wrap">
    <div class="overlay"></div>

    <div class="main-offer__wrap">
        <h1 class="main-offer"><?= txt('main_offer') ?></h1>
        <a data-fancybox data-src="#feedback-modal" href="javascript:;"
           class="btn btn-lg btn-main btn-rounded main-offer__link">
            <?= t('Send order') ?>
        </a>
    </div>
</div>

