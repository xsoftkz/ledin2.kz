<?php

use frontend\models\Feedback;
use himiklab\yii2\recaptcha\ReCaptcha2;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\widgets\MaskedInput;

$model = new Feedback();
$model->type_id = Feedback::LIDS;

?>

<div class="main-lids-form bg-light section-item">

    <div class="container">
        <div class="row f-aic">
            <div class="col-lg-3 offset-lg-2 text-center order-lg-1 order-2">
                <div class="main-lids__title">
                    <span>Получите Бесплатно</span>
                </div>
                <div class="main-lids__file">
                    <span>pdf-файл</span>
                </div>
                <div class="main-lids__catalog--name">
                    "Дизайн Код"
                </div>
                <div class="main-lids__catalog--desc">
                    <span>С помощью данной Методички</span>
                    <span>Вы узнаете правила правила архитектурной подсветки именно</span>
                    <span> для вашего сооружения.</span>
                </div>

                <div class="main-lids__form">
                    <?php $form = ActiveForm::begin(['id' => 'form-lids', 'action' => ['/site/feedback']]); ?>

                    <div>
                        <?=  $form->field($model, 'username')->hiddenInput(['value' => 'не задано', 'id' => 'lids-username'])->label(false); ?>
                        <?=  $form->field($model, 'email')->hiddenInput(['value' => 'не задано', 'id' => 'lids-email'])->label(false); ?>
                        <?= $form->field($model, 'type_id')->hiddenInput(['id' => 'lids-type_id'])->label(false); ?>
                        <?= $form->field($model, 'link')->hiddenInput(['value' => Yii::getAlias('@frontendWebroot').'/uploads/files/dizayn-kod-goroda.pdf', 'id' => 'lids-link'])->label(false); ?>

                        <?=  $form->field($model, 'phone')
                            ->widget(MaskedInput::className(), [
                                'mask'=> '+7 (999) 999-99-99',
                                'options' => [
                                    'id' => 'lids-phone',
                                ],
                            ])->label(t('You phone')); ?>


                            <?= $form->field($model, 'reCaptcha')->widget(
                                ReCaptcha2::class,
                                [
                                    'siteKey' => Feedback::RECAPTCHA_SITE_KEY,
                                    'id' => 'lids-reCaptcha',
                                ]
                            )->label(false) ?>

                        <div>
                            <?=  Html::submitButton('Скачать методичку', ['class' => 'btn btn-lg btn-outline-main w-100']); ?>
                        </div>

                    </div>

                    <?php  ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-lg-5 text-center order-lg-2 order-1">
                <img src="/img/lids-img.png" alt="">
            </div>
        </div>
    </div>
</div>
