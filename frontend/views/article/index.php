<?php
use yii\widgets\ListView;

/**
 * @var $categories \frontend\models\ContentCategory[]
 * @var $category \frontend\models\ContentCategory
 */

$this->title = t('Articles').' | '. siteName();

$this->params['breadcrumbs'][] = t('Articles');
$this->registerMetaTag(['name' => 'description', 'content' => 'Полезные статьи о светодиодном освещении. Ответим на Ваши вопросы и поможем сделать правильный выбор при покупке светотехнического оборудования.']);

?>
<div class="container">
    <h1 class="page-title"><?= t('Articles') ?></h1>

    <div class="article-categories mb-3">
        <?php if ($categories): ?>
            <a href="<?= url(['/article']) ?>"
               class="btn btn-sm mr-1 mb-1 <?= (!$category) ? 'btn-main' : 'btn-outline-main' ?>">
                <?= t('All') ?>
            </a>
            <?php foreach ($categories as $cat): ?>
                <a href="<?= url(['/article/index', 'c' => $cat->slug]) ?>"
                   class="btn btn-sm mr-1 mb-1 <?= ($category && $category->id == $cat->id) ? 'btn-main' : 'btn-outline-main' ?>">
                    <?= $cat->getTitle() ?>
                </a>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'options' => [
            'class' => 'news-list row'
        ],
        'itemOptions' => [
            'class' => 'col-lg-3 mb-3 ',
        ],
        'layout' => "{items}\n{pager}",
    ]) ?>
</div>

<hr>
<script src="//web.webformscr.com/apps/fc3/build/loader.js" sp-form-id="eb57b7a48513e8e3e0b0d60a753739e586fbc9e8413d081bc755aa5d32643c7a"></script>

<style>
	.feedback {
		display: none;
	}
</style>