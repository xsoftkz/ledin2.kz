<?php
?>

<div class="news-item card hoverable h-100">
    <a href="<?= url(['/article/view', 'slug' => $model->slug]) ?>" class="d-block">
        <img src="<?= thumb($model->imagePath, 600) ?>" alt="">
        <span class="d-block card-body">
            <div class="d-block mb-2">
                <div class="row f-aic">
					<div class="col-6 text-accent">
						<?= Yii::$app->formatter->asDate($model->created_at) ?>
					</div>
					<div class="col-6 text-right">
						<img src="/svg/eye.svg" style="width: 20px" />
						<?= $model->views ?>
					</div>
				</div>
            </div>
            <span class="d-block">
                <?= $model->title ?>
            </span>
        </span>
    </a>
</div>






