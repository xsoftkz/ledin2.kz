<?php


use frontend\models\Content;

/** @var Content $model  */
$this->title = $model->getSeoTitle();

$this->params['breadcrumbs'][] = ['label' => t('Articles'), 'url' => ['/article']];
$this->params['breadcrumbs'][] = $model->title;

$this->registerMetaTag(['name'=> 'keywords', 'content' => $model->meta_keywords]);
$this->registerMetaTag(['name'=> 'description', 'content' => $model->meta_description]);

?>

<div class="content-header blog-column">
    <h1 class="page-title text-center"><?= $model->title?></h1>
</div>
<div class="content-page">
    <div class="content-column">
        <div class="content-body">
            <?= $model->desc?>
        </div>
        <div>
            <div id="hypercomments_widget"></div>
            <script type="text/javascript">
                _hcwp = window._hcwp || [];
                _hcwp.push({widget:"Stream", widget_id: 107651});
                (function() {
                    if("HC_LOAD_INIT" in window)return;
                    HC_LOAD_INIT = true;
                    var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
                    var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
                    hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/107651/"+lang+"/widget.js";
                    var s = document.getElementsByTagName("script")[0];
                    s.parentNode.insertBefore(hcc, s.nextSibling);
                })();
            </script>
            <a href="http://hypercomments.com" class="hc-link" title="comments widget">comments powered by HyperComments</a>
        </div>
    </div>
</div>
<div style="position:relative">
    <script src="//web.webformscr.com/apps/fc3/build/loader.js" async sp-form-id="eb57b7a48513e8e3e0b0d60a753739e586fbc9e8413d081bc755aa5d32643c7a"></script>
</div>
