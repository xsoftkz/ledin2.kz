<?php
use yii\helpers\Html;
?>
<div class="mb-3 shadow-sm">
    <div class="vacancy-item p-4">
        <div class="row align-items-center">
            <div class="col-lg-2 mb-2">
                <?= Yii::$app->formatter->asDate($model->created_at) ?>
            </div>
            <div class="col-lg-3 mb-3">
                <strong>
                    <?= h($model->title) ?>
                </strong>
            </div>
            <div class="col-lg-5 mb-4">
                <?= substr($model->intro, 0 , 200) ?>
            </div>
            <div class="col-lg-2 text-lg-center">
                <?= Html::a(t('Readmore'), ['/vacancy/view', 'slug' => $model->slug], ['class' => 'btn btn-outline-main']) ?>
            </div>
        </div>
    </div>
</div>







