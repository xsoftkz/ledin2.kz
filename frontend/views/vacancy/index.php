<?php
use yii\widgets\ListView;
use common\models\Settings;

$this->title = Settings::getValue('site_name');

$this->params['breadcrumbs'][] = t('Vacancies');
$this->registerMetaTag(['name' => 'description', 'content' => 'Сейчас наша команда в поиске молодых и перспективных специалистов. Смотрите актуальные вакансии на сайте.']);

?>
<div class="container">
    <h1 class="page-title"><?= t('Career') ?></h1>
    
    <div class="text-center mb-4">
        <img src="/img/team.jpg" alt="" class="shadow">
    </div>

	<div>
		<?= txt('vacancy_desc') ?>
	</div>
	<br/>
	
    <div class="mb-3 shadow-sm d-none d-lg-block">
        <div class="vacancy-item p-4 font-weight-bold">
            <div class="row align-items-center">
                <div class="col-lg-2">
                    <?= t('Date') ?>
                </div>
                <div class="col-lg-3">
                    <?= t('Position') ?>
                </div>
                <div class="col-lg-5">
                    <?= t('Additionally') ?>
                </div>
                <div class="col-lg-2 text-center">
                </div>
            </div>
        </div>
    </div>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'layout' => "{items}\n{pager}",
    ]) ?>
</div>

<style>
	.feedback {
		display: none;
	}
</style>