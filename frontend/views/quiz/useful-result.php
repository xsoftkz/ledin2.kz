<?php
$this->title = t('Quiz');
?>

<div class="quiz-wrap" style="background-image: url(/img/quiz1.jpg); background-position: center bottom;">
    <div class="quiz-body">
        <h1>
            Всё записали
        </h1>
        <p>
            Хотите посмотреть, что интересного есть на сайте?
        </p>
        <br>
        <a href="<?= url(['/']) ?>" class="btn btn-secondary">
            Да, хочу
        </a>
    </div>
</div>


