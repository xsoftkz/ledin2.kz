<?php
$this->title = t('Quiz');
?>

<div class="quiz-wrap" style="background-image: url(/img/quiz2.jpg); background-position: right bottom;">
    <div class="quiz-body text-center">
        <h1>
            Всё записали
        </h1>
        <p>
            Прочитаем и постараемся стать лучше. <br>
            Хотите посмотреть, что интересного есть на сайте?
        </p>
        <br>
        <a href="<?= url(['/']) ?>" class="btn btn-secondary">
            Да, хочу
        </a>
    </div>
</div>

