<?php

use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;


$this->title = t('Quiz');
?>

<div class="quiz-wrap" style="background-image: url(/img/quiz1.jpg); background-position: center bottom;">
    <div class="quiz-body">
        <h1>
            Спасибо за ответ!
        </h1>
        <p>
            Пройдёте небольшой опрос?
        </p>
        <br>
        <a href="javascript:;" class="btn btn-secondary" data-fancybox data-src="#quizModal">
            Пройти опрос
        </a>
    </div>
</div>

<div style="display: none;" id="quizModal" class="shadow">
    <div class="quiz-modal-wrap">
        <?php $form = ActiveForm::begin(); ?>

        <?=  $form->field($model, 'question1')->textarea(['rows' => 2]); ?>
        <br>
        <?=  $form->field($model, 'question2')->textarea(['rows' => 2]); ?>

        <div class="text-center">
            <br>
            <?=  Html::submitButton(t('Send'), ['class' => 'btn btn-lg btn-outline-main']); ?>
        </div>

        <?php  ActiveForm::end(); ?>
    </div>
</div>
