<?php

use frontend\widgets\portfolio\PortfolioSorterWidget;
use yii\widgets\ListView;

if ($year && $year != 'all') {
    $this->title = t('Portfolio {year}', 'app', ['year' => $year]).$category->title;

} else {
    $this->title = t('Portfolio').' | '.$category->title;
}

$this->params['breadcrumbs'][] = ['label' => t('Portfolio'), 'url' => ['/portfolio/index']];
$this->params['breadcrumbs'][] = $category->title;

$this->registerMetaTag(['name' => 'description', 'content' => $category->metaDescription]);

?>
<div class="container">

    <div class="row align-items-center pt-2 pb-2">
        <div class="col-lg-9 col-12">
            <h1 class="page-title"><?= $category->title ?></h1>
        </div>
        <div class="col-lg-3 col-12">
            <?= PortfolioSorterWidget::widget(['category' => $category, 'defaultYear' => date("Y")]) ?>
        </div>
    </div>
	
	<p><?= $category->desc ?></p>
    <br>

    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item',
        'options' => [
            'class' => 'portfolio-list row'
        ],
        'itemOptions' => [
            'class' => 'col-lg-3 mb-3',
        ],
        'layout' => "{items}\n{pager}",
    ]) ?>
</div>