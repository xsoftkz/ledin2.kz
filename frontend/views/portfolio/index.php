<?php

$this->title .= t('Portfolio').' || '.siteName();

$this->params['breadcrumbs'][] = t('Portfolio');

$this->registerMetaTag(['name'=> 'keywords', 'content' => '']);
$this->registerMetaTag(['name'=> 'description', 'content' => 'Ознакомьтесь с нашими проектами в сферах концепции визуализации, фасадное освещение, наружное освещение и внутреннее освещение.']);

?>

<div class="container">
    <h1 class="page-title"><?= t('Portfolio') ?></h1>
    <div class="row">
        <?php if ($models){ ?>
            <?php foreach ($models as $model){ ?>
                <div class="col-lg-3">
                    <a href="<?= url(['/portfolio/index', 'slug' => $model->slug]) ?>" class="hoverable d-block p-2">
                        <img src="<?= thumb($model->imagePath, 400, 400) ?>" alt="" class="mb-2">
                        <span class="d-block mb-2 p-2 text-center">
                            <?= $model->title ?>
                        </span>
                    </a>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>

