<?php
use frontend\models\Content;

/** @var Content $model */
$this->title = $model->getSeoTitle();

$this->params['breadcrumbs'][] = ['label' => t('Portfolio'), 'url' => ['/portfolio/index']];
if ($model->category) {
    $this->params['breadcrumbs'][] = ['label' => $model->category->title, 'url' => ['/portfolio/index', 'slug' => $model->category->slug]];
}
$this->params['breadcrumbs'][] = $model->title;

$this->registerMetaTag(['name'=> 'keywords', 'content' => $model->metaKeywords]);
$this->registerMetaTag(['name'=> 'description', 'content' => $model->metaDescription]);

?>

<div class="content-header blog-column">
    <h1 class="page-title text-center"><?= $model->title?></h1>
</div>
<div class="content-page">
    <div class="content-column">
        <div class="content-body">
            <?= $model->desc?>
            <br>
            <?php if ($images = $model->images) { ?>
                <div class="row">
                    <?php foreach ($images as $item) { ?>
                        <div class="col-lg-6">
                            <a href="<?= $item->wmImage ?>" data-fancybox="images" data-caption="<?= $model->title ?>" class="portfolio-gallery__item mb-3 d-block">
                                <img src="<?= thumb($item->wmImagePath, 600, 400, true) ?>" alt="<?= $model->title ?>" class="hoverable" />
                            </a>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($relatedProducts) { ?>
                <h4 class="text-accent text-center mt-4 mb-4">
                    <?= t('Installed products') ?>
                </h4>
                <div class="product-list portfolio-product-list">
                    <?php foreach ($relatedProducts as $product) { ?>
                        <div class="pr-item-col">
                            <?= $this->render('/catalog/_item', [
                                'model' => $product
                            ]) ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>