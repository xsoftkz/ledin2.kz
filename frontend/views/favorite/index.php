<?php
use yii\helpers\Url;

$this->title = t('Favorites');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">

    <div class="row">
        <div class="col-lg-10">
            <h1 class="page-title font-primary"><?= t('Favorites') ?></h1>
        </div>
        <div class="col-lg-2 text-lg-right">
            <a href="<?= Url::toRoute(['/favorite/clear']) ?>" class="btn btn-sm btn-light"><?= t('Clear favorites list') ?></a>
        </div>
    </div>

    <?php if ($models){ ?>

        <div class="favorite-list product-list">
            <?php foreach ($models as $model){ ?>
                <div class="pr-item-col">
                    <?= $this->render('@app/views/catalog/_model_item', [
                        'model' => $model->product
                    ]); ?>
                </div>
            <?php } ?>
        </div>

    <?php } else { ?>
        <br>

        <h6><?= t('The list of products is empty') ?></h6>

    <?php } ?>

</div>


