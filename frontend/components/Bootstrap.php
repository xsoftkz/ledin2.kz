<?php
namespace frontend\components;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
				 
                'contacts' => 'site/contact',
                'about' => 'site/about',
                'search' => 'site/search',

                '<controller:(catalog)>/filter/<slug>' => '<controller>/filter',
                '<controller:(catalog)>/filter' => '<controller>/filter',
                '<controller:(catalog)>/<slug>' => '<controller>/index',
                '<controller:(catalog)>/view/<slug>' => '<controller>/view',
                '<controller:(catalog)>/info/<id>' => '<controller>/info',
                '<controller:(catalog)>' => '<controller>/index',


                '<controller:(page|vacancy|news|partners|services|article)>' => '<controller>/index',
                '<controller:(page|vacancy|news|partners|services|article)>/<slug>' => '<controller>/view',

                '<controller:(portfolio)>/<slug>' => '<controller>/index',
                '<controller:(portfolio)>' => '<controller>/index',
                '<controller:(portfolio)>/view/<slug>' => '<controller>/view',

                '<controller:(feedback)>' => '<controller>/index',

            ]
        );
    }
}