<?php
namespace frontend\components;

use yii\base\Component;

class SeoComponent extends Component {

    public function init(){
        parent::init();
    }

    public function putFacebookMetaTags($tags){
        foreach($tags as $prop => $content){
            \Yii::$app->view->registerMetaTag([
                'property' => $prop,
                'content' => $content,
            ], $prop);
        }
    }

    public function putTwitterMetaTags($tags){
        foreach($tags as $name => $content){
            \Yii::$app->view->registerMetaTag([
                'name' => $name,
                'content' => $content,
            ], $name);
        }
    }

}