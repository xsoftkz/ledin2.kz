<?php
namespace frontend\components;


class Roistat
{
    private $_key = 'OTdkOGZkODc5NzNhNTUwZTg0MmRhZmNiZmFkMGI0Mjg6MTU1ODI5';

	public function createProxyLead($name, $phone, $email, $leadName, $comment, $is_skip_sending = 1)
		{
			if (mb_strlen($phone) || mb_strlen($email)) {
				$roistatData = array(
						'roistat' => array_key_exists('roistat_visit', $_COOKIE) ? $_COOKIE['roistat_visit'] : null,
						'key' => $this -> _key,
						'title' => $leadName,
						'comment' => $comment,
						'name' => $name,
						'phone' => $phone,
						'email' => $email,
						'is_skip_sending' => $is_skip_sending,
						'fields' => [
							'UF_CRM_1603183552' => '{landingPage}',
							'UF_CRM_1603183577' => '{source}',
							'UF_CRM_1603183599' => '{city}',
							'UF_CRM_1603183631' => '{utmSource}',
							'UF_CRM_1603183647' => '{utmMedium}',
							'UF_CRM_1603183665' => '{utmCampaign}',
							'UF_CRM_1603183701' => '{utmTerm}',
							'UF_CRM_1603183721' => '{utmContent}'
						]
				);	
				file_get_contents("https://cloud.roistat.com/api/proxy/1.0/leads/add?" . http_build_query($roistatData));
			}
		}
}