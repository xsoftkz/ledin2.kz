<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%lang}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $locale
 * @property string $name
 * @property integer $default
 * @property integer $date_update
 * @property integer $date_create
 */
class Lang extends ActiveRecord
{
    static $current = null;
    const DEFAULT_LANG = 'ru';

    const LANG_RU = 'ru';
    const LANG_KZ = 'kz';
    const LANG_EN = 'en';

    public static function tableName()
    {
        return '{{%lang}}';
    }

    public function rules()
    {
        return [
            [['url', 'locale', 'name'], 'required'],
            [['status'], 'integer'],
            [['url', 'locale', 'name'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'locale' => Yii::t('app', 'locale'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public static function getList(){
        return ArrayHelper::map(Lang::find()->orderBy('id DESC')->all(), 'id', 'name');
    }
    //Получение текущего объекта языка
    static function getCurrent()
    {
        if( self::$current === null){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    //Установка текущего объекта языка и локаль пользователя
    static function setCurrent($url = null)
    {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->locale;
    }

    //Получения объекта языка по умолчанию
    static function getDefaultLang()
    {
        return Lang::findOne(['id' => self::DEFAULT_LANG]);
    }


    //Получения объекта языка по буквенному идентификатору
    static function getLangByUrl($url = null)
    {
        if ($url === null) {
            return null;
        } else {
            $language = Lang::findOne(['id' => $url]);
            if ( $language === null ) {
                return null;
            }else{
                return $language;
            }
        }
    }

}
