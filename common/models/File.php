<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%file}}".
 *
 * @property int $id
 * @property string $filename
 * @property int $item_id
 * @property string $model_name
 * @property string|null $title_ru
 * @property string|null $title_kz
 * @property string|null $title_en
 * @property string|null $folder
 * @property int|null $sort_index
 */
class File extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%file}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file', 'item_id', 'model_name', 'title_ru'], 'required'],
            [['item_id', 'sort_index'], 'integer'],
            [['filename', 'model_name', 'title_ru', 'title_kz', 'title_en', 'folder'], 'string', 'max' => 255],
            [['file'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'item_id' => 'Item ID',
            'model_name' => 'Model Name',
            'title_ru' => 'Название RU',
            'title_kz' => 'Название KZ',
            'title_en' => 'Название EN',
            'folder' => 'Folder',
            'sort_index' => 'Sort Index',
        ];
    }

    public function afterDelete()
    {
        parent::afterDelete(); //
        if ($this->checkFileExists()){
            unlink($this->filePath);
        }
    }

    public function uploadFile()
    {
        $result = false;
        $this->file = UploadedFile::getInstance($this, 'file');

        if ($this->file && $this->validate()) {
            $this->filename = uniqid() . '.' . $this->file->extension;
            $this->file->saveAs(Yii::getAlias('@files/'.$this->folder . '/' . $this->filename));
            $result = $this->save(false);
        }
        return $result;
    }


    public function getFileLink(){
        if ($this->checkFileExists()) {
            $file = ($this->folder) ? $this->folder.'/'.$this->filename : $this->filename;
            return Yii::getAlias('@filesWebroot').'/' . $file;
        } else {
            return null;
        }
    }


    public function getFilePath(){
        if ($this->checkFileExists()) {
            $file = ($this->folder) ? $this->folder.'/'.$this->filename : $this->filename;
            return Yii::getAlias('@files').'/' . $file;
        } else {
            return null;
        }
    }


    public function checkFileExists(){
        if ($this->filename) {
            $file = ($this->folder) ? $this->folder.'/'.$this->filename : $this->filename;
            $filePath = Yii::getAlias('@files/') . ($file);
            if (file_exists($filePath)) {
                return true;
            } else {
                return false;
            }
        }
    }



}
