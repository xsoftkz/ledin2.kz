<?php

namespace common\models;

use common\models\helper\ImageHelper;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%image}}".
 *
 * @property integer $id
 * @property string $filename
 * @property integer $item_id
 * @property string $model_name
 * @property string $title
 * @property integer $sort_index
 * @property integer $folder
 */
class Image extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%image}}';
    }

    public function rules()
    {
        return [
            [['filename', 'item_id', 'model_name'], 'required'],
            [['item_id', 'sort_index'], 'integer'],
            [['filename', 'model_name', 'title', 'folder'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Filename',
            'folder' => 'Folder',
            'item_id' => 'Item ID',
            'model_name' => 'Model Name',
            'title' => 'Title',
            'sort_index' => 'Sort Index',
        ];
    }


    public function behaviors()
    {
        return [
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort_index'
            ],
        ];

    }


    public function afterDelete()
    {
        parent::afterDelete(); //
        if ($this->checkFileExists()){
            unlink($this->filePath);
        }
    }

    public static function create($filename, $model_name, $item_id, $folder = false){
        $image = new self;
        $image->filename = $filename;
        $image->model_name = $model_name;
        $image->folder = $folder;
        $image->item_id = $item_id;
        $image->save();
    }


    public function getImage()
    {
        return ImageHelper::getImage($this->filename, $this->folder);
    }

    public function getWmImage()
    {
        return ImageHelper::getWmImage($this->filename, $this->folder);
    }

    public function getImagePath()
    {
        return ImageHelper::getImagePath($this->filename, $this->folder);
    }

    public function getWmImagePath()
    {
        return ImageHelper::getWmImagePath($this->filename, $this->folder);
    }


    public function getFileLink(){
        if ($this->checkFileExists()) {
            $file = ($this->folder) ? $this->folder.'/'.$this->filename : $this->filename;
            return Yii::getAlias('@imagesWebroot').'/' . $file;
        } else {
            return null;
        }
    }


    public function getFilePath(){
        if ($this->checkFileExists()) {
            $file = ($this->folder) ? $this->folder.'/'.$this->filename : $this->filename;
            return Yii::getAlias('@images').'/' . $file;
        } else {
            return null;
        }
    }


    public function checkFileExists(){
        if ($this->filename) {
            $file = ($this->folder) ? $this->folder.'/'.$this->filename : $this->filename;
            $filePath = Yii::getAlias('@images/') . ($file);
            if (file_exists($filePath)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
