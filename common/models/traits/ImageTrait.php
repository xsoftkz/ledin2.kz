<?php

namespace common\models\traits;

use common\models\helper\ImageHelper;
use common\models\Image;
use yii\web\UploadedFile;
use Yii;

trait ImageTrait
{
    public function getImagePath()
    {
        return ImageHelper::getImagePath($this->photo, $this::IMAGE_FOLDER);
    }

    public function getIconImagePath()
    {
        return ImageHelper::getImagePath($this->icon, $this::IMAGE_FOLDER);
    }

    public function getImage()
    {
        return ImageHelper::getImage($this->photo, $this::IMAGE_FOLDER);
    }

    public function getIconImage()
    {
        return ImageHelper::getImage($this->icon, $this::IMAGE_FOLDER);
    }

    public function getImages()
    {
        return Image::find()->where(['model_name' => $this::MODEL_NAME, 'item_id' => $this->id])->orderBy('sort_index')->all();
    }


    public function getAllImages()
    {
        $image = null;
        $images = $this->getImages();

        if ($this->photo) {
            $image = new Image(['filename' => $this->photo, 'item_id' => $this->id, 'model_name' => $this::MODEL_NAME, 'folder' => $this::IMAGE_FOLDER]);
            array_unshift($images, $image);
        }

        if (!$this->photo && !$images) {
            $image = new Image(['filename' => 'placeholder.jpg', 'item_id' => $this->id, 'model_name' => $this::MODEL_NAME, 'folder' => $this::IMAGE_FOLDER]);
            array_unshift($images, $image);
        }

        return $images;
    }


    public function saveImages()
    {
        $this->file = UploadedFile::getInstances($this, 'file');

        if ($this->file && $this->validate()) {
            foreach ($this->file as $file) {
                $filename = uniqid() . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@images/'.self::IMAGE_FOLDER . '/' . $filename));
                Image::create($filename, self::MODEL_NAME, $this->id, self::IMAGE_FOLDER);
            }
        }
    }
}