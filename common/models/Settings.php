<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property integer $id
 * @property string $param_name
 * @property string $param_value
 * @property string $comment
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param_name'], 'unique'],
            [['param_name', 'param_value'], 'required'],
            [['param_value', 'comment'], 'string'],
            [['param_name', 'module'], 'string', 'max' => 255],
            [['status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'param_name' => Yii::t('app', 'Системная переменная'),
            'param_value' => Yii::t('app', 'Значение'),
            'module' => Yii::t('app', 'Модуль'),
            'comment' => Yii::t('app', 'Комментарий'),
            'status_id' => Yii::t('app', 'Status ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public static function getValue($param_name) {
        $model = self::findOne(['param_name' => $param_name]);
        return ($model) ? $model->param_value : null;
    }

}
