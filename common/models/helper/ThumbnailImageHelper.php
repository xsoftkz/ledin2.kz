<?php
/**
 * @link https://github.com/himiklab/yii2-easy-thumbnail-image-helper
 * @copyright Copyright (c) 2014 HimikLab
 * @license http://opensource.org/licenses/MIT MIT
 */

namespace common\models\helper;

use himiklab\thumbnail\EasyThumbnailImage;
use himiklab\thumbnail\FileNotFoundException;
use yii\helpers\Html;

class ThumbnailImageHelper extends EasyThumbnailImage
{
    const NOT_FOUND = 'notFound';
    
    protected static function errorHandler($error, $filename)
    {
        if ($error instanceof FileNotFoundException) {
            return Html::img('@frontendWebroot/images/placeholder.png', ['class' => 'img-fluid']);
        }
        else {
            $filename = basename($filename);
            return Html::img("@frontendWebroot/images/$filename");
        }
    }
}