<?php

namespace common\models\helper;

class SendPulseData
{
    public $email;
    public $phone;

    public function __construct($email, $phone)
    {
        $this->email = $email;
        $this->phone = $phone;
    }
}