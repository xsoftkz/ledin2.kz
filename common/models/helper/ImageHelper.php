<?php

namespace common\models\helper;

use yii\base\Model;
use Yii;

class ImageHelper extends Model
{
    const PLACEHOLDER_IMG = 'placeholder.jpg';

    public static function getImage($filename, $folder = false){
        if (self::checkFileExists($filename, $folder)) {
            $image = ($folder) ? $folder.'/'.$filename : $filename;
            return Yii::getAlias('@imagesWebroot').'/' . $image;
        } else {
            return self::getPlaceholderUrl();
        }
    }

    public static function getWmImage($filename, $folder = false){
        if (self::checkFileExists($filename, $folder)) {
            $image = ($folder) ? $folder.'/'.$filename : $filename;
            return Yii::getAlias('@wmWebroot').'/' . $image;
        } else {
            return self::getPlaceholderUrl();
        }
    }


    public static function getImagePath($filename, $folder = false){
        if (self::checkFileExists($filename, $folder)) {
            $image = ($folder) ? $folder.'/'.$filename : $filename;
            return Yii::getAlias('@images/') . $image;
        } else {
            return self::getPlaceholderPath();
        }
    }

    public static function getWmImagePath($filename, $folder = false){
        if (self::checkFileExists($filename, $folder)) {
            $image = ($folder) ? $folder.'/'.$filename : $filename;
            return Yii::getAlias('@wm/') . $image;
        } else {
            return self::getPlaceholderPath();
        }
    }


    public static function checkFileExists($filename, $folder = false){
        if ($filename) {
            $image = ($folder) ? $folder.'/'.$filename : $filename;
            $filePath = Yii::getAlias('@images/') . ($image);
            if (file_exists($filePath)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getPlaceholderPath(){
        return Yii::getAlias('@images/') .self::PLACEHOLDER_IMG;
    }

    public static function getPlaceholderUrl(){
        return Yii::getAlias('@imagesWebroot/') .self::PLACEHOLDER_IMG;
    }
}