<?php

namespace common\models\helper;


use yii\httpclient\Client;

class SendPulseClient
{
    const URL = 'https://events.sendpulse.com/events/id/d7f570b265184633895840f02eea229a/7119686';
    private $data;

    public function __construct($email, $phone)
    {
        $this->data = new SendPulseData($email, $phone);
    }

    public function sendData(){
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('POST')
            ->setFormat(Client::FORMAT_RAW_URLENCODED)
            ->setUrl(self::URL)
            ->setData($this->data)
            ->send();

		if ($response->isOk) {
            return true;
        } else {
            return false;
        }
    }
}

