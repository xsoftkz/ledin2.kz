<?php

namespace common\models\helper;

use Yii;
use yii\imagine\Image;

class WatermarkHelper
{
    public static function watermark($imagePath, $newImagePath)
    {
        if (!file_exists($imagePath)) {
            return;
        }

        if (file_exists($newImagePath)) {
            return;
        }

        $image = Image::getImagine()->open($imagePath);
        $imgSize = $image->getSize();

        $imgWidth = $imgSize->getWidth();

        if ($imgWidth >= 800) {
            $wm = 'wm.png';
        } elseif ($imgWidth > 500 && $imgWidth < 800) {
            $wm = 'wm-md.png';
        } elseif ($imgWidth > 300 && $imgWidth < 500) {
            $wm = 'wm-sm.png';
        } else {
            $wm = 'wm-xs.png';
        }

        $watermark = Image::getImagine()->open(Yii::getAlias('@frontend/web/img/'.$wm));
        $wtmSize = $watermark->getSize();

        if ($imgSize->getWidth() - $wtmSize->getWidth() > 0) {
            $x = ($imgSize->getWidth() - $wtmSize->getWidth()) / 2;
        } else {
            $x = 0;
        }
        if ($imgSize->getHeight() - $wtmSize->getHeight() > 0) {
            $y = ($imgSize->getHeight() - $wtmSize->getHeight()) / 2;
        } else {
            $y = 0;
        }

        Image::watermark($imagePath, $watermark, [$x, $y])->save($newImagePath);
    }
}