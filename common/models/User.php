<?php
namespace common\models;

use backend\modules\rbac\interfaces\UserRbacInterface;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * User model
 */
class User extends UserIdentity
{
    public static function getCurrent()
    {
        if(Yii::$app->user->identity){
            return self::findOne(Yii::$app->user->identity->getId());
        } else {
            return null;
        }
    }

    public static function createUser($name, $phone, $pass){

        $user = User::findByPhone($phone);

        if($user){
            return $user;
        } else {
            $user = new User();
            $user->firstname = $name;
            $user->phone = $phone;
            $user->role = User::ROLE_USER;
            $user->activated = User::STATUS_ACTIVATED;
            $user->status = User::STATUS_ACTIVE;
            $user->setPassword($pass);
            $user->generateAuthKey();
            $user->generateEmailActivationKey();

            if ($user->save()) {
                return $user;
            } else {
                return null;
            }
        }
    }

    public function getFormatPhone(){
        return substr($this->phone, 2);
    }

}
