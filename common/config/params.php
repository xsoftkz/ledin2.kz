<?php
return [
    'appDomain' => 'ledin.kz',
	'companyName' => 'LEDIN',
    'senderEmail' => 'ledin@xsoft.kz',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
];
