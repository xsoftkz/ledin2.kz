<?php
namespace common\modules\eav\widgets;

use yii\base\Widget;

class EntityParamsWidget extends Widget
{
    public $models;

    public function init()
    {
        parent::init();
        echo $this->render('entity-params', [
            'models' => $this->models
        ]);
    }
}