<?php

namespace common\modules\eav\widgets;

class EntityParamsFormWidget extends \yii\base\Widget
{
    public $formModel;
    public function init()
    {
        parent::init();
        echo $this->render('entity-params-form', [
            'formModel' => $this->formModel,
        ]);
    }
}