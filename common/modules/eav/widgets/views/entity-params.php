<?php
?>

<?php if ($models) {?>
    <table class="table table-striped table-sm">
        <?php  foreach ($models as $key => $value) { ?>
            <?php if (!empty($value['value']) || !empty($value['image'])) { ?>
                <tr>
                    <td>
                        <span><?= $value['param'] ?></span>
                    </td>
                    <td>
                        <?php if ($value['image']){ ?>
                            <img src="<?= $value['image'] ?>" alt="" class="img-fluid" style="width: 50px">
                        <?php } ?>
                        <span><?= $value['value'] ?></span>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>
    </table>
<?php } ?>
