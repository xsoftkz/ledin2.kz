<?php

use common\modules\eav\models\Param;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */

$params = $formModel->params;
$paramModels = $formModel->paramModels;

$form = ActiveForm::begin();
?>

<?php if ($params){ ?>

    <?php foreach ($params as $param) { ?>

        <?php if ($param->type_id == Param::TYPE_CHECKBOX) { ?>
            <?= $form->field($paramModels[$param->id], '[' . $param->id . ']value_ru')
                ->checkbox()->label($param->title) ?>
        <?php } ?>
        <?php if ($param->type_id == Param::TYPE_STRING) { ?>
            <div class="row">
                <div class="col-lg-4">
                    <?= $form->field($paramModels[$param->id], '[' . $param->id . ']value_ru')->textInput()->label($param->title.' (RU)') ?>
                </div>
                <div class="col-lg-4">
                    <?= $form->field($paramModels[$param->id], '[' . $param->id . ']value_kz')->textInput()->label($param->title.' (KZ)') ?>
                </div>
                <div class="col-lg-4">
                    <?= $form->field($paramModels[$param->id], '[' . $param->id . ']value_en')->textInput()->label($param->title.' (EN)') ?>
                </div>
            </div>
        <?php } ?>
        <?php if ($param->type_id == Param::TYPE_TEXT) { ?>
            <?= $form->field($paramModels[$param->id], '[' . $param->id . ']value_ru')
                ->textarea()->label($param->title) ?>
        <?php } ?>
        <?php if ($param->type_id == Param::TYPE_SELECT) { ?>
            <div class="row">
                <div class="col-lg-6">
                    <?= $form->field($paramModels[$param->id], '[' . $param->id . ']value_ru')
                        ->dropDownList(ArrayHelper::map($param->options, 'id', 'value_ru'), ['prompt' => '-'])->label($param->title) ?>
                </div>
            </div>
        <?php } ?>
        <?php if ($param->type_id == Param::TYPE_CHECKBOXLIST) {
            if (!$param->optionTypeIsFile()) {
                echo $form->field($paramModels[$param->id], '[' . $param->id . ']value_ru')
                    ->checkboxList(ArrayHelper::map($param->options, 'id', 'value_ru'))->label($param->title);
            } else {
                $imgs = ArrayHelper::map($param->options, 'id', 'icon');
                echo $form->field($paramModels[$param->id], '[' . $param->id . ']value_ru',
                    ['template' => '<div class="ghs-pict"> {label}{input} </div>'])
                    ->checkboxList(Param::imageList($imgs), [ 'encode' => false ])->label($param->title);
            }
        } ?>

    <?php } ?>

<?php }?>



