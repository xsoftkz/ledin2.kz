<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\Param */

$this->title = 'Атрибуты. Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Атрибуты', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="param-update">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="btn-box btn-group">
                        <?= \backend\widgets\LangSwitchWidget::widget(['current' => $model->language, 'model_id' => $model->id]) ?>
                    </div>
                </div>
                <div class="col-lg-3 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> Отменить',
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить новую'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Удалить'),
                            ['delete', 'id' => $model->id], ['class' => 'btn btn-sm btn-default', 'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?')]) ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
            <div class="panel panel-default">
                <div class="panel-body form-panel-body">
                    <?= $this->render('_form', [
                        'model' => $model,
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <?php if ($model->hasOptions()) { ?>
                <?php echo $this->render('_option_form', [
                    'model' => $model,
                ]); ?>

                <?php echo $this->render('_options_list', [
                    'dataProvider' => $dataProvider,
                ]); ?>
            <?php } ?>
        </div>
    </div>
</div>
