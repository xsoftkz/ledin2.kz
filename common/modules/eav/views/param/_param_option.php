<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data']
]); ?>


<div class="row">
    <div class="col-lg-6">
        <?= $form->field($model, 'value_ru')->textInput(['maxlength' => true]); ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'value_kz')->textInput(['maxlength' => true]); ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'value_en')->textInput(['maxlength' => true]); ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'icon')->widget(
            FileAPI::className(), [
            'settings' => [
                'url' => ['fileapi-upload'],
                'elements' => ['preview' => ['width' => 100,'height' => 75]],
                'imageAutoOrientation' => false
            ],
        ]); ?>
    </div>
</div>

<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
