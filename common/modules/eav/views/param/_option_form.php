<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\modules\eav\models\ParamOption;
use vova07\fileapi\Widget as FileAPI;

$optionModel = new ParamOption(['param_id' => $model->id]);
?>
<h4>Опции</h4>
<div class="panel panel-default">
    <div class="panel-body form-panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'form-param-create',
            'action' => '/eav/param/create-option',
            'options' => ['enctype' => 'multipart/form-data']
        ]); ?>

        <?=  $form->errorSummary($optionModel);  ?>
        <?= $form->field($optionModel, 'param_id')->hiddenInput()->label(false) ?>

        <div class="row">
            <div class="col-lg-6">
                <?= $form->field($optionModel, 'value_ru')->textInput(['maxlength' => true]); ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($optionModel, 'value_kz')->textInput(['maxlength' => true]); ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($optionModel, 'value_en')->textInput(['maxlength' => true]); ?>
            </div>
            <div class="col-lg-6">
                <?= $form->field($optionModel, 'icon')->widget(
                    FileAPI::className(), [
                    'settings' => [
                        'url' => ['fileapi-upload'],
                        'elements' => ['preview' => ['width' => 100,'height' => 75]],
                        'imageAutoOrientation' => false
                    ],
                ]); ?>
            </div>
            <div class="col-lg-12">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-sm btn-success']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
