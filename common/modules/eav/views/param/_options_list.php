<?php
use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use common\modules\eav\models\common\Param;

?>

<div class="panel panel-default">
    <div class="panel-body form-panel-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'value_ru',
                'value_kz',
                'value_en',
                [
                    'label' => 'Иконка',
                    'format' => 'html',
                    'value' => function ($data) {
                        return Html::img($data->image, ['width' => 30]);
                    },
                ],
                [
                    'header' => 'Действия',
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'width:80px; text-align: center'],
                    'template' => '{update} {delete}',
                    'buttons' => [

                        'update' => function ($url, $model, $key) {
                            $url = ['/#'];
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'class' => 'js-param-popup btn btn-xs btn-default',
                                'data-target' => '#paramOptionModal',
                                'data-url' => Url::toRoute(['/eav/param/update-option', 'id' => $model->id]),
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            $url = Url::to(['/eav/param/delete-option', 'id' => $model->id]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'data-method' => 'post',
                                'class' => 'btn btn-xs btn-default',
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>


<?php Modal::begin([
    'id' => 'paramOptionModal',
    'header' => 'Опция',
]); ?>

<div class='modalContent'></div>

<?php Modal::end(); ?>
