<?php

use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\eav\models\common\search\ParamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Атрибуты. Управление';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'title',
//                    'name',
                    'desc',
                    [
                        'attribute' => 'type_id',
                        'value' => function ($data) {
                            return Yii::$app->params['paramTypes'][$data->type_id];
                        },
                    ],
//                    [
//                        'attribute' => 'group_id',
//                        'contentOptions' => ['style' => 'width:150px;'],
//                        'value' => function ($data) {
//                            return ($data->group) ? $data->group->name : '-';
//                        },
//                    ],
                    [
                        'label' => 'Опции',
                        'contentOptions' => ['style' => 'width:70px; text-align: center'],
                        'value' => function ($data) {
                            return ($data->options) ? count($data->options) : '';
                        },
                    ],
//                    [
//                        'attribute' => 'sort',
//                        'contentOptions' => ['style' => 'width:130px;'],
//                        'value' => function ($data) {
//                            return $data->sort;
//                        },
//                    ],
                    [
                        'header' => 'Языки',
                        'class' => 'lav45\translate\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:250px; '],
                        'languages' => $langList,
                    ],
                    [
                        'header' => 'Действия',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:80px;'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => 'Удалить',
                                    'data-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

        </div>
    </div>



</div>
