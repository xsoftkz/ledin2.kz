<?php

use common\modules\eav\models\Type;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\ArrayHelper;
use common\modules\eav\models\ParamGroup;
use yii\helpers\Html;

$groups = ArrayHelper::map(ParamGroup::find()->all(), 'id', 'title');

/* @var $this yii\web\View */
/* @var $model common\modules\eav\models\Param */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#main" aria-controls="main" role="tab" data-toggle="tab">Данные</a>
        </li>
        <li role="presentation">
            <a href="#types" aria-controls="types" role="tab" data-toggle="tab">Типы объектов</a>
        </li>
        <li class="pull-right">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
        </li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="main">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'type_id')->dropDownList(\Yii::$app->params['paramTypes']) ?>

            <div class="row">
                <div class="col-lg-4">
                    <?= $form->field($model, 'photo')->widget(
                        FileAPI::className(), [
                        'settings' => [
                            'url' => ['fileapi-upload'],
                            'elements' => ['preview' => ['width' => 30,'height' => 30]],
                            'imageAutoOrientation' => false
                        ],
                    ]); ?>
                </div>
            </div>

            <?php if($model->hasOptions()){
                echo $form->field($model, 'option_type_id')->dropDownList(\Yii::$app->params['optionTypes']);
            } ?>

            <?= $form->field($model, 'desc')->textarea(['rows' => 2]) ?>

        </div>
        <div role="tabpanel" class="tab-pane fade" id="types">
            <?= $form->field($model, 'related_types')->
            checkboxList(ArrayHelper::map(Type::find()->all(), 'id', 'title'))->label('Использовать в следующих типах объектов:') ?>
        </div>
    </div>
</div>


<?php ActiveForm::end(); ?>
