<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\modules\eav\models\Param;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="view-form">

    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab"> <?= Yii::t('app', 'Данные')?> </a></li>
                    <li class="pull-right">
                        <?= Html::submitButton(
                            ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить') :
                                '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Сохранить'),
                            ['class' =>
                                ($model->isNewRecord) ? 'btn btn-sm btn-success pull-right' : 'btn btn-sm btn-primary pull-right',
                            ]) ?>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="main">
                        <div class="row">
                            <div class="col-lg-9">
                                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <?= $form->field($model, 'related_params')->
                                        checkboxList(ArrayHelper::map(Param::find()->all(), 'id', 'full_title')) ?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?= $form->field($model, 'related_options')->
                                        checkboxList(ArrayHelper::map(Param::find()->where(['type_id' =>
                                            Param::TYPE_CHECKBOXLIST])->all(), 'id', 'full_title')) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>