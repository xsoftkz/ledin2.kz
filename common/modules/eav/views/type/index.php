<?php

use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы объектов. Управление';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Корзина'),
                            ['cart'], ['class' => 'btn btn-sm btn-default ']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'contentOptions' => ['style' => 'width:40px;'],
                    ],
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:20px;'],
                        'label' => Yii::t('app', 'ID'),
                        'value' => function ($data) {
                            return $data->id;
                        },
                    ],
                    [
                        'attribute' => 'title',
                        'label' => Yii::t('app', 'Название'),
                        'value' => function ($data) {
                            return $data->title;
                        },
                    ],
                    [
                        'header' => Yii::t('app', 'Действия'),
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:80px;'],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Редактировать'),
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-primary',
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'data-method' => 'post',
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-confirm' => Yii::t('app', 'После удаление восстановить невозможно, все равно удалить?'),
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

        </div>
    </div>


</div>
