<?php

use yii\helpers\Html;


/* @var $this yii\web\View */

$this->title = 'Типы объектов. Добавление';
$this->params['breadcrumbs'][] = ['label' => 'Типы объектов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="сreate">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('app', 'Отменить'),
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
