<?php

use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;


/* @var $this yii\web\View */
/* @var $searchModel common\modules\eav\models\common\search\ParamGroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы атрибутов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="param-group-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:20px;'],
                        'value' => function ($data) {
                            return $data->id;
                        },
                    ],
                    'name',
                    'description',
                    [
                        'attribute' => 'sort',
                        'contentOptions' => ['style' => 'width:150px;'],
                        'value' => function ($data) {
                            return $data->sort;
                        },
                    ],
                    [
                        'header' => 'Действия',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:80px;'],
                        'template' => '{update} {delete}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => 'Редактировать',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-primary',
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => 'Удалить',
                                    'data-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

        </div>
    </div>

</div>
