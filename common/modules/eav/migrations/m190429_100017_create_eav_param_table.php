<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

class m190429_100017_create_eav_param_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_param}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'group_id' => $this->integer(),
            'option_type_id' => $this->integer(),
            'display_type_id' => $this->integer(),
            'name' => $this->string()->notNull(),
            'photo' => $this->string(),
            'unit' => $this->string(),
            'required' => $this->tinyInteger(),
            'sort_index' => $this->integer(),
            'is_filter' => $this->smallInteger(),
            'is_main' => $this->smallInteger(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_param}}');
    }
}
