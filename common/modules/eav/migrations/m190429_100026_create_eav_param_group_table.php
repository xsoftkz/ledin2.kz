<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

class m190429_100026_create_eav_param_group_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_param_group}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'sort' => $this->integer(),
            'status_id' => $this->smallInteger()->defaultValue(1),
        ], $tableOptions);

        $this->batchInsert('{{%eav_param_group}}',
            ['id', 'name', 'description', 'sort', 'status_id'],
            [
                [1, 'Дополнительная информация', '', 3, 1]
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_param_group}}');
    }
}
