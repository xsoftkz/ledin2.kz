<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

class m190429_102742_create_eav_entity_param_value_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_entity_param_value}}', [
            'id' => $this->primaryKey(),
            'entity_id' => $this->integer()->notNull(),
            'param_id' => $this->integer()->notNull(),
            'value_kz' => $this->text(),
            'value_en' => $this->text(),
            'value_ru' => $this->text(),
            'created_at' => $this->integer(),

        ], $tableOptions);


    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_entity_param_value}}');
    }



}
