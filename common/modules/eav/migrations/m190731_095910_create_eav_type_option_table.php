<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

class m190731_095910_create_eav_type_option_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_type_option}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'param_id' => $this->integer()->notNull(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_type_option}}');
    }
}
