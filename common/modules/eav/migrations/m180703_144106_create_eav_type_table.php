<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

class m180703_144106_create_eav_type_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_type}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
        ], $tableOptions);

        $this->batchInsert('{{%eav_type}}',
            ['id', 'title'],
            [
                [1, 'Светильники'],
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_type}}');
    }
}

