<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

class m190429_102816_create_eav_type_param_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_type_param}}', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->notNull(),
            'param_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->batchInsert('{{%eav_type_param}}',
            ['id', 'type_id', 'param_id'],
            [
                [1, 1, 1],
                [2, 1, 2],
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_type_param}}');
    }
}
