<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%eav_param_lang}}`.
 */
class m200119_124601_create_eav_param_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_param_lang}}', [
            'id' => $this->primaryKey(),
            'param_id' => $this->integer()->notNull(),
            'lang_id' => $this->string(8)->notNull(),
            'title' => $this->string(65)->notNull(),
            'desc' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%eav_param_lang}}');
    }
}
