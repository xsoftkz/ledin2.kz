<?php
namespace common\modules\eav\migrations;

use yii\db\Migration;

class m190429_102723_create_eav_param_option_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%eav_param_option}}', [
            'id' => $this->primaryKey(),
            'param_id' => $this->integer()->notNull(),
            'sort' => $this->integer(),
            'value_ru' => $this->string()->notNull(),
            'value_kz' => $this->string(),
            'value_en' => $this->string(),
            'icon' => $this->string(),
            'comment' => $this->string(),
        ], $tableOptions);

//        $this->batchInsert('{{%eav_param_option}}',
//            ['id', 'param_id', 'sort', 'value'],
//            [
//                [1,  1, 1,  '19-22'],
//                [2,  1, 2,  '22-25'],
//                [3,  1, 3,  '25-27'],
//            ]
//        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%eav_param_option}}');
    }
}
