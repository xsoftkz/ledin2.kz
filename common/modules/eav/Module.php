<?php
namespace common\modules\eav;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\eav\controllers';
    public function init()
    {
        parent::init();

        \Yii::$app->params['paramTypes'] = [
            '4' => 'Текст',
            '1' => 'Выпадающий список',
            '2' => 'Список чекбоксов (множественный)',
            '3' => 'Чекбокс',
        ];

        \Yii::$app->params['paramDisplayTypes'] = [
            '1' => 'Выпадающий список',
            '2' => 'Список чекбоксов (множественный)',
        ];

        \Yii::$app->params['optionTypes'] = [
            '1' => 'Текст',
            '2' => 'Изображение',
        ];

    }
}