<?php

namespace common\modules\eav\controllers;

use common\models\Lang;
use common\modules\eav\models\Entity;
use common\modules\eav\models\EntityParamFormModel;
use common\modules\eav\models\ParamOption;
use common\modules\eav\models\search\ParamOptionSearch;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use common\modules\eav\models\Param;
use common\modules\eav\models\search\ParamSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

class ParamController extends Controller
{

    public function actionIndex()
    {
        $searchModel = new ParamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'langList' => Lang::getList(),
        ]);
    }


    public function actionCreate()
    {
        $model = new Param();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->saveTypes();
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $lang = Lang::findOne($model->language);
        Yii::$app->sourceLanguage = ($lang) ? $lang->locale : Lang::getDefaultLang()->locale;

        $searchModel = new ParamOptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $model->id);

        $model->related_types = ArrayHelper::getColumn($model->types, 'id');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->saveTypes();
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
        }

        return $this->render('update', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }



    public function actionCreateOption()
    {
        $model = new ParamOption();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            } else {
                Yii::$app->session->setFlash('danger', 'Возникла ошибка');
            }
            return $this->redirect(['update', 'id' => $model->param_id]);
        } else {
            return $this->redirect(Yii::$app->request->referrer);
        }
    }


    public function actionUpdateOption($id)
    {
        $model = ParamOption::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save(false)) {
                Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            } else {
                Yii::$app->session->setFlash('danger', 'Возникла ошибка');
            }
            return $this->redirect(['update', 'id' => $model->param_id]);
        } else {
            return $this->renderAjax('_param_option', [
                'model' => $model,
            ]);
        }
    }



    public function actionDeleteOption($id)
    {
        $this->findOptionModel($id)->delete();
        Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionEntityParamsForm($type_id = false, $id = false){

        $formModel = new EntityParamFormModel([
            'type_id' => $type_id,
            'entity_id' => $id
        ]);

        return $this->renderAjax('_entity_params_form', [
            'formModel' => $formModel,
        ]);
    }


    protected function findModel($id)
    {
        if (($model = Param::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    protected function findEntityModel($id)
    {
        if (($model = Entity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    protected function findOptionModel($id)
    {
        if (($model = ParamOption::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => ParamOption::className(),
            ],
            'sort_index' => [
                'class' => SortableGridAction::className(),
                'modelName' => Param::className(),
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@images/' . Param::IMAGE_FOLDER,
                'unique' => true,
            ],
        ];
    }

}
