<?php

namespace common\modules\eav\assets;

use yii\web\AssetBundle;

class EavModuleAdminAsset extends AssetBundle
{

    public $sourcePath = '@common/modules/eav/assets/lib';
    public $basePath = '@backendWebroot/web/assets';

    public $css = [
        'css/eav__styles.css',
    ];

    public $js = [
        'js/eav__scripts.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
