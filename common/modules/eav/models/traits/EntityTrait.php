<?php

namespace common\modules\eav\models\traits;

use common\modules\eav\models\Entity;

trait EntityTrait
{
    public function getEntity()
    {
        return Entity::findOne($this->id);
    }

    public function getParams()
    {
        if ($this->entity) {
            return $this->entity->params;
        } else {
            return null;
        }
    }

    public function getParamsValues()
    {
        if ($this->entity) {
            return $this->entity->params;
        } else {
            return null;
        }
    }

    public function getParamsMap()
    {
        if ($this->entity) {
            return $this->entity->paramsMap;
        } else {
            return null;
        }
    }

    public function getMainParamsMap()
    {
        if ($this->entity) {
            return $this->entity->mainParamsMap;
        } else {
            return null;
        }
    }
}