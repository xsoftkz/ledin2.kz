<?php
namespace common\modules\eav\models;

use common\modules\shop\models\ProductModel;

class Entity extends ProductModel
{

    public function getParams()
    {
        return $this->hasMany(Param::className(), ['id' => 'param_id'])
            ->viaTable('{{%eav_entity_param_value}}', ['entity_id' => 'id']);
    }


    public function getMainParamsMap(){
        $paramsMap = [];

        $query = EntityParam::find()->where(['entity_id' => $this->id]);
        $query->leftJoin('{{%eav_param}}', '{{%eav_param}}.id = {{%eav_entity_param_value}}.param_id');
        $query->andWhere(['{{%eav_param}}.is_main' => 1]);
        $models = $query->all();

        if ($models){
            foreach ($models as $model) {
                $paramsMap[$model->id]['param'] = ($model->param) ? $model->param->title : '-';
                $paramsMap[$model->id]['value'] = $model->valueText;
                $paramsMap[$model->id]['image'] = ($model->paramOption && $model->paramOption->icon) ?  $model->paramOption->image : null;
            }
        }
        return $paramsMap;
    }

    public function getParamsMap(){
        $paramsMap = [];
        $models = EntityParam::find()->where(['entity_id' => $this->id])->all();
        if ($models){
            foreach ($models as $model) {
                $paramsMap[$model->id]['param'] = ($model->param) ? $model->param->title : '-';
                $paramsMap[$model->id]['value'] = $model->valueText;
                $paramsMap[$model->id]['image'] = ($model->paramOption && $model->paramOption->icon) ?  $model->paramOption->image : null;
            }
        }
        return $paramsMap;
    }

}