<?php

namespace common\modules\eav\models;

use common\models\traits\ImageTrait;
use common\modules\eav\models\lang\ParamLang;
use lav45\translate\TranslatedTrait;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%eav_param}}".
 *
 * @property int $id
 * @property int $type_id
 * @property int $group_id
 * @property string $name
 * @property string $title
 * @property string $desc
 * @property string $unit
 * @property int $required
 * @property int $sort_index
 * @property int $is_filter
 * @property int $is_main
 */
class Param extends \yii\db\ActiveRecord
{
    use ImageTrait;
    use TranslatedTrait;

    const IMAGE_FOLDER = 'eav';

    public $related_types;
    public $full_title;

    const TYPE_SELECT = 1;
    const TYPE_CHECKBOXLIST = 2;
    const TYPE_CHECKBOX = 3;
    const TYPE_STRING = 4;
    const TYPE_TEXT = 5;

    const DISPLAY_TYPE_SELECT = 1;
    const DISPLAY_TYPE_CHECKBOXLIST = 2;

    const OPTION_TYPE_TEXT = 1;
    const OPTION_TYPE_IMAGE = 2;

    public static function tableName()
    {
        return '{{%eav_param}}';
    }

    public function rules()
    {
        return [
            [['type_id', 'title'], 'required'],
            [['type_id', 'group_id', 'required', 'sort_index', 'is_filter', 'option_type_id', 'is_main', 'display_type_id'], 'integer'],
            [['desc'], 'string'],
            [['name', 'title', 'unit', 'photo'], 'string', 'max' => 255],
            [['related_types', 'full_title'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_id' => 'Тип',
            'group_id' => 'Группа',
            'option_type_id' => 'Тип данных опций',
            'title' => 'Название',
            'name' => 'Алиас',
            'photo' => 'Иконка',
            'desc' => 'Описание',
            'unit' => 'Единица измерения',
            'required' => 'Обязательный',
            'sort_index' => 'Порядок сортировки',
            'is_filter' => 'Фильтр',
            'is_main' => 'Основная характеристика',
            'related_types' => 'Типы объектов',
            'display_type_id' => 'Тип отображение',
        ];
    }

    public function behaviors()
    {
        return [
            'sort_index' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort_index'
            ],
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'photo' => [
                        'path' => '@images/'.self::IMAGE_FOLDER,
                        'tempPath' => '@images/'.self::IMAGE_FOLDER,
                    ],
                ]
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'title',
                'slugAttribute' => 'name',
                'ensureUnique' => true,
//                'immutable' => true,
            ],
            [
                'class' => 'lav45\translate\TranslatedBehavior',
                'translateRelation' => 'paramLangs',
                'languageAttribute' => 'lang_id',
                'translateAttributes' => [
                    'title',
                    'desc',
                ]
            ]

        ];
    }

    public function getParamLangs()
    {
        return $this->hasMany(ParamLang::className(), ['param_id' => 'id']);
    }

    public function getGroup(){
        return $this->hasOne(ParamGroup::className(), ['id' => 'group_id']);
    }

    public function getOptions(){
        return $this->hasMany(ParamOption::className(), ['param_id' => 'id'])->orderBy('sort');
    }

    public function hasOptions(){
        if ($this->type_id == self::TYPE_CHECKBOXLIST || $this->type_id == self::TYPE_SELECT){
            return true;
        } else {
            return false;
        }
    }

    public function saveTypes()
    {
        $models = Type::findAll($this->related_types);
        $this->unlinkAll('types', true);

        foreach ($models as $model) {
            $this->link('types', $model);
        }
    }

    public function getTypes()
    {
        return $this->hasMany(Type::className(), ['id' => 'type_id'])
            ->viaTable('{{%eav_type_param}}', ['param_id' => 'id']);
    }

    public function optionTypeIsFile(){
        if ($this->option_type_id === Param::OPTION_TYPE_IMAGE){
            return true;
        } else {
            return false;
        }
    }

    public static function imageList($filenames) {
        $imageList = [];
        foreach ($filenames as $key => $value) {
            $imageList[$key] = '<span class="label-img">'. Html::img( Yii::getAlias('@imagesWebroot/eav/') . $value ).'</span>';
        }
        return $imageList;
    }

    public static function labelList($items) {
        $itemList = [];
        foreach ($items as $key => $value) {
            $itemList[$key] = '<span class="label-item">'.$value.'</span>';
        }
        return $itemList;
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->full_title = $this->title;
        if ($this->desc){
            $this->full_title .= '/'.$this->desc;
        }
    }

}
