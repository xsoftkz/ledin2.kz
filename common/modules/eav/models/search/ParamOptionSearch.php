<?php

namespace common\modules\eav\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\eav\models\ParamOption;

/**
 * ParamOptionSearch represents the model behind the search form of `common\modules\eav\models\ParamOption`.
 */
class ParamOptionSearch extends ParamOption
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'param_id', 'sort'], 'integer'],
            [['value'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $param_id)
    {
        $query = ParamOption::find();
        $query->andWhere(['param_id' => $param_id]);
        $query->orderBy('sort DESC');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'param_id' => $this->param_id,
            'sort' => $this->sort,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}
