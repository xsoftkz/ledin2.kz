<?php

namespace common\modules\eav\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\eav\models\Param;

/**
 * ParamSearch represents the model behind the search form of `common\modules\eav\models\Param`.
 */
class ParamSearch extends Param
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type_id', 'group_id', 'required', 'sort_index', 'is_filter'], 'integer'],
            [['name', 'title', 'desc', 'unit'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Param::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if(!$params){
            $query->orderBy('sort_index');
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type_id' => $this->type_id,
            'group_id' => $this->group_id,
            'required' => $this->required,
            'sort_index' => $this->sort_index,
            'is_filter' => $this->is_filter,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'unit', $this->unit]);

        return $dataProvider;
    }
}
