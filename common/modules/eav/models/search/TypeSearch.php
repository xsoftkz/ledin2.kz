<?php
namespace common\modules\eav\models\search;

use common\modules\eav\models\Type;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class TypeSearch extends Type
{
    public function rules()
    {
        return [
            [['title'], 'safe'],
        ];
    }


    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params, $action = false)
    {
        $query = Type::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
