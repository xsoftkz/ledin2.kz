<?php

namespace common\modules\eav\models;

use common\models\helper\ImageHelper;
use common\models\Image;
use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "xs_eav_param_option".
 *
 * @property int $id
 * @property int $param_id
 * @property int $sort
 * @property string $value_ru
 * @property string $value_kz
 * @property string $value_en
 */
class ParamOption extends ActiveRecord
{
    const IMAGE_FOLDER = 'eav';
    const MODEL_NAME = 'param-option';

    public static function tableName()
    {
        return '{{%eav_param_option}}';
    }

    public function rules()
    {
        return [
            [['param_id', 'value_ru'], 'required'],
            [['param_id', 'sort', 'temp_id'], 'integer'],
            [['value_ru', 'value_kz', 'value_en', 'comment', 'icon'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'Файл',
            'icon' => 'Иконка',
            'comment' => 'Комментарий',
            'temp_id' => 'Temp ID',
            'param_id' => 'Param ID',
            'sort' => 'Порядок сортировки',
            'value_ru' => 'Значение RU',
            'value_kz' => 'Значение KZ',
            'value_en' => 'Значение EN',
        ];
    }

    public function behaviors()
    {
        return [
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort'
            ],
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'icon' => [
                        'path' => '@images/'.self::IMAGE_FOLDER,
                        'tempPath' => '@images/'.self::IMAGE_FOLDER,
                    ],
                ]
            ],
        ];
    }

    public function getValue(){
        return $this->value_ru;
    }

    public function getParam(){
        return $this->hasOne(Param::className(), ['id' => 'param_id']);
    }


    public function getImage()
    {
        return ImageHelper::getImage($this->icon, $this::IMAGE_FOLDER);
    }


    public function typeIsFile(){
        if ($this->param && $this->param->option_type_id == Param::OPTION_TYPE_IMAGE){
            return true;
        } else {
            return false;
        }
    }


    public static function getOptionsValueText($options){
        $text = '';
        $result = [];
        $models = ParamOption::findAll($options);
        foreach ($models as $model){
            $result[$model->id]['param'] = ($model->param) ? $model->param->title : '-';

            if ($model->param->option_type_id == Param::OPTION_TYPE_TEXT){
                $result[$model->id]['value'] = $model->value;
            } else {
                $result[$model->id]['value'] = $model->comment;
            }
        }
        foreach ($result as $key => $value) {
            $text .= $value['param'].'-'.$value['value'].'<br>';
        }
        return $text;
    }


    public static function getOptionsValueMap($options){
        $map = [];
        $result = [];
        $models = ParamOption::findAll($options);
        foreach ($models as $model){
            $result[$model->id]['param'] = ($model->param) ? $model->param->title : '-';

            if ($model->param->option_type_id == Param::OPTION_TYPE_TEXT){
                $result[$model->id]['value'] = $model->value;
            } else {
                $result[$model->id]['value'] = $model->comment;
            }
        }
        foreach ($result as $key => $value) {
            $map[$value['param']] =$value['value'];
        }
        return $map;
    }

}
