<?php

namespace common\modules\eav\models;

use yii\base\Model;

/**
 * This is the model class for table "{{%eav_entity_param_value}}".
 *
 * @property array $params
 * @property array $paramModels
 */

class EntityParamFormModel extends Model
{
    public $type_id;
    public $entity_id;

    public function getParams(){
        $type = Type::findOne($this->type_id);
        return ($type) ? $type->params : null;
    }

    public function getParamModels(){
        $entity = Entity::findOne($this->entity_id);
        return EntityParam::getEntityParamModels($this->params, $entity);
    }
}