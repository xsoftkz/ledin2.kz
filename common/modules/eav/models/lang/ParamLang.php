<?php

namespace common\modules\eav\models\lang;

use Yii;

/**
 * This is the model class for table "shop_param_lang".
 *
 * @property int $id
 * @property int $param_id
 * @property int $lang_id
 * @property string $title
 * @property string|null $desc
 */
class ParamLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%eav_param_lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['param_id', 'lang_id', 'title'], 'required'],
            [['param_id'], 'integer'],
            [['desc'], 'string'],
            [['title', 'lang_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'param_id' => 'Param ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'desc' => 'Desc',
        ];
    }
}
