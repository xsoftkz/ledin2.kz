<?php

namespace common\modules\eav\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%eav_entity_param_value}}".
 *
 * @property int $id
 * @property int $entity_id
 * @property int $param_id
 * @property double $number_value
 * @property string $string_value
 * @property string $text_value
 * @property int $option_value
 * @property int $created_at
 */
class EntityParam extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return '{{%eav_entity_param_value}}';
    }

    public function rules()
    {
        return [
            [['entity_id', 'param_id'], 'required'],
            [['entity_id', 'param_id', 'created_at'], 'integer'],
            [['value_kz', 'value_en'], 'string'],
            [['value_ru'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entity_id' => 'Entity ID',
            'param_id' => 'Param ID',
            'value_kz' => 'Value KZ',
            'value_en' => 'Value EN',
            'value_ru' => 'Value',
            'created_at' => 'Created At',
        ];
    }

    public function getParam()
    {
        return $this->hasOne(Param::className(), ['id' => 'param_id']);
    }

    public function getValue(){
        return $this->value_ru;
    }

    public function afterFind(){

        parent::afterFind();
        $this->value_ru = ($this->param && $this->param->type_id == Param::TYPE_CHECKBOXLIST) ? explode(',', $this->value_ru) : $this->value_ru;
    }

    public static function saveEntityParams($entity, $entityParamModels){
        $entity->unlinkAll('params', true);

        if ($entityParamModels){
            foreach ($entityParamModels as $index => $entityParam) {
                $param = Param::findOne($index);

                $value_kz = '';
                $value_en = '';

                if ($param->type_id == (Param::TYPE_CHECKBOXLIST && is_array($entityParam['value_ru']))){
                    $value_ru = implode(',', $entityParam['value_ru']);
                } else {
                    $value_ru = $entityParam['value_ru'];
                    $value_kz = isset($entityParam['value_kz']) ? $entityParam['value_kz'] : '';
                    $value_en = isset($entityParam['value_en']) ? $entityParam['value_en'] : '';
                }

                $entity->link('params', $param,
                    ['value_ru' => $value_ru, 'value_kz' => $value_kz, 'value_en' => $value_en]
                );
            }
        }
    }

    public static function getEntityParamModels($params, $entity = false){
        $entityParams = null;
        $entityParamModels = [];

        if ($entity) {
            $entityParams = ArrayHelper::index(EntityParam::find()->where(['entity_id' => $entity->id])->all(), 'param_id');
        }

        if ($params){
            foreach ($params as $entityParam) {
                if($entityParams &&  isset($entityParams[$entityParam->id])){
                    $entityParamModels[$entityParam->id] = $entityParams[$entityParam->id];
                } else{
                    $entityParamModels[$entityParam->id] = new EntityParam();
                }
            }
        }

        return $entityParamModels;
    }

    public static function getTypeParams($type_id = false){
        $type = Type::findOne($type_id);
        return ($type) ? $type->getParams() : null;
    }

    public function getValueText()
    {

        if ($this->param) {
            switch ($this->param->type_id) {
                case Param::TYPE_CHECKBOXLIST:
                    if ($this->paramOptions) {
                        if ($this->param->option_type_id == Param::OPTION_TYPE_TEXT){
                            return implode(', ', $this->paramOptions ? ArrayHelper::getColumn($this->paramOptions, 'value_ru') : '');
                        } else {
                            return implode(', ', $this->paramOptions ? ArrayHelper::getColumn($this->paramOptions, 'comment') : '');
                        }
                    }
                    break;
                case Param::TYPE_SELECT:
                    return $this->paramOption ? $this->paramOption->value_ru : '';
                    break;
                case Param::TYPE_CHECKBOX:
                    return ($this->value_ru) ? 'да' : 'нет';
                    break;
                case Param::TYPE_STRING:
                    return $this->value_ru;
                    break;
                case Param::TYPE_TEXT:
                    return $this->value_ru;
                    break;
            }
        }
    }


    public function getParamOptions()
    {
        return $this->hasMany(ParamOption::className(), ['id' => 'value_ru']);
    }

    public function getParamOption()
    {
        return $this->hasOne(ParamOption::className(), ['id' => 'value_ru']);
    }

}


