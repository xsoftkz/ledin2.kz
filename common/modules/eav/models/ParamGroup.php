<?php

namespace common\modules\eav\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%eav_param_group}}".
 *
 * @property int $id
 * @property string $name
 * @property int $sort
 * @property int $description
 */
class ParamGroup extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%eav_param_group}}';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название группы атрибутов',
            'sort' => 'Порядок сортировки',
            'description' => 'Описание',
        ];
    }

    public function behaviors()
    {
        return [
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort'
            ],
        ];
    }


    public function getTitle(){
        return $this->name.' - '.$this->description;
    }

}
