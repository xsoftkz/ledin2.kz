<?php
namespace common\modules\eav\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%eav_group}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $$related_params

 */
class Type extends ActiveRecord
{
    public $related_params;
    public $related_options;

    const LIGHT_TYPE_ID = 1;

    public static function tableName()
    {
        return '{{%eav_type}}';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['related_params', 'related_options' ], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'related_params' => 'Атрибуты типа',
            'related_options' => 'Опции типа',
        ];
    }

    public function saveParams()
    {
        $models = Param::findAll($this->related_params);
        $this->unlinkAll('params', true);

        foreach ($models as $model) {
            $this->link('params', $model);
        }
    }


    public function saveOptions()
    {
        $models = Param::findAll($this->related_options);
        $this->unlinkAll('options', true);

        foreach ($models as $model) {
            $this->link('options', $model);
        }
    }

    public function getParams()
    {
        return $this->hasMany(Param::className(), ['id' => 'param_id'])
            ->viaTable('{{%eav_type_param}}', ['type_id' => 'id'])->orderBy('type_id');
    }

    public function getOptions()
    {
        return $this->hasMany(Param::className(), ['id' => 'param_id'])
            ->viaTable('{{%eav_type_option}}', ['type_id' => 'id'])->orderBy('type_id');
    }


    public static function getDropdownList()
    {
        $models = Type::find()->orderBy('id DESC')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }


    public static function getEntityOptions($entity_id){
        $models = [];
        $entity = Entity::findOne($entity_id);
        if ($entity){
            $type = self::findOne($entity->type_id);
            $param_ids = ArrayHelper::getColumn($type->options, 'id');
            $models = EntityParam::find()->where(['entity_id' => $entity->id, 'param_id' => $param_ids])->all();
        }
        return $models;
    }

}
