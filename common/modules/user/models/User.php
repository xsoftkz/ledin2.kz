<?php
namespace common\modules\user\models;

use Yii;
use yii\helpers\Url;

class User extends \common\models\User
{
    public function sendEmailConfirmation()
    {
        $mailer = Yii::$app->mailer;

        $subject = 'Активация аккаунта';

        $result = $mailer->compose('@common/modules/user/mail/confirm-email', [
            'user' => $this,
            'url' => Url::to(['@frontendWebroot/auth/confirm-email', 'code' => $this->email_activation_key], 'http')
        ])
            ->setTo($this->email)
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['appDomain']])
            ->setSubject($subject)
            ->send();

        return $result;
    }

    public function confirm()
    {
        if(!$this->activated){
            $this->activated = static::STATUS_ACTIVATED;
            $this->status = static::STATUS_ACTIVE;
            if($this->save()){
                $this->email_activation_key = null;
                $this->save();
            }
        }
    }
}
