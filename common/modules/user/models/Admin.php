<?php
namespace common\modules\user\models;

use Yii;
use yii\base\Model;

class Admin extends Model
{
    public $firstname;
    public $username;
    public $lastname;
    public $password;
    public $status;

    public function rules()
    {
        return [

            [['firstname', 'lastname', 'username'], 'required'],
            [['password'], 'required', 'on' => 'default'],
            [['firstname', 'lastname', 'password', 'username'], 'string'],
            [['status'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['firstname', 'lastname', 'username', 'status', 'password'],
            'change' => ['firstname', 'lastname', 'username', 'status'],
        ];
    }


    public function saveProfile()
    {
        if ($this->validate()) {
            $user = new User();
            $user->setAttributes($this->attributes);
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();
            return $user;
        }
        return null;
    }

    public function changeProfile($id)
    {
        if ($this->validate()) {
            $user = User::findOne($id);
            $user->setAttributes($this->attributes);
            if ($user->save()) {
                return true;
            }
            return false;
        }
        return false;
    }


    public function checkUsername()
    {
        $user = User::find()->where(['username' => $this->username])->one();
        if ($user) {
            return true;
        } else {
            return false;
        }
    }

    public function usernameIsAvailableForUpdate($oldUsername, $newUsername)
    {
        if ($oldUsername == $newUsername) {
            return true;
        }

        $user = User::find()->where(['username' => $newUsername])->one();
        if ($user) {
            return false;
        } else {
            return true;
        }

    }

    public function attributeLabels()
    {
        return [
            'lastname' => Yii::t('app', 'Фамилия'),
            'firstname' => Yii::t('app', 'Имя'),
            'username' => Yii::t('app', 'Логин'),
            'password' => Yii::t('app', 'Пароль'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }


}
