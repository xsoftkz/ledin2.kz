<?php
namespace common\modules\user\models;

use Yii;
use yii\base\Model;

class Profile extends Model
{
    public $firstname;
    public $email;
    public $lastname;
    public $password;
    public $role;
    public $status;

    public function rules()
    {
        return [

            [['firstname', 'lastname', 'email'], 'required'],
            [['password'], 'required', 'on' => 'default'],
            [['firstname', 'lastname', 'password', 'email'], 'string'],
            [['role', 'status'], 'integer'],
            [['email'], 'email'],
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['firstname', 'lastname', 'email', 'status', 'role', 'password'],
            'change' => ['firstname', 'lastname', 'email', 'status', 'role'],
        ];
    }


    public function saveProfile()
    {
        if ($this->validate()) {
            $user = new User();
            $user->setAttributes($this->attributes);
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->save();
            return $user;
        }
        return null;
    }

    public function changeProfile($id)
    {
        if ($this->validate()) {
            $user = User::findOne($id);
            $user->setAttributes($this->attributes);
            if ($user->save()) {
                return true;
            }
            return false;
        }
        return false;
    }


    public function checkEmail()
    {
        $user = User::find()->where(['email' => $this->email])->one();
        if ($user) {
            return true;
        } else {
            return false;
        }
    }

    public function emailIsAvailableForUpdate($oldEmail, $newEmail)
    {
        if ($oldEmail == $newEmail) {
            return true;
        }

        $user = User::find()->where(['email' => $newEmail])->one();
        if ($user) {
            return false;
        } else {
            return true;
        }

    }

    public function attributeLabels()
    {
        return [
            'lastname' => Yii::t('app', 'Фамилия'),
            'firstname' => Yii::t('app', 'Имя'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Пароль'),
            'role' => Yii::t('app', 'Роль'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }


}
