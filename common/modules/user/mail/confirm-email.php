<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>

Добрый день, <?= Html::encode($user->firstname)?>!
<br>
Вы зарегистрировались на сайте <?= Url::to(['@frontendWebroot/'], 'http') ?>.
<br>
Для завершения регистрации, перейдите пожалуйста по ссылке:
<br>
<?= Html::a($url, $url) ?>
