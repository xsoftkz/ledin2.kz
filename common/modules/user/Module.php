<?php
namespace common\modules\user;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\user\controllers';
    public function init()
    {
        parent::init();

        \Yii::$app->params['userStatus'] = [
            '10' => 'Активирован',
            '0' => 'Заблокирован',
        ];
    }
}