<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="user-form">
     <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                 <div class="nav-tabs-custom">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab"> <?= Yii::t('app', 'Данные')?> </a></li>
                        <li class="pull-right">
                            <?= Html::submitButton('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                                ['class' => 'btn btn-success pull-right']) ?>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="main">

                            <?= $form->field($model, 'firstname')->textInput(['maxlength' => 255]) ?>

                            <?= $form->field($model, 'lastname')->textInput(['maxlength' => 255]) ?>

                            <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

                            <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

                            <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['userStatus']) ?>

                        </div>
                    </div>

                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>

