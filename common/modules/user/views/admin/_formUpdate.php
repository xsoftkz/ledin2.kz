<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="user-form">

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab"><?= Yii::t('app', 'Данные') ?></a></li>
            <li><a href="#tab_2" data-toggle="tab"><?= Yii::t('app', 'Смена пароля') ?></a></li>
        </ul>
        <div class="tab-content">

            <div class="tab-pane active" id="tab_1">
                <div class="row">
                    <div class="col-lg-6">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

                        <?= $form->field($model, 'firstname')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'lastname')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'username')->textInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['userStatus']) ?>

                        <?= Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> ' . Yii::t('app', 'Сохранить'),
                            ['class' => 'btn btn-primary']) ?>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>

            <div class="tab-pane" id="tab_2">
                <?php $form = ActiveForm::begin(['action' => '/user/admin/change-password']); ?>

                <div class="row">
                    <div class="col-lg-6">
                        <?= $form->field($passwordChangeForm, 'userid')->hiddenInput(['value' => $user->id])->label(false) ?>

                        <?= $form->field($passwordChangeForm, 'password')->passwordInput(['maxlength' => 255]) ?>

                        <?= $form->field($passwordChangeForm, 'passwordRepeat')->passwordInput(['maxlength' => 255]) ?>

                        <?= Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> ' . Yii::t('app', 'Сохранить'),
                            ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div>


</div>


