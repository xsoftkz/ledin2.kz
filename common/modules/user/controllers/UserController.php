<?php
namespace common\modules\user\controllers;

use common\modules\user\models\PasswordChangeForm;
use common\modules\user\models\Profile;
use common\modules\user\models\search\UserSearch;
use common\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class UserController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'change-password'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), User::ROLE_USER);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate()
    {
        $model = new Profile();
        $model->scenario = 'default';

        if ($model->load(Yii::$app->request->post())) {

            if($model->checkEmail()){
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Этот email не доступен'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            $user = $model->saveProfile();

            if($user){
                return $this->redirect(['update', 'id' => $user->id]);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $passwordChangeForm = new PasswordChangeForm();
        $model = new Profile();
        $model->scenario = 'change';

        $model->setAttributes($user->attributes);

        $currentEmail = $user->email;
//            VarDumper::dump($user->attributes, 10, true); die;


        if ($model->load(Yii::$app->request->post())) {
//            VarDumper::dump($user->attributes, 10, true); die;

            if($model->emailIsAvailableForUpdate($currentEmail, $model->email)){

                if($model->changeProfile($id)){
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Success saved'));
                    return $this->redirect(['update', 'id' => $id]);
                } else {
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error'));
                    return $this->render('update', [
                        'model' => $model,
                        'passwordChangeForm' => $passwordChangeForm,
                        'user' => $user
                    ]);
                }

            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Этот email не доступен'));
                return $this->render('update', [
                    'model' => $model,
                    'passwordChangeForm' => $passwordChangeForm,
                    'user' => $user
                ]);
            }

        }
        else {
            return $this->render('update', [
                'model' => $model,
                'passwordChangeForm' => $passwordChangeForm,
                'user' => $user,
            ]);
        }
    }

    public function actionChangePassword()
    {
        $passwordChangeForm = new PasswordChangeForm();

        if ($passwordChangeForm->load(Yii::$app->request->post())) {
            if($passwordChangeForm->changePassword()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Password successfully changed!'));
                return $this->redirect(['update', 'id' => $passwordChangeForm->userid]);
            }
            else {
                Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'Error'));
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {

        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена'));
        }

    }

}
