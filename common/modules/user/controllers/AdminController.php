<?php
namespace common\modules\user\controllers;

use common\modules\user\models\Admin;
use common\modules\user\models\PasswordChangeForm;
use common\modules\user\models\search\UserSearch;
use common\modules\user\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class AdminController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams(), User::ROLE_ADMIN);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionCreate()
    {
        $model = new Admin();
        $model->scenario = 'default';

        if ($model->load(Yii::$app->request->post())) {

            if($model->checkUsername()){
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Этот логин не доступен'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            if($user = $model->saveProfile()){
                return $this->redirect(['update', 'id' => $user->id]);
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error'));
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $passwordChangeForm = new PasswordChangeForm();

        $model = new Admin();
        $model->scenario = 'change';
        $model->setAttributes($user->attributes);

        $currentUsername = $user->username;

        if ($model->load(Yii::$app->request->post())) {
            if($model->usernameIsAvailableForUpdate($currentUsername, $model->username)){
                if($model->changeProfile($id)){
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Success saved'));
                    return $this->redirect(['update', 'id' => $id]);
                } else {
                    Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Error'));
                    return $this->render('update', [
                        'model' => $model,
                        'passwordChangeForm' => $passwordChangeForm,
                        'user' => $user
                    ]);
                }
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Этот логин не доступен'));
                return $this->render('update', [
                    'model' => $model,
                    'passwordChangeForm' => $passwordChangeForm,
                    'user' => $user
                ]);
            }
        }
        else {
            return $this->render('update', [
                'model' => $model,
                'passwordChangeForm' => $passwordChangeForm,
                'user' => $user,
            ]);
        }
    }

    public function actionChangePassword()
    {
        $passwordChangeForm = new PasswordChangeForm();

        if ($passwordChangeForm->load(Yii::$app->request->post())) {
            if($passwordChangeForm->changePassword()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Пароль успешно изменен'));
                return $this->redirect(['update', 'id' => $passwordChangeForm->userid]);
            }
            else {
                Yii::$app->getSession()->setFlash('danger', Yii::t('app', 'Error'));
                return $this->redirect(Yii::$app->request->referrer);
            }
        }

    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }


    protected function findModel($id)
    {
        $model = User::find()->where(['id' => $id, 'role' => User::ROLE_ADMIN])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Страница не найдена'));
        }

    }

}
