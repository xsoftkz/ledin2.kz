<?php

namespace common\modules\shop\controllers;

use common\models\Lang;
use common\modules\shop\models\Product;
use common\modules\shop\models\ProductInfo;
use himiklab\sortablegrid\SortableGridAction;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use yii\web\Controller;
use Yii;

class InfoController extends Controller
{

    public function actionCreate($product_id, $lang_id)
    {
        $model = new ProductInfo(['product_id' => $product_id]);
        $model->language = $lang_id;
        $model->type_id = ProductInfo::TYPE_INFO;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->renderAjax('_product_info_list', [
                'models' => ProductInfo::getInfoList($product_id),
            ]);
        } else {
            return $this->renderAjax('_product_info_form', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id, $lang_id)
    {
        $model = ProductInfo::findOne($id);
        $model->language = $lang_id;

        $lang = Lang::findOne($model->language);
        Yii::$app->sourceLanguage = ($lang) ? $lang->locale : Lang::getDefaultLang()->locale;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->renderAjax('_product_info_list', [
                'models' => ProductInfo::getInfoList($model->product_id),
            ]);
        } else {
            return $this->renderAjax('_product_info_form', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        if ($model = ProductInfo::findOne($id)) {
            $model->delete();
            return $this->renderAjax('_product_info_list', [
                'models' => ProductInfo::getInfoList($model->product_id),
            ]);
        } else {
            return false;
        }
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@images/'.Product::IMAGE_FOLDER,
                'unique' => true,
            ],
        ];
    }
}