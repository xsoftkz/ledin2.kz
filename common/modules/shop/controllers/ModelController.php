<?php

namespace common\modules\shop\controllers;

use common\modules\eav\models\EntityParam;
use common\modules\shop\models\Product;
use common\modules\shop\models\ProductModel;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Yii;

class ModelController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                    'delete-image' => ['POST'],
                ],
            ],
        ];
    }


    public function actionCreate($id)
    {
        $product = $this->findProduct($id);
        $model = new ProductModel();
        $model->product_id = $product->id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                EntityParam::saveEntityParams($model->entity, Yii::$app->request->post('EntityParam'));
                Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            } else {
                Yii::$app->session->setFlash('danger', 'Возникла ошибка');
            }
            return $this->renderAjax('_product_models_list', [
                'models' => ProductModel::find()->where(['product_id' => $model->product_id])->all(),
            ]);
        } else {
            return $this->renderAjax('_product_model_form', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save(false)) {
                EntityParam::saveEntityParams($model->entity, Yii::$app->request->post('EntityParam'));
                Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            } else {
                Yii::$app->session->setFlash('danger', 'Возникла ошибка');
            }
            return $this->renderAjax('_product_models_list', [
                'models' => ProductModel::find()->where(['product_id' => $model->product_id])->all(),
            ]);
        } else {
            return $this->renderAjax('_product_model_form', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $model = ProductModel::findOne($id);
        $prod_id = $model->product_id;
        $model->delete();
        return $this->renderAjax('_product_models_list', [
            'models' => ProductModel::find()->where(['product_id' => $prod_id])->all(),
        ]);
    }

    protected function findProduct($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }

    protected function findModel($id)
    {
        if (($model = ProductModel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }
}