<?php

namespace common\modules\shop\controllers;

use common\models\Lang;
use common\modules\shop\models\ProductInfo;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;

class VideoController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                    'delete-image' => ['POST'],
                ],
            ],
        ];
    }

    public function actionCreate($product_id, $lang_id)
    {
        $model = new ProductInfo(['product_id' => $product_id]);
        $model->language = $lang_id;
        $model->type_id = ProductInfo::TYPE_VIDEO;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->renderAjax('_product_video_list', [
                'models' => ProductInfo::getVideoList($product_id),
            ]);
        } else {
            return $this->renderAjax('_product_video_form', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id, $lang_id)
    {
        $model = ProductInfo::findOne($id);
        $model->language = $lang_id;

        $lang = Lang::findOne($model->language);
        Yii::$app->sourceLanguage = ($lang) ? $lang->locale : Lang::getDefaultLang()->locale;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->renderAjax('_product_video_list', [
                'models' => ProductInfo::getVideoList($model->product_id),
            ]);
        } else {
            return $this->renderAjax('_product_video_form', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        if ($model = ProductInfo::findOne($id)) {
            $model->delete();
            return $this->renderAjax('_product_video_list', [
                'models' => ProductInfo::getVideoList($model->product_id),
            ]);
        } else {
            return false;
        }
    }
}