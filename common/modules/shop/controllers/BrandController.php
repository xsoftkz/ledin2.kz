<?php

namespace common\modules\shop\controllers;

use common\modules\shop\models\ModuleConstant;
use Yii;
use common\modules\shop\models\Brand;
use common\modules\shop\models\search\BrandSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use himiklab\sortablegrid\SortableGridAction;


class BrandController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new BrandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_INDEX);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCart()
    {
        $searchModel = new BrandSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_CART);

        return $this->render('cart', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {
        $model = new Brand();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
                return $this->redirect(['create']);
            } else {
                Yii::$app->session->setFlash('danger', 'Ошибка');
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('danger', 'Ошибка');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionStatus($id){
        $model = $this->findModel($id);

        ($model->status_id == ModuleConstant::IN_ACTIVE) ?
            $model->status_id = ModuleConstant::ACTIVE :
            $model->status_id = ModuleConstant::IN_ACTIVE;

        $model->save();

        Yii::$app->session->setFlash('success', 'Статус сохранен');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::DELETED;
        $model->save();
        Yii::$app->session->setFlash('success', 'Перемещен в корзину');
        return $this->redirect(['index']);
    }


    public function actionRemove($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Успешно удален');
        return $this->redirect(['cart']);
    }


    // Восстановить из корзины
    public function actionReturn($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::IN_ACTIVE;
        $model->save();
        Yii::$app->session->setFlash('success', 'Восстановлен из корзины');
        return $this->redirect(Yii::$app->request->referrer);
    }


    protected function findModel($id)
    {
        if (($model = Brand::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@frontendWebroot/images/'),
                'path' => '@frontend/web/images',
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@frontendWebroot/files/'),
                'path' => '@frontend/web/files/',
                'uploadOnlyImage' => false,
            ],
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Brand::className(),
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@images/'.Brand::IMAGE_FOLDER,
                'unique' => true,
            ],
        ];
    }
}
