<?php

namespace common\modules\shop\controllers;

use common\models\File;
use common\modules\shop\models\Product;
use yii\web\Controller;
use Yii;

class FileController extends Controller
{
    public function actionCreate($product_id, $lang_id)
    {
        $model = new File([
            'item_id' => $product_id,
            'model_name' => Product::MODEL_NAME,
            'folder' => Product::FILE_FOLDER
        ]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->uploadFile()) {
                $models = File::find()->where(['item_id' => $model->item_id, 'model_name' => Product::MODEL_NAME, 'folder' => Product::FILE_FOLDER])->all();
                return $this->renderAjax('_product_file_list', [
                    'models' => $models,
                ]);
            } else {
                return false;
            }
        } else {
            return $this->renderAjax('_product_file_form', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = File::findOne($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->uploadFile()) {
                $models = File::find()->where(['item_id' => $model->item_id, 'model_name' => Product::MODEL_NAME, 'folder' => Product::FILE_FOLDER])->all();
                return $this->renderAjax('_product_file_list', [
                    'models' => $models,
                ]);
            } else {
                return false;
            }
        } else {
            return $this->renderAjax('_product_file_form', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $model = File::findOne($id);
        if ($model){
            $model->delete();
            $models = File::find()->where(['item_id' => $model->item_id, 'model_name' => Product::MODEL_NAME,
                'folder' => Product::FILE_FOLDER])->all();
            return $this->renderAjax('_product_file_list', [
                'models' => $models,
            ]);
        } else {
            return false;
        }
    }
}