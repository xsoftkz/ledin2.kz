<?php

namespace common\modules\shop\controllers;

use common\models\Lang;
use common\modules\eav\models\Type;
use common\modules\shop\models\ModuleConstant;
use Yii;
use common\modules\shop\models\Category;
use common\modules\shop\models\search\CategorySearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;
use himiklab\sortablegrid\SortableGridAction;


class CategoryController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_INDEX);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'langList' => Lang::getList(),
        ]);
    }


    public function actionCart()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_CART);

        return $this->render('cart', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {
        $model = new Category();
        $categories = Category::find()->notDeleted()->orderBy('parent_id')->all();
        $types = Type::find()->all();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
            'categories' => $categories,
            'types' => $types,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $categories = Category::find()->notDeleted()->orderBy('parent_id')->notEqualId($id)->all();
        $types = Type::find()->all();

        $lang = Lang::findOne($model->language);
        Yii::$app->sourceLanguage = ($lang) ? $lang->locale : Lang::getDefaultLang()->locale;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
        }
        return $this->render('update', [
            'model' => $model,
            'categories' => $categories,
            'types' => $types,
        ]);
    }


    public function actionStatus($id)
    {
        $model = $this->findModel($id);

        ($model->status_id == ModuleConstant::IN_ACTIVE) ?
            $model->status_id = ModuleConstant::ACTIVE :
            $model->status_id = ModuleConstant::IN_ACTIVE;

        $model->save();

        Yii::$app->session->setFlash('success', 'Статус сохранен');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::DELETED;
        $model->save();
        Yii::$app->session->setFlash('success', 'Перемещен в корзину');
        return $this->redirect(['index']);
    }


    public function actionRemove($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Успешно удален');
        return $this->redirect(['cart']);
    }


    // Восстановить из корзины
    public function actionReturn($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::IN_ACTIVE;
        $model->save();
        Yii::$app->session->setFlash('success', 'Восстановлен из корзины');
        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@frontendWebroot/images/'),
                'path' => '@frontend/web/images',
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@frontendWebroot/files/'),
                'path' => '@frontend/web/files/',
                'uploadOnlyImage' => false,
            ],
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Category::className(),
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@images/' . Category::IMAGE_FOLDER,
                'unique' => true,
            ],
        ];
    }
}
