<?php

namespace common\modules\shop\controllers;

use common\models\File;
use common\models\Lang;
use common\modules\content\models\Content;
use common\modules\eav\models\Entity;
use common\modules\eav\models\EntityParamFormModel;
use common\modules\eav\models\Type;
use common\modules\shop\models\Category;
use common\modules\shop\models\Image;
use common\modules\shop\models\ModuleConstant;
use common\modules\eav\models\EntityParam;
use common\modules\shop\models\ProductInfo;
use common\modules\shop\models\ProductModel;
use common\modules\shop\services\ProductWaterMarkService;
use Yii;
use common\modules\shop\models\Product;
use common\modules\shop\models\search\ProductSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use himiklab\sortablegrid\SortableGridAction;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

class ProductController extends Controller
{
    public function actionInfo()
    {
        ProductInfo::updateAll(['type_id' => 1]);
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_INDEX);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'langList' => Lang::getList(),
        ]);
    }


    public function actionCart()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_CART);

        return $this->render('cart', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {
        $model = new Product();
        $portfolios = Content::find()->withSection(Content::SECTION_PORTFOLIOS)->notDeleted()->orderBy('sort_index DESC')->all();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->save()) {
                $model->saveImages();
                $model->saveFacadeImages();
                $model->savePortfolios();
                Yii::$app->session->setFlash('success','Данные успешно сохранены');
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('danger', 'Ошибка');
            }
        } else {
            return $this->render('create', [
                'portfolios' => $portfolios,
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $portfolios = Content::find()->withSection(Content::SECTION_PORTFOLIOS)->notDeleted()->orderBy('sort_index DESC')->all();
        $subcategories = Category::find()
            ->where(['parent_id' => $model->category_id])
            ->all();

        $lang = Lang::findOne($model->language);
        Yii::$app->sourceLanguage = ($lang) ? $lang->locale : Lang::getDefaultLang()->locale;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $model->saveImages();
                $model->saveFacadeImages();
                $model->savePortfolios();
                $model->saveSubcategories();

                (new ProductWaterMarkService($model))->run();

                Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
            } else {
                Yii::$app->session->setFlash('danger', 'Ошибка');
            }
            return $this->redirect(Yii::$app->request->referrer);
        }
        $model->related_portfolio = ArrayHelper::getColumn($model->portfolios, 'id');
        $model->related_categories = ArrayHelper::getColumn($model->subcategories, 'id');
        return $this->render('update', [
            'portfolios' => $portfolios,
            'subcategories' => $subcategories,
            'model' => $model
        ]);
    }


    public function actionStatus($id){
        $model = $this->findModel($id);
        $model->status_id = ($model->status_id == ModuleConstant::IN_ACTIVE) ? ModuleConstant::ACTIVE : ModuleConstant::IN_ACTIVE;
        $model->save();

        Yii::$app->session->setFlash('success', 'Статус сохранен');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::DELETED;
        $model->save();
        Yii::$app->session->setFlash('success', 'Перемещен в корзину');
        return $this->redirect(['index']);
    }


    public function actionRemove($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Успешно удален');
        return $this->redirect(['cart']);
    }

    // Восстановить из корзины
    public function actionReturn($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::IN_ACTIVE;
        $model->save();
        Yii::$app->session->setFlash('success', 'Восстановлен из корзины');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionDeleteImage($id){
        $image = Image::findOne($id);
        if($image){
            $image->delete();
            Yii::$app->session->setFlash('success', 'Данные успешно сохранены');
        }
        return $this->redirect(Yii::$app->request->referrer);
    }


    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }


    protected function findInfoModel($id)
    {
        if (($model = ProductInfo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемая страница не существует.');
        }
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'remove' => ['POST'],
                    'delete-image' => ['POST'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@imagesWebroot/').Product::IMAGE_FOLDER,
                'path' => '@frontend/web/images',
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@staticsWebroot/files/'),
                'path' => '@statics/web/files/',
                'uploadOnlyImage' => false,
            ],
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Product::className(),
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@images/'.Product::IMAGE_FOLDER,
                'unique' => true,
            ],
        ];
    }
}
