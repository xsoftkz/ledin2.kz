<?php
use yii\helpers\Url;
/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */
?>

<h2><?= $model->getOrderTitle() ?></h2>
<h3>Детали заказа</h3>
<table  cellpadding="7" border="1" >
    <tr>
        <td>Имя</td>
        <td><?= $model->username ?></td>
    </tr>
    <tr>
        <td>Телефон</td>
        <td><?= $model->phone ?></td>
    </tr>
    <tr>
        <td>Email</td>
        <td><?= $model->email ?></td>
    </tr>
    <tr>
        <td>Адрес</td>
        <td><?= $model->address_info ?></td>
    </tr>	
    <tr>
        <td>Пожелания к заказу</td>
        <td><?= $model->user_comment ?></td>
    </tr>
</table>
<br/>
<h3>Товары</h3>
<?php if($model->products) { ?>

    <table  cellpadding="7" border="1" >
        <tr>
            <th>Наименование</th>
			<th>Артикуль</th>
            <th>Количество</th>
            <th>Цена</th>
        </tr>
        <?php foreach($model->products as $item) { ?>
            <tr>
                <td><?= $item->title ?></td>
				<td><?= ($item->product) ? $item->product->article : '-' ?></td>
                <td><?= $item->quantity ?></td>
                <td><?= $item->price ?> тг.</td>
            </tr>
        <?php }?>
    </table>

<?php }?>

<br/>
<p>Ссылка на заказ:</p>
<p><?= Url::to(['@backendWebroot/shop/order/view/', 'id' => $model->id], 'http') ?></p>
<hr/>
<div>

    <h3>Общая сумма: <strong><?= $model->getProductsSum() ?></strong> тенге</h3>
    <h3>Доставка: <strong><?= $model->getDeliverySum() ?></strong> тенге</h3>
    <h2>Итого к оплате: <strong><?= $model->getTotalSum() ?></strong> тенге</h2>
</div>
