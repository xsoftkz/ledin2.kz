<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_gallery}}`.
 */
class m200130_095328_create_shop_product_portfolio_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shop_product_portfolio}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'portfolio_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_product_portfolio}}');
    }
}
