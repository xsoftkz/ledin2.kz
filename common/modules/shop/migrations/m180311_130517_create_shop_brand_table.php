<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

class m180311_130517_create_shop_brand_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_brand}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),

            'photo' => $this->string(),
            'description' => $this->text(),

            'sort' => $this->integer(),
            'status_id' => $this->smallInteger()->defaultValue(0),

            'slug' => $this->string(),
            'meta_title' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ], $tableOptions);

//        $this->batchInsert('{{%shop_brand}}',
//            ['id', 'title', 'photo', 'description', 'sort', 'status_id', 'slug', 'meta_title', 'meta_keywords', 'meta_description', 'created_at', 'updated_at', 'created_by', 'updated_by'],
//            [
//
//            ]
//        );
    }

    public function down()
    {
        $this->dropTable('{{%shop_brand}}');
    }

}
