<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

class m170302_171906_create_shop_category_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_category}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(),
            'parent_id' => $this->integer(),
            'product_type_id' => $this->integer(),

            'icon' => $this->string(),
            'photo' => $this->string(),

            'sort' => $this->integer(),
            'status_id' => $this->smallInteger()->defaultValue(1),

            'slug' => $this->string(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ], $tableOptions);

        $this->batchInsert('{{%shop_category}}',
            ['id', 'uid', 'parent_id', 'product_type_id', 'icon', 'photo', 'sort', 'status_id', 'slug', 'created_at', 'updated_at', 'created_by', 'updated_by'],
            [
                [1, NULL, 2, NULL, '', '', 5, 1, 'svetilniki-downlight', 1579442465, 1579536366, 1, 1],
                [2, NULL, NULL, NULL, '', '', 7, 1, 'vnutrennee-osvesenie', 1579523686, 1579544620, 1, 1],
                [3, NULL, 2, NULL, '', '', 1, 1, 'administrativno-ofisnye-svetilniki', 1579523698, 1579536371, 1, 1],
                [4, NULL, 2, NULL, '', '', 6, 1, 'svetilniki-spotlight', 1579523713, 1579536375, 1, 1],
                [5, NULL, 2, NULL, '', '', 2, 1, 'svetilniki-linejnye', 1579523722, 1579536381, 1, 1],
                [6, NULL, 2, NULL, '', '', 3, 1, 'svetilniki-mnogo-funkcionalnye', 1579523731, 1579536413, 1, 1],
                [7, NULL, 2, NULL, '', '', 4, 1, 'svetilniki-modulnye', 1579523741, 1579536417, 1, 1],
                [8, NULL, 2, NULL, '', '', 8, 1, 'svetilniki-downlight-povorotnye', 1579523769, 1579536423, 1, 1],
                [9, NULL, 2, NULL, '', '', 9, 1, 'potolocnye-nastennye-mounted', 1579523778, 1579536428, 1, 1],
                [10, NULL, 2, NULL, '', '', 10, 1, 'svetilniki-nastennye', 1579523787, 1579536444, 1, 1],
                [11, NULL, 2, NULL, '', '', 11, 1, 'ekspozicionnye-track', 1579523796, 1579536449, 1, 1],
                [12, NULL, 2, NULL, '', '', 12, 1, 'promyslennye-high-bay', 1579523806, 1579536454, 1, 1],
                [13, NULL, 2, NULL, '', '', 13, 1, 'lampy-led-buld-tube', 1579523821, 1579536458, 1, 1],
                [14, NULL, 2, NULL, '', '', 14, 1, 'podvesnye-svetilniki', 1579523830, 1579536462, 1, 1],
                [15, NULL, 2, NULL, '', '', 15, 1, 'svetodiodnye-led-stripes', 1579523838, 1579536466, 1, 1],
                [16, NULL, 2, NULL, '', '', 16, 1, 'smart-led-svetilniki', 1579523845, 1579536471, 1, 1],
                [17, NULL, 2, NULL, '', '', 17, 1, 'avarijnye-svetilniki', 1579523854, 1579536475, 1, 1],
                [18, NULL, 2, NULL, '', '', 18, 1, 'svetilniki-nakladnye', 1579523862, 1579536479, 1, 1],
                [19, NULL, NULL, NULL, '', '', 19, 1, 'naruznoe-osvesenie', 1579523905, 1579536362, 1, 1],
                [20, NULL, 19, NULL, '', '', 20, 1, 'gorodskie-svetilniki', 1579523910, 1579536484, 1, 1],
                [21, NULL, 19, NULL, '', '', 21, 1, 'klassiceskie-svetilniki', 1579523938, 1579536501, 1, 1],
                [22, NULL, 19, NULL, '', '', 22, 1, 'ulicnye-svetilniki', 1579524042, 1579536504, 1, 1],
                [23, NULL, 19, NULL, '', '', 23, 1, 'sadovo-parkovye-svetilniki', 1579524058, 1579536508, 1, 1],
                [24, NULL, 19, NULL, '', '', 24, 1, 'prozektora', 1579524072, 1579536512, 1, 1],
                [25, NULL, 19, NULL, '', '', 25, 1, 'linejnye-svetilniki', 1579524092, 1579536523, 1, 1],
                [26, NULL, 19, NULL, '', '', 26, 1, 'medijnye', 1579524419, 1579536527, 1, 1],
                [27, NULL, 19, NULL, '', '', 27, 1, 'potolocnye-svetilniki', 1579524428, 1579536532, 1, 1],
                [28, NULL, 19, NULL, '', '', 28, 1, 'nastennye-svetilniki', 1579524436, 1579536537, 1, 1],
                [29, NULL, 19, NULL, '', '', 29, 1, 'vstraivaemye-svetilniki', 1579524446, 1579536542, 1, 1],
                [30, NULL, 19, NULL, '', '', 30, 1, 'gruntovye-svetilniki', 1579524453, 1579536546, 1, 1],
                [31, NULL, 19, NULL, '', '', 31, 1, 'landsaftnye-svetilniki', 1579524464, 1579536556, 1, 1],
                [32, NULL, 19, NULL, '', '', 32, 1, 'podvodnye-svetilniki', 1579524475, 1579536560, 1, 1],
                [33, NULL, 19, NULL, '', '', 33, 1, 'svetodiodnye-lenty', 1579524482, 1579536564, 1, 1],
                [34, NULL, 19, NULL, '', '', 34, 1, 'smart-svetilniki-i-sistemy-avtonomnogo-osvesenia', 1579524492, 1579536568, 1, 1],
                [35, NULL, 19, NULL, '', '', 35, 1, 'opory-osvesenia', 1579524502, 1579536572, 1, 1]
            ]
        );
    }


    public function down()
    {
        $this->dropTable('{{%shop_category}}');
    }

}
