<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shop_product_info_lang}}`.
 */
class m200123_112202_create_shop_product_info_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shop_product_info_lang}}', [
            'id' => $this->primaryKey(),
            'item_id' => $this->integer()->notNull(),
            'lang_id' => $this->string(8)->notNull(),
            'title' => $this->string(65)->notNull(),
            'desc' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_product_info_lang}}');
    }
}
