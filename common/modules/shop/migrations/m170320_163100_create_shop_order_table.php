<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

class m170320_163100_create_shop_order_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_order}}', [

            'id' => $this->primaryKey(),
            'hash' => $this->string()->notNull(), // уникальный токен для просмотра заказа без авторизации
            'user_id' => $this->integer()->defaultValue(0),

            'username' => $this->string()->notNull(),
            'email' => $this->string(),
            'phone' => $this->string()->notNull(),

            'address_id' => $this->integer()->defaultValue(0), // выбранный адрес клиента к заказау
            'address_info' => $this->string(), // инормация об адресе

            'delivery_type_id' => $this->integer()->defaultValue(1), // тип доставки (1-куръерская доставка)
            'delivery_date' => $this->integer(), // день доставки
            'delivery_time' => $this->string(), // время доставки
            'persons_count' => $this->integer(), // количество персон
            'surrender_sum' => $this->integer(), // нужна сдача с суммы

            'rate_delivery_sum' => $this->integer(), // ставка суммы доставки
            'rate_free_delivery_sum' => $this->integer(), // ставка суммы заказа для бесплатной доставки
            'rate_discount' => $this->integer(), // ставка скидки (процент %)

            'payment_type_id' => $this->integer()->defaultValue(1), // тип оплаты ( 1-наличный )
            'manager_id' => $this->integer(), // оператор который принял заказ

            'user_comment' => $this->text(), // пожелания клиента к заказа
            'admin_comment' => $this->text(), // комментарий оператора к заказу

            'is_paid' =>  $this->integer()->defaultValue(0), // заказ оплачен
            'app_type_id' =>  $this->integer(), // тип клиентского приложения (сайт/приложения)
            'app_user_agent' =>  $this->string(), // устройство пользователя
            'app_ip' =>  $this->string(), // ip адрес пользователя

            'status_id' =>  $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ], $tableOptions);
    }


    public function down()
    {
        $this->dropTable('{{%shop_order}}');
    }


}
