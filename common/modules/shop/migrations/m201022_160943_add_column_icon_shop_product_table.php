<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Class m201022_160943_add_column_icon_shop_product_table
 */
class m201022_160943_add_column_icon_shop_product_table extends Migration
{
    public $tableName = '{{%shop_product}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'icon', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'icon');
    }
}
