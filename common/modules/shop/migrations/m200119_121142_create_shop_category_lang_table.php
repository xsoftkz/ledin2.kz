<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shop_category_lang}}`.
 */
class m200119_121142_create_shop_category_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_category_lang}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'lang_id' => $this->string(8)->notNull(),
            'title' => $this->string(65)->notNull(),
            'desc' => $this->text(),
            'meta_title' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),
        ]);

        $this->batchInsert('{{%shop_category_lang}}',
            ['id', 'category_id', 'lang_id', 'title', 'desc', 'meta_title', 'meta_keywords', 'meta_description'],
            [
                [1, 1, 'ru', 'Светильники Downlight', '<p>Downlight - это универсальные световые приборы, предназначенные как для общего, так и для направленного освещения в зависимости от используемой оптики. Благодаря компактным размерам, простой и удобной установке, широкому диапазону световых потоков и потребляемых мощностей Downlight светильники являются наиболее востребованным типом приборов и применяются в торговых, офисно-административных помещениях, холлах и фойе.</p>', 'Светильники Downlight', NULL, NULL],
                [2, 1, 'kz', 'Светильники Downlight KZ', '', 'Светильники Downlight KZ', NULL, NULL],
                [3, 2, 'ru', 'Внутреннее освещение', '', 'Внутреннее освещение', NULL, NULL],
                [4, 3, 'ru', 'Административно - офисные светильники', '', 'Административно - офисные светильники', NULL, NULL],
                [5, 4, 'ru', 'Светильники Spotlight', '<p>Светильники Spotlight. Широкий выбор внешнего оформления, большой диапазон светового потока и удобная форма крепления дают возможность для решения любого круга задач местного освещения акцентированной направленности. Небольшие габариты, низкая потребляемая мощность, широкий выбор цветовых температур, светодиодных светильников точечного света являются главными, но не единственными достоинствами источников света этого типа.</p>', 'Светильники Spotlight', NULL, NULL],
                [6, 5, 'ru', 'Светильники Линейные', '', 'Светильники Линейные', NULL, NULL],
                [7, 6, 'ru', 'Светильники Много - функциональные', '', 'Светильники Много - функциональные', NULL, NULL],
                [8, 7, 'ru', 'Светильники Модульные', '', 'Светильники Модульные', NULL, NULL],
                [9, 8, 'ru', 'Светильники  Downlight поворотные', '', 'Светильники  Downlight поворотные', NULL, NULL],
                [10, 9, 'ru', 'Потолочные, настенные Mounted', '', 'Потолочные, настенные Mounted', NULL, NULL],
                [11, 10, 'ru', 'Светильники Настенные', '', 'Светильники Настенные', NULL, NULL],
                [12, 11, 'ru', 'Экспозиционные TRACK', '', 'Экспозиционные TRACK', NULL, NULL],
                [13, 12, 'ru', 'Промышленные High bay', '', 'Промышленные High bay', NULL, NULL],
                [14, 13, 'ru', 'Лампы Led Buld Tube', '', 'Лампы Led Buld Tube', NULL, NULL],
                [15, 14, 'ru', 'Подвесные светильники', '', 'Подвесные светильники', NULL, NULL],
                [16, 15, 'ru', 'Светодиодные Led Stripes', '', 'Светодиодные Led Stripes', NULL, NULL],
                [17, 16, 'ru', 'Smart Led светильники', '', 'Smart Led светильники', NULL, NULL],
                [18, 17, 'ru', 'Аварийные светильники', '', 'Аварийные светильники', NULL, NULL],
                [19, 18, 'ru', 'Светильники Накладные', '', 'Светильники Накладные', NULL, NULL],
                [20, 19, 'ru', 'Наружное освещение', '', 'Наружное освещение', NULL, NULL],
                [21, 20, 'ru', 'Городские светильники', '', 'Городские светильники', NULL, NULL],
                [22, 21, 'ru', 'Классические светильники', '', 'Классические светильники', NULL, NULL],
                [23, 22, 'ru', 'Уличные светильники', '', 'Уличные светильники', NULL, NULL],
                [24, 23, 'ru', 'Садово-парковые светильники', '', 'Садово-парковые светильники', NULL, NULL],
                [25, 24, 'ru', 'Прожектора', '', 'Прожектора', NULL, NULL],
                [26, 25, 'ru', 'Линейные светильники', '', 'Линейные светильники', NULL, NULL],
                [27, 26, 'ru', 'Медийные', '', 'Медийные', NULL, NULL],
                [28, 27, 'ru', 'Потолочные светильники', '', 'Потолочные светильники', NULL, NULL],
                [29, 28, 'ru', 'Настенные светильники', '', 'Настенные светильники', NULL, NULL],
                [30, 29, 'ru', 'Встраиваемые светильники', '', 'Встраиваемые светильники', NULL, NULL],
                [31, 30, 'ru', 'Грунтовые светильники', '', 'Грунтовые светильники', NULL, NULL],
                [32, 31, 'ru', 'Ландшафтные светильники', '', 'Ландшафтные светильники', NULL, NULL],
                [33, 32, 'ru', 'Подводные светильники', '', 'Подводные светильники', NULL, NULL],
                [34, 33, 'ru', 'Светодиодные ленты', '', 'Светодиодные ленты', NULL, NULL],
                [35, 34, 'ru', 'Smart светильники и системы автономного освещения', '', 'Smart светильники и системы автономного освещения', NULL, NULL],
                [36, 35, 'ru', 'Опоры освещения', '', 'Опоры освещения', NULL, NULL],
                [37, 2, 'kz', 'Внутреннее освещение KZ', '', 'Внутреннее освещение KZ', NULL, NULL],
                [38, 19, 'kz', 'Наружное освещение KZ', '', 'Наружное освещение KZ', NULL, NULL],
                [39, 3, 'kz', 'Административно - офисные светильники KZ', '', 'Административно - офисные светильники KZ', NULL, NULL],
                [40, 4, 'kz', 'Светильники Spotlight KZ', '', 'Светильники Spotlight KZ', NULL, NULL],
                [41, 5, 'kz', 'Светильники Линейные KZ', '', 'Светильники Линейные KZ', NULL, NULL],
                [42, 6, 'kz', 'Светильники Много - функциональные KZ', '', 'Светильники Много - функциональные KZ', NULL, NULL],
                [43, 7, 'kz', 'Светильники Модульные KZ', '', 'Светильники Модульные KZ', NULL, NULL],
                [44, 8, 'kz', 'Светильники  Downlight поворотные KZ', '', 'Светильники  Downlight поворотные KZ', NULL, NULL],
                [45, 9, 'kz', 'Потолочные, настенные Mounted KZ', '', 'Потолочные, настенные Mounted KZ', NULL, NULL],
                [46, 10, 'kz', 'Светильники Настенные KZ', '', 'Светильники Настенные KZ', NULL, NULL],
                [47, 11, 'kz', 'Экспозиционные TRACK KZ', '', 'Экспозиционные TRACK KZ', NULL, NULL],
                [48, 12, 'kz', 'Промышленные High bay KZ', '', 'Промышленные High bay KZ', NULL, NULL],
                [49, 13, 'kz', 'Лампы Led Buld Tube KZ', '', 'Лампы Led Buld Tube KZ', NULL, NULL],
                [50, 14, 'kz', 'Подвесные светильники KZ', '', 'Подвесные светильники KZ', NULL, NULL],
                [51, 15, 'kz', 'Светодиодные Led Stripes KZ', '', 'Светодиодные Led Stripes KZ', NULL, NULL],
                [52, 16, 'kz', 'Smart Led светильники KZ', '', 'Smart Led светильники KZ', NULL, NULL],
                [53, 17, 'kz', 'Аварийные светильники KZ', '', 'Аварийные светильники KZ', NULL, NULL],
                [54, 18, 'kz', 'Светильники Накладные KZ', '', 'Светильники Накладные KZ', NULL, NULL],
                [55, 20, 'kz', 'Городские светильники KZ', '', 'Городские светильники KZ', NULL, NULL],
                [56, 21, 'kz', 'Классические светильники KZ', '', 'Классические светильники KZ', NULL, NULL],
                [57, 22, 'kz', 'Уличные светильники KZ', '', 'Уличные светильники KZ', NULL, NULL],
                [58, 23, 'kz', 'Садово-парковые светильники KZ', '', 'Садово-парковые светильники KZ', NULL, NULL],
                [59, 24, 'kz', 'Прожектора KZ', '', 'Прожектора KZ', NULL, NULL],
                [60, 25, 'kz', 'Линейные светильники KZ', '', 'Линейные светильники KZ', NULL, NULL],
                [61, 26, 'kz', 'Медийные KZ', '', 'Медийные KZ', NULL, NULL],
                [62, 27, 'kz', 'Потолочные светильники KZ', '', 'Потолочные светильники KZ', NULL, NULL],
                [63, 28, 'kz', 'Настенные светильники KZ', '', 'Настенные светильники KZ', NULL, NULL],
                [64, 29, 'kz', 'Встраиваемые светильники KZ', '', 'Встраиваемые светильники KZ', NULL, NULL],
                [65, 30, 'kz', 'Грунтовые светильники KZ', '', 'Грунтовые светильники KZ', NULL, NULL],
                [66, 31, 'kz', 'Ландшафтные светильники KZ', '', 'Ландшафтные светильники KZ', NULL, NULL],
                [67, 32, 'kz', 'Подводные светильники KZ', '', 'Подводные светильники KZ', NULL, NULL],
                [68, 33, 'kz', 'Светодиодные ленты KZ', '', 'Светодиодные ленты KZ', NULL, NULL],
                [69, 34, 'kz', ' Smart светильники и системы автономного освещения KZ', '', ' Smart светильники и системы автономного освещения KZ', NULL, NULL],
                [70, 35, 'kz', 'Опоры освещения KZ', '', 'Опоры освещения KZ', NULL, NULL],
                [71, 2, 'en', 'Внутреннее освещение ENG', '', 'Внутреннее освещение ENG', NULL, NULL],
                [72, 3, 'en', 'Административно - офисные светильники ENG', '', 'Административно - офисные светильники ENG', NULL, NULL],
                [73, 19, 'en', 'Наружное освещение ENG', '', 'Наружное освещение ENG', NULL, NULL]
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_category_lang}}');
    }
}
