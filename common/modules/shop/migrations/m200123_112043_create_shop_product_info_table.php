<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shop_product_info}}`.
 */
class m200123_112043_create_shop_product_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shop_product_info}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'photo' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_product_info}}');
    }
}
