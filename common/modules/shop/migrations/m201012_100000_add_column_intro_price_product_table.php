<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Class m201012_100000_add_column_intro_price_product_table
 */
class m201012_100000_add_column_intro_price_product_table extends Migration
{
    public $tableName = '{{%shop_product_lang}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'intro', $this->text());
        $this->addColumn($this->tableName, 'price_label', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'intro');
        $this->dropColumn($this->tableName, 'price_label');
    }
}
