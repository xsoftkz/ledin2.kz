<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Class m200602_053953_add_column_second_title_shop_category_lang_table
 */
class m200602_053953_add_column_second_title_shop_category_lang_table extends Migration
{
    public $tableName = '{{%shop_category_lang}}';

    public function safeUp()
    {
//        $this->addColumn($this->tableName, 'singular_title', $this->string(255));
    }

    public function safeDown()
    {
//        $this->dropColumn($this->tableName, 'singular_title');
    }
}
