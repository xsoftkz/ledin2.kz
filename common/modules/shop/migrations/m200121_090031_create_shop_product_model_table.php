<?php

namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shop_product_model}}`.
 */
class m200121_090031_create_shop_product_model_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shop_product_model}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'type_id' => $this->integer(),
            'name' => $this->string()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_product_model}}');
    }
}
