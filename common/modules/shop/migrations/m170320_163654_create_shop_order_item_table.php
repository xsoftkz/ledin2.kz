<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

class m170320_163654_create_shop_order_item_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_order_item}}', [

            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),

            'title' => $this->string()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'price' => $this->float()->notNull(),

        ], $tableOptions);
    }


    public function down()
    {
        $this->dropTable('{{%shop_order_item}}');
    }

}
