<?php

namespace common\modules\shop\migrations;

use yii\db\Migration;

class m170302_165459_create_shop_product_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(),
            'article' => $this->string(),
            'model_name' => $this->string(),
            'price' => $this->decimal(),

            'photo' => $this->string(),
            'video' => $this->string(),

            'category_id' => $this->integer()->notNull(),
            'brand_id' => $this->integer(),
            'type_id' => $this->integer(),

            'value_type_id' => $this->integer()->defaultValue(1),
            'quantity' => $this->integer()->defaultValue(0),
            'sort' => $this->integer(),
            'views' => $this->integer()->defaultValue(0),
            'status_id' => $this->smallInteger()->defaultValue(1),
            'available_id' => $this->smallInteger()->defaultValue(1),

            'slug' => $this->string(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ], $tableOptions);

        $this->batchInsert('{{%shop_product}}',
            ['id', 'uid', 'article', 'model_name', 'price', 'photo', 'category_id', 'brand_id', 'type_id', 'value_type_id', 'quantity', 'sort', 'views', 'status_id', 'available_id', 'slug', 'created_at', 'updated_at', 'created_by', 'updated_by'],
            [
                [1, NULL, NULL, NULL, NULL, '5e25ab1d63d46.png', 1, NULL, NULL, 1, 0, 1, 0, 1, 1, 'li-5071-12-1', 1579526859, 1579536764, 1, 1],
                [2, NULL, NULL, NULL, NULL, '5e25ab6931eae.png', 1, NULL, NULL, 1, 0, 2, 0, 1, 1, 'li-5072-25-2', 1579527020, 1579536756, 1, 1],
                [3, NULL, NULL, NULL, NULL, '5e25abbca3727.png', 1, NULL, NULL, 1, 0, 3, 0, 1, 1, 'li-5075-09-3', 1579527104, 1579536746, 1, 1],
                [4, NULL, NULL, NULL, NULL, '5e25bfc7c8055.png', 1, NULL, NULL, 1, 0, 4, 0, 1, 1, 'li-5093a-09-4', 1579532233, 1579536794, 1, 1],
                [5, NULL, NULL, NULL, NULL, '5e25bfde6f0de.png', 1, NULL, NULL, 1, 0, 5, 0, 1, 1, 'li-5202-13-5', 1579532257, 1579536786, 1, 1],
                [6, NULL, NULL, NULL, NULL, '5e25c04d57ae0.png', 1, NULL, NULL, 1, 0, 6, 0, 1, 1, 'li-5202-13-6', 1579532371, 1579536780, 1, 1]
            ]
        );
    }

    public function down()
    {
        $this->dropTable('{{%shop_product}}');
    }

}
