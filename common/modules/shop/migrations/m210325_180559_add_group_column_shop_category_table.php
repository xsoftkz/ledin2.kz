<?php
namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Class m210325_180559_add_group_column_shop_category_table
 */
class m210325_180559_add_group_column_shop_category_table extends Migration
{
    public $tableName = '{{%shop_category}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'group_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'group_id');
    }
}
