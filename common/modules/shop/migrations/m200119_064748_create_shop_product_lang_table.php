<?php

namespace common\modules\shop\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table '{{%shop_product_lang}}'.
 */
class m200119_064748_create_shop_product_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%shop_product_lang}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'lang_id' => $this->string(8)->notNull(),
            'title' => $this->string(65)->notNull(),
            'desc' => $this->text(),
            'meta_title' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),
        ]);

        $this->batchInsert('{{%shop_product_lang}}',
            ['id', 'product_id', 'lang_id', 'title', 'desc', 'meta_title', 'meta_keywords', 'meta_description'],
            [
                [1, 1, 'ru', 'LI-5071-12', '', 'LI-5071-12', NULL, NULL],
                [2, 2, 'ru', 'LI-5072-25', '', 'LI-5072-25', NULL, NULL],
                [3, 3, 'ru', 'LI-5075-09', '', 'LI-5075-09', NULL, NULL],
                [4, 4, 'ru', 'LI-5093A-09', '', 'LI-5093A-09', NULL, NULL],
                [5, 5, 'ru', 'LI-5202-06', '', 'LI-5202-06', NULL, NULL],
                [6, 6, 'ru', 'LI-5202-13', '', 'LI-5202-13', NULL, NULL],
                [7, 6, 'kz', 'LI-5202-13', '', 'LI-5202-13', NULL, NULL],
                [8, 5, 'kz', 'LI-5202-06', '', 'LI-5202-06', NULL, NULL],
                [9, 4, 'kz', 'LI-5093A-09', '', 'LI-5093A-09', NULL, NULL],
                [10, 3, 'kz', 'LI-5075-09', '', 'LI-5075-09', NULL, NULL],
                [11, 3, 'en', 'LI-5075-09', '', 'LI-5075-09', NULL, NULL],
                [12, 2, 'kz', 'LI-5072-25', '', 'LI-5072-25', NULL, NULL],
                [13, 2, 'en', 'LI-5072-25', '', 'LI-5072-25', NULL, NULL],
                [14, 1, 'kz', 'LI-5071-12', '', 'LI-5071-12', NULL, NULL],
                [15, 1, 'en', 'LI-5071-12', '', 'LI-5071-12', NULL, NULL],
                [16, 6, 'en', 'LI-5202-13', '', 'LI-5202-13', NULL, NULL],
                [17, 5, 'en', 'LI-5202-13', '', 'LI-5202-13', NULL, NULL],
                [18, 4, 'en', 'LI-5093A-09', '', 'LI-5093A-09', NULL, NULL]
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shop_product_lang}}');
    }
}
