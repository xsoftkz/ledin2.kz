<?php

namespace common\modules\shop\models;

use common\models\traits\ImageTrait;
use common\modules\shop\models\lang\ProductInfoLang;
use lav45\translate\TranslatedTrait;
use Yii;

/**
 * This is the model class for table "shop_product_info".
 *
 * @property int $id
 * @property int $product_id
 * @property string|null $photo
 */
class ProductInfo extends \yii\db\ActiveRecord
{
    use ImageTrait;
    use TranslatedTrait;
    const IMAGE_FOLDER = 'product';
    const TYPE_INFO = 1;
    const TYPE_VIDEO = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_product_info}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['product_id'], 'required'],
            [['product_id', 'type_id'], 'integer'],
            [['desc'], 'string'],
            [['photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'desc' => 'Описание',
            'type_id' => 'Тип',
            'product_id' => 'Товар',
            'photo' => 'Изображение',
        ];
    }

    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'photo' => [
                        'path' => '@images/'.self::IMAGE_FOLDER,
                        'tempPath' => '@images/'.self::IMAGE_FOLDER,
                    ],
                ]
            ],
            [
                'class' => 'lav45\translate\TranslatedBehavior',
                'translateRelation' => 'productInfoLangs',
                'languageAttribute' => 'lang_id',
                'translateAttributes' => [
                    'title',
                    'desc',
                ]
            ]
        ];
    }


    public function getProductInfoLangs()
    {
        return $this->hasMany(ProductInfoLang::className(), ['item_id' => 'id']);
    }

    public static function getVideoList($item_id){
        return ProductInfo::find()->where(['product_id' => $item_id, 'type_id' => self::TYPE_VIDEO])->all();
    }

    public static function getInfoList($item_id){
        return ProductInfo::find()->where(['product_id' => $item_id, 'type_id' => self::TYPE_INFO])->all();
    }

}
