<?php

namespace common\modules\shop\models;

use common\modules\eav\models\traits\EntityTrait;
use Yii;

/**
 * This is the model class for table "xs_shop_product_model".
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 */
class ProductModel extends \yii\db\ActiveRecord
{
    use EntityTrait;

    const MODEL_NAME = 'product-model';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_product_model}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'name'], 'required'],
            [['product_id', 'type_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'type_id' => 'Тип товара',
            'name' => 'Название',
        ];
    }

    public function getProduct(){
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getProductName(){
        if ($this->product){
            return $this->product->title;
        } else {
            return '-';
        }
    }

}
