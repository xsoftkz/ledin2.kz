<?php
namespace common\modules\shop\models;

use common\models\Lang;
use common\models\traits\ImageTrait;
use common\modules\shop\models\lang\CategoryLang;
use common\modules\shop\models\query\CategoryQuery;
use lav45\translate\TranslatedTrait;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%shop_category}}".
 *
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property string $icon
 * @property string $photo
 * @property string $description
 * @property integer $sort
 * @property integer $status
 * @property string $slug
 * @property string $meta_title
 * @property string $seo_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $product_type_id
 */
class Category extends ActiveRecord
{
    use ImageTrait;
    use TranslatedTrait;

    const IMAGE_FOLDER = 'category';

    public static function tableName()
    {
        return '{{%shop_category}}';
    }

    public function rules()
    {
        return [
            [['title'], 'required'],
            [['parent_id', 'sort', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'product_type_id', 'group_id'], 'integer'],
            [['desc', 'meta_description', 'link'], 'string'],
            [['uid', 'title', 'singular_title', 'icon', 'photo', 'slug', 'meta_keywords', 'meta_title', 'seo_title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'uid' => 'UID',
            'parent_id' => 'Родительская категория',
            'icon' => 'Иконка',
            'photo' => 'Изображения',
            'link' => 'Ссылка',
            'desc' => 'Описание',
            'sort' => 'Порядок сортировки',
            'status_id' => 'Статус',
            'slug' => 'SEO ссылка',
            'seo_title' => 'SEO заголовок',
            'created_at' => 'Добавлен',
            'updated_at' => 'Обновлен',
            'created_by' => 'Добавил',
            'updated_by' => 'Обновил',
            'product_type_id' => 'Тип товара',
            'group_id' => 'Группа подкатегорий',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'photo' => [
                        'path' => '@images/'.self::IMAGE_FOLDER,
                        'tempPath' => '@images/'.self::IMAGE_FOLDER,
                    ],
                    'icon' => [
                        'path' => '@images/'.self::IMAGE_FOLDER,
                        'tempPath' => '@images/'.self::IMAGE_FOLDER,
                    ],
                ]
            ],
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort'
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            [
                'class' => 'lav45\translate\TranslatedBehavior',
                'translateRelation' => 'categoryLangs',
                'languageAttribute' => 'lang_id',
                'translateAttributes' => [
                    'title',
                    'singular_title',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                    'desc',
                ]
            ]
        ];

    }

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public function afterDelete()
    {
        parent::afterDelete(); //
        CategoryLang::deleteAll(['category_id' => $this->id]);
    }

    public function getCategoryLangs()
    {
        return $this->hasMany(CategoryLang::className(), ['category_id' => 'id']);
    }


    public static function getDropdownList()
    {
        $categories = Category::find()->notDeleted()->orderBy('parent_id')->all();
        return ArrayHelper::map($categories, 'id', 'titleSelect');
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getCategories()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id'])
            ->where(['status_id' => ModuleConstant::ACTIVE])->orderBy('sort');
    }


    public function getSubCategoryIds(){
        $ids = [];

        if($this->categories){
            $ids = ArrayHelper::getColumn($this->categories, 'id');
        }
        array_push($ids, $this->id);

        return $ids;
    }

    public function getTitleSelect()
    {
        $title = $this->defaultTitle;
        if ($this->parent) {
            $title = $this->parent->defaultTitle . ' - ' . $this->defaultTitle;

            if ($this->parent->parent) {
                $title = $this->parent->parent->defaultTitle . ' - ' . $title;

                if ($this->parent->parent->parent) {
                    $title = $this->parent->parent->parent->defaultTitle . ' - ' . $title;

                    if ($this->parent->parent->parent->parent) {
                        $title = $this->parent->parent->parent->parent->defaultTitle . ' - ' . $title;
                    }
                }
            }
        }
        return $title;
    }

    public function getDefaultContent(){
        return CategoryLang::find()->where(['category_id' => $this->id, 'lang_id' => Lang::DEFAULT_LANG])->one();
    }

    public function getDefaultTitle(){
        if ($this->defaultContent) {
            return $this->defaultContent->title;
        } else {
            return '-';
        }
    }

    public static function getGroups()
    {
        return [
            1 => 'По типу',
            2 => 'По цвету',
            3 => 'По мощности',
            4 => 'Популярные бренды',
            5 => 'По стране',
        ];
    }
}
