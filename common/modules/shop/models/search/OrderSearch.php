<?php

namespace common\modules\shop\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\shop\models\Order;

/**
 * OrderSearch represents the model behind the search form of `common\modules\shop\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'delivery_type_id', 'payment_type_id', 'is_paid', 'app_type_id', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['hash', 'username', 'email', 'phone', 'address_info', 'user_comment', 'admin_comment', 'app_user_agent', 'app_ip'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

//        if(!$params){
            $query->orderBy(['id' => SORT_DESC]);
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'delivery_type_id' => $this->delivery_type_id,
            'payment_type_id' => $this->payment_type_id,
            'is_paid' => $this->is_paid,
            'app_type_id' => $this->app_type_id,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'hash', $this->hash])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address_info', $this->address_info])
            ->andFilterWhere(['like', 'user_comment', $this->user_comment])
            ->andFilterWhere(['like', 'admin_comment', $this->admin_comment])
            ->andFilterWhere(['like', 'app_user_agent', $this->app_user_agent])
            ->andFilterWhere(['like', 'app_ip', $this->app_ip]);

        return $dataProvider;
    }
}
