<?php
namespace common\modules\shop\models\search;

use common\models\Lang;
use common\modules\shop\models\ModuleConstant;
use common\modules\shop\models\Category;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class CategorySearch extends Category
{
    public function rules()
    {
        return [
            [['id', 'parent_id', 'sort', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'icon', 'photo', 'desc', 'slug', 'meta_title', 'meta_keywords', 'meta_description'], 'safe'],
        ];
    }


    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params, $action = false)
    {
        $query = Category::find();

        if($action == ModuleConstant::ACTION_INDEX){
            $query->notDeleted();
        }

        if($action == ModuleConstant::ACTION_CART){
            $query->deleted();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if(!$params){
            $query->orderBy('parent_id');
        } else {
            $query->orderBy('sort');

        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'parent_id' => $this->parent_id,
            'sort' => $this->sort,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        if ($this->title) {
            $query->leftJoin("{{%shop_category_lang}}", '{{%shop_category_lang}}.category_id = {{%shop_category}}.id');
            $query->andFilterWhere(['like', '{{%shop_category_lang}}.title', $this->title]);
            $query->andWhere(['{{%shop_category_lang}}.lang_id' => Lang::DEFAULT_LANG]);
        }

        return $dataProvider;
    }
}
