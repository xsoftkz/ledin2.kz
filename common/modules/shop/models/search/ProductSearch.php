<?php
namespace common\modules\shop\models\search;

use common\models\Lang;
use common\modules\shop\models\ModuleConstant;
use common\modules\shop\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ProductSearch extends Product
{

    public function rules()
    {
        return [
            [['id', 'category_id', 'brand_id', 'sort', 'views', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'article', 'photo', 'desc', 'slug', 'meta_title', 'meta_keywords', 'meta_description'], 'safe'],
            [['price'], 'number'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }


    public function search($params, $action = false)
    {
        $query = Product::find();

        if($action == ModuleConstant::ACTION_INDEX){
            $query->notDeleted();
        }

        if($action == ModuleConstant::ACTION_CART){
            $query->deleted();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if(!$params){
            $query->orderBy('created_at DESC');
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'brand_id' => $this->brand_id,
            'price' => $this->price,
            'sort' => $this->sort,
            'views' => $this->views,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'article', $this->article])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'desc', $this->desc])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        if ($this->title) {
            $query->leftJoin("{{%shop_product_lang}}", '{{%shop_product_lang}}.product_id = {{%shop_product}}.id');
            $query->andFilterWhere(['like', '{{%shop_product_lang}}.title', $this->title]);
            $query->andWhere(['{{%shop_product_lang}}.lang_id' => Lang::DEFAULT_LANG]);
        }


        return $dataProvider;
    }
}
