<?php
namespace common\modules\shop\models\search;

use common\modules\shop\models\ModuleConstant;
use common\modules\shop\models\Brand;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class BrandSearch extends Brand
{
    public function rules()
    {
        return [
            [['id', 'sort', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'photo', 'description', 'slug', 'meta_title', 'meta_keywords', 'meta_description'], 'safe'],
        ];
    }


    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    public function search($params, $action = false)
    {
        $query = Brand::find();

        if($action == ModuleConstant::ACTION_INDEX){
            $query->notDeleted();
        }

        if($action == ModuleConstant::ACTION_CART){
            $query->deleted();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        $this->load($params);

        if(!$params){
            $query->orderBy('sort');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sort' => $this->sort,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        return $dataProvider;
    }
}
