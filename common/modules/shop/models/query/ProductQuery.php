<?php
namespace common\modules\shop\models\query;

use common\modules\shop\models\ModuleConstant;
use yii\db\ActiveQuery;


class ProductQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['<>','status_id', ModuleConstant::DELETED]);
    }

    public function notEqualedId($id)
    {
        return $this->andWhere(['<>','id', $id]);
    }

    public function deleted()
    {
        return $this->andWhere(['status_id' => ModuleConstant::DELETED]);
    }

    public function published()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function notPublished()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function withCategory($id)
    {
        return $this->andWhere(['category_id' => $id]);
    }

    public function withBrand($id)
    {
        return $this->andWhere(['brand_id' => $id]);
    }

    public function withType($id)
    {
        return $this->andWhere(['type_id' => $id]);
    }

    public function withSlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }
}
