<?php

namespace common\modules\shop\models\query;

use common\modules\shop\models\ModuleConstant;
use yii\db\ActiveQuery;

class BrandQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['<>','status_id', ModuleConstant::DELETED]);
    }

    public function deleted()
    {
        return $this->andWhere(['status_id' => ModuleConstant::DELETED]);
    }

    public function published()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function notPublished()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function withSlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    public function notEqualId($id)
    {
        return $this->andWhere(['<>','id', $id]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }


    public function one($db = null)
    {
        return parent::one($db);
    }
}
