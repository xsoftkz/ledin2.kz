<?php

namespace common\modules\shop\models\query;

/**
 * This is the ActiveQuery class for [[\common\modules\shop\models\Order]].
 *
 * @see \common\modules\shop\models\Order
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    public function withId($id)
    {
        return $this->andWhere(['id' => $id]);

    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    public function withUser($id)
    {
        return $this->andWhere(['user_id' => $id]);

    }

    public function one($db = null)
    {
        return parent::one($db);
    }
}
