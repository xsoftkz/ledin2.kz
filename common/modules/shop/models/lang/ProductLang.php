<?php

namespace common\modules\shop\models\lang;

use Yii;

/**
 * This is the model class for table "xs_shop_product_lang".
 *
 * @property int $id
 * @property int $product_id
 * @property int $lang_id
 * @property string $title
 * @property string $seo_title
 * @property string|null $desc
 */
class ProductLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_product_lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'lang_id', 'title'], 'required'],
            [['product_id'], 'integer'],
            [['desc', 'meta_keywords', 'meta_description', 'intro'], 'string'],
            [['title', 'meta_title', 'lang_id', 'price_label', 'seo_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'seo_title' => 'Seo title',
            'meta_title' => 'Mета-тег Title',
            'meta_keywords' => 'Мета-тег Keywords',
            'meta_description' => 'Мета-тег Description',
            'desc' => 'Desc',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->meta_title)) {
                $this->meta_title = $this->title;
            }
            return true;
        }
        return false;
    }
}
