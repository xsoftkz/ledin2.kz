<?php

namespace common\modules\shop\models\lang;

use Yii;

/**
 * This is the model class for table "shop_product_info_lang".
 *
 * @property int $id
 * @property int $item_id
 * @property string $lang_id
 * @property string $title
 * @property string|null $desc
 */
class ProductInfoLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_product_info_lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'lang_id', 'title'], 'required'],
            [['item_id'], 'integer'],
            [['desc'], 'string'],
            [['lang_id'], 'string', 'max' => 8],
            [['title'], 'string', 'max' => 65],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'desc' => 'Desc',
        ];
    }
}
