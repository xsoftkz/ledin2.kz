<?php

namespace common\modules\shop\models\lang;

use Yii;

/**
 * This is the model class for table "xs_shop_category_lang".
 *
 * @property int $id
 * @property int $category_id
 * @property int $lang_id
 * @property string $title
 * @property string $seo_title
 * @property string|null $desc
 */
class CategoryLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%shop_category_lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'lang_id', 'title'], 'required'],
            [['category_id'], 'integer'],
            [['desc', 'meta_keywords', 'meta_description'], 'string'],
            [['title', 'meta_title', 'lang_id', 'singular_title', 'seo_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'meta_title' => 'Мета-тег Title',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'singular_title' => 'Singular Title',
            'meta_keywords' => 'Мета-тег Keywords',
            'meta_description' =>'Мета-тег Description',
            'seo_title' => 'Seo title',
            'desc' => 'Desc',
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (empty($this->meta_title)){
                $this->meta_title = $this->title;
            }
            return true;
        }
        return false;
    }
}
