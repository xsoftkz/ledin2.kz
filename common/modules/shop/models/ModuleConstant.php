<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 28.10.2016
 * Time: 23:57
 */

namespace common\modules\shop\models;

use yii\base\Model;

class ModuleConstant extends Model
{
    const IN_ACTIVE = 0;
    const ACTIVE = 1;
    const DELETED = 2;

    const ACTION_INDEX = 'index';
    const ACTION_CART = 'cart';

    // клиентские приложения с которых были сделаны заказы
    const APP_SITE_MAIN = 1;
    const APP_SITE_MOBILE = 2;
    const APP_ANDROID = 3;
    const APP_IOS = 4;

    // ставки для оформление заказа
    const DEFAULT_RATE_DELIVERY_SUM = 0;
    const DEFAULT_RATE_FREE_DELIVERY_SUM = 100000;
    const DEFAULT_RATE_MIN_ORDER_SUM = 0;

    // название переменных в настройках
    const RATE_DELIVERY_SUM = 'rate_delivery_sum';
    const RATE_FREE_DELIVERY_SUM = 'rate_free_delivery_sum';
    const RATE_MIN_ORDER_SUM = 'rate_min_order_sum';

    // статусы заказов
    const ORDER_NEW = 0;
    const ORDER_ACCEPTED = 1;
    const ORDER_CANCELLED = 2;
    const ORDER_DELIVERED = 3;

    // статусы заказов
    const ORDER_NEW_COLOR = '#dc3545';
    const ORDER_ACCEPTED_COLOR = '#ffc107';
    const ORDER_CANCELLED_COLOR = '#868e96';
    const ORDER_DELIVERED_COLOR = '#28a745';


    const DELIVERY_TYPE_COORIER = 1;
    const DELIVERY_TYPE_USER = 2;

    const PAYMENT_TYPE_ONLINE = 1;
    const PAYMENT_TYPE_OFFLINE = 0;


}