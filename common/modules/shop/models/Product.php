<?php

namespace common\modules\shop\models;

use common\models\File;
use common\models\Image;
use common\models\traits\ImageTrait;
use common\modules\content\models\Content;
use common\modules\eav\models\traits\EntityTrait;
use common\modules\shop\models\lang\ProductLang;
use common\modules\shop\models\query\ProductQuery;
use lav45\translate\TranslatedTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $photo
 * @property string $short_desc
 * @property string $full_desc
 * @property integer $category_id
 * @property integer $brand_id
 * @property string $price
 * @property integer $is_new
 * @property integer $is_hit
 * @property integer $sort
 * @property integer $views
 * @property integer $status
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $model_name
 * @property integer $type_id
 * @property integer $brand
 * @property integer $category
 */
class Product extends ActiveRecord
{
    use ImageTrait;
    use TranslatedTrait;

    const MODEL_NAME = 'product';
    const FILES_PIC_MODEL_NAME = 'product-pic';
    const IMAGE_FOLDER = 'product';
    const FILE_FOLDER = 'product';

    public $file;
    public $file_pic;
    public $related_portfolio;
    public $related_categories;

    public static function tableName()
    {
        return '{{%shop_product}}';
    }

    public function rules()
    {
        return [
            [['title', 'category_id'], 'required'],
            [['desc', 'meta_description', 'intro'], 'string'],
            [['quantity', 'category_id', 'brand_id', 'type_id', 'sort', 'views', 'available_id',
                'value_type_id', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['price'], 'number'],
            [['related_portfolio', 'related_categories'], 'safe'],
            [['file', 'file_pic'], 'file', 'maxFiles' => 10],
            [['uid', 'title', 'photo', 'slug', 'meta_keywords', 'article', 'model_name', 'price_label', 'icon', 'seo_title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'UID',
            'title' => 'Название товара',
            'article' => 'Артикул',
            'photo' => 'Изображения',
            'icon' => 'Иконка',
            'desc' => 'Описание',
            'intro' => 'Короткое описание',
            'category_id' => 'Категория',
            'manufacturer_id' => 'Производитель',
            'value_type_id' => 'Единица измерения',
            'type_id' => 'Тип товара',
            'model_name' => 'Модель',
            'price' => 'Цена',
            'price_label' => 'Цена',
            'file' => 'Файлы',
            'sort' => 'Порядок сортировки',
            'available_id' => 'Наличие товара',
            'quantity' => 'Количество товаров',
            'views' => 'Количество просмотров',
            'status_id' => 'Активен',
            'slug' => 'SEO ссылка',
            'seo_title' => 'SEO заголовок',
            'created_at' => 'Добавлен',
            'updated_at' => 'Обновлен',
            'created_by' => 'Добавил',
            'updated_by' => 'Обновил',
            'related_gallery' => 'Галерея',
        ];
    }

    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior'
            ],
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'photo' => [
                        'path' => '@images/' . self::IMAGE_FOLDER,
                        'tempPath' => '@images/' . self::IMAGE_FOLDER,
                    ],
                    'icon' => [
                        'path' => '@images/' . self::IMAGE_FOLDER,
                        'tempPath' => '@images/' . self::IMAGE_FOLDER,
                    ],
                ]
            ],
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort'
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => ['title', 'id'],
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            'translated' => [
                'class' => 'lav45\translate\TranslatedBehavior',
                'translateRelation' => 'productLangs',
                'languageAttribute' => 'lang_id',
                'translateAttributes' => [
                    'title',
                    'desc',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                    'intro',
                    'price_label',
                ]
            ]
        ];

    }

    public function afterDelete()
    {
        parent::afterDelete(); //
        ProductLang::deleteAll(['product_id' => $this->id]);
    }

    public function savePortfolios()
    {
        $models = Content::findAll($this->related_portfolio);
        $this->unlinkAll('portfolios', true);
        foreach ($models as $model) {
            $this->link('portfolios', $model);
        }
    }

    public function saveSubcategories()
    {
        $models = Category::findAll($this->related_categories);
        $this->unlinkAll('subcategories', true);
        foreach ($models as $model) {
            $this->link('subcategories', $model);
        }
    }

    public function getSubcategories()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('{{%shop_category_product}}', ['product_id' => 'id']);
    }

    public function getPortfolios()
    {
        return $this->hasMany(Content::className(), ['id' => 'portfolio_id'])
            ->viaTable('{{%shop_product_portfolio}}', ['product_id' => 'id']);
    }

    public function getProductLangs()
    {
        return $this->hasMany(ProductLang::className(), ['product_id' => 'id']);
    }

    public function getProductModels()
    {
        return $this->hasMany(ProductModel::className(), ['product_id' => 'id']);
    }

    public function getProductInfoItems()
    {
        return $this->hasMany(ProductInfo::className(), ['product_id' => 'id'])->where(['type_id' => ProductInfo::TYPE_INFO]);
    }

    public function getProductVideos()
    {
        return $this->hasMany(ProductInfo::className(), ['product_id' => 'id'])->where(['type_id' => ProductInfo::TYPE_VIDEO]);
    }

    public function getProductFiles()
    {
        return $this->hasMany(File::className(), ['item_id' => 'id'])->where(['model_name' => self::MODEL_NAME]);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    public static function getDropdownList()
    {
        $models = self::find()->notDeleted()->orderBy('sort DESC')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

    public function saveFacadeImages()
    {
        $this->file_pic = UploadedFile::getInstances($this, 'file_pic');

        if ($this->file_pic && $this->validate()) {
            foreach ($this->file_pic as $file) {
                $filename = uniqid() . '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@images/'.self::IMAGE_FOLDER . '/' . $filename));
                Image::create($filename, self::FILES_PIC_MODEL_NAME, $this->id, self::IMAGE_FOLDER);
            }
        }
    }


    public function getFacadeImages()
    {
        return Image::find()->where(['model_name' => $this::FILES_PIC_MODEL_NAME, 'item_id' => $this->id])->orderBy('sort_index')->all();
    }


    public function getAllProductImages(){
        $all = array_merge($this->allImages, $this->facadeImages);

        if ($this->icon) {
            $image = new Image(['filename' => $this->icon, 'item_id' => $this->id, 'model_name' => $this::MODEL_NAME, 'folder' => $this::IMAGE_FOLDER]);
            array_unshift($all, $image);
        }

        return $all;
    }
}
