<?php

namespace common\modules\shop\models;

use common\models\traits\ImageTrait;
use common\modules\shop\models\query\BrandQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%shop_brand}}".
 *
 * @property int $id
 * @property string $title
 * @property string $photo
 * @property string $description
 * @property int $sort
 * @property int $status_id
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Brand extends \yii\db\ActiveRecord
{
    use ImageTrait;

    const IMAGE_FOLDER = 'brand';

    public static function tableName()
    {
        return '{{%shop_brand}}';
    }


    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description', 'meta_description'], 'string'],
            [['sort', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'photo', 'slug', 'meta_title', 'meta_keywords'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'uid' => 'UID',
            'photo' => 'Изображения',
            'description' => 'Описание',
            'sort' => 'Порядок сортировки',
            'status_id' => 'Статус',
            'slug' => 'SEO ссылка',
            'meta_title' => 'Мета-тег Title',
            'meta_keywords' => 'Мета-тег Keywords',
            'meta_description' => 'Мета-тег Description',
            'created_at' => 'Добавлен',
            'updated_at' => 'Обновлен',
            'created_by' => 'Добавил',
            'updated_by' => 'Обновил',
        ];
    }


    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'photo' => [
                        'path' => '@images/'.self::IMAGE_FOLDER,
                        'tempPath' => '@images/'.self::IMAGE_FOLDER,
                    ],
                ]
            ],
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort'
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
            ],
        ];

    }

    public static function find()
    {
        return new BrandQuery(get_called_class());
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (empty($this->meta_title)){
                $this->meta_title = $this->title;
            }
            return true;
        }
        return false;
    }

    public static function getDropdownList()
    {
        $models = Brand::find()->notDeleted()->orderBy('id DESC')->all();
        return ArrayHelper::map($models, 'id', 'title');
    }

}
