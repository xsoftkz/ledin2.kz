<?php
namespace common\modules\shop\widgets\backend;

use common\modules\shop\models\Order;
use yii\base\Widget;

class OrdersSumInfoWidget extends Widget
{
    public function init()
    {
        parent::init();
        $sum = 0;
        $models = Order::find()->with('products')->all();
        foreach ($models as $model) {
            $sum += $model->totalSum;
        }
        echo $this->render('orders-sum-info', [
            'sum' => $sum,
        ]);
    }
}
