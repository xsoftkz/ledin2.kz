<?php
use yii\helpers\Html;

?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Последние заказы</h3>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 20px">№</th>
                    <th>Клиент</th>
                    <th>Статус</th>
                    <th>Дата</th>
                    <th>Сумма</th>
                    <th></th>
                </tr>

                <?php if ($models) {
                    foreach ($models as $model) { ?>
                        <tr>
                            <td><?= $model->id ?></td>
                            <td><?= $model->username ?></td>
                            <td><?= Yii::$app->params['orderStatusPanel'][$model->status_id] ?></td>
                            <td><?= $model->getOrderDate() ?></td>
                            <td><?= $model->getTotalSum() ?> тг.</td>
                            <td>
                                <?=
                                Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['/shop/order/view/', 'id' => $model->id],
                                    [
                                        'title' => Yii::t('app', 'Просмотр'),
                                        'data-toggle' => 'tooltip',
                                        'data-placement' => 'top',
                                        'class' => 'btn btn-xs btn-primary',
                                    ]);
                                ?>
                            </td>

                        </tr>
                    <?php }
                } ?>

                </tbody>
            </table>
        </div>
        <div class="text-center">
            <?= Html::a('все заказы <i class="fa fa-arrow-circle-right"></i>', ['/shop/order/index'], ['class' => 'small-box-footer']); ?>
        </div>
    </div>
    <!-- /.box-body -->
</div>







