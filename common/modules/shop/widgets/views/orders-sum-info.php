<?php
use yii\helpers\Url;
?>
<a href="<?= Url::toRoute(['/shop/order/index']) ?>">
    <div class="info-box">
        <span class="info-box-icon bg-green"><i class="fa fa-credit-card" aria-hidden="true"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Продажи</span>
            <span class="info-box-number"><?= $sum ?> тг.</span>
        </div>
    </div>
</a>



