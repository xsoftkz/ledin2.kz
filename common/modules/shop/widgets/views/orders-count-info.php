<?php
use yii\helpers\Url;
?>
<a href="<?= Url::toRoute(['/shop/order/index']) ?>">
    <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="fa fa-fw fa-shopping-cart"></i></span>
        <div class="info-box-content">
            <span class="info-box-text">Заказы</span>
            <span class="info-box-number"><?= $ordersCount ?></span>
            <?php if($new){ ?>
                <span>Новый: <span class="badge"><?= $new?></span> </span>
            <?php }?>
        </div>
    </div>
</a>

