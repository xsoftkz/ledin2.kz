<?php
namespace common\modules\shop\widgets;

use common\modules\shop\models\Order;
use yii\base\Widget;

class LatestOrdersWidget extends Widget
{
    public function init()
    {
        parent::init();

        $models = Order::find()->orderBy('created_at DESC')->limit(5)->all();

        echo $this->render('latest-orders', [
            'models' => $models,
        ]);
    }
}
