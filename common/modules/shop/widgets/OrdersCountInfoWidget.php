<?php
namespace common\modules\shop\widgets\backend;

use common\modules\shop\models\ModuleConstant;
use common\modules\shop\models\Order;
use yii\base\Widget;

class OrdersCountInfoWidget extends Widget
{
    public function init()
    {
        parent::init();

        $ordersCount = Order::find()->count();
        $new = Order::find()->where(['status_id' => ModuleConstant::ORDER_NEW])->count();

        echo $this->render('orders-count-info', [
            'ordersCount' => $ordersCount,
            'new' => $new,
        ]);

    }
}
