<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if($model->facadeImages) { ?>
    <?php foreach($model->facadeImages as $img) { ?>
        <div class="col-xs-6 col-lg-2">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools pull-right">
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ',
                            ['/shop/product/delete-image', 'id' => $img->id], ['data-method' => 'POST']) ?>
                    </div>
                </div>
                <div class="box-body" style="display: block;">
                    <img src="<?= $img->image?>" alt="" class="img-responsive" style="height: 100px">
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
