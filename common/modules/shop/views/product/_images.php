<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if($model->images) { ?>
    <?php foreach($model->images as $image) { ?>
        <div class="col-xs-6 col-lg-2">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools pull-right">
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ',
                            ['/shop/product/delete-image', 'id' => $image->id], ['data-method' => 'POST']) ?>
                    </div>
                </div>
                <div class="box-body" style="display: block;">
                    <img src="<?= $image->image?>" alt="" class="img-responsive" style="max-height: 100px">
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
