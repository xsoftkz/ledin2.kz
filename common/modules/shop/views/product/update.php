<?php

use yii\helpers\Html;

\common\modules\shop\assets\ShopModuleAdminAsset::register($this);

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\models\Product */

$this->title = 'Товары. Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Товары', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';

?>

<div class="product-update">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-4">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="btn-box btn-group">
                        <?= \backend\widgets\LangSwitchWidget::widget(['current' => $model->language, 'model_id' => $model->id]) ?>
                    </div>
                </div>
                <div class="col-lg-5 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> Отменить',
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> Добавить новую',
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> Удалить',
                            ['delete', 'id' => $model->id], ['class' => 'btn btn-sm btn-default', 'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'subcategories' => $subcategories,
        'portfolios' => $portfolios,
        'model' => $model
    ]) ?>

</div>
