<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\fileapi\Widget as FileAPI;
use kartik\select2\Select2;
use common\modules\shop\models\Category;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\modules\shop\models\Product */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="view-form">

    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#main" aria-controls="main" role="tab" data-toggle="tab">Данные</a>
                    </li>
                    <?php if (!$model->isNewRecord) { ?>
                        <li role="presentation">
                            <a href="#models" aria-controls="models" role="tab" data-toggle="tab">Модели</a>
                        </li>
                        <li role="presentation">
                            <a href="#categories" aria-controls="info" role="tab" data-toggle="tab">Подкатегории</a>
                        </li>
                        <li role="presentation">
                            <a href="#info" aria-controls="info" role="tab" data-toggle="tab">Инфо</a>
                        </li>
                        <li role="presentation">
                            <a href="#file" aria-controls="file" role="tab" data-toggle="tab">Файлы</a>
                        </li>
                        <li role="presentation">
                            <a href="#video" aria-controls="video" role="tab" data-toggle="tab">Видео</a>
                        </li>
                        <li role="presentation">
                            <a href="#portfolio" aria-controls="portfolio" role="tab" data-toggle="tab">Портфолио</a>
                        </li>
                    <?php } ?>

                    <li role="presentation">
                        <a href="#images" aria-controls="images" role="tab" data-toggle="tab">Изображение</a>
                    </li>

                    <li role="presentation">
                        <a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a>
                    </li>
                    <li class="pull-right">
                        <?= Html::submitButton('<span class="glyphicon glyphicon-floppy-disk"></span> Сохранить',
                            ['class' => 'btn btn-sm btn-primary pull-right',]) ?>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="main">
                        <table class="table table-form">
                            <tr>
                                <td><label for="product-title">Название товара</label></td>
                                <td><?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-title">Код товара</label></td>
                                <td><?= $form->field($model, 'model_name')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-category_id">Категория</label></td>
                                <td>
                                    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                                        'data' => Category::getDropdownList(),
                                        'options' => [
                                            'placeholder' => 'Без категория'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label(false); ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="product-available_id">В наличии</label></td>
                                <td><?= $form->field($model, 'available_id')->dropDownList(Yii::$app->params['booleanParam'])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-status_id">Активен</label></td>
                                <td><?= $form->field($model, 'status_id')->dropDownList(Yii::$app->params['booleanParam'])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-title">Цена</label></td>
                                <td><?= $form->field($model, 'price_label')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-full_desc">Краткое описание</label></td>
                                <td>
                                    <?= $form->field($model, 'intro')->textarea(['rows' => 3])->label(false); ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="product-full_desc">Описание</label></td>
                                <td>
                                    <?= $form->field($model, 'desc')->widget(Widget::className(), [
                                        'settings' => [
                                            'lang' => 'ru',
                                            'minHeight' => 300,
                                            'imageUpload' => Url::to(['/shop/product/image-upload']),
                                            'plugins' => [
                                                'fontsize',
                                                'fontcolor',
                                                'table',
                                                'video',
                                                'fullscreen'
                                            ],
                                        ]
                                    ])->label(false); ?>
                                </td>
                            </tr>
                        </table>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="models">
                        <?=
                        Html::a('Добавить', ['/#'], [
                            'class' => 'js-model-popup btn btn-default',
                            'data-target' => '#productModelModal',
                            'data-url' => Url::toRoute(['/shop/model/create', 'id' => $model->id]),
                        ]);
                        ?>
                        <hr>
                        <div id="js-product-models-wrap">
                            <?= $this->render('/model/_product_models_list', [
                                'models' => $model->productModels
                            ]) ?>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="categories">
                        <?= $form->field($model, 'related_categories')->checkboxList(ArrayHelper::map($subcategories, 'id', 'title'))->label(false) ?>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="info">
                        <?= Html::a('Добавить', '#',
                            ['class' => 'btn btn-default js-popup', 'data-target' => '#productInfoModal',
                                'data-url' => Url::toRoute(['/shop/info/create', 'product_id' => $model->id, 'lang_id' => $model->language] )]
                        ) ?>
                        <hr>
                        <div id="product_info_list_wrap" data-product-id="<?= $model->id ?>">
                            <?= $this->render('/info/_product_info_list', [
                                'models' => $model->productInfoItems
                            ]) ?>
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="file">
                        <div class="row">
                            <div class="col-lg-6">
                                <?= Html::a('Добавить', '#',
                                    ['class' => 'btn btn-default js-popup', 'data-target' => '#productFileModal',
                                        'data-url' => Url::toRoute(['/shop/file/create', 'product_id' => $model->id, 'lang_id' => $model->language] )]
                                ) ?>
                                <hr>
                                <div id="product-file-list-wrap">
                                    <?= $this->render('/file/_product_file_list', [
                                        'models' => $model->productFiles
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="video">
                        <div class="row">
                            <div class="col-lg-6">
                                <?= Html::a('Добавить', '#',
                                    ['class' => 'btn btn-default js-popup', 'data-target' => '#productVideoModal',
                                        'data-url' => Url::toRoute(['/shop/video/create', 'product_id' => $model->id, 'lang_id' => $model->language] )]
                                ) ?>
                                <hr>
                                <div id="product-video-list-wrap">
                                    <?= $this->render('/video/_product_video_list', [
                                        'models' => $model->productVideos
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="portfolio">
                        <td><?= $form->field($model, 'related_portfolio')->checkboxList(ArrayHelper::map($portfolios, 'id', 'title'))->label(false) ?></td>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="images">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Изображение
                            </div>
                            <div class="panel-body">
                                <div style="max-width: 300px">
                                    <?= $form->field($model, 'photo')->widget(
                                        FileAPI::className(),
                                        [
                                            'settings' => [
                                                'url' => ['fileapi-upload'],
                                                'elements' => [
                                                    'preview' => ['width' => 100, 'height' => 100]
                                                ],
                                                'imageAutoOrientation' => false
                                            ],
                                        ])->label(false)
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Иконка
                            </div>
                            <div class="panel-body">
                                <div style="max-width: 300px">
                                    <?= $form->field($model, 'icon')->widget(
                                        FileAPI::className(),
                                        [
                                            'settings' => [
                                                'url' => ['fileapi-upload'],
                                                'elements' => [
                                                    'preview' => ['width' => 100, 'height' => 100]
                                                ],
                                                'imageAutoOrientation' => false
                                            ],
                                        ])->label(false)
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Дополнительные изображения
                            </div>
                            <div class="panel-body">
                                <br>
                                <?= $form->field($model, 'file[]')->fileInput(['multiple' => true])->label(false) ?>
                                <br>
                                <div id="js-content-images" class="row">
                                    <?= $this->render('_images', [
                                        'model' => $model
                                    ]) ?>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Изображения фасадов
                            </div>
                            <div class="panel-body">
                                <br>
                                <?= $form->field($model, 'file_pic[]')->fileInput(['multiple' => true])->label(false) ?>
                                <br>
                                <div id="js-content-images_pic" class="row">
                                    <?= $this->render('_images_pic', [
                                        'model' => $model
                                    ]) ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div role="tabpanel" class="tab-pane fade" id="seo">
                        <table class="table table-form">
                            <tr>
                                <td><label for="product-slug">SEO URL</label></td>
                                <td><?= $form->field($model, 'slug')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-slug">SEO заголовок</label></td>
                                <td><?= $form->field($model, 'seo_title')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-slug">H1</label></td>
                                <td><?= $form->field($model, 'meta_title')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-meta_keywords">Мета-тег Keywords</label></td>
                                <td><?= $form->field($model, 'meta_keywords')->textarea(['rows' => 2])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="product-meta_description">Мета-тег Description</label></td>
                                <td><?= $form->field($model, 'meta_description')->textarea(['rows' => 4])->label(false) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>




<?php Modal::begin([
    'id' => 'productFileModal',
    'header' => 'Файл',
]); ?>
<div class='modalContent'></div>
<?php Modal::end(); ?>



<?php Modal::begin([
    'id' => 'productInfoModal',
    'header' => 'Информация',
]); ?>
<div class='modalContent'></div>
<?php Modal::end(); ?>



<?php Modal::begin([
    'id' => 'productVideoModal',
    'header' => 'Видео',
]); ?>
<div class='modalContent'></div>
<?php Modal::end(); ?>



<?php Modal::begin([
    'id' => 'productModelModal',
    'header' => 'Модель',
    'size' => Modal::SIZE_LARGE,
]); ?>
<div class='modalContent'></div>
<?php Modal::end(); ?>


