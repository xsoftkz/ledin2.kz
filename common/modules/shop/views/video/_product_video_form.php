<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'product_video_form']); ?>
<div class="info-items">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Ссылка на видео (youtube)') ?>

    <?= $form->field($model, 'product_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>

    <?= Html::button('Отменить', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>

