<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Ссылка на видео</th>
        <th></th>
    </tr>
    <?php if ($models) {
        foreach ($models as $item) { ?>
            <tr>
                <td style="width: 200px;">
                    <?= $item->title ?>
                </td>
                <td style="width: 80px">
                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#',
                        ['class' => 'btn btn-xs btn-primary js-popup', 'data-target' => '#productVideoModal',
                            'data-url' => Url::toRoute(['/shop/video/update', 'id' => $item->id, 'lang_id' => $item->language] )]) ?>

                    <a href="<?= Url::toRoute(['/shop/video/delete', 'id' => $item->id]) ?>" class="js-video-item-delete btn btn-xs btn-danger">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php }
    } ?>
</table>

