<?php
use yii\helpers\Html;
use yii\helpers\Url;
use common\modules\eav\widgets\EntityParamsWidget;
?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php if ($models) { $i=0;
        foreach ($models as $modelItem) { $i++; ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading<?= $modelItem->id ?>">
                    <h4 class="panel-title">
                        #<?= $i ?> &nbsp;
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $modelItem->id ?>"
                           aria-expanded="<?= ($i == 0) ? true : false ?>" aria-controls="collapse<?= $modelItem->id ?>" class="btn btn-default">
                            <?= $modelItem->name ?>
                        </a>
                        <div class="pull-right">
                            <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['#'], [
                                'class' => 'js-model-popup btn btn-xs btn-primary',
                                'data-target' => '#productModelModal',
                                'data-url' => Url::toRoute(['/shop/model/update', 'id' => $modelItem->id]),
                            ]); ?>

                            <?= Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                Url::toRoute(['/shop/model/delete', 'id' => $modelItem->id]),
                                [
                                    'class' => 'js-delete-product-model btn btn-xs btn-danger',
                                ]
                            ); ?>
                        </div>
                    </h4>
                </div>
                <div id="collapse<?= $modelItem->id ?>" class="panel-collapse collapse <?= ($i == 0) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="heading<?= $modelItem->id ?>">
                    <div class="panel-body">
                        <?= EntityParamsWidget::widget(['models' => $modelItem->getParamsMap()]) ?>
                    </div>
                </div>
            </div>
        <?php }
    } ?>
</div>


