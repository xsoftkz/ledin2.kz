<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\modules\eav\models\Type;

?>
<?php $form = ActiveForm::begin([
    'id' => 'form-model-create',
    'options' => ['enctype' => 'multipart/form-data']
]); ?>

<?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>

<?= $form->field($model, 'type_id')->dropDownList(Type::getDropdownList(),
    ['prompt' => 'не задано', 'class' => 'entity-type_id form-control']); ?>
<?= $form->field($model, 'product_id')->hiddenInput()->label(false) ?>

<div id="js-entity-params" class="wrap-product-params" data-product_id="<?= $model->id ?>">

</div>
<hr>
<?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>

<?php ActiveForm::end(); ?>

