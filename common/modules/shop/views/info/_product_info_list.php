<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<table class="table table-striped table-bordered">
    <tr>
        <th>Изображение</th><th>Название</th><th>Описание</th><th></th>
    </tr>
    <?php if ($models) {
        foreach ($models as $item) { ?>
            <tr>
                <td style="width: 100px;">
                    <a href="<?= $item->image?>" target="_blank">
                        <img src="<?= $item->image ?>" alt="" width="50">
                    </a>
                </td>
                <td style="width: 200px;">
                    <?= $item->title ?>
                </td>
                <td><?= $item->desc ?></td>
                <td style="width: 80px">
                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#',
                        ['class' => 'btn btn-xs btn-primary js-popup', 'data-target' => '#productInfoModal',
                            'data-url' => Url::toRoute(['/shop/info/update', 'id' => $item->id, 'lang_id' => $item->language] )]) ?>

                    <a href="<?= Url::toRoute(['/shop/info/delete', 'id' => $item->id]) ?>" class="js-info-item-delete btn btn-xs btn-danger">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php }
    } ?>
</table>

