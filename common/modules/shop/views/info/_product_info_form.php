<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'product_info_form']); ?>
<div class="info-items">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label('Название') ?>

    <?= $form->field($model, 'photo')->widget(
        FileAPI::className(),
        [
            'settings' => [
                'url' => ['fileapi-upload'],
                'elements' => [
                    'preview' => ['width' => 100, 'height' => 100]
                ],
                'imageAutoOrientation' => false
            ],
        ])->label(false)
    ?>

    <?= $form->field($model, 'desc')->textarea(['rows' => 2])->label('Описание') ?>

    <?= $form->field($model, 'product_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'type_id')->hiddenInput()->label(false) ?>

    <?= Html::button('Отменить', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>

