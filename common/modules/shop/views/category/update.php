<?php

use yii\helpers\Html;

\common\modules\shop\assets\ShopModuleAdminAsset::register($this);

/* @var $this yii\web\View */
/* @var $model common\modules\shop\models\Product */

$this->title = Yii::t('app', 'Категория') .'. '.Yii::t('app', 'Редактирование').': '.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Категории'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="product-update">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="btn-box btn-group">
                        <?= \backend\widgets\LangSwitchWidget::widget(['current' => $model->language, 'model_id' => $model->id]) ?>
                    </div>
                </div>
                <div class="col-lg-3 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('app', 'Отменить'),
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить новую'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Удалить'),
                            ['delete', 'id' => $model->id], ['class' => 'btn btn-sm btn-default', 'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?')]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'categories' => $categories,
        'types' => $types,
    ]) ?>

</div>
