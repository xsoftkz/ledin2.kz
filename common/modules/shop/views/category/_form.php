<?php

use common\modules\shop\models\Category;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use vova07\imperavi\Widget;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\modules\eav\models\Type;

/* @var $this yii\web\View */
/* @var $model \common\modules\shop\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="view-form">
    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab"> Данные </a></li>
                    <li role="presentation"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab"> SEO</a></li>
                    <li class="pull-right">
                        <?= Html::submitButton(
                            ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> Добавить' :
                                '<span class="glyphicon glyphicon-floppy-disk"></span> Сохранить',
                            ['class' =>
                                ($model->isNewRecord) ? 'btn btn-sm btn-success pull-right' : 'btn btn-sm btn-primary pull-right',
                            ]) ?>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="main">
                        <table class="table table-form">
                            <tr>
                                <td><label for="category-title">Название категории</label></td>
                                <td><?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-title">Название категории в единственном числе</label></td>
                                <td><?= $form->field($model, 'singular_title')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-parent_id">Родительская категория</label></td>
                                <td>
                                    <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map($categories, 'id', 'titleSelect'),
                                        'options' => [
                                            'placeholder' => 'Без категория'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label(false); ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="category-product_type_id">Тип товара (для фильтра)</label></td>
                                <td>
                                    <?= $form->field($model, 'product_type_id')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map($types, 'id', 'title'),
                                    ])->label(false); ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="category-product_type_id">Группа подкатегорий</label></td>
                                <td>
                                    <?= $form->field($model, 'group_id')->widget(Select2::classname(), [
                                        'data' => Category::getGroups(),
                                    ])->label(false); ?>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="category-link">Ссылка на каталог</label></td>
                                <td><?= $form->field($model, 'link')->textInput()->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-photo">Изображение</label> </td>
                                <td>
                                    <div style="width: 200px">
                                        <?= $form->field($model, 'photo')->widget(
                                            FileAPI::className(),
                                            [
                                                'settings' => [
                                                    'url' => ['fileapi-upload'],
                                                    'elements' => [
                                                        'preview' => ['width' => 100,'height' => 100]
                                                    ],
                                                    'imageAutoOrientation' => false
                                                ],
                                            ])->label(false)
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="category-icon">Иконка</label> </td>
                                <td>
                                    <div style="width: 200px">
                                        <?= $form->field($model, 'icon')->widget(
                                            FileAPI::className(),
                                            [
                                                'settings' => [
                                                    'url' => ['fileapi-upload'],
                                                    'elements' => [
                                                        'preview' => ['width' => 50,'height' => 50]
                                                    ],
                                                    'imageAutoOrientation' => false
                                                ],
                                            ])->label(false)
                                        ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="category-status_id">Активен</label> </td>
                                <td>
                                    <div style="width: 200px">
                                        <?= $form->field($model, 'status_id')->dropDownList(Yii::$app->params['booleanParam'])->label(false) ?>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="category-desc">Описание</label> </td>
                                <td>
                                    <?= $form->field($model, 'desc')->widget(Widget::className(), [
                                        'settings' => [
                                            'lang' => 'ru',
                                            'minHeight' => 300,
                                            'imageUpload' => Url::to(['/shop/category/image-upload']),
                                            'plugins' => [
                                                'fontfamily',
                                                'fontsize',
                                                'fontcolor',
                                                'table',
                                                'video',
                                                'imagemanager',
                                                'filemanager',
                                                'fullscreen'
                                            ],
                                        ]
                                    ])->label(false); ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="seo">
                        <table class="table table-form">
                            <tr>
                                <td><label for="category-slug">SEO URL</label></td>
                                <td><?= $form->field($model, 'slug')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-slug">H1</label></td>
                                <td><?= $form->field($model, 'meta_title')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-slug">Seo заголовок</label></td>
                                <td><?= $form->field($model, 'seo_title')->textInput(['maxlength' => true])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-meta_keywords">Мета-тег Keywords</label></td>
                                <td><?= $form->field($model, 'meta_keywords')->textarea(['rows' => 4])->label(false) ?></td>
                            </tr>
                            <tr>
                                <td><label for="category-meta_description">Мета-тег Description</label></td>
                                <td><?= $form->field($model, 'meta_description')->textarea(['rows' => 4])->label(false) ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>