<?php

use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Корзина';
$this->params['breadcrumbs'][] = ['label' => 'Категории', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-chevron-left"></span> Список',
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'contentOptions' => ['style' => 'width:40px;'],
                    ],
                    'title',
                    [
                        'attribute' => 'updated_at',
                        'contentOptions' => ['style' => 'width:150px;'],
                        'value' => function ($data) {
                            return ($data->updated_at) ? \Yii::$app->formatter->asDatetime($data->updated_at, "php:d-m-Y H:i:s") : '';
                        },
                    ],

                    [
                        'header' => 'Действия',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:100px;'],
                        'template' => '{return} {remove}',
                        'buttons' => [
                            'return' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                    'title' => 'Восстановить',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-confirm' => 'Восстановить из корзины?',

                                ]);
                            },
                            'remove' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => 'Удалить',
                                    'data-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-confirm' => 'После удаление восстановить невозможно, все равно удалить?',

                                ]);
                            },
                        ],
                    ],

                ],
            ]); ?>

        </div>
    </div>
</div>


