<?php

use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;
use common\modules\shop\models\Category;
use common\modules\shop\models\ModuleConstant;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\shop\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Категории');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Корзина'),
                            ['cart'], ['class' => 'btn btn-sm btn-default ']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <?php \yii\widgets\Pjax::begin()?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'contentOptions' => ['style' => 'width:40px;'],
                    ],
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:20px;'],
                        'value' => function ($data) {
                            return $data->id;
                        },
                    ],
                    [
                        'attribute' => 'photo',
                        'contentOptions' => ['style' => 'width:40px;'],
                        'format' => 'html',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function ($data) {
                            return Html::img($data->iconImage, ['width' => 40]);
                        },
                    ],
                    [
                        'attribute' => 'title',
                        'contentOptions' => ['style' => 'width:300px;'],
                        'value' => function ($data) {
                            return $data->title;
                        },
                    ],
                    [
                        'attribute' => 'parent_id',
                        'value' => function ($data) {
                            return ($data->parent_id) ? $data->titleSelect : '(не задано)';
                        },
                        'filter' => Category::getDropdownList()
                    ],
                    [
                        'attribute' => 'status_id',
                        'contentOptions' => ['style' => 'width:80px; text-align: center;'],
                        'format' => 'html',
                        'value' => function ($data) {
                            return Html::a(
                                Html::tag('span', '',
                                    ['class' => 'glyphicon glyphicon-' . (($data->status_id == ModuleConstant::ACTIVE) ? 'ok' : 'remove')]),
                                ['status', 'id' => $data->id], ['class' => 'status-link']);
                        },
                        'filter' => Yii::$app->params['booleanParam']
                    ],
                    [
                        'header' => 'Языки',
                        'class' => 'lav45\translate\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:250px; '],
                        'languages' => $langList,
                    ],
                    [
                        'header' => 'Действия',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:40px;'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => 'Удалить',
                                    'data-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>

            <?php \yii\widgets\Pjax::end()?>


        </div>
    </div>


</div>
