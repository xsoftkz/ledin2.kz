<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<table class="table table-striped table-bordered">
    <tr>
        <th>Название</th>
        <th></th>
        <th></th>
    </tr>
    <?php if ($models) {
        foreach ($models as $item) { ?>
            <tr>
                <td>
                    <?= $item->title_ru ?>
                </td>
                <td style="width: 120px">
                    <a href="<?= $item->fileLink ?>" class="btn btn-sm btn-default" target="_blank">Посмотреть</a>
                </td>
                <td style="width: 100px">
                    <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#',
                        ['class' => 'btn btn-xs btn-primary js-popup', 'data-target' => '#productFileModal',
                            'data-url' => Url::toRoute(['/shop/file/update', 'id' => $item->id] )]) ?>

                    <a href="<?= Url::toRoute(['/shop/file/delete', 'id' => $item->id]) ?>" class="js-file-item-delete btn btn-xs btn-danger">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
        <?php }
    } ?>
</table>
