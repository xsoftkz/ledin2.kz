<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;


/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(['id' => 'product_file_form']); ?>
<div class="file-items">
    <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true])?>
    <?= $form->field($model, 'title_kz')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'title_en')->textInput(['maxlength' => true]) ?>

    <td><?= $form->field($model, 'file')->fileInput()->label(false) ?></td>

    <?= $form->field($model, 'item_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'model_name')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'folder')->hiddenInput()->label(false) ?>

    <?= Html::button('Отменить', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) ?>

    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end(); ?>

