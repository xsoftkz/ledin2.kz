<?php
namespace common\modules\shop;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\shop\controllers';
    public function init()
    {
        parent::init();

        \Yii::$app->params['valueTypes'] = [
            '1' => 'шт',
            '2' => 'кг',
        ];

        \Yii::$app->params['productStatus'] = [
            '1' => 'Активиен',
            '0' => 'Не активиен',
        ];
        \Yii::$app->params['productAvailable'] = [
            '1' => 'В наличии',
            '0' => 'Нет в наличии',
            '2' => 'На заказ',
        ];
        \Yii::$app->params['deliveryType'] = [
            '1' => 'Курьерская доставка',
            '2' => 'Самовывоз',
        ];
        \Yii::$app->params['paymentType'] = [
            '0' => 'Наличными при доставке',
            '1' => 'Карточкой (Visa, Mastercard)',
        ];
        \Yii::$app->params['orderStatus'] = [
            '0' => 'Заказ обрабатывается',
            '1' => 'Заказ подтвержден',
            '2' => 'Заказ отменен',
            '3' => 'Заказ доставлен',
        ];
        \Yii::$app->params['orderStatusPanel'] = [
            '0' => 'Новый заказ',
            '1' => 'Заказ подтвержден',
            '2' => 'Заказ отменен',
            '3' => 'Заказ доставлен',
        ];
        \Yii::$app->params['booleanParam'] = [
            '1' => 'да',
            '0' => 'нет',
        ];

        \Yii::$app->params['paramTypes'] = [
            '1' => 'Выпадающий список',
            '2' => 'Список чекбоксов (множественный)',
            '3' => 'Чекбокс',
            '4' => 'Короткий текст (до 250 символов)',
            '5' => 'Текст',
        ];

        \Yii::$app->params['productSetTypes'] = [
            '1' => 'Категория товаров',
            '2' => 'Группа товаров',
            '3' => 'Список товаров',
        ];

    }
}