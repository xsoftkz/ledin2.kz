<?php

namespace common\modules\shop\services;

use common\models\helper\WatermarkHelper;
use common\modules\shop\models\Product;
use Yii;

class ProductWaterMarkService
{
    /** @var Product $_product */
    private $_product;

    public function __construct(Product $product)
    {
        $this->_product = $product;
    }

    public function run(){

        $product = $this->_product;

        $images = $product->getAllProductImages();

        foreach ($images as $imageItem) {
            $filename = $imageItem->filename;

            $imagePath = Yii::getAlias('@images/product/') . $filename;
            $newImagePath = Yii::getAlias('@wm/product/') . $filename;

            WatermarkHelper::watermark($imagePath, $newImagePath);
        }
    }
}