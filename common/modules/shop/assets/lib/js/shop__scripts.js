$(document).ready(function() {
    $('body').on('click', '.js-popup', function () {
        $($(this).data('target')).modal('show')
            .find('.modalContent')
            .load($(this).attr('data-url'));
        return false;
    });


    //модели товаров
    function getEntityParamsForm() {
        var type_id = $('body').find(".entity-type_id").val();
        var form_box = $('body').find("#js-entity-params");
        var id = form_box.attr("data-product_id") ;

        $.ajax({
            type: "GET",
            url: '/eav/param/entity-params-form',
            data: {
                type_id: type_id,
                id: id,
            }
        }).done(function (data) {
            form_box.html(data);
        }).fail(function () {
        });
    }
    $('body').on('change', '.entity-type_id', function () {
        getEntityParamsForm();
    });
    $('body').on('click', '.js-model-popup', function () {
        $($(this).data('target')).modal('show')
            .find('.modalContent')
            .load($(this).attr('data-url'), {} , function () {
                getEntityParamsForm();
            });
        return false;
    });
    $('body').on('click', '.js-delete-product-model', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            success: function(res){
                $('#js-product-models-wrap').html(res)
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });
    $('body').on('beforeSubmit', 'form#form-model-create', function () {
        var data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            success: function(res){
                $('#js-product-models-wrap').html(res)
                $('#productModelModal').modal('hide')
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });


    //доп. информация о товарах
    $('body').on('click', '.js-info-item-delete', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            success: function(res){
                $('#product_info_list_wrap').html(res)
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });
    $('body').on('beforeSubmit', 'form#product_info_form', function () {
        var data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            success: function(res){
                $('#product_info_list_wrap').html(res)
                $('#productInfoModal').modal('hide')
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });


    //файлы товаров
    $('body').on('beforeSubmit', 'form#product_file_form', function () {
        var data = new FormData(this);
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            success: function(res){
                $('#product-file-list-wrap').html(res)
                $('#productFileModal').modal('hide')
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });
    $('body').on('click', '.js-file-item-delete', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            success: function(res){
                $('#product-file-list-wrap').html(res)
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });


    //доп. информация о товарах
    $('body').on('click', '.js-video-item-delete', function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            type: 'GET',
            success: function(res){
                $('#product-video-list-wrap').html(res)
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });
    $('body').on('beforeSubmit', 'form#product_video_form', function () {
        var data = $(this).serialize();
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: data,
            success: function(res){
                $('#product-video-list-wrap').html(res)
                $('#productVideoModal').modal('hide')
                mdtoast('Данные успешно сохранены.', { duration: 3000, type: mdtoast.SUCCESS });
            },
            error: function(){
                alert('Error!');
            }
        });
        return false;
    });

});
