<?php
namespace common\modules\shop\assets;

use yii\web\AssetBundle;

class ShopModuleAdminAsset extends AssetBundle
{

    public $sourcePath = '@common/modules/shop/assets/lib';
    public $basePath = '@backendWebroot/web/assets';

    public $css = [
        'css/shop__styles.css',
    ];

    public $js = [
        'js/shop__scripts.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
