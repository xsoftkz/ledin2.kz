<?php
namespace common\modules\content;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\content\controllers';
    public function init()
    {
        parent::init();
        \Yii::$app->params['booleanParam'] = [
            '1' => 'да',
            '0' => 'нет',
        ];
        \Yii::$app->params['booleanParamReverse'] = [
            '0' => 'нет',
            '1' => 'да',
        ];
    }
}