<?php
namespace common\modules\content\models\query;

use common\modules\content\models\ModuleConstant;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\models\Category]].
 *
 * @see \common\models\Category
 */
class CategoryQuery extends ActiveQuery
{
    public function withSection($section)
    {
        return $this->andWhere(['section' => $section]);
    }

    public function withSlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    public function withCategory($id)
    {
        return $this->andWhere(['parent_id' => $id]);
    }

    public function isParent()
    {
        return $this->andWhere(['parent_id' => null]);
    }

    public function isNotParent()
    {
        return $this->andWhere(['<>','parent_id', null]);
    }

    public function notDeleted()
    {
        return $this->andWhere(['<>','status_id', ModuleConstant::DELETED]);
    }

    public function deleted()
    {
        return $this->andWhere(['status_id' => ModuleConstant::DELETED]);
    }

    public function published()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function notPublished()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\Category|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
