<?php
namespace common\modules\content\models\query;

use common\modules\content\models\ModuleConstant;
use yii\db\ActiveQuery;
use Yii;

class ContentQuery extends ActiveQuery
{
    public function withSection($section)
    {
        return $this->andWhere(['section' => $section]);
    }

    public function withSlug($slug)
    {
        return $this->andWhere(['slug' => $slug]);
    }

    public function withCategory($id)
    {
        return $this->andWhere(['category_id' => $id]);
    }

    public function notDeleted()
    {
        return $this->andWhere(['<>','status_id', ModuleConstant::DELETED]);
    }


    public function withDateInterval(){
        $today = Yii::$app->formatter->asTimestamp(date("Y-m-d"));
        return $this->andWhere(['<=','start_at', $today])->andWhere(['>=','stop_at', $today]);
    }

    public function deleted()
    {
        return $this->andWhere(['status_id' => ModuleConstant::DELETED]);
    }

    public function published()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function notPublished()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }


    public function one($db = null)
    {
        return parent::one($db);
    }
}
