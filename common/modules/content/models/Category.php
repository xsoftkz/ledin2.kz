<?php
namespace common\modules\content\models;

use common\models\traits\ImageTrait;
use common\modules\content\models\lang\CategoryLang;
use common\modules\content\models\query\CategoryQuery;
use lav45\translate\TranslatedTrait;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%category}}".
 *
 * @property integer $id
 * @property integer $section_id
 * @property integer $parent_id
 * @property string $title
 * @property string $description
 * @property integer $status_id
 * @property integer $sort_index
 * @property string $slug
 * @property string $meta_title
 * @property string $seo_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Category extends ActiveRecord
{
    use ImageTrait;
    use TranslatedTrait;

    const MODEL_NAME = 'content-category';
    const IMAGE_FOLDER = 'content';

    public static function tableName()
    {
        return '{{%category}}';
    }


    public function rules()
    {
        return [
            [['section', 'title'], 'required'],
            [['slug'], 'unique', 'message' => 'Значение должно быть уникальным'],
            [['parent_id', 'status_id', 'sort_index', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['description', 'meta_description', 'section', 'desc'], 'string'],
            [['title', 'slug', 'meta_title', 'meta_keywords', 'photo','seo_title'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section' => 'Раздел',
            'photo' => 'Фото',
            'parent_id' => 'Родительская категория',
            'title' => 'Название',
            'description' => 'Описание',
            'slug' => 'СЕО ссылка',
            'meta_title' => 'Мета заголовок',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
            'status_id' => 'Статус',
            'sort_index' => 'Порядок сортировки',
            'created_at' => 'Добавлено',
            'updated_at' => 'Обновлено',
            'created_by' => 'Добавил',
            'updated_by' => 'Изменил',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort_index'
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
            ],
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'photo' => [
                        'path' => '@images/'.self::IMAGE_FOLDER,
                        'tempPath' => '@images/'.self::IMAGE_FOLDER,
                    ],
                ]
            ],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'title',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            [
                'class' => 'lav45\translate\TranslatedBehavior',
                'translateRelation' => 'categoryLangs',
                'languageAttribute' => 'lang_id',
                'translateAttributes' => [
                    'title',
                    'desc',
                    'seo_title',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ]
            ]
        ];

    }

    public static function find()
    {
        return new CategoryQuery(get_called_class());
    }

    public function afterDelete()
    {
        parent::afterDelete();
        CategoryLang::deleteAll(['category_id' => $this->id]);
    }

    public function getCategoryLangs()
    {
        return $this->hasMany(CategoryLang::className(), ['category_id' => 'id']);
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(), ['key' => 'section']);
    }

    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }

    public function getCategories()
    {
        return $this->hasMany(self::className(), ['parent_id' => 'id'])->where(['status_id' => ModuleConstant::ACTIVE])->orderBy('sort_index');
    }

    public static function getDropdownList($section)
    {
        $categories = Category::find()->withSection($section)->notDeleted()->orderBy('parent_id')->all();
        return ArrayHelper::map($categories, 'id', 'titleSelect');
    }

    public function getTitleSelect()
    {
        $title = $this->title;
        if ($this->parent) {
            $title = $this->parent->title . ' - ' . $this->title;

            if ($this->parent->parent) {
                $title = $this->parent->parent->title . ' - ' . $title;

                if ($this->parent->parent->parent) {
                    $title = $this->parent->parent->parent->title . ' - ' . $title;

                    if ($this->parent->parent->parent->parent) {
                        $title = $this->parent->parent->parent->parent->title . ' - ' . $title;
                    }
                }
            }
        }
        return $title;
    }


}
