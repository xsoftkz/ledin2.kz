<?php

namespace common\modules\content\models\lang;

use Yii;

/**
 * This is the model class for table "{{%category_lang}}".
 *
 * @property int $id
 * @property int $category_id
 * @property string $lang_id
 * @property string $title
 * @property string|null $desc
 * @property string|null $meta_title
 * @property string|null $seo_title
 * @property string|null $meta_keywords
 * @property string|null $meta_description
 */
class CategoryLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%category_lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'lang_id', 'title'], 'required'],
            [['category_id'], 'integer'],
            [['desc', 'meta_description'], 'string'],
            [['lang_id'], 'string', 'max' => 8],
            [['title'], 'string', 'max' => 65],
            [['meta_title', 'meta_keywords', 'seo_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'lang_id' => 'Lang ID',
            'title' => 'Title',
            'desc' => 'Desc',
            'meta_title' => 'Meta Title',
            'seo_title' => 'Seo Title',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
        ];
    }
}
