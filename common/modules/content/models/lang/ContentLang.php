<?php

namespace common\modules\content\models\lang;

use Yii;

/**
 * This is the model class for table "{{%content_lang}}".
 *
 * @property int $id
 * @property int $content_id
 * @property string $lang_id
 * @property string $title
 * @property string|null $desc
 * @property string|null $seo_title
 * @property string|null $meta_title
 * @property string|null $meta_h1
 * @property string|null $meta_keywords
 * @property string|null $meta_description
 * @property string|null $singular_title
 */
class ContentLang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_id', 'lang_id', 'title'], 'required'],
            [['content_id'], 'integer'],
            [['desc', 'meta_description'], 'string'],
            [['lang_id'], 'string', 'max' => 8],
            [['title', 'seo_title'], 'string', 'max' => 255],
            [['meta_title', 'meta_h1', 'meta_keywords', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content_id' => 'Content ID',
            'lang_id' => 'Lang ID',
            'link' => 'Link',
            'title' => 'Title',
            'desc' => 'Desc',
            'meta_title' => 'Meta Title',
            'seo_title' => 'Seo Title',
            'meta_h1' => 'Meta H1',
            'meta_keywords' => 'Meta Keywords',
            'meta_description' => 'Meta Description',
        ];
    }
}
