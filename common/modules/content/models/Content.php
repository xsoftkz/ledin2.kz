<?php
namespace common\modules\content\models;

use common\models\traits\ImageTrait;
use common\modules\content\models\lang\ContentLang;
use common\modules\content\models\query\ContentQuery;
use lav45\translate\TranslatedTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%content}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $photo
 * @property string $introtext
 * @property string $fulltext
 * @property string $section
 * @property integer $category_id
 * @property integer $sort_index
 * @property integer $views
 * @property integer $status
 * @property string $slug
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property Category[] $categories
 */
class Content extends ActiveRecord
{
    use ImageTrait;
    use TranslatedTrait;

    public $file;
    public $category_relation;

    const MODEL_NAME = 'content';
    const IMAGE_FOLDER = 'content';

    const SECTION_PAGE = 'page';
    const SECTION_BANNER = 'banner';
    const SECTION_PORTFOLIOS = 'portfolio';
    const SECTION_SERVICE = 'service';
    const SECTION_VACANCY = 'vacancy';
    const SECTION_NEWS = 'news';
    const SECTION_PARTNERS = 'partners';
    const SECTION_CONTACT = 'contact';
    const SECTION_COMPANY = 'company';
    const SECTION_FAQ = 'faq';
    const SECTION_ARTICLE = 'article';
    const SECTION_REVIEW = 'review';
    const SECTION_OFFER = 'offer';

    public static function tableName()
    {
        return '{{%content}}';
    }

    public function rules()
    {
        return [
            [['title', 'section'], 'required'],
            [['category_id', 'sort_index', 'views', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'add_int', 'year'], 'integer'],
            [['title', 'photo', 'section', 'link', 'slug', 'seo_title'], 'string', 'max' => 255],
            [['file'], 'file', 'maxFiles' => 10],
            [['start_at', 'stop_at', 'category_relation'], 'safe'],
            [['intro', 'desc', 'meta_description'], 'string'],
            [['meta_title', 'meta_keywords'], 'string', 'max' => 255],
  ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'photo' => 'Фото',
            'file' => 'Файл',
            'section' => 'Раздел',
            'category_id' => 'Категория',
            'category_relation' => 'Категория',
            'start_at' => 'Дата начала публикации',
            'stop_at' => 'Дата остановки публикации',
            'sort_index' => 'Порядок сортировки',
            'views' => 'Просмотры',
            'status_id' => 'Активен',
            'slug' => 'СЕО ссылка',
            'created_at' => 'Добавлено',
            'updated_at' => 'Обновлено',
            'created_by' => 'Добавил',
            'updated_by' => 'Изменил',
            'year' => 'Год',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            'uploadBehavior' => [
                'class' => 'vova07\fileapi\behaviors\UploadBehavior',
                'attributes' => [
                    'photo' => [
                        'path' => '@images/' . self::IMAGE_FOLDER,
                        'tempPath' => '@images/' . self::IMAGE_FOLDER,
                    ],
                ]
            ],
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort_index'
            ],
            ['class' => 'yii\behaviors\BlameableBehavior'],
            [
                'class' => 'yii\behaviors\SluggableBehavior',
                'attribute' => 'title',
                'slugAttribute' => 'slug',
                'ensureUnique' => true,
                'immutable' => true,
            ],
            [
                'class' => 'lav45\translate\TranslatedBehavior',
                'translateRelation' => 'contentLangs',
                'languageAttribute' => 'lang_id',
                'translateAttributes' => [
                    'title',
                    'intro',
                    'desc',
                    'link',
                    'seo_title',
                    'meta_title',
                    'meta_keywords',
                    'meta_description',
                ]
            ]
        ];

    }

    public static function find()
    {
        return new ContentQuery(get_called_class());
    }

    public function afterDelete()
    {
        parent::afterDelete(); //
        ContentLang::deleteAll(['content_id' => $this->id]);
    }

    public function getContentLangs()
    {
        return $this->hasMany(ContentLang::className(), ['content_id' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->viaTable( '{{%content_category}}', ['content_id' => 'id']);
    }

    public function saveCategories(){
        $this->unlinkAll('categories', true);

        if ($this->category_relation) {
            foreach ($this->category_relation as $cat_id) {
                $item = new ContentCategory([
                    'content_id' => $this->id,
                    'category_id' => $cat_id,
                ]);
                $item->save();
            }
        }
    }

    public function convertDateToForm(){
        if($this->isNewRecord){
            $this->start_at = Yii::$app->formatter->asDate(date("Y-m-d"), 'dd.MM.yyyy');
            $this->stop_at = Yii::$app->formatter->asDate(date("Y-m-d").'+365 day', 'dd.MM.yyyy');
        } else {
            $this->start_at = Yii::$app->formatter->asDate($this->start_at, 'dd.MM.yyyy');
            $this->stop_at = Yii::$app->formatter->asDate($this->stop_at, 'dd.MM.yyyy');
        }
    }

    public function convertDateToSave(){
        if ($this->start_at && \DateTime::createFromFormat('Y-m-d', $this->start_at)) {
            $this->start_at = Yii::$app->formatter->asTimestamp($this->start_at);
        } else {
            $this->start_at = Yii::$app->formatter->asTimestamp(date("Y-m-d"));
        }

        if ($this->stop_at && \DateTime::createFromFormat('Y-m-d', $this->stop_at)) {
            $this->stop_at = Yii::$app->formatter->asTimestamp($this->stop_at);
        } else {
            $this->stop_at = Yii::$app->formatter->asTimestamp(date("Y-m-d").'+365 day');
        }
    }

}
