<?php

namespace common\modules\content\models;

use Yii;

/**
 * This is the model class for table "{{%content_category}}".
 *
 * @property int $id
 * @property int $content_id
 * @property int $category_id
 */
class ContentCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content_category}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_id', 'category_id'], 'required'],
            [['content_id', 'category_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content_id' => Yii::t('app', 'Content ID'),
            'category_id' => Yii::t('app', 'Category ID'),
        ];
    }
}
