<?php
namespace common\modules\content\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%section}}".
 *
 * @property integer $id
 * @property string $key
 * @property string $name
 * @property string $icon
 * @property integer $sort_index
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Section extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%section}}';
    }

    public function rules()
    {
        return [
            [['key', 'name'], 'required'],
            [['sort_index', 'created_at', 'updated_at', 'created_by', 'updated_by', 'status_id'], 'integer'],
            [['key', 'name', 'icon'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Ключ'),
            'name' => Yii::t('app', 'Название'),
            'icon' => Yii::t('app', 'Иконка'),
            'status_id' => Yii::t('app', 'Статус'),
            'sort_index' => Yii::t('app', 'Порядок сортировки'),
            'created_at' => Yii::t('app', 'Добавлено'),
            'updated_at' => Yii::t('app', 'Обновлено'),
            'created_by' => Yii::t('app', 'Добавил'),
            'updated_by' => Yii::t('app', 'Изменил'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            'sort' => [
                'class' => 'himiklab\sortablegrid\SortableGridBehavior',
                'sortableAttribute' => 'sort_index'
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];

    }
    public static function getName($key){
        $model = self::findOne(['key' => $key]);
        if($model){
            return $model->name;
        } else {
            return '';
        }
    }

    public static function getCategories($section){
        return Category::find()->where(['section' => $section])->notDeleted()->all();
    }

    public static function getCategoriesForUpdate($section, $cat_id){
        return Category::find()->where(['!=', 'id', $cat_id])->andWhere(['section' => $section])->notDeleted()->all();
    }
}
