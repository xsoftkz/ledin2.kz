<?php
namespace common\modules\content\models\search;

use common\modules\content\models\ModuleConstant;
use common\modules\content\models\Section;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SectionSearch extends Section
{

    public function rules()
    {
        return [
            [['id', 'sort_index', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['key', 'name', 'icon'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params, $action)
    {
        $query = Section::find();

        if($action == ModuleConstant::ACTION_INDEX){
            $query->andWhere(['<>','status_id', ModuleConstant::DELETED]);
        }

        if($action == ModuleConstant::ACTION_CART){
            $query->andWhere(['status_id' => ModuleConstant::DELETED]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sort_index' => $this->sort_index,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'icon', $this->icon]);

        return $dataProvider;
    }
}
