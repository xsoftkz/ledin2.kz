<?php
namespace common\modules\content\models\search;

use common\models\Lang;
use common\modules\content\models\Content;
use common\modules\content\models\ModuleConstant;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;


class ContentSearch extends Content
{

    public function rules()
    {
        return [
            [['id', 'category_id', 'sort_index', 'views', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'photo', 'section', 'slug', 'meta_title', 'meta_keywords', 'meta_description', 'link'], 'safe'],
        ];
    }


    public function scenarios()
    {
        return Model::scenarios();
    }


    public function search($params, $action, $section)
    {
        $query = Content::find();

        if($action == ModuleConstant::ACTION_INDEX){
            $query->notDeleted()->withSection($section);
        }

        if($action == ModuleConstant::ACTION_CART){
            $query->deleted()->withSection($section);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

//        if(!$params){
//        }
        $query->orderBy('sort_index DESC');


        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category_id' => $this->category_id,
            'sort_index' => $this->sort_index,
            'views' => $this->views,
            'status_id' => $this->status_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'photo', $this->photo])
            ->andFilterWhere(['like', 'section', $this->section])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'link', $this->link])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description]);

        if ($this->title) {
            $query->leftJoin("{{%content_lang}}", '{{%content_lang}}.content_id = {{%content}}.id');
            $query->andFilterWhere(['like', '{{%content_lang}}.title', $this->title]);
            $query->andWhere(['{{%content_lang}}.lang_id' => Lang::DEFAULT_LANG]);
        }

        return $dataProvider;
    }
}
