<?php
namespace common\modules\content\migrations;

use yii\db\Migration;

class m161016_052620_create_section_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%section}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'icon' => $this->string(),
            'status_id' => $this->integer()->defaultValue(0),
            'sort_index' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ], $tableOptions);

        $this->batchInsert('{{%section}}', ['key', 'name'],
            [
                ['page',  'Страницы'],
                ['article',  'Статьи'],
                ['banner', 'Баннеры'],
            ]
        );
    }

    public function down()
    {
        $this->dropTable('{{%section}}');
    }
}
