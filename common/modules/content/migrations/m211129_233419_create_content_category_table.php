<?php

namespace common\modules\content\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%content_category}}`.
 */
class m211129_233419_create_content_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%content_category}}', [
            'id' => $this->primaryKey(),
            'content_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%content_category}}');
    }
}
