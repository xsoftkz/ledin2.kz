<?php
namespace common\modules\content\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%content_lang}}`.
 */
class m200124_134150_create_content_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%content_lang}}', [
            'id' => $this->primaryKey(),
            'content_id' => $this->integer()->notNull(),
            'lang_id' => $this->string(8)->notNull(),
            'title' => $this->string(255)->notNull(),
            'intro' => $this->text(),
            'desc' => $this->text(),
            'link' => $this->string(),
            'meta_title' => $this->string(),
            'meta_h1' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%content_lang}}');
    }
}
