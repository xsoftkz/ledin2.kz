<?php
namespace common\modules\content\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%category_lang}}`.
 */
class m200124_134635_create_category_lang_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category_lang}}', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'lang_id' => $this->string(8)->notNull(),
            'title' => $this->string(65)->notNull(),
            'desc' => $this->text(),
            'meta_title' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%category_lang}}');
    }
}
