<?php
namespace common\modules\content\migrations;

use yii\db\Migration;

class m161015_132227_create_content_table extends Migration
{

    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%content}}', [
            'id' => $this->primaryKey(),
            'photo' => $this->string(),
            'section' => $this->string()->notNull(),
            'category_id' => $this->integer(),

            'start_at' => $this->integer(),
            'stop_at' => $this->integer(),

            'sort_index' => $this->integer(),
            'views' => $this->integer()->defaultValue(0),
            'status_id' => $this->smallInteger()->defaultValue(0),

            'slug' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ], $tableOptions);

        $this->batchInsert('{{%content}}',
            ['id', 'title', 'photo', 'introtext', 'fulltext', 'section', 'category_id', 'link', 'start_at', 'stop_at', 'sort_index', 'views', 'status_id', 'slug', 'meta_title', 'meta_h1', 'meta_keywords', 'meta_description', 'created_at', 'updated_at', 'created_by', 'updated_by'],
            [
                [1, 'Оплата и доставка', '', NULL, '<p>Lorem ipsum </p>', 'page', NULL, NULL, 1562781600, 1594317600, NULL, 5, 1, 'delivery', NULL, NULL, '', '', NULL, 1562823728, NULL, 1],
                [2, 'Информация об онлайн оплате', '', NULL, '<p>Lorem ipsum</p>', 'page', NULL, NULL, 1562781600, 1594317600, NULL, 1, 1, 'payment', NULL, NULL, '', '', NULL, 1562823820, NULL, 1],
                [3, 'Возврат', '', NULL, '<p>Информация о возврате товаров</p>', 'page', NULL, NULL, 1562781600, 1594317600, NULL, 1, 1, 'vozvrat', NULL, NULL, '', '', NULL, 1562823873, NULL, 1],
                [4, 'О компании', '', NULL, '<p>Lorem ipsum</p>', 'page', NULL, NULL, 1562781600, 1594317600, NULL, 1, 1, 'about', NULL, NULL, '', '', NULL, 1562823778, NULL, 1],
                [5, 'Гарантия', '', NULL, '<p>Lorem ipsum</p>', 'page', NULL, NULL, 1562781600, 1594317600, 1, 1, 1, 'garantia', NULL, NULL, '', '', 1562823839, 1562823848, 1, 1]
            ]
        );

    }

    public function down()
    {
        $this->dropTable('{{%content}}');
    }

}
