<?php
namespace common\modules\content\migrations;

use yii\db\Migration;

class m161029_191537_create_category_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey(),
            'section' => $this->string()->notNull(),
            'parent_id' => $this->integer(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(),
            'photo' => $this->string(),

            'status_id' => $this->integer()->defaultValue(0),
            'sort_index' => $this->integer(),

            'slug' => $this->string(),
            'meta_title' => $this->string(),
            'meta_keywords' => $this->string(),
            'meta_description' => $this->text(),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),

        ], $tableOptions);

        $this->batchInsert('{{%category}}',
            ['section', 'title', 'slug'],
            [
                ['banner', 'Слайдер на главной', 'main-slider'],
                ['article', 'Категория 1', 'kategoria-1'],
            ]
        );

    }

    public function down()
    {
        $this->dropTable('{{%category}}');
    }
}
