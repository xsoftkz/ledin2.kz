<?php

namespace common\modules\content\migrations;

use yii\db\Migration;

/**
 * Class m201004_091944_add_column_year_content_table
 */
class m201004_091944_add_column_year_content_table extends Migration
{
    public $tableName = '{{%content}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'year', $this->integer(4));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'year');
    }
}
