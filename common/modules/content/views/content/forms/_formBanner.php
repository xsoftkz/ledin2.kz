<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\modules\content\models\Section;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="view-form">

    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

                <div class="nav-tabs-custom">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab"> <?= Yii::t('app', 'Данные')?> </a></li>
                        <li class="pull-right">
                            <?= Html::submitButton(
                                ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить') :
                                    '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Сохранить'),
                                ['class' =>
                                    ($model->isNewRecord) ? 'btn btn-success pull-right' : 'btn btn-primary pull-right',
                                ]) ?>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="main">

                            <div class="row">
                                <div class="col-lg-7">
                                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                                    <?= $form->field($model, 'category_id')->widget(Select2::classname(), [
                                        'data' => ArrayHelper::map(Section::getCategories($section), 'id', 'title'),
                                        'options' => [
                                            'allowClear' => true,
                                            'placeholder' => Yii::t('app', 'Без категория')
                                        ],
                                    ]); ?>
                                    <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>

                                </div>
                                <div class="col-lg-5">

                                    <?= $form->field($model, 'start_at')
                                        ->widget(DatePicker::className(), [
                                            'language' => 'ru',
                                            'pluginOptions' => [
                                                'todayHighlight' => 'true',
//                                                'startDate' => 'today',
                                                'autoclose' => true,
                                                'format' => 'dd.mm.yyyy'
                                            ]
                                        ]) ?>

                                    <?= $form->field($model, 'stop_at')
                                        ->widget(DatePicker::className(), [
                                            'language' => 'ru',
                                            'pluginOptions' => [
                                                'todayHighlight' => 'true',
//                                                'startDate' => 'today',
                                                'autoclose' => true,
                                                'format' => 'dd.mm.yyyy'
                                            ]
                                        ]) ?>

                                    <?= $form->field($model, 'status_id')->dropDownList(Yii::$app->params['booleanParam'])->label(Yii::t('app', 'Опубликован')) ?>

                                    <?= $form->field($model, 'photo')->widget(
                                        FileAPI::className(),
                                        [
                                            'settings' => [
                                                'url' => ['fileapi-upload'],
                                                'elements' => [
//                                                    'preview' => ['width' => 100,'height' => 75]
                                                    'preview' => false
                                                ],
                                                'imageAutoOrientation' => false
                                            ],
                                        ])
                                    ?>

                                    <?php if(!$model->isNewRecord){
                                        echo Html::img($model->image, ['class' => 'img-responsive']);
                                    }?>

                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
