<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;


/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="view-form">

    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

                <div class="nav-tabs-custom">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab"> Данные</a></li>
                        <li class="pull-right">
                            <?= Html::submitButton(
                                ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> Добавить' :
                                    '<span class="glyphicon glyphicon-floppy-disk"></span> Сохранить',
                                ['class' =>
                                    ($model->isNewRecord) ? 'btn btn-success pull-right' : 'btn btn-primary pull-right',
                                ]) ?>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="main">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-2 text-right"><label class="control-label" for="content-title">Название</label></div>
                                        <div class="col-lg-10">
                                            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 text-right"><label class="control-label" for="content-intro">Карта</label></div>
                                        <div class="col-lg-10">
                                            <?= $form->field($model, 'intro')->textarea(['rows' => 4])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 text-right"><label class="control-label" for="content-fulltext">Описание</label></div>
                                        <div class="col-lg-10">
                                            <?= $form->field($model, 'desc')->widget(Widget::className(), [
                                                'settings' => [
                                                    'lang' => 'ru',
                                                    'minHeight' => 300,
                                                    'plugins' => [
                                                        'fullscreen'
                                                    ],
                                                ]
                                            ])->label(false); ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-3">
                                    <?= $form->field($model, 'status_id')->dropDownList(Yii::$app->params['booleanParam']) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
