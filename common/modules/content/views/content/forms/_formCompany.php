<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use common\modules\content\models\Section;


/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="view-form">

    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

                <div class="nav-tabs-custom">

                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab"> Данные</a></li>
                        <li role="presentation"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab"> SEO</a></li>
                        <li class="pull-right">
                            <?= Html::submitButton(
                                ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> Добавить' :
                                    '<span class="glyphicon glyphicon-floppy-disk"></span> Сохранить',
                                ['class' =>
                                    ($model->isNewRecord) ? 'btn btn-success pull-right' : 'btn btn-primary pull-right',
                                ]) ?>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="main">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="row">
                                        <div class="col-lg-2 text-right"><label class="control-label" for="content-title">Заголовок</label></div>
                                        <div class="col-lg-10">
                                            <?= $form->field($model, 'title')->textInput(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 text-right"><label class="control-label" for="content-fulltext">Описание</label></div>
                                        <div class="col-lg-10">
                                            <?= $form->field($model, 'desc')->textarea(['rows' => 6])->label(false); ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-3">
                                    <?= $form->field($model, 'status_id')->dropDownList(Yii::$app->params['booleanParam']) ?>

                                    <?= $form->field($model, 'photo')->widget(
                                        FileAPI::className(),
                                        [
                                            'settings' => [
                                                'url' => ['fileapi-upload'],
                                                'elements' => [
                                                    'preview' => ['width' => 100,'height' => 75]
                                                ],
                                                'imageAutoOrientation' => false
                                            ],
                                        ])
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="seo">

                            <div class="row">
                                <div class="col-lg-2 text-right"><label class="control-label" for="content-slug">СЕО ссылка</label></div>
                                <div class="col-lg-10">
                                    <?= $form->field($model, 'slug')->textInput(['maxlength' => true])->label(false) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 text-right"><label class="control-label" for="content-meta_keywords">Мета-тег Keywords</label></div>
                                <div class="col-lg-10">
                                    <?= $form->field($model, 'meta_keywords')->textarea(['rows' => 2])->label(false) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 text-right"><label class="control-label" for="content-meta_description">Мета-тег Description</label></div>
                                <div class="col-lg-10">
                                    <?= $form->field($model, 'meta_description')->textarea(['rows' => 4])->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
