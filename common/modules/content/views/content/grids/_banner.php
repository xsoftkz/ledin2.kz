<?php
use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'contentOptions' => ['style' => 'width:40px;'],
        ],
        [
            'attribute' => 'photo',
            'contentOptions' => ['style' => 'width:40px;'],
            'format' => 'html',
            'filter' => false,
            'enableSorting' => false,
            'value' => function ($data) {
                return Html::img($data->image, ['width' => 60]);
            },
        ],
        'title',
        'link',
        [
            'attribute' => 'status_id',
            'contentOptions' => ['style' => 'width:80px; text-align: center;'],
            'format' => 'html',
            'value' => function ($data) {
                return Html::a(
                    Html::tag('span', '',
                        ['class' => 'glyphicon glyphicon-' . (($data->status_id == \common\modules\content\models\ModuleConstant::ACTIVE) ? 'ok' : 'remove')]),
                    ['status', 'id' => $data->id], ['class' => 'status-link']);
            },
            'filter' => Yii::$app->params['booleanParam']
        ],
        [
            'attribute' => 'created_at',
            'contentOptions' => ['style' => 'width:150px;'],
            'label' => Yii::t('app', 'Добавлено'),
            'value' => function ($data) {
                return ($data->created_at) ? \Yii::$app->formatter->asDatetime($data->created_at, "php:d-m-Y H:i:s") : '';
            },
        ],

        [
            'header' => Yii::t('app', 'Действия'),
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:100px;'],
            'template' => '{update} {delete}',
            'buttons' => [
                'update' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'Редактировать'),
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'class' => 'btn btn-xs btn-primary',
                    ]);
                },
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'Удалить'),
                        'data-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'class' => 'btn btn-xs btn-danger',
                        'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                    ]);
                },
            ],
        ],

    ],
]); ?>

