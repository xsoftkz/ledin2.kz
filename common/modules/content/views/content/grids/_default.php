<?php
use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'contentOptions' => ['style' => 'width:40px;'],
        ],
        'title',
        [
            'attribute' => 'updated_at',
            'contentOptions' => ['style' => 'width:150px;'],
            'label' => Yii::t('app', 'Обновлено'),
            'value' => function ($data) {
                return ($data->created_at) ? \Yii::$app->formatter->asDatetime($data->updated_at, "php:d-m-Y H:i:s") : '';
            },
        ],
        [
            'header' => 'Языки',
            'class' => 'lav45\translate\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:250px; '],
            'languages' => $langList,
        ],
        [
            'header' => Yii::t('app', 'Действия'),
            'class' => 'yii\grid\ActionColumn',
            'contentOptions' => ['style' => 'width:100px;'],
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'Удалить'),
                        'data-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'class' => 'btn btn-xs btn-danger',
                        'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                    ]);
                },
            ],
        ],

    ],
]); ?>

