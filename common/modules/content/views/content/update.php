<?php

use yii\helpers\Html;
use common\modules\content\models\Section;
use common\modules\content\models\Content;

/* @var $this yii\web\View */
/* @var $model common\models\Content */

$this->title = Section::getName($section).'. '.Yii::t('app', 'Редактирование').': '.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', Section::getName($section)), 'url' => ['index', 'section' => $section]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="content-update">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="btn-box btn-group">
                        <?= \backend\widgets\LangSwitchWidget::widget(['current' => $model->language, 'model_id' => $model->id]) ?>
                    </div>
                </div>
                <div class="col-lg-3 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('app', 'Отменить'),
                            ['index', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить новую'),
                            ['create', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Удалить'),
                            ['delete', 'id' => $model->id], ['class' => 'btn btn-sm btn-default', 'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?')]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    switch ($section) {
        case Content::SECTION_PAGE:
            echo $this->render('forms/_formPage', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_BANNER:
            echo $this->render('forms/_formBanner', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_PORTFOLIOS:
            echo $this->render('forms/_formPortfolio', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_VACANCY:
            echo $this->render('forms/_formVacancy', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_PARTNERS:
            echo $this->render('forms/_formPartner', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_NEWS:
            echo $this->render('forms/_formNews', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_SERVICE:
            echo $this->render('forms/_formService', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_CONTACT:
            echo $this->render('forms/_formContact', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_COMPANY:
            echo $this->render('forms/_formCompany', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_FAQ:
            echo $this->render('forms/_formFaq', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_ARTICLE:
            echo $this->render('forms/_formArticle', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_REVIEW:
            echo $this->render('forms/_formReview', ['model' => $model, 'section' => $section]);
            break;
        case Content::SECTION_OFFER:
            echo $this->render('forms/_formOffer', ['model' => $model, 'section' => $section]);
            break;
        default:
            echo $this->render('forms/_default', ['model' => $model, 'section' => $section]);
    }
    ?>

</div>
