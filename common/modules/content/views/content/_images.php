<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if($images) { ?>
    <?php foreach($images as $image) { ?>

        <div class="col-xs-6 col-lg-3 content-img-item">

            <div class="box">

                <div class="box-header">
                    <div class="box-tools pull-right">
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ', ['javascript:void(0);'], ['class' => 'js-img-delete', 'data-id' => $image->id ]) ?>
                    </div>
                </div>

                <div class="box-body" style="display: block;">
                    <img src="<?= $image->image?>" alt="" class="img-responsive">
                </div>
            </div>

        </div>

    <?php } ?>
<?php } ?>
