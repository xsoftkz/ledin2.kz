<?php

use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;
use yii\widgets\Pjax;
use common\modules\content\models\Section;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Корзина');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', Section::getName($section)), 'url' => ['index', 'section' => $section]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-chevron-left"></span> ' . Yii::t('app', 'Список'),
                            ['index', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-body">
            <?php Pjax::begin(); ?>
                <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'contentOptions' => ['style' => 'width:40px;'],
                    ],
                    [
                        'attribute' => 'photo',
                        'contentOptions' => ['style' => 'width:40px;'],
                        'format' => 'html',
                        'filter' => false,
                        'enableSorting' => false,
                        'value' => function ($data) {
                            return Html::img($data->image, ['width' => 40]);
                        },
                    ],
                    'title',
                    [
                        'attribute' => 'created_at',
                        'contentOptions' => ['style' => 'width:150px;'],
                        'label' => Yii::t('app', 'Удалено'),
                        'value' => function ($data) {
                            return ($data->updated_at) ? \Yii::$app->formatter->asDatetime($data->updated_at, "php:d-m-Y H:i:s") : '';
                        },
                    ],

                    [
                        'header' => Yii::t('app', 'Действия'),
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:120px;'],
                        'template' => '{return} {remove}',
                        'buttons' => [
                            'return' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                    'title' => Yii::t('app', 'Восстановить'),
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-primary',
                                    'data-confirm' => Yii::t('app', 'Восстановить из корзины?'),
                                ]);
                            },
                            'remove' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Удалить'),
                                    'data-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-confirm' => Yii::t('app', 'После удаление восстановить невозможно, все равно удалить?'),
                                ]);
                            },
                        ],
                    ],

                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>


