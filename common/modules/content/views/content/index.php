<?php

use yii\helpers\Html;
use common\modules\content\models\Content;
use yii\widgets\Pjax;
use common\modules\content\models\Section;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel */

$this->title = Section::getName($section);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-th-list"></span> ' . Yii::t('app', 'Категории'),
                            ['category/index', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Корзина'),
                            ['cart', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?php Pjax::begin(); ?>

            <?php switch ($section) {
                default:
                    echo $this->render('grids/_default', [
                        'searchModel' => $searchModel,
                        'dataProvider' => $dataProvider,
                        'langList' => $langList,
                    ]);
                }
            ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>


