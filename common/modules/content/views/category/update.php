<?php

use yii\helpers\Html;
use common\modules\content\models\Section;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = Section::getName($section).'. '. Yii::t('app', 'Категория').'. '.Yii::t('app', 'Редактирование').': '.$model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', Section::getName($section)), 'url' => ['index', 'section' => $section]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');

?>
<div class="category-update">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-3 text-center">
                    <div class="btn-box btn-group">
                        <?= \backend\widgets\LangSwitchWidget::widget(['current' => $model->language, 'model_id' => $model->id]) ?>
                    </div>
                </div>
                <div class="col-lg-3 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('app', 'Отменить'),
                            ['index', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить новую'),
                            ['create', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Удалить'),
                            ['delete', 'id' => $model->id], ['class' => 'btn btn-sm btn-default', 'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?')]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'section' => $section,
        'categories' => $categories,
    ]) ?>

</div>
