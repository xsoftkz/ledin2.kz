<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model common\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="view-form">

    <div class="panel panel-default">
        <div class="panel-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="nav-tabs-custom">

                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab"> <?= Yii::t('app', 'Данные')?> </a></li>
                    <li role="presentation"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab"><?= Yii::t('app', 'SEO')?></a></li>
                    <li class="pull-right">
                        <?= Html::submitButton(
                            ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить') :
                                '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Сохранить'),
                            ['class' =>
                                ($model->isNewRecord) ? 'btn btn-success pull-right' : 'btn btn-primary pull-right',
                            ]) ?>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="main">

                        <div class="row">
                            <div class="col-lg-9">
                                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                                <?= $form->field($model, 'parent_id')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map($categories, 'id', 'titleSelect'),
                                    'options' => [
                                        'placeholder' => Yii::t('app', 'Без категория')
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ]); ?>

                                <?= $form->field($model, 'desc')->widget(Widget::className(), [
                                    'settings' => [
                                        'lang' => 'ru',
                                        'minHeight' => 300,
                                        'imageUpload' => Url::to(['/content/content/image-upload']),
                                        'fileUpload' => Url::to(['/content/content/file-upload']),
                                        'plugins' => [
                                            'fontfamily',
                                            'fontsize',
                                            'fontcolor',
                                            'table',
                                            'video',
                                            'fullscreen'
                                        ],
                                    ]
                                ]); ?>
                            </div>
                            <div class="col-lg-3">
                                <?= $form->field($model, 'status_id')->dropDownList(Yii::$app->params['booleanParam'])->label(Yii::t('app', 'Опубликован')) ?>

                                <?= $form->field($model, 'photo')->widget(
                                    FileAPI::className(),
                                    [
                                        'settings' => [
                                            'url' => ['fileapi-upload'],
                                            'elements' => [
                                                'preview' => ['width' => 100,'height' => 75]
                                            ],
                                            'imageAutoOrientation' => false
                                        ],
                                    ])
                                ?>
                            </div>
                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="seo">
                        <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'seo_title')->textInput(['maxlength' => true]) ?>
                        <?= $form->field($model, 'meta_description')->textarea(['rows' => 4]) ?>
                    </div>
                </div>


            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>



</div>
