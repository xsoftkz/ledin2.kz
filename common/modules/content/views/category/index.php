<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\modules\content\models\Section;
use yii\widgets\Pjax;
use common\modules\content\models\Category;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Section::getName($section).'. '.Yii::t('app', 'Категории');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-chevron-left"></span> ' . Section::getName($section),
                            ['content/index', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Корзина'),
                            ['cart', 'section' => $section], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:50px;'],
                        'value' =>'id'
                    ],
                    'title',
                    [
                        'attribute' => 'parent_id',
                        'label' => Yii::t('app', 'Родительская категория'),
                        'value' => function ($data) {
                            return ($data->parent_id) ? $data->titleSelect : '(не задано)';
                        },
                        'filter' => Category::getDropdownList($section)
                    ],
                    [
                        'attribute' => 'status_id',
                        'contentOptions' => ['style' => 'width:80px; text-align: center;'],
                        'format' => 'html',
                        'value' => function ($data) {
                            return Html::a(
                                Html::tag('span', '',
                                    ['class' => 'glyphicon glyphicon-' . (($data->status_id == \common\modules\content\models\ModuleConstant::ACTIVE) ? 'ok' : 'remove')]),
                                ['status', 'id' => $data->id], ['class' => 'status-link']);
                        },
                        'filter' => Yii::$app->params['booleanParam']
                    ],
                    [
                        'header' => 'Языки',
                        'class' => 'lav45\translate\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:250px; '],
                        'languages' => $langList,
                    ],
                    [
                        'header' => Yii::t('app', 'Действия'),
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:50px;'],
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Удалить'),
                                    'data-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>

</div>
