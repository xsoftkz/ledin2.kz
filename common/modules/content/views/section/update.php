<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Section */
$this->title = Yii::t('app', 'Разделы').'. '.Yii::t('app', 'Редактирование').': '.$model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Разделы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');

?>
<div class="section-update">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('app', 'Отменить'),
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить новую'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Удалить'),
                            ['delete', 'id' => $model->id], ['class' => 'btn btn-sm btn-default', 'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?')]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
