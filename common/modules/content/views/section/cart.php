<?php

use yii\helpers\Html;
use himiklab\sortablegrid\SortableGridView as GridView;

/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Корзина');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Разделы'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="view-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-chevron-left"></span> ' . Yii::t('app', 'Список'),
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:50px;'],
                        'value' =>'id'
                    ],
                    'key',
                    'name',
                    [
                        'header' => Yii::t('app', 'Действия'),
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:120px;'],
                        'template' => '{return} {remove}',
                        'buttons' => [
                            'return' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-refresh"></span>', $url, [
                                    'title' => Yii::t('app', 'Восстановить'),
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-confirm' => Yii::t('app', 'Восстановить из корзины?'),

                                ]);
                            },
                            'remove' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Удалить'),
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'data-confirm' => Yii::t('app', 'После удаление восстановить невозможно, все равно удалить?'),

                                ]);
                            },
                        ],
                    ],

                ],
            ]); ?>


        </div>
    </div>
</div>


