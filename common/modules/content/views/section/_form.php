<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Section */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="section-form">

    <div class="panel panel-default">
        <div class="panel-body">

            <?php $form = ActiveForm::begin(); ?>

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab"
                                                              data-toggle="tab"> <?= Yii::t('app', 'Данные') ?> </a>
                    </li>
                    <li class="pull-right">
                        <?= Html::submitButton(
                            ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить') :
                                '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Сохранить'),
                            ['class' =>
                                ($model->isNewRecord) ? 'btn btn-success pull-right' : 'btn btn-primary pull-right',
                            ]) ?>
                    </li>
                </ul>

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="main">
                        <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
