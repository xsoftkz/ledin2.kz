<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */

/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Разделы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="section-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Корзина'),
                            ['cart'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:50px;'],
                        'value' =>'id'
                    ],
                    'key',
                    'name',
                    'icon',

                    [
                        'header' => Yii::t('app', 'Действия'),
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:80px;'],
                        'template' => '{update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
    </div>


</div>
