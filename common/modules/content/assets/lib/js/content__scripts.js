$(document).ready(function() {

    // удаление картинки
    $('body').on('click', '.js-img-delete', function (e) {
        e.preventDefault();
        let link = $(this);

        $.ajax({
            type: "POST",
            url: '/content/content/delete-image',
            data: {'id': link.data('id')}
        }).done(function (data) {
            $('#js-content-images').html(data);
        }).fail(function () {
            alert("error");
        });

    });

});
