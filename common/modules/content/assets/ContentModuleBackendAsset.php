<?php
namespace common\modules\content\assets;

use yii\web\AssetBundle;

class ContentModuleBackendAsset extends AssetBundle
{
    public $sourcePath = '@common/modules/content/assets/lib';
    public $basePath = '@backendWebroot/web/assets';

    public $css = [
        'css/content__styles.css',
    ];
    public $js = [
        'js/content__scripts.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];
}
