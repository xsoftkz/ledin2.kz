<?php

namespace common\modules\content\services;

use common\models\helper\WatermarkHelper;
use common\modules\content\models\Content;
use Yii;

class PortfolioWaterMarkService
{
    /** @var Content $_content */
    private $_content;

    public function __construct(Content $content)
    {
        $this->_content = $content;
    }

    public function run(){

        $content = $this->_content;

        foreach ($content->images as $imageItem) {
            $filename = $imageItem->filename;

            $imagePath = Yii::getAlias('@images/content/') . $filename;
            $newImagePath = Yii::getAlias('@wm/content/') . $filename;

            WatermarkHelper::watermark($imagePath, $newImagePath);
        }
    }
}