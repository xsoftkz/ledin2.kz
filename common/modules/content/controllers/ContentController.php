<?php
namespace common\modules\content\controllers;

use common\models\Lang;
use common\modules\content\models\Content;
use common\models\Image;
use common\modules\content\models\ModuleConstant;
use common\modules\content\models\search\ContentSearch;
use common\modules\content\models\Section;
use common\modules\content\services\PortfolioWaterMarkService;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use himiklab\sortablegrid\SortableGridAction;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;


class ContentController extends Controller
{
    public function actionIndex($section)
    {
        $this->checkSectionExists($section);
        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_INDEX, $section);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'section' => $section,
            'langList' => Lang::getList(),
        ]);
    }

    public function actionCart($section)
    {
        $this->checkSectionExists($section);
        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_CART, $section);

        return $this->render('cart', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'section' => $section,
        ]);
    }

    public function actionCreate($section)
    {
        $this->checkSectionExists($section);
        $model = new Content();
        $model->section = $section;

        $model->convertDateToForm();

        if ($model->load(Yii::$app->request->post())) {

            $model->convertDateToSave();

            if ($model->save()) {
                $model->saveImages();
                if (in_array($model->section, [Content::SECTION_PORTFOLIOS, Content::SECTION_ARTICLE])) {
                    $model->saveCategories();
                }
                Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
                return $this->render('create', [
                    'model' => $model,
                    'section' => $section
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'section' => $section
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->convertDateToForm();

        $lang = Lang::findOne($model->language);
        Yii::$app->sourceLanguage = ($lang) ? $lang->locale : Lang::getDefaultLang()->locale;

        $model->category_relation = ArrayHelper::getColumn($model->categories, 'category_id');
        if ($model->load(Yii::$app->request->post())) {
            $model->convertDateToSave();

            if ($model->save()) {
                $model->saveImages();
                if (in_array($model->section, [Content::SECTION_PORTFOLIOS, Content::SECTION_ARTICLE])) {
                    $model->saveCategories();
                }

                if($model->section == Content::SECTION_PORTFOLIOS){
                    (new PortfolioWaterMarkService($model))->run();
                }

                Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
                return $this->redirect(['update', 'id' => $model->id, 'lang_id' => $model->language]);
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
                return $this->render('update', [
                    'model' => $model,
                    'section' => $model->section
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'section' => $model->section
            ]);
        }
    }

    public function actionStatus($id){
        $model = $this->findModel($id);

        $model->status_id = ($model->status_id == ModuleConstant::IN_ACTIVE) ? ModuleConstant::ACTIVE : ModuleConstant::IN_ACTIVE;
        $model->save();

        Yii::$app->session->setFlash('success', 'Статус сохранен');
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $section = $model->section;
        $model->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Успешно удален'));
        return $this->redirect(['cart', 'section' => $section]);
    }


    // Восстановить из корзины
    public function actionReturn($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::IN_ACTIVE;
        $model->save();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Восстановлен из корзины'));
        return $this->redirect(Yii::$app->request->referrer);
    }

    // удаление картинки через ajax
    public function actionDeleteImage(){
        Yii::$app->response->format = 'json';

        $data = Yii::$app->request->post();
        $image = Image::findOne($data['id']);

        if($image){
            $model_id = $image->item_id;
            $image->delete();
            $model =  $this->findModel($model_id);
            return $this->renderPartial('_images',[
                'images' => $model->images
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }

    protected function checkSectionExists($section){
        if(! Section::findOne(['key' => $section])){
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
        ];
    }

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Content::className(),
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => Yii::getAlias('@imagesWebroot/'),
                'path' => '@images',
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@images/'.Content::IMAGE_FOLDER,
                'unique' => true,
            ]
        ];
    }
}
