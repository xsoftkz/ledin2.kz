<?php
namespace common\modules\content\controllers;

use common\modules\content\models\ModuleConstant;
use common\modules\content\models\search\SectionSearch;
use common\modules\content\models\Section;
use himiklab\sortablegrid\SortableGridAction;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SectionController extends Controller
{

    public function actionIndex()
    {
        $searchModel = new SectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_INDEX);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCart()
    {
        $searchModel = new SectionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_CART);

        return $this->render('cart', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCreate()
    {
        $model = new Section();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model){
            $model->status = ModuleConstant::DELETED;
            if($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Перемещен в корзину'));
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
            }
        } else {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
        }
        return $this->redirect(['index']);
    }


    public function actionRemove($id)
    {
        $model = $this->findModel($id);
        if($model){
            $model->delete();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Успешно удален'));
        } else {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
        }
        return $this->redirect(['cart']);
    }

    // Восстановить из корзины
    public function actionReturn($id)
    {
        $model = $this->findModel($id);

        if($model){
            $model->status = ModuleConstant::IN_ACTIVE;
            if($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Восстановлен из корзины'));
            } else {
                Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
            }
        } else {
            Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }


    protected function findModel($id)
    {
        if (($model = Section::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [],
            ],
        ];
    }


    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Section::className(),
            ],
        ];
    }



}
