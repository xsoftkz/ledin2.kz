<?php
namespace common\modules\content\controllers;

use common\models\Lang;
use common\modules\content\models\Category;
use common\modules\content\models\Image;
use common\modules\content\models\ModuleConstant;
use common\modules\content\models\search\CategorySearch;
use common\modules\content\models\Section;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use himiklab\sortablegrid\SortableGridAction;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;


class CategoryController extends Controller
{
    public function actionIndex($section)
    {
        $this->checkSectionExists($section);
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_INDEX, $section);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'section' => $section,
            'langList' => Lang::getList(),
        ]);
    }

    public function actionCart($section)
    {
        $this->checkSectionExists($section);
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_CART, $section);

        return $this->render('cart', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'section' => $section,
        ]);
    }

    public function actionCreate($section)
    {
        $this->checkSectionExists($section);
        $model = new Category();
        $categories = Section::getCategories($section);
        $model->section = $section;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
                return $this->redirect(['update', 'id' => $model->id]);
            }
            Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
        } else {
            return $this->render('create', [
                'model' => $model,
                'section' => $section,
                'categories' => $categories,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $section = $model->section;
        $categories = Section::getCategoriesForUpdate($section, $id);

        $lang = Lang::findOne($model->language);
        Yii::$app->sourceLanguage = ($lang) ? $lang->locale : Lang::getDefaultLang()->locale;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
                return $this->redirect(['update', 'id' => $model->id, 'lang_id' => $model->language]);
            }
            Yii::$app->session->setFlash('danger', Yii::t('app', 'Ошибка'));
        } else {
            return $this->render('update', [
                'model' => $model,
                'section' => $section,
                'categories' => $categories,
            ]);
        }
    }

    public function actionStatus($id)
    {
        $model = $this->findModel($id);

        ($model->status_id == ModuleConstant::IN_ACTIVE) ?
            $model->status_id = ModuleConstant::ACTIVE :
            $model->status_id = ModuleConstant::IN_ACTIVE;

        $model->save();

        Yii::$app->session->setFlash('success', 'Статус сохранен');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::DELETED;
        $model->save();
        Yii::$app->session->setFlash('success', 'Перемещен в корзину');
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionRemove($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Успешно удален');
        return $this->redirect(['cart']);
    }


    // Восстановить из корзины
    public function actionReturn($id)
    {
        $model = $this->findModel($id);
        $model->status_id = ModuleConstant::IN_ACTIVE;
        $model->save();
        Yii::$app->session->setFlash('success', 'Восстановлен из корзины');
        return $this->redirect(Yii::$app->request->referrer);
    }


    // удаление картинки через ajax
    public function actionDeleteImage(){
        Yii::$app->response->format = 'json';

        $data = Yii::$app->request->post();
        $image = Image::findOne($data['id']);

        if($image){
            $model_id = $image->item_id;
            $image->delete();
            $model =  $this->findModel($model_id);
            return $this->renderPartial('_images',[
                'images' => $model->images
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }

    protected function checkSectionExists($section){
        if(! Section::findOne(['key' => $section])){
            throw new NotFoundHttpException(Yii::t('app', 'Запрашиваемая страница не существует.'));
        }
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'sort' => [
                'class' => SortableGridAction::className(),
                'modelName' => Category::className(),
            ],
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => '@images/'.Category::IMAGE_FOLDER,
                'unique' => true,
            ]
        ];
    }
}
