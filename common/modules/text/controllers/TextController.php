<?php
namespace common\modules\text\controllers;

use common\modules\text\models\ModuleConstant;
use common\modules\text\models\search\TextSearch;
use common\modules\text\models\Text;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class TextController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TextSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_INDEX);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionCart()
    {
        $searchModel = new TextSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ModuleConstant::ACTION_CART);

        return $this->render('cart', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Text();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Данные успешно сохранены'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    public function actionStatus($id){
        $model = $this->findModel($id);
        $model->changeStatus();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Статус сохранен'));
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->setDeleted();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Перемещен в корзину'));
        return $this->redirect(['index']);
    }

    public function actionRemove($id)
    {
        $model = $this->findModel($id);
        $model->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Успешно удален'));
        return $this->redirect(['cart']);
    }

    // Восстановить из корзины
    public function actionReturn($id)
    {
        $model = $this->findModel($id);
        $model->setInActive();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Восстановлен из корзины'));
        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id)
    {
        if (($model = Text::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
