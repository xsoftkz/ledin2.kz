<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Text */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="text-form">

    <div class="panel panel-default">
        <div class="panel-body">

           <div class="row">
               <div class="col-lg-8">
                   <?php $form = ActiveForm::begin(); ?>

                   <?= $form->field($model, 'key')->textInput(['maxlength' => true]) ?>

                   <?= $form->field($model, 'value_ru')->textarea(['rows' => 4]) ?>
                   <?= $form->field($model, 'value_kz')->textarea(['rows' => 4]) ?>
                   <?= $form->field($model, 'value_en')->textarea(['rows' => 4]) ?>

                   <?= $form->field($model, 'comment')->textInput() ?>

                   <div class="form-group">
                       <?= Html::submitButton(
                           ($model->isNewRecord) ? '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить') :
                               '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Сохранить'),
                           ['class' =>
                               ($model->isNewRecord) ? 'btn btn-success' : 'btn btn-primary',
                           ]) ?>
                   </div>

                   <?php ActiveForm::end(); ?>
               </div>
           </div>

        </div>
    </div>

</div>
