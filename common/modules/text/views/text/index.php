<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\TextSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Текстовые блоки');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="text-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Добавить'),
                            ['create'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Корзина'),
                            ['cart'], ['class' => 'btn btn-sm btn-default']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'key',
                    [
                        'attribute' => 'value_ru',
                        'format' => 'html',
                        'value' => function ($data) {
                            return substr(strip_tags($data->value_ru), 0, 100);
                        },
                    ],
                    [
                        'attribute' => 'value_kz',
                        'format' => 'html',
                        'value' => function ($data) {
                            return substr(strip_tags($data->value_kz), 0, 100);
                        },
                    ],
                    [
                        'attribute' => 'value_en',
                        'format' => 'html',
                        'value' => function ($data) {
                            return substr(strip_tags($data->value_en), 0, 100);
                        },
                    ],
                    [
                        'attribute' => 'comment',
                        'contentOptions' => ['style' => 'width:150px;'],
                        'value' => function ($data) {
                            return $data->comment;
                        },
                    ],
//                    [
//                        'attribute' => 'status_id',
//                        'contentOptions' => ['style' => 'width:100px; text-align: center;'],
//                        'label' => Yii::t('app', 'Опубликован'),
//                        'format' => 'html',
//                        'value' => function ($data) {
//                            return $data->statusLink;
//                        },
//                        'filter' => Yii::$app->params['booleanParam']
//                    ],

                    [
                        'header' => Yii::t('app', 'Действия'),
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'width:50px;'],
                        'template' => '{update}',
                        'buttons' => [
                            'update' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Редактировать'),
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-primary',
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Удалить'),
                                    'data-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-placement' => 'top',
                                    'class' => 'btn btn-xs btn-danger',
                                    'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?'),
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>

</div>
