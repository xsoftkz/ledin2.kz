<?php
namespace common\modules\text\migrations;

use yii\db\Migration;

class m170501_135355_create_text_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%text}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'value_ru' => $this->text(),
            'value_kz' => $this->text(),
            'value_en' => $this->text(),
            'comment' => $this->text(),
            'type_id' => $this->smallInteger()->defaultValue(1), // html или текст
            'status_id' => $this->smallInteger()->defaultValue(1),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $this->batchInsert('{{%text}}',
            ['key', 'value', 'comment', 'created_at', 'updated_at'],
            [
//                ['address', 'г.Шымкент, пл. Аль-Фараби №1', 'адрес', time(), time()],
            ]
        );
    }

    public function down()
    {
        $this->dropTable('{{%text}}');
    }

}
