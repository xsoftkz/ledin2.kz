<?php
namespace common\modules\text;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\text\controllers';
    public function init()
    {
        parent::init();
        \Yii::$app->params['booleanParam'] = [
            '1' => 'да',
            '0' => 'нет',
        ];
    }
}