<?php
namespace common\modules\text\models;

use common\modules\text\models\query\TextQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%text}}".
 *
 * @property integer $id
 * @property string $key
 * @property string $value
 * @property string $comment
 */
class Text extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%text}}';
    }


    public function rules()
    {
        return [
            [['key', 'value_ru', 'value_kz', 'value_en'], 'required'],
            [['comment', 'value'], 'string'],
            [['status_id'], 'integer'],
            [['key'], 'unique'],
            [['key'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'key' => Yii::t('app', 'Alias'),
            'value_ru' => Yii::t('app', 'Текст RU'),
            'value_kz' => Yii::t('app', 'Текст KZ'),
            'value_en' => Yii::t('app', 'Текст ENG'),
            'comment' => Yii::t('app', 'Комментарий'),
            'status_id' => Yii::t('app', 'Статус'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public static function find()
    {
        return new TextQuery(get_called_class());
    }


    public function getStatusLink()
    {
        return Html::a(
            Html::tag('span', '', ['class' => 'glyphicon glyphicon-' . (($this->status_id == ModuleConstant::ACTIVE) ? 'ok' : 'remove')]),
            ['status', 'id' => $this->id], ['class' => 'status-link']);
    }

    public function changeStatus(){
        if($this->status_id == ModuleConstant::IN_ACTIVE){
            $this->status_id = ModuleConstant::ACTIVE;
            if($this->save()){ return true;}
            else { return false;}
        }
        elseif ($this->status_id == ModuleConstant::ACTIVE){
            $this->status_id = ModuleConstant::IN_ACTIVE;
            if($this->save()){ return true;}
            else { return false;}
        }
    }

    public function setDeleted(){
        $this->status_id = ModuleConstant::DELETED;
        $this->save();
    }

    public function setInActive(){
        $this->status_id = ModuleConstant::IN_ACTIVE;
        $this->save();
    }
}
