<?php
namespace common\modules\text\models;

use yii\base\Model;

class ModuleConstant extends Model
{
    const IN_ACTIVE = 0;
    const ACTIVE = 1;
    const DELETED = 2;

    const ACTION_INDEX = 'index';
    const ACTION_CART = 'cart';
}