<?php
namespace common\modules\text\models\search;

use common\modules\text\models\ModuleConstant;
use common\modules\text\models\Text;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * TextSearch represents the model behind the search form about `common\models\Text`.
 */
class TextSearch extends Text
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['key', 'value_ru', 'value_kz', 'value_en', 'comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $action)
    {
        $query = Text::find();

        if($action == ModuleConstant::ACTION_INDEX){
            $query->notDeleted();
        }

        if($action == ModuleConstant::ACTION_CART){
            $query->deleted();
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'key', $this->key])
            ->andFilterWhere(['like', 'value_ru', $this->value_ru])
            ->andFilterWhere(['like', 'value_kz', $this->value_kz])
            ->andFilterWhere(['like', 'value_en', $this->value_en])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
