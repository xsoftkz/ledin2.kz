<?php
namespace common\modules\text\models\query;

use common\modules\text\models\ModuleConstant;
use yii\db\ActiveQuery;

class TextQuery extends ActiveQuery
{
    public function notDeleted()
    {
        return $this->andWhere(['<>','status_id', ModuleConstant::DELETED]);
    }

    public function deleted()
    {
        return $this->andWhere(['status_id' => ModuleConstant::DELETED]);
    }

    public function published()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function notPublished()
    {
        return $this->andWhere(['status_id' => ModuleConstant::ACTIVE]);
    }

    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }
}
