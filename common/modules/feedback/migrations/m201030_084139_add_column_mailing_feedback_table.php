<?php

namespace common\modules\feedback\migrations;

use yii\db\Migration;

/**
 * Class m201030_084139_add_column_mailing_feedback_table
 */
class m201030_084139_add_column_mailing_feedback_table extends Migration
{
    public $tableName = '{{%feedback}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_mailing', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_mailing');
    }
}
