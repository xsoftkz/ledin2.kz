<?php

namespace common\modules\feedback\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `{{%feedback_item}}`.
 */
class m200220_080713_create_feedback_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%feedback_item}}', [
            'id' => $this->primaryKey(),
            'feedback_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%feedback_item}}');
    }
}
