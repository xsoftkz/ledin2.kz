<?php
namespace common\modules\feedback\migrations;

use yii\db\Migration;

/**
 * Class m200220_090014_create_feedback_add_column
 */
class m200220_090014_create_feedback_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%feedback}}', 'company', $this->string());
        $this->addColumn('{{%feedback}}', 'filename', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%feedback}}', 'company');
        $this->dropColumn('{{%feedback}}', 'filename');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200220_090014_create_feedback_add_column cannot be reverted.\n";

        return false;
    }
    */
}
