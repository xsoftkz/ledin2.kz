<?php
namespace common\modules\feedback\migrations;

use yii\db\Migration;

/**
 * Class m200221_104834_create_feedback_item_quantity_column
 */
class m200221_104834_create_feedback_item_quantity_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%feedback_item}}', 'quantity', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%feedback_item}}', 'quantity');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200221_104834_create_feedback_item_quantity_column cannot be reverted.\n";

        return false;
    }
    */
}
