<?php
namespace common\modules\feedback\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `feedback`.
 */
class m171031_115157_create_feedback_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%feedback}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'subject' => $this->string(),
            'comment' => $this->text(),

            'status_id' =>  $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%feedback}}');
    }
}
