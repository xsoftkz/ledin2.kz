<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\modules\feedback\models\search\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Обратная связь');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feedback-index">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

//                    'id',
                        'username',

                        'phone',
                        'utm',
//                    'email:email',
                        //'subject',
                        [
                            'attribute' => 'comment',
                            'contentOptions' => ['style' => 'max-width:600px; word-wrap: break-word;'],
                            'label' => Yii::t('app', 'Сообщение'),
                            'value' => function ($data) {
                                return $data->comment;
                            },
                        ],
                        [
                            'attribute' => 'created_at',
                            'contentOptions' => ['style' => 'width:150px;'],
                            'label' => Yii::t('app', 'Добавлено'),
                            'value' => function ($data) {
                                return ($data->created_at) ? \Yii::$app->formatter->asDatetime($data->created_at, "php:d-m-Y H:i:s") : '(не задано)';
                            },
                        ],
                        [
                            'attribute' => 'type_id',
                            'contentOptions' => ['style' => 'width:150px; text-align: center;'],
                            'format' => 'html',
                            'value' => function ($data) {
                                return Yii::$app->params['feedbackType'][$data->type_id];
                            },
                            'filter' => Yii::$app->params['feedbackType']
                        ],
                        [
                            'attribute' => 'status_id',
                            'contentOptions' => ['style' => 'width:100px; text-align: center;'],
                            'label' => Yii::t('app', 'Статус'),
                            'format' => 'html',
                            'value' => function ($data) {
                                return Yii::$app->params['feedbackStatus'][$data->status_id];
                            },
                            'filter' => Yii::$app->params['feedbackStatus']
                        ],

                        //'updated_at',
                        //'created_by',
                        //'updated_by',

                        [
                            'header' => Yii::t('app', 'Действия'),
                            'class' => 'yii\grid\ActionColumn',
                            'contentOptions' => ['style' => 'width:80px;'],
                            'template' => '{update}',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>

</div>
