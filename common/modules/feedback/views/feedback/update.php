<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\modules\feedback\models\Feedback;

/* @var $this yii\web\View */
/* @var $model common\modules\feedback\models\Feedback */

$this->title = 'Обратная связь от ' . $model->username.' - '.Yii::$app->formatter->asDatetime($model->created_at);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Обратная связь'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');

?>
<div class="feedback-update">

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="page-title">
                        <?= $this->title ?>
                    </div>
                </div>
                <div class="col-lg-6 text-right">
                    <div class="btn-box btn-group" role="group" aria-label="">
                        <?= Html::a('<span class="glyphicon glyphicon-remove"></span> ' . Yii::t('app', 'Отменить'),
                            ['index'], ['class' => 'btn btn-sm btn-default']) ?>
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span> ' . Yii::t('app', 'Удалить'),
                            ['delete', 'id' => $model->id],
                            ['class' => 'btn btn-sm btn-default', 'data-method' => 'post',
                                'data-confirm' => Yii::t('app', 'Вы уверены, что хотите удалить этот элемент?')]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>ФИО</td>
                                <td><?= $model->username ?></td>
                            </tr>
                            <tr>
                                <td>Телефон</td>
                                <td><?= $model->phone ?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?= $model->email ?></td>
                            </tr>
                            <tr>
                                <td style="word-wrap: break-word;">Сообщение</td>
                                <td><?= $model->comment ?></td>
                            </tr>
                            <tr>
                                <td>Согласен на рассылку</td>
                                <td><?= ($model->is_mailing) ? 'да' : 'нет' ?></td>
                            </tr>
                            <tr>
                                <td>UTM source</td>
                                <td><?= ($model->utm) ? $model->utm : '-' ?></td>
                            </tr>

                            <?php if ($model->type_id == Feedback::RESUME){ ?>
                                <tr>
                                    <td>Резюме</td>
                                    <td>
                                        <?php if ($model->filename) { ?>
                                            <a target="_blank" href="<?= $model->fileLink ?>">Посмотреть</a>
                                        <?php } else { ?>
                                            <i>(не задано)</i>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td>Файлы</td>
                                    <td>
                                        <?php if ($model->files) { ?>
                                            <?php foreach ($model->files as $item) { ?>
                                                <a href="<?= $item->fileLink ?>" class="btn btn-sm btn-default" target="_blank">Посмотреть</a>
                                            <?php } ?>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Дата</td>
                                    <td><?= Yii::$app->formatter->asDatetime($model->created_at) ?></td>
                                </tr>
                                <tr>
                                    <td>Организация</td>
                                    <td><?= ($model->company) ? $model->company : '<i>(не задано)</i>' ?></td>
                                </tr>
                                <tr>
                                    <td>Реквизиты</td>
                                    <td>
                                        <?php if ($model->filename) { ?>
                                            <a target="_blank" href="<?= $model->fileLink ?>">Посмотреть</a>
                                        <?php } else { ?>
                                            <i>(не задано)</i>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Продукция</td>
                                    <td>
                                        <?php if ($model->items) {
                                            foreach ($model->items as $item) { ?>
                                                Товар: <?= ($item->product) ? $item->product->productName : '-' ?> <br>
                                                Модель: <?= ($item->product) ? $item->product->name : '-' ?><br>
                                                Количество: <?= $item->quantity ?>
                                                <hr>
                                            <?php }
                                        } else { ?>
                                            <i>(не задано)</i>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php }?>
                        </table>
                    </div>
                </div>
                <div class="col-lg-6">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'status_id')->dropDownList(Yii::$app->params['feedbackStatus']) ?>

                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Сохранить'), ['class' => 'btn btn-primary']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
