<?php

namespace common\modules\feedback\models;

use common\modules\shop\models\ProductModel;
use Yii;

/**
 * This is the model class for table "xs_feedback_item".
 *
 * @property int $id
 * @property int $feedback_id
 * @property int $product_id
 */
class FeedbackItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%feedback_item}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['feedback_id', 'product_id'], 'required'],
            [['feedback_id', 'product_id', 'quantity'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'feedback_id' => 'Feedback ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
        ];
    }

    public function getProduct(){
        return $this->hasOne(ProductModel::className(), ['id' => 'product_id']);
    }
}
