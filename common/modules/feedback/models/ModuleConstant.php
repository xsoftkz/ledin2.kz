<?php
namespace common\modules\feedback\models;

use yii\base\Model;

class ModuleConstant extends Model
{
    const IS_NEW = 0;
    const PROCESSED = 1;

}