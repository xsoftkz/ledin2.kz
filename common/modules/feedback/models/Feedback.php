<?php

namespace common\modules\feedback\models;

use common\models\File;
use common\models\Settings;
use common\modules\feedback\models\query\FeedbackQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use common\models\helper\SendPulseClient;

/**
 * This is the model class for table "{{%feedback}}".
 *
 * @property int $id
 * @property string $username
 * @property int $type_id
 * @property string $phone
 * @property string $email
 * @property string $subject
 * @property string $comment
 * @property int $status_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $utm
 */
class Feedback extends ActiveRecord
{
    public $file;

    const CALLBACK = 1;
    const LETTER = 2;
    const PRODUCT = 3;
    const CATALOG = 4;
    const RESUME = 5;
    const EXTEND_FEEDBACK = 6;
    const CONSULTATION = 7;
    const QUIZ = 8;
    const LIDS = 9;

    const FOLDER = 'feedback';
    const MODEL_NAME = 'feedback';

    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'username', 'phone'], 'required'],
            [['type_id', 'status_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'is_mailing'], 'integer'],
            [['comment'], 'string', 'max' => 1000 ],
            [['username', 'phone', 'email', 'subject', 'company', 'filename', 'utm'], 'string', 'max' => 255],
            [['link'], 'string'],
            [['file'], 'file'],

//            [['username'], 'required', 'message' => 'Как к Вам обращаться?'],
//            [['phone'], 'required', 'message' => 'Как с Вами связаться?'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя',
            'company' => 'Компания',
            'filename' => 'Файл',
            'file' => 'Файл',
            'type_id' => 'Тип обращение',
            'phone' => 'Телефон',
            'email' => 'Email',
            'link' => 'Ссылка',
            'subject' => 'Тема',
            'comment' => 'Сообщение',
            'status_id' => 'Статус',
            'created_at' => 'Добавлен',
            'updated_at' => 'Обновлен',
            'created_by' => 'Добавил',
            'updated_by' => 'Обновил',
            'utm' => 'UTM source',
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior'
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\modules\feedback\models\query\FeedbackQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new FeedbackQuery(get_called_class());
    }

    public function getType(){
        return Yii::$app->params['feedbackType'][$this->type_id];
    }

    public function getStatus(){
        return Yii::$app->params['feedbackStatus'][$this->status_id];
    }

    public function getDate(){
        return Yii::$app->formatter->asDatetime($this->created_at, "php:d-m-Y H:i:s");
    }


    public static function createFeedback($name, $phone, $msg = ''){
        $model = new self();
        $model->username = $name;
        $model->phone = $phone;
        $model->comment = $msg;
        $model->type_id = self::CALLBACK;
        if ($model->save()) {
            $model->sendEmailToAdmin();
            return true;
        } else {
            return false;
        }
    }

    public function sendEmailToAdmin()
    {
        return \Yii::$app->mailer->compose('@common/modules/feedback/mail/feedback', [
            'username' => $this->username,
            'phone' => $this->phone,
            'comment' => $this->comment,
        ])
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['appDomain']])
            ->setTo(Settings::getValue('admin_email'))
            ->setSubject('Заявка с сайта')
            ->send();
    }

    public function sendEmailToHr()
    {
        return \Yii::$app->mailer->compose('@common/modules/feedback/mail/resume', [
            'username' => $this->username,
            'phone' => $this->phone,
            'comment' => $this->comment,
            'fileLink' => $this->getFileLink(),
        ])
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['appDomain']])
            ->setTo(Settings::getValue('vacancy_email'))
            ->setSubject('Резюме с сайта')
            ->send();
    }

    function getItems(){
        return $this->hasMany(FeedbackItem::className(), ['feedback_id' => 'id']);
    }

    public function uploadFile()
    {
        $result = false;
        $this->file = UploadedFile::getInstance($this, 'file');

        if ($this->file && $this->validate()) {
            $this->filename = uniqid() . '.' . $this->file->extension;
            $this->file->saveAs(Yii::getAlias('@files/'.self::FOLDER . '/' . $this->filename));
            $result = $this->save(false);
        }
        return $result;
    }


    public function getFileLink(){
        if ($this->filename) {
            return Yii::getAlias('@filesWebroot').'/' . self::FOLDER.'/'.$this->filename;
        } else {
            return null;
        }
    }

    public function getFiles()
    {
        return $this->hasMany(File::className(), ['item_id' => 'id'])->where(['model_name' => self::MODEL_NAME]);
    }
	
	public function sendDataToSendPulse(){
        $client = new SendPulseClient($this->email, $this->phone);
        $client->sendData();
    }
}
