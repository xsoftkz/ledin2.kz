<?php
namespace common\modules\feedback;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'common\modules\feedback\controllers';
    public function init()
    {
        parent::init();
        \Yii::$app->params['feedbackType'] = [
            '1' => 'Обратный звонок',
            '2' => 'Письмо',
            '3' => 'Заявка на продукцию',
            '4' => 'Скачивание каталога',
            '5' => 'Резюме',
            '6' => 'Расширенная форма',
            '7' => 'Консультация',
            '8' => 'Опрос',
            '9' => 'Лид/Дизайн код',
        ];
        \Yii::$app->params['feedbackStatus'] = [
            '0' => 'Новый',
            '1' => 'Обработан',
        ];
    }
}