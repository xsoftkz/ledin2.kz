<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */
?>

<p>Обратная связь с сайта</p>
<p>Имя: <?= Html::encode($username) ?></p>
<p>Телефон:<?= Html::encode($phone) ?> </p>
<p>Сообщение:<?= $comment ?> </p>

